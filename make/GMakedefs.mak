ARCH := $(shell uname)
UNAMEN := $(shell uname -n)
HOSTNAMEF := $(shell hostname -f)

ifndef HOST
  HOST := $(UNAMEN)
endif

FC       :=
F90      :=
CC       :=
CXX      :=
F90FLAGS := -L /usr/lib/x86_64-linux-gnu
FFLAGS   :=
CFLAGS   :=
CXXFLAGS :=

ATLAS_LIBDIR ?= /usr/lib64/atlas
SILO_LIBDIR  ?= $(HOME)/usr/local/SILO/lib
SILO_INCLUDE ?= $(HOME)/usr/local/SILO/include
HDF5_LIBDIR  ?= $(HOME)/usr/local/HDF5/lib
HDF5_INCLUDE ?= $(HOME)/usr/local/HDF5/include
MKLROOT      ?= $(HOME)/usr/local/MKL/compilers_and_libraries_2016.0.109/linux/mkl/
CUDAROOT     ?= /usr/local/cuda

MPI_ROOTDIR  ?= /usr/lib/openmpi
#MPI_ROOTDIR  ?= /usr/lib64/openmpi

FCOMP_VERSION :=

VPATH_LOCATIONS :=
INCLUDE_LOCATIONS :=

ifdef DIM3
dim_suffix := .3d
else
dim_suffix := .2d
endif

ifdef OMP
  ifdef INTEL_THREAD
    omp_suffix  := .iomp
  else
    omp_suffix 	:= .gomp
  endif
endif

ifdef MKL
blas_suffix := .mkl
else
blas_suffix := .atlas
endif

ifdef CUDA
gpu_suffix := .cuda
else
gpu_suffix :=
endif

ifdef MPI
  mpi_suffix 	:= .mpi
endif
ifdef PROF
  prof_suffix 	:= .prof
endif

ifdef ACC
  acc_suffix 	:= .acc
endif
ifndef NDEBUG
  debug_suffix 	:= .debug
endif
ifdef TEST
  ifdef NDEBUG
    debug_suffix := .test
  endif
endif
ifdef MIC
  mic_suffix    := .mic
endif
ifdef SDC
  sdc_suffix 	:= .SDC
endif
ifdef ROSE
  rose_suffix   := .rose
endif
ifdef ZMQ
  zmq_suffix := .zmq
endif
ifdef HDF
  hdf_suffix := .hdf
endif

suf=$(ARCH).$(COMP)$(debug_suffix)$(prof_suffix)$(mpi_suffix)$(omp_suffix)$(hpc_suffix)$(sdc_suffix)$(zmq_suffix)$(hdf_suffix)$(blas_suffix)${gpu_suffix}${dim_suffix}

sources     =
fsources    =
f90sources  =
sf90sources  =
csources    =
cxxsources  =
libraries   = -lsiloh5 -lz -lstdc++ -lhdf5 -lhdf5_fortran -lhdf5_hl -lhdf5hl_fortran -ldl
xtr_libraries =
hypre_libraries =
mpi_libraries = -lmpi_f90 -lmpi_f77 -lmpi -lhwloc
mpi_include_dir = $(MPI_ROOTDIR)/include
mpi_lib_dir = $(MPI_ROOTDIR)/lib

ifdef CUDA
VPATH_LOCATIONS += $(CUDAROOT)/src/
csources += fortran.c
endif

ifdef MKL
libraries += -lmkl_gf_lp64 -lmkl_core
  ifdef INTEL_THREAD
    libraries += -lmkl_intel_thread -liomp5 -ldl -lpthread -lm
  else
    libraries += -lmkl_gnu_thread -ldl -lpthread -lm
  endif
else
libraries += -lblas -llapack
endif

ifdef CUDA
libraries += -lcudart -lcublas
endif


# test for endianness to distinguish older IBM Mac's from newer Intel ones
endian := $(shell perl -MConfig -e '$$i=index $$Config{byteorder},1;if ($$i == 0){print "little"};')

ifeq ($(ARCH),Darwin)
  ifeq ($(findstring little,$(endian)),little)
    CPPFLAGS += -DBL_$(ARCH)_little
  else
    CPPFLAGS += -DBL_$(ARCH)
  endif
else
  CPPFLAGS += -DBL_$(ARCH)
endif

CPPFLAGS += -DFORTRAN_BOXLIB

ifdef TEST
  CPPFLAGS += -DBL_TESTING
endif

ifndef NDEBUG
  CPPFLAGS += -DDEBUG
endif

F_C_LINK := UNDERSCORE

odir=.
mdir=.
tdir=.

tdir = t/$(suf)
odir = $(tdir)/o
mdir = $(tdir)/m
hdir = t/html

ifdef DIM3
fpp_flags += -cpp -DDIM3
endif

ifeq ($(COMP),g95)
  include $(DEEPFRY_HOME)/Tools/F_mk/comps/g95.mak
endif

ifeq ($(findstring gfortran, $(COMP)), gfortran)
  include $(DEEPFRY_HOME)/make/gfortran.mak
endif

ifeq ($(COMP),xlf)
  include $(DEEPFRY_HOME)/Tools/F_mk/comps/xlf.mak
endif

ifeq ($(ARCH),Darwin)
  ifeq ($(COMP),IBM)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Darwin_ibm.mak
  endif

  ifeq ($(COMP),Intel)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Darwin_intel.mak
  endif
endif

ifeq ($(ARCH),FreeBSD)
endif

ifeq ($(ARCH),Linux)
  ifeq ($(COMP),Cray)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_cray.mak
  endif

  ifeq ($(COMP),PGI)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_pgi.mak
  endif

  ifeq ($(COMP),SunStudio)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_sunstudio.mak
  endif

  ifeq ($(COMP),PathScale)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_pathscale.mak
  endif

  ifeq ($(COMP),Intel)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_intel.mak
  endif

  # Gottingen machines
  ifeq ($(HOST),hicegate0)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_intel.mak
  endif

  ifeq ($(COMP),NAG)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_nag.mak
  endif

  ifeq ($(COMP),Lahey)
    include $(DEEPFRY_HOME)/Tools/F_mk/comps/Linux_lahey.mak
  endif
endif

ifeq ($(ARCH),AIX)
  include $(DEEPFRY_HOME)/Tools/F_mk/comps/aix.mak
endif

ifeq ($(ARCH),OSF1)
  include $(DEEPFRY_HOME)/Tools/F_mk/comps/osf1.mak
endif

ifeq ($(findstring mira, $(HOSTNAMEF)), mira)
  include $(DEEPFRY_HOME)/Tools/F_mk/comps/bgq.mak
endif

ifeq ($(strip $(F90)),)
   $(error "COMP=$(COMP) is not supported")   
endif

ifdef ROSE
ifeq ($(strip $(ROSECOMP)),)
   $(error "ROSECOMP is not defined")   
endif
endif

# ifdef MPI
#   include $(DEEPFRY_HOME)/Tools/F_mk/GMakeMPI.mak
# endif

ifdef mpi_include_dir
  fpp_flags += -I$(mpi_include_dir)
endif

ifdef mpi_lib_dir
  fld_flags += -L$(mpi_lib_dir)
endif

ifdef mpi_lib_dir
  fld_flags += -L$(mpi_lib_dir)
endif

fpp_flags += -I$(SILO_INCLUDE)
fpp_flags += -I$(HDF5_INCLUDE) -Wl,-rpath -Wl,/home/kunalp/usr/local/HDF5/lib

fld_flags += -L$(HDF5_LIBDIR) -L/home/kunalp/Enthought/Canopy_64bit/User/lib
fld_flags += -L$(SILO_LIBDIR)

fpp_flags += -I$(MPI_ROOTDIR)/include/ -I$(MPI_ROOTDIR)/lib
fld_flags += -L$(MPI_ROOTDIR)/lib

ifdef MKL
fld_flags += -L$(MKLROOT)/lib/intel64
else
fld_flags += -L$(ATLAS_LIBDIR)
endif

CUDA_FLAGS :=
ifdef CUDA
CUDA_FLAGS = $(addprefix -I, $(CUDAROOT)/include $(CUDAROOT)/src)
INCLUDE_LOCATIONS += $(CUDAROOT)/include $(CUDAROOT)/src
fpp_flags += -cpp -DCUDA
fld_flags += -cpp -DCUDA
else
fpp_flags += -cpp -DNOCUDA
fld_flags += -cpp -DNOCUDA
endif

ifdef BOXLIB
fpp_flags += -cpp -DBOXLIB
fld_flags += -cpp -DBOXLIB
endif

f_includes = $(addprefix -I , $(FINCLUDE_LOCATIONS))
c_includes = $(addprefix -I , $(INCLUDE_LOCATIONS))

TCSORT  :=  $(DEEPFRY_HOME)/Tools/F_scripts/tcsort.pl
MODDEP  :=  $(DEEPFRY_HOME)/Tools/F_scripts/moddep.pl
MKDEP   :=  $(DEEPFRY_HOME)/Tools/F_scripts/mkdep.pl
F90DOC  :=  $(DEEPFRY_HOME)/Tools/F_scripts/f90doc/f90doc

FPPFLAGS += $(fpp_flags) $(addprefix -I, $(FINCLUDE_LOCATIONS))
LDFLAGS  += $(fld_flags)
libraries += $(hypre_libraries) $(mpi_libraries) $(xtr_libraries)

ifdef CUDA
LDFLAGS += $(addprefix -L, $(CUDAROOT)/lib64)
endif

CPPFLAGS += -DBL_FORT_USE_$(F_C_LINK) $(addprefix -I, $(INCLUDE_LOCATIONS))

objects = $(addprefix $(odir)/,       \
	$(sort $(f90sources:.f90=.o)) \
	$(sort $(sf90sources:.f90=.o)) \
	$(sort $(fsources:.f=.o))     \
	$(sort $(csources:.c=.o))     \
	$(sort $(cxxsources:.cpp=.o))     \
	)
sources =                     \
	$(sort $(f90sources)) \
	$(sort $(fsources)  ) \
	$(sort $(csources)  ) \
	$(sort $(cxxsources)  )

html_sources = $(addprefix $(hdir)/,     \
	$(sort $(f90sources:.f90=.html)) \
	$(sort $(fsources:.f=.html))     \
	)

pnames = $(addsuffix .$(suf).exe, $(basename $(programs)))

ifdef MKL
  F90FLAGS += -m64 -I$(MKLROOT)/include
endif

ifndef ROSE
   COMPILE.f   = $(FC)  $(FFLAGS) $(FPPFLAGS) $(TARGET_ARCH) -c
   COMPILE.f90 = $(F90) $(F90FLAGS) $(FPPFLAGS) $(TARGET_ARCH) -c
else
   COMPILE.cxx = $(ROSECOMP) $(CXXFLAGS) $(ROSEFLAGS) $(CPPFLAGS) -c
   COMPILE.c   = $(ROSECOMP) $(CFLAGS)   $(ROSEFLAGS) $(CPPFLAGS) -c
   COMPILE.f   = $(ROSECOMP) $(FFLAGS)   $(ROSEFLAGS) $(FPPFLAGS) -c
   COMPILE.f90 = $(ROSECOMP) $(F90FLAGS) $(ROSEFLAGS) $(FPPFLAGS) -c
endif

LINK.f      = $(FC)  $(FFLAGS) $(FPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)
LINK.f90    = $(F90) $(F90FLAGS) $(FPPFLAGS) $(LDFLAGS) $(TARGET_ARCH)


# some pretty printing stuff
bold=`tput bold`
normal=`tput sgr0`
