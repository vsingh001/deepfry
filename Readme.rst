DeepFRy: A 2D/3D Flux-Reconstruction code on Unstructured Grids
-----------------------------------------------------------------

DeepFRy is a set of utilities/libraies to perform CFD computations
employing the `Flux-Reconstruction
<http://arc.aiaa.org/doi/abs/10.2514/6.2007-4079>`_ numerical
method. The name camme about from an earlier verison of the code
(SmallFRy) which was intended as an acronym for Small
Flux-Reconstruction code in Python.

Features
---------

- Unstructured triangular element formulation `(Williams et al.)  <http://www.sciencedirect.com/science/article/pii/S0021999113003318>`_
- Arbitrary solution order

Installation
-------------

DeepFRy requires the `SILO
<https://wci.llnl.gov/simulation/computer-codes/silo>`_ library to
write `VisIt <https://wci.llnl.gov/simulation/computer-codes/visit>`_
readable solution files. These can be installed together from the top
level directory::

      $ make -f Makefile.init

This will install `HDF5 <https://www.hdfgroup.org/HDF5/>`_ and SILO in
``$HOME/usr/local`` which will be the default search path.

To compile the examples::

      $ cd examples/CNS/euler_vortex
      $ make ATLAS_LIBDIR=/usr/lib64/atlas

Where, ``ATLAS_LIBDIR`` is the location for your local BLAS
implementation. Additionally, you can provide ``HDF5_INCLUDE``,
``HDF5_LIBDIR``, ``SILO_INCLUDE`` and ``SILO_LIBDIR`` as optional
arguments to make.

Running the examples
--------------------

After successful compilation, you should have a an executable within
the example directory. The executable can be run like so::

    $ ./main.Linux.gfortran.exe vortex.in

Where, ``vortex.in`` is the input file for the example. The solution
output is a series of files in the current directory which can be
amalgamated into a single database like so::
 

    $ ls -1 plt.vortex* | tee vortex.visit

The resulting file can be opened using the VisIt visualization
package.

Credits
--------

Most of the build system was borrowed from the `BoxLib
<https://ccse.lbl.gov/BoxLib/>`_ AMR framekwork.