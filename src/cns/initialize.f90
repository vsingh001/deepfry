module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    type(FieldData), intent(inout) :: data
    type(mesh2d), intent(inout)    :: mesh
    
    ! locals
    integer(c_int) :: index
    real(c_double), allocatable :: xs(:), ys(:), r2(:)
    
    allocate(xs(mesh%Np), ys(mesh%Np), r2(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys)
       r2 = xs**2 + ys**2

       mesh%u(:, 1, index) = ONE + exp(-TEN*r2)!sin(M_PI*xs)*sin(M_PI*ys)
       mesh%u(:, 2, index) = xs + ys
       mesh%u(:, 3, index) = xs - ys
       mesh%u(:, 4, index) = FOUR3RD
    end do
    
    deallocate(xs, ys, r2)

  end subroutine set_initial_values

  subroutine test_initial_values(mesh)
    type(mesh2d), intent(in) :: mesh
  end subroutine test_initial_values

end module initialize_module
