module stats_module
  use input_module
  use deepfry_constants_module
  use iso_c_binding, only: c_double, c_int

  use mesh_module
  use fieldvar_module

  use userstats_module

#ifdef BOXLIB
  use parallel
#else
  use omp_lib
  use mpi
#endif

  implicit none

contains
  
  subroutine open_stats_file(fname, myRank, numProcs)
    use input_module, only: restart

    character(len=256), intent(in) :: fname
    integer(c_int),     intent(in) :: myRank, numProcs
    
    ! locals
    logical :: exist

    
    if (myRank .eq. 0) then
       inquire(file=fname, exist=exist)
       if (exist) then
          if (restart .gt. 0) then
             open(12, file=fname, status='old', position='append', action='write')
          else
             write(*, *) 'Old stats file exists! exiting...'
             stop
          end if
       else
          open(12, file=fname, status='new', action='write')
       end if
    end if

  end subroutine open_stats_file

  subroutine write_stats(fname, time, mesh, data, myRank, numProcs)
    use input_module, only: n_userstats

    integer(c_int),     intent(in) :: myRank, numProcs
    character(len=256), intent(in) :: fname
    real(c_double),     intent(in) :: time
    type(mesh2d),       intent(in) :: mesh
    type(FieldData_t),  intent(in) :: data

    ! locals
    integer(c_int) :: elem_index, j, mpi_err
    real(c_double) :: rho, velx, vely, velz
    real(c_double) :: vx, uy, wy, vz, wx, uz

    real(c_double) :: stats_local(2 + n_userstats), stats_global(2 + n_userstats)
    real(c_double) :: ke(mesh%Np), ws(mesh%Np)

    stats_local = ZERO

    open(12, file=fname, status='old', position='append', action='write')

    !$omp parallel do private(elem_index, j, rho, velx, vely, velz, &
    !$omp                     vx, uy, wy, vz, wx, uz, ke, ws) reduction(+:stats_local)
    do elem_index = 1, mesh%Nelements_local
       do j = 1, mesh%Np
          
          rho  = data%u(j, 1, elem_index)
          velx = data%u(j, 2, elem_index)/rho
          vely = data%u(j, 3, elem_index)/rho
          velz = ZERO
          
          vx = ONE/rho * (data%ux(j, 3, elem_index) - vely*data%ux(j, 1, elem_index))
          uy = ONE/rho * (data%uy(j, 2, elem_index) - velx*data%uy(j, 1, elem_index))
          
          wy = ZERO; vz = ZERO; wx = ZERO; uz = ZERO

#ifdef DIM3
          velz = data%u(j, 4, elem_index)/rho

          wx = ONE/rho * (data%ux(j, 4, elem_index) - velz*data%ux(j, 1, elem_index))
          wy = ONE/rho * (data%uy(j, 4, elem_index) - velz*data%uy(j, 1, elem_index))

          uz = ONE/rho * (data%uz(j, 2, elem_index) - velx*data%uz(j, 1, elem_index))
          vz = ONE/rho * (data%uz(j, 3, elem_index) - vely*data%uz(j, 1, elem_index))         
#endif
          ke(j) = HALF * rho * (velx*velx + vely*vely + velz*velz)
          ws(j) = HALF * rho * (vx - uy)**2 - (wy - vz)**2 + (wx - uz)**2

       end do
       
       ! compute the average ke using the quadrature weights
       stats_local(1) = stats_local(1) + sum( mesh%w * ke*mesh%detJn(:, elem_index) )
       stats_local(2) = stats_local(2) + sum( mesh%w * ws*mesh%detJn(:, elem_index) )
    end do
    !$omp end parallel do

    ! call any additional statistics
    call get_user_stats(time, mesh, data, myRank, numProcs)

    ! get the sum across all ranks
    stats_global = stats_local
    if (numProcs .gt. 1) then
       call MPI_Reduce(stats_local, stats_global, size(stats_local), MPI_DOUBLE, &
                       MPI_SUM, 0, MPI_COMM_WORLD, mpi_err)
    end if
    
    ! write to the stats file
    if (myRank .eq. 0) then
       write(12, *) time, stats_global
    end if
    close(12)
    
  end subroutine write_stats

end module stats_module
