program main
  use iso_c_binding, only: c_double, c_int
  use logo_module
  use mesh_module
  use initialize_module
  use fr_module
  use frutils_module
  use system_module
  use input_module
  use runtime_init_module
  use advance_module
  use linked_list_module
  use restart_module 

  use vtk_headers_module
  use vtk_writer_module

  use visit_writer_module
  use hdf5_writer_module

  use limiter_module
  use parallel_module

  use stats_module
  use userstats_module

#ifdef CUDA  
  use gpu_module
#endif

#ifdef BOXLIB
  use df_boxlib_module
  use df_boxlib_interface_module

  use parallel
  use ml_layout_module
  use multifab_module

  use boxlib_initialize_grid_module
  use bl_plot_module

  use knapsack_module
  use cluster_module
  use layout_module

#else
  use mpi
  use omp_lib
#endif

  implicit none

  ! locals
  character(len=3)   :: partition_index
  character(len=9)   :: plot_index, restart_index
  character(len=256) :: plotfile_name, restartfile_name
  integer(c_int)     :: istep, init_step, last_plt_written, last_restrt_written
  real(c_double)     :: time, dt, dtold, dt_global
  integer(c_int)     :: max_iterations

  integer(c_int) :: restart_step
  real(c_double) :: restart_time

  real(c_double)     :: time_to_write

  real(c_double), allocatable :: Wmin(:), Wmin_global(:)
  real(c_double), allocatable :: Wmax(:), Wmax_global(:)

  real(c_double) :: fraction_done

  ! the mesh data structure
  type(mesh2d)                :: mesh
  type(NavierStokesSystem)    :: system  
  type(FieldData_t)           :: FieldData

  ! linked list data structure
  type(linked_list_data) :: linked_list

  ! MPI variables
  integer(c_int) :: myrank, numProcs, mpi_err

#ifdef CUDA  
  ! cuBLAS GPU data structure
  type(device_data_t), target :: device_data
#endif

  ! MPI data
  type(mpi_data_t) :: mpi_data

  ! Timing variables
  real(c_double)     :: start_time, end_time
  real(c_double)     :: time_to_advance, mpi_time, mpi_ratio
  real(c_double)     :: time_to_advance_global(2), time_to_advance_local(2)
  real(c_double)     :: t1, t2

#ifdef BOXLIB
  integer :: level
  character(len=256) :: bl_plotfile_name
  real(c_double) :: write_pf_time
#endif

  type(vtk_mesh_header_t) :: vtk_header

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! read parameters from input file
  call runtime_init(myrank)

  ! initialize MPI and get the rank and size info
  myrank = 0; numProcs = 1

#ifdef BOXLIB
  ! Initialize the BoxLib library. Under the hood, MPI_Init is called
  call initialize_df_boxlib()

  ! BoxLib parameters
  call cluster_set_minwidth(minwidth)
  call cluster_set_blocking_factor(blocking_factor)
  call cluster_set_min_eff(min_eff)
  
  call knapsack_set_verbose(the_knapsack_verbosity)
  
  call layout_set_verbosity(the_layout_verbosity)
  call layout_set_copyassoc_max(the_copy_cache_max)
  call layout_set_sfc_threshold(the_sfc_threshold)

  call ml_layout_set_strategy(the_ml_layout_strategy)

#else
  call MPI_Init(mpi_err)
#endif

  call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, mpi_err)
  call MPI_Comm_Size(MPI_COMM_WORLD, numProcs, mpi_err)

  write(unit=partition_index, fmt='(i3.3)') myrank 

  ! Allocate data for the VTK output 
  call allocate_vtk_header(vtk_header)

  ! build the mesh and associated data
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)
  if (verbose .and. myRank .eq. 0) then
     call logo
     write(*, *) 'Initializing mesh ...'
  end if
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

  mesh%vtk_header = vtk_header
  call initialize_deepfry(mesh, numProcs, myrank)

  ! allocate a field on the mesh. Currently, the default field is a
  ! CNS field with Nvar variables (conserved variables). In general,
  ! other fields can be allocated
  call allocate_field(mesh%DIM, mesh%elem_num_verts, mesh%Np, mesh%Nflux, &
                      vtk_header%PlotNumberOfPoints, &
                      mesh%Nvar, mesh%Nelements, FieldData)

  ! allocate boundary face data. This will allocate arrays for the
  ! right element for boundary faces. This can be used to set
  ! appropriate boundary conditions, especially periodic bcs
  call allocate_bface_data(mesh, FieldData)
  if (df_is_periodic) then
     call setup_local_periodic_maps(mesh, FieldData)
  end if

  ! Initialize the linked list for nearest element
  ! queries. !FIXME. Might want to consider a global nearest neighbor
  ! search
  call initialize_linked_list(mesh, linked_list)

  ! initialize MPI data
  call initialize_mpi_data(mesh, mpi_data, myRank, numProcs)

  ! initialize the system. FIXME: Currently, this is reduced to
  ! setting non-dimensional parameters
  call initialize_system(system, gamma, mu, Cp, Pr)

  ! initialize limiter data (L2 projection operators)
  call initialize_limiter(mesh)

#ifdef CUDA
  ! Initialize cuBLAS and associated data structures
  call cublas_init()
  call initialize_GPU(mesh%DIM, device_data, mesh%Np, mesh%Nflux, mesh%Nvar, mesh%Nelements, &
                      mesh%Lf, mesh%Lgrad)

  mesh%device_data => device_data
#endif

  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)
  if ( verbose .and. myRank .eq. 0) then
     write(*, *)
     write(*, *) 'Initializing solution ...'
     write(*, *)
  end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialization of data on the mesh
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  time = ZERO
  if (restart .lt. 0) then
     istep = 0

     ! set initiali values
     call set_initial_values( mesh, FieldData )

#ifdef BOXLIB
     call initialize_with_adaptive_grids(&
          mesh%BoxLib_data%bl_mla,  mesh%BoxLib_data%bl_dx, &
          mesh%BoxLib_data%bl_Uold, mesh%BoxLib_data%bl_the_bc_tower)
     
     ! make the mapping multifabs
     allocate(mesh%BoxLib_data%bl_mask(mesh%BoxLib_data%bl_mla%nlevel))
     do level = 1, mesh%BoxLib_data%bl_mla%nlevel
        call multifab_build( mesh%BoxLib_data%bl_mask(level), &
                             mesh%BoxLib_data%bl_mla%la(level), 2, 0 )

        call setval(mesh%BoxLib_data%bl_mask(level), ZERO, all=.true.)
     end do

     ! allocate and set the interface data
     call allocate_interface_data(mesh, mesh%BoxLib_data%bl_mla, mesh%BoxLib_data%bl_dx, &
          mesh%BoxLib_data%fr_interface_data, mesh%BoxLib_data%bl_interface_data)

     call set_interface_data(mesh, FieldData, mesh%BoxLib_data%bl_mla, mesh%BoxLib_data%bl_dx, &
          mesh%BoxLib_data%bl_mask, &
          mesh%BoxLib_data%fr_interface_data, mesh%BoxLib_data%bl_interface_data)

#endif     

  else
     ! read a restart file
     write(unit=restart_index, fmt='(i9.9)') restart
     restartfile_name = trim(plot_filename) //".part" // partition_index // ".restart_" // restart_index // ".hdf5"
     write(*, *) 'Processor_'//partition_index, ' reading restart file ', trim(restartfile_name)
     call read_restart_file(restartfile_name, mesh, FieldData, restart_step, restart_time)

     time  = restart_time
     istep = restart_step

  end if 

  ! make sure all processes have their initial data defined
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

  ! allocate min and max for primitive variables
  allocate(Wmin(mesh%Nvar))
  allocate(Wmax(mesh%Nvar))

  allocate(Wmin_global(mesh%Nvar))
  allocate(Wmax_global(mesh%Nvar))

  ! determine the mesh characteristic length. FIXME: rudimentary 3D
  ! implementation
  call get_mesh_characteristic_length(mesh)

  ! get the maximum velocity from the initial condition. Perform a
  ! "soft start by increasing the estimate of the maximum velocity
  call get_maxvel(mesh, FieldData, system%gamma)
  mesh%maxvel = mesh%maxvel + TEN

  if (stop_time >= ZERO) then
     if (time+dt > stop_time) dt = min(dt, stop_time - time)
  end if

  if(fixed_dt .gt. ZERO) then
     dt = fixed_dt
     if (verbose .and. myRank .eq. 0) then
        write(*, *) "Setting fixed dt =",dt
        write(*, *)
     end if
  else 
     dt_global = estimate_dt(mesh%P, mesh%hmin, mesh%maxvel)
     call MPI_AllReduce(dt_global, dt, 1, MPI_DOUBLE, &
          MPI_MIN, MPI_COMM_WORLD, mpi_err)
  end if

  dtold = dt

  ! mark troubled cells for the initial condition. Note that we need
  ! to interpolate to the flux points because the limiter uses this
  ! value. This is done in the get gradients routine which is needed
  ! anyway for the initial plotfile

  call get_gradients(mesh, FieldData, time, mpi_data, myRank, numProcs)
  call compute_averages(mesh, FieldData)

  ! compute the avisc value from the initial condition
  if (shock_sensor_type .gt. 0) then
     call Moro_Nguyen_Peraire_divergence_sensor(mesh, FieldData)
  end if

  if (limiter_type .gt. 0) then
     call limit(mesh, FieldData, time, linked_list, mpi_data)
  end if

  !-----------------------------------------------------------------------
  ! write an initial plotfile
  !-----------------------------------------------------------------------
  if ( plot_int .gt. 0 .or. plot_deltat .gt. ZERO ) then
     if ( (plot_int .gt. 0 .and. mod(istep,plot_int) .eq. 0) .or. &
          (plot_deltat > ZERO .and. &
          mod(time - dt,plot_deltat) > mod(time,plot_deltat)) ) then       
        
        write(unit=plot_index, fmt='(i9.9)') istep

        call cpu_time(t1)
        plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index
        if (visit_output) then
           if (myRank .eq. 0 .and. numProcs .gt. 1) then
              call write_master(plotfile_name, len_trim(plotfile_name)+5, mesh, istep, time)
           end if
           call write_visit_file(plotfile_name, mesh, FieldData, istep, time)

        else if (vtk_output) then
           plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index // ".vtu"
           call write_vtk_file(plotfile_name, mesh, FieldData, vtk_header, system%gamma, istep, time)

        else
           call write_hdf5_file(plotfile_name, mesh, FieldData, istep, time)
        end if

#ifdef BOXLIB
        bl_plotfile_name = trim(plot_filename) // plot_index
        call make_bl_plotfile(bl_plotfile_name, &
             mesh%BoxLib_data%bl_mla, mesh%BoxLib_data%bl_Uold, mesh%BoxLib_data%bl_mask, &
             bl_plot_names, mesh%BoxLib_data%bl_mla%mba, &
             mesh%BoxLib_data%bl_dx, dt, mesh%BoxLib_data%bl_the_bc_tower, &
             write_pf_time, time)
#endif

        call cpu_time(t2)

        time_to_write = t2 - t1
        last_plt_written = istep
     end if
  end if

  ! open or append the stats file
  if (writestats) then
     call open_stats_file(stats_filename, myRank, numProcs)
  end if
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Main evolution loop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  call CPU_TIME(start_time)
  !$ start_time = OMP_GET_WTIME()
  init_step = istep + 1

  max_iterations = int(stop_time/Dt)

  if ( (max_step .ge. istep) .and. (time .lt. stop_time .or. stop_time .lt. ZERO) ) then
     main_evolution_loop: do istep = init_step, max_step
        !---------------------------------------------------------------------
        ! get the new timestep
        !---------------------------------------------------------------------
        dtold = dt

        if (istep .gt. 1) then
           if (fixed_dt .gt. ZERO) then
              dt_global = fixed_dt
              dt = fixed_dt
           else
              call get_maxvel(mesh, FieldData, system%gamma)
              dt_global = estimate_dt(mesh%P, mesh%hmin, mesh%maxvel)

              call MPI_AllReduce(dt_global, dt, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD, mpi_err)                   
           end if
           
           if (dt .gt. max_dt) then
              print*,'max_dt limits the new dt =',max_dt
              dt = max_dt
           end if
           
        end if
        
        if (stop_time .ge. ZERO) then
           if (time+dt > stop_time) then
              dt = stop_time - time 
              if (myRank .eq. 0) then
                 print*, "Stop time limits dt =",dt
              end if
           end if
        end if
        
        if (verbose) then
           if (myRank .eq. 0) then
              write(6,1000) istep,time,dt
              write(6,*)
           end if

           call primitive_variable_min_max(mesh, FieldData, Wmin, Wmax)
           call MPI_Barrier(MPI_COMM_WORLD, mpi_err)
           call MPI_Reduce(Wmin, Wmin_global, mesh%Nvar, MPI_DOUBLE, &
                MPI_MIN, 0, MPI_COMM_WORLD, mpi_err)
           call MPI_Reduce(Wmax, Wmax_global, mesh%Nvar, MPI_DOUBLE, &
                MPI_MAX, 0, MPI_COMM_WORLD, mpi_err)

           if (myRank .eq. 0) then
              write(6, 1001) istep, Wmin_global(1), Wmax_global(1)
              write(6, 1002) istep, Wmin_global(2), Wmax_global(2)
              write(6, 1003) istep, Wmin_global(3), Wmax_global(3)
#ifdef DIM3
              write(6, 1004) istep, Wmin_global(4), Wmax_global(4)
#endif           
              write(6,*)
           end if
        end if

1000 format('STEP = ',i7,1x,' TIME = ',es16.10,1x,' DT = ',es16.10)

1001 format('... step: min/max : old density   ', i8,2x,e17.10,2x,e17.10)
1002 format('... step: min/max : old x-vel     ', i8,2x,e17.10,2x,e17.10)
1003 format('... step: min/max : old y-vel     ', i8,2x,e17.10,2x,e17.10)
#ifdef DIM3
1004 format('... step: min/max : old z-vel     ', i8,2x,e17.10,2x,e17.10)
#endif

        !---------------------------------------------------------------------
        ! Advance a single timestep.
        !---------------------------------------------------------------------
        !$ t1 = omp_get_wtime()
        mpi_time = ZERO
        call advance_timestep(system, mesh, FieldData, dt, time, istep, linked_list, mpi_data, &
             myRank, numProcs, mpi_time)
        !$ t2 = omp_get_wtime()
        time = time + dt

        ! compute stats
        if (writestats) then
           call write_stats(stats_filename, time, mesh, FieldData, myRank, numProcs)
        end if

        time_to_advance = t2 - t1
        fraction_done   = time/stop_time * 100

        if (timings .and. myRank .eq. 0) then
           print*, '... Timings'
           print*,
           write(6, 5000) rhs_evaluation_time
           write(6, 5001) interpolate_at_face_time, interpolate_at_face_time/rhs_evaluation_time*100

           write(6, 5002) get_ucommon_time, get_ucommon_time/rhs_evaluation_time*100
           write(6, 50021) boundary_ucommon_time, boundary_ucommon_time/rhs_evaluation_time*100

           write(6, 5003) get_gradients_time, get_gradients_time/rhs_evaluation_time*100

           write(6, 5004) discontinuous_flux_sol_time, discontinuous_flux_sol_time/rhs_evaluation_time*100
           !write(6, 5004) discontinuous_flux_flux_time, discontinuous_flux_flux_time/rhs_evaluation_time*100

           write(6, 5005) get_interaction_flux_time, get_interaction_flux_time/rhs_evaluation_time*100
           write(6, 50051) boundary_interaction_time, boundary_interaction_time/rhs_evaluation_time*100

           write(6, 5006) flux_divergence_time, flux_divergence_time/rhs_evaluation_time*100

#ifdef CUDA
           write(6, 5007) gpu_ugrad_write_time, gpu_ugrad_write_time/rhs_evaluation_time*100
           write(6, 5008) gpu_ugrad_read_time, gpu_ugrad_read_time/rhs_evaluation_time*100
           write(6, 5009) gpu_Fgrad_write_time, gpu_Fgrad_write_time/rhs_evaluation_time*100
           write(6, 5010) gpu_Fgrad_read_time, gpu_Fgrad_read_time/rhs_evaluation_time*100
#endif

           print*,
        end if

5000  format('        RHS Eval                            : ', e17.10)
5001  format('            Interpolation                   : ', e17.10, F10.3)
5002  format('            Ucommon                         : ', e17.10, F10.3)
50021 format('               Boundary Ucommon             : ', e17.10, F10.3)
5003  format('            Gradients                       : ', e17.10, F10.3)
5004  format('            Discontinuous Flux              : ', e17.10, F10.3)
5005  format('            Interaction Flux                : ', e17.10, F10.3)
50051 format('               Boundary interaction flux    : ', e17.10, F10.3)
5006  format('            Flux Divergence                 : ', e17.10, F10.3)

#ifdef CUDA
5007  format('            GPU ugrad write     : ', e17.10, F10.3)
5008  format('            GPU ugrad read      : ', e17.10, F10.3)
5009  format('            GPU Fgrad write     : ', e17.10, F10.3)
5010  format('            GPU Fgrad read      : ', e17.10, F10.3)
#endif

        if (verbose) then
           call primitive_variable_min_max(mesh, FieldData, Wmin, Wmax)
           !call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

           call MPI_Reduce(Wmin, Wmin_global, mesh%Nvar, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD, mpi_err)
           call MPI_Reduce(Wmax, Wmax_global, mesh%Nvar, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD, mpi_err)
                
           time_to_advance_local(1) = time_to_advance
           time_to_advance_local(2) = mpi_time

           call MPI_Reduce(time_to_advance_local, time_to_advance_global, 2, MPI_DOUBLE, MPI_MAX, &
                0, MPI_COMM_WORLD, mpi_err)

           if (myRank .eq. 0) then
              write(6, 2001) istep, Wmin_global(1), Wmax_global(1)
              write(6, 2002) istep, Wmin_global(2), Wmax_global(2)
              write(6, 2003) istep, Wmin_global(3), Wmax_global(3)
#ifdef DIM3
              write(6, 2004) istep, Wmin_global(4), Wmax_global(4)
#endif
              time_to_advance = time_to_advance_global(1)
              mpi_time        = time_to_advance_global(2)

              mpi_ratio = TEN*TEN*(mpi_time/time_to_advance)
              write(6,*)
              write(6, 2005) 'Time to advance in seconds: Overall =',time_to_advance, 'MPI = ',mpi_time, 'Ratio = ', mpi_ratio, '%'
              write(6,*)
           end if
        end if

2001 format('... step: min/max : new density   ', i8,2x,e17.10,2x,e17.10)
2002 format('... step: min/max : new x-vel     ', i8,2x,e17.10,2x,e17.10)
2003 format('... step: min/max : new y-vel     ', i8,2x,e17.10,2x,e17.10)
#ifdef DIM3
2004 format('... step: min/max : new z-vel     ', i8,2x,e17.10,2x,e17.10)
#endif
2005 format(A37, F8.4, 2x, A5,F8.4,2x,A9,F8.4,1x,A1)

        !---------------------------------------------------------------------
        ! Filter the solution
        !---------------------------------------------------------------------
        if (filter_int .gt. 0) then
           if (mod(istep, filter_int) .eq. 0) then
              call apply_filter(mesh%Np, mesh%Nvar, mesh%Nelements, mesh%Filter, FieldData%u)
           end if
        end if

        !-----------------------------------------------------------------------
        ! write a plotfile
        !-----------------------------------------------------------------------
        if ( plot_int .gt. 0 .or. plot_deltat .gt. ZERO ) then
           if ( (plot_int .gt. 0 .and. mod(istep,plot_int) .eq. 0) .or. &
                (plot_deltat > ZERO .and. &
                mod(time - dt,plot_deltat) > mod(time,plot_deltat)) ) then

              ! Avoid calling gradients to save time. The output file
              ! will have gradients from the previous time-step.  
              !call get_gradients(mesh, FieldData, time, mpi_data, myRank, numProcs)
              call compute_averages(mesh, FieldData)

              if (shock_sensor_type .gt. 0) then
                 call Moro_Nguyen_Peraire_divergence_sensor(mesh, FieldData)
              end if

              if (limiter_type .gt. 0) then
                 call limit(mesh, FieldData, time, linked_list, mpi_data)
              end if

              write(unit=plot_index, fmt='(i9.9)') istep
              call cpu_time(t1)
              plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index
              if (visit_output) then
                 call write_visit_file(plotfile_name, mesh, FieldData, istep, time)
                 if (myRank .eq. 0 .and. numProcs .gt. 1) then
                    call write_master(plotfile_name, len_trim(plotfile_name)+5, mesh, istep, time)
                 end if
              else if (vtk_output) then
                 plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index // ".vtu"
                 call write_vtk_file(plotfile_name, mesh, FieldData, vtk_header, system%gamma, istep, time)
              else
                 call write_hdf5_file(plotfile_name, mesh, FieldData, istep, time)
              end if

              call cpu_time(t2)

              time_to_write = t2 - t1
              last_plt_written = istep
           end if
        end if

        !-----------------------------------------------------------------------
        ! write a restart file
        !-----------------------------------------------------------------------
        if ( restart_int .gt. 0 ) then
           if (mod(istep, restart_int) .eq. 0) then

              write(unit=restart_index, fmt='(i9.9)') istep
              restartfile_name = trim(plot_filename) // ".part" // partition_index // ".restart_" // restart_index // ".hdf5"

              call cpu_time(t1)
              call write_restart_file(restartfile_name, mesh, FieldData, istep, time)
              call cpu_time(t2)

              last_restrt_written = istep
           end if

        end if

        ! have we reached the stop time?
        if (stop_time >= 0.d0) then
           if (time >= stop_time) then 
              goto 999
           end if
        end if
        
     end do main_evolution_loop

999  continue

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! write the final plotfile
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     !-----------------------------------------------------------------------
     ! write a plotfile
     !-----------------------------------------------------------------------
     call get_gradients(mesh, FieldData, time, mpi_data, myRank, numProcs)
     call compute_averages(mesh, FieldData)

     if (shock_sensor_type .gt. 0) then
        call Moro_Nguyen_Peraire_divergence_sensor(mesh, FieldData)
     end if

     if (limiter_type .gt. 0) then
        call limit(mesh, FieldData, time, linked_list, mpi_data)
     end if    

     write(unit=plot_index, fmt='(i9.9)') istep

     call cpu_time(t1)
     plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index

     if (visit_output) then
        if (myRank .eq. 0 .and. numProcs .gt. 1) then
           call write_master(plotfile_name, len_trim(plotfile_name)+5, mesh, istep, time)
        end if
        call write_visit_file(plotfile_name, mesh, FieldData, istep, time)

     else if (vtk_output) then
        plotfile_name = trim(plot_filename) // ".part" // partition_index // "_" // plot_index // ".vtu"
        call write_vtk_file(plotfile_name, mesh, FieldData, vtk_header, system%gamma, istep, time)
     else
        call write_hdf5_file(plotfile_name, mesh, FieldData, istep, time)
     end if
     call cpu_time(t2)

     time_to_write = t2-t1

  end if
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! clean-up
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  deallocate(Wmin)
  deallocate(Wmax)

  deallocate(Wmin_global)
  deallocate(Wmax_global)

  call destroy_mpi_data(mesh, mpi_data, myRank, numProcs)
  call deallocate_linked_list(linked_list)

  ! deallocate the boundary face data for the mesh/field
  call deallocate_bface_data(FieldData)

  call CPU_TIME(end_time)
  !$ end_time = OMP_GET_WTIME() 

#ifdef CUDA
  call cublas_shutdown()
  call shutdown_GPU(device_data)
#endif

  call runtime_close()  

  ! finalize MPI & Boxlib
#ifdef BOXLIB
  do level = 1, mesh%BoxLib_data%bl_mla%nlevel
     call destroy(mesh%BoxLib_data%bl_Uold(level))
     call destroy(mesh%BoxLib_data%bl_mask(level))
  end do  

  call destroy(mesh%BoxLib_data%bl_mla)
  call bc_tower_destroy(mesh%BoxLib_data%bl_the_bc_tower)

  deallocate(mesh%BoxLib_data%bl_dx)
  deallocate(mesh%BoxLib_data%bl_Uold)
  deallocate(mesh%BoxLib_data%bl_mask)

  call deallocate_interface_data(mesh%BoxLib_data%fr_interface_data, &
                                 mesh%BoxLib_data%bl_interface_data)

#else
  call MPI_Finalize(mpi_err)
#endif

  ! delete the primary mesh data structure
  call delete_mesh(mesh)

  ! delete the field data
  call deallocate_field(FieldData)

  ! delete the vtk header data
  call deallocate_vtk_header(vtk_header)

  if (myRank .eq. 0) then
     write(*, *) 'Run took ', end_time-start_time, ' seconds'
  end if

end program main

subroutine progress_bar(iteration, maximum)
  !
  ! Prints progress bar.
  !
  ! Args: 
  !     iteration - iteration number
  !     maximum - total iterations
  !
  implicit none
  integer :: iteration, maximum
  integer :: counter
  integer :: step, done
  
  step = nint(iteration * 100 / (1.0 * maximum))
  done = floor(step / 10.0)  ! mark every 10%
  
  do counter = 1, 36                    ! clear whole line - 36 chars
     write(6,'(a)',advance='no') '\b'  ! (\b - backslash)
  end do
  
  write(6,'(a)',advance='no') ' -> In progress... ['
  if (done .LE. 0) then
     do counter = 1, 10
        write(6,'(a)',advance='no') '='
     end do
  else if ((done .GT. 0) .and. (done .LT. 10)) then
     do counter = 1, done
        write(6,'(a)',advance='no') '#'
     end do
     do counter = done+1, 10
        write(6,'(a)',advance='no') '='
     end do
  else
     do counter = 1, 10
        write(6,'(a)',advance='no') '#'
     end do
  end if
  write(6,'(a)',advance='no') '] '
  write(6,'(I3.1)',advance='no') step
  write(6,'(a)',advance='no') '%'
end subroutine progress_bar
