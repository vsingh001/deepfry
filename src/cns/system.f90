! Module for various systems (Euler, Navier Stokes)
module system_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use fr_module
  use linked_list_module
  use limiter_module
  use flux_module

  use df_bc_module
  use userbc_module

  use parallel_module
  use timings_module

  use fieldvar_module

#ifdef CUDA  
  use gpu_module
#endif

#ifdef BOXLIB
  use parallel
  use omp_module
  use df_boxlib_interface_module
#else
  use omp_lib
  use parallel_module
#endif

  implicit none

  type BaseSystem
  end type BaseSystem

  type, EXTENDS(BaseSystem) :: NavierStokesSystem
     ! constants defining the system
     real(c_double) :: gamma, mu, Cp, Cv, Pr, Rs
  end type NavierStokesSystem

  contains
    subroutine initialize_system(system, gamma, mu, Cp, Pr)
      type(NavierStokesSystem), intent(inout) :: system
      real(c_double), optional, intent(in)    :: gamma, mu, Cp, Pr

      ! locals
      real(c_double) :: igamma, imu, iCp, iPr

      ! initialize constants
      igamma = 1.4_c_double  ; if (present(gamma)) igamma = gamma
      imu    = ZERO          ; if (present(mu))    imu    = mu
      iCp    = 1005_c_double ; if (present(Cp))    iCp    = Cp
      iPr    = 0.72_c_double ; if (present(Pr))    iPr    = Pr

      system%gamma = igamma
      system%mu    = imu
      system%Cp    = iCp
      system%Pr    = iPr
      system%Cv    = system%Cp/system%gamma
      system%Rs    = system%Cp-system%Cv

    end subroutine initialize_system

    subroutine evaluate_rhs(system, mesh, data, time, istep, &
         linked_list, mpi_data, myRank, numProcs, mpi_time)

      use input_module, only: limiter_type, shock_sensor_type, initial_limit_nsteps, &
                              use_shock_sensor_as_marker, ldg_beta

      integer(c_int),           intent(in   ) :: istep, myRank, numProcs
      real(c_double),           intent(in   ) :: time
      type(NavierStokesSystem), intent(inout) :: system
      type(mesh2d),             intent(inout) :: mesh
      type(FieldData_t),        intent(inout) :: data
      type(linked_list_data),   intent(inout) :: linked_list
      type(mpi_data_t),         intent(inout) :: mpi_data
      real(c_double),           intent(out  ) :: mpi_time

      real(c_double) :: t_start, t_end
      real(c_double) :: t1, t2, t3, mpi_t1, mpi_t2
      logical :: initial_limiting, ilimit, ishock_sensor

      initial_limiting = .false.

      !$ t_start = omp_get_wtime()

      ! Exchange updated solution values for the ghost-remote cells.
      if (mesh%numPartitions .gt. 1) then
         mpi_t1 = mpi_wtime()
         call exchange_data(mesh%Np, mesh%Nvar, mesh, data%u, USOL_TAG, &
              mpi_data%sol_send, mpi_data%sol_recv, myRank, numProcs, MPI_DOUBLE)
         mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
      end if

      ! Interpolate the solution to the flux points. (local + ghost)
      !$ t1 = omp_get_wtime()
      call interpolate_to_flux_points(mesh%Np, mesh%Nflux, mesh%Nvar, mesh%Nelements, mesh%Lf, data%u, data%uflux)
      !$ t2 = omp_get_wtime()
      interpolate_at_face_time = t2-t1

      ! handle periodicity
      if (df_is_periodic) then
         call copy_locally_mapped_elements(mesh, data)
         
         if (mesh%numPartitions .gt. 1) then
            mpi_t1 = mpi_wtime()
            call exchange_periodic_data(mesh%Nflux, mesh%P1, mesh%Nvar, mesh, &
                 data%uflux, data%bface_data%uflux_rght, UFLX_TAG, &
                 mpi_data%periodic_sendbuf, mpi_data%periodic_recvbuf, &
                 myRank, numProcs, MPI_DOUBLE)
            mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
         end if
      end if

      ! check if limiting needs to be done
      ilimit = (limiter_type .gt. 0) .or. (istep .le. initial_limit_nsteps)

      limiter: if (ilimit) then
         
         ! Compute the cell averages. The cell averages play an
         ! important role in ensuring positivity of the schemes.
         call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, &
              mesh%w, mesh%reference_area, data%u, data%uavg)

         ! limit the solution and flux point values. Note that the
         ! limiting process is typically determined by checking the
         ! values in a neighboring region of the element
         ! (STj/SVj). This is computed within the limiter and mpi
         ! exchange is called to share the value.
         call limit(mesh, data, time, linked_list, mpi_data, initial_limiting)

         ! Exchange ghost solution and flux point values which may
         ! have been limited
         if (mesh%numPartitions .gt. 1) then
            mpi_t1 = mpi_wtime()
            call exchange_data(mesh%Np, mesh%Nvar, mesh, data%u, USOL_TAG, &
                 mpi_data%sol_send, mpi_data%sol_recv, myRank, numProcs, MPI_DOUBLE)

            call exchange_data(mesh%Nflux, mesh%Nvar, mesh, data%uflux, UFLX_TAG, &
                 mpi_data%flux_send, mpi_data%flux_recv, myRank, numProcs, MPI_DOUBLE)
            mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
         end if

         ! ensure we're working with physically meaningful values
         if (debug) then
            call checkbounds(mesh, data)
         end if

         ! for periodic problems, copy right-boundary interface data for
         ! local elements.
         if (df_is_periodic) then
            call copy_locally_mapped_elements(mesh, data)

            if (mesh%numPartitions .gt. 1) then
               mpi_t1 = mpi_wtime()
               call exchange_periodic_data(mesh%Nflux, mesh%P1, mesh%Nvar, mesh, &
                    data%uflux, data%bface_data%uflux_rght, UFLX_TAG, &
                    mpi_data%periodic_sendbuf, mpi_data%periodic_recvbuf, &
                    myRank, numProcs, MPI_DOUBLE)
               mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
            end if
         end if

      end if limiter

#ifdef CUDA
      ! write solution vector to the GPU and take the gradient
      !$ t1 = omp_get_wtime()
      call gpu_ugrad(mesh%DIM, mesh%device_data, data%u)
      !$ t2 = omp_get_wtime()
      gpu_ugrad_write_time = t2-t1
#endif

      ! Ucommon
      !$ t1 = omp_get_wtime()
      call get_common_solution_values(mesh, data%uflux, data%ucommon)
      !$ t3 = omp_get_wtime()

      call get_boundary_common_solution_values(mesh, data, time, ldg_beta)
      call get_userbc_common_solution_values(  mesh, data, time)

#ifdef BOXLIB
      call get_BoxLib_interface_common_solution_values(mesh, data)
#endif

      !$ t2 = omp_get_wtime()
      get_ucommon_time      = t2 - t1
      boundary_ucommon_time = t2 - t3

      ! Exchange ghost common solution values
      if (mesh%numPartitions .gt. 1) then
         mpi_t1 = mpi_wtime()
         call exchange_data(mesh%Nflux, mesh%Nvar, mesh, data%ucommon, UCOMMON_TAG, &
                            mpi_data%flux_send, mpi_data%flux_recv, myRank, numProcs, MPI_DOUBLE)
         mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
      end if

#ifdef CUDA
      !$ t1 = omp_get_wtime()
      call read_host_ugrad(mesh%DIM, mesh%device_data, data%host_ugrad, data%ux, data%uy, data%uz)
      !$ t2 = omp_get_wtime()
      gpu_ugrad_read_time =  t2-t1
#endif

      ! Auxillary variables (gradients)
      !$ t1 = omp_get_wtime()
      call compute_gradients(mesh, data%u, data%uflux, data%ucommon, &
           data%ux, data%uy, data%uz, data%ux_flux, data%uy_flux, data%uz_flux)
      !$ t2 = omp_get_wtime()
      get_gradients_time = t2 - t1

      ! for periodic problems, copy right-boundary interface data for
      ! local elements. This copies the updated flux point gradients
      ! which are used for the viscous fluxes.
      if (df_is_periodic) then
         call copy_locally_mapped_elements(mesh, data)

         if (mesh%numPartitions .gt. 1) then
            mpi_t1 = mpi_wtime()
            call exchange_periodic_flux_point_gradients(mesh%Nflux, mesh%P1, mesh%Nvar, mesh, &
                 data%ux_flux, data%uy_flux, data%uz_flux, &
                 data%bface_data%ux_rght, data%bface_data%uy_rght, data%bface_data%uz_rght, UXYZ_TAG, &
                 mpi_data%periodic_sendbuf3, mpi_data%periodic_recvbuf3, &
                 myRank, numProcs, MPI_DOUBLE)
            mpi_time = mpi_time + (mpi_wtime() - mpi_t1)
         end if
      end if

      ! Shock sensor and artificial viscosity
      ishock_sensor = (shock_sensor_type .gt. 0) .or. (use_shock_sensor_as_marker)
      if (ishock_sensor) then
         if (shock_sensor_type .eq. MORO_NGUYEN_PERAIRE_SENSOR) then
            call Moro_Nguyen_Peraire_divergence_sensor(mesh, data)
         end if
      end if

      ! Discontinuous flux at solution points
      !$ t1 = omp_get_wtime()
      call get_discontinuous_flux_at_solution_points(system, mesh, data)
      !$ t2 = omp_get_wtime()
      discontinuous_flux_sol_time = t2 - t1

#ifdef CUDA
      ! Write the discontinuous flux and take the gradient
      !$ t1 = omp_get_wtime()
      call gpu_Fgrad(mesh%DIM, mesh%device_data, data%Fdiscont)
      !$ t2 = omp_get_wtime()
      gpu_Fgrad_write_time = t2 - t1
#endif

      ! Interaction flux
      !$ t1 = omp_get_wtime()
      call get_interaction_flux(system, mesh, data)
      !$ t3 = omp_get_wtime()

      call get_boundary_interaction_flux(mesh, data, time, system%mu, system%Cp, system%Cv, system%Pr, system%Rs)
      call get_userbc_interaction_flux(  mesh, data, time, system%mu, system%Cp, system%Cv, system%Pr, system%Rs)
      
      !$ t2 = omp_get_wtime()
      get_interaction_flux_time = t2 - t1
      boundary_interaction_time = t2 - t3

#ifdef CUDA
      !$ t1 = omp_get_wtime()
      call read_host_Fdoutput(mesh%DIM, mesh%device_data, data%host_Fdoutput)
                           
      !$ t2 = omp_get_wtime()
      gpu_Fgrad_read_time = t2 - t1
#endif      
      
      ! Flux divergence
      !$ t1 = omp_get_wtime()
      call compute_flux_divergence(mesh, data, data%Fdiscont, data%Fi, data%Gi, data%Hi, mesh%detJn, data%k1)           
      !$ t2 = omp_get_wtime()
      flux_divergence_time = t2 - t1

      !$ t_end = omp_get_wtime()
      rhs_evaluation_time = t_end - t_start
    
    end subroutine evaluate_rhs

  subroutine get_discontinuous_flux_at_solution_points(system, mesh, data)
    type(NavierStokesSystem), intent(inout) :: system
    type(mesh2d),             intent(inout) :: mesh
    type(FieldData_t),        intent(inout) :: data

    ! locals
    integer(c_int) :: elem_index, var
    real(c_double) :: gamma, mu, Rs, Cv

    real(c_double), allocatable :: rho(:), Mx(:), My(:), Mz(:), E(:)
    real(c_double), allocatable :: P(:), cs(:)

    real(c_double), allocatable :: rho_x(:), Mx_x(:), My_x(:), Mz_x(:), E_x(:)
    real(c_double), allocatable :: rho_y(:), Mx_y(:), My_y(:), Mz_y(:), E_y(:)
    real(c_double), allocatable :: rho_z(:), Mx_z(:), My_z(:), Mz_z(:), E_z(:)

    real(c_double), allocatable :: velx(:), vely(:), velz(:)
    
    real(c_double), allocatable :: velx_x(:), velx_y(:), velx_z(:)
    real(c_double), allocatable :: vely_x(:), vely_y(:), vely_z(:)
    real(c_double), allocatable :: velz_x(:), velz_y(:), velz_z(:)
    
    real(c_double), allocatable :: T_x(:), T_y(:), T_z(:)
    
    real(c_double), allocatable :: xr(:), xs(:), xt(:)
    real(c_double), allocatable :: yr(:), ys(:), yt(:)
    real(c_double), allocatable :: zr(:), zs(:), zt(:)
    
    real(c_double) :: Fd(mesh%Np, mesh%Nvar), Fv(mesh%Np, mesh%Nvar)
    real(c_double) :: Gd(mesh%Np, mesh%Nvar), Gv(mesh%Np, mesh%Nvar)
    real(c_double) :: Hd(mesh%Np, mesh%Nvar), Hv(mesh%Np, mesh%Nvar)
    
    !mesh  = system%mesh
    gamma = system%gamma
    mu    = system%mu
    Rs    = system%Rs
    Cv    = system%Cv

    allocate( rho(mesh%Np), Mx(mesh%Np), My(mesh%Np), Mz(mesh%Np), E(mesh%Np) )
    
    allocate( P(mesh%Np), cs(mesh%Np) )
    allocate( rho_x(mesh%Np), Mx_x(mesh%Np), My_x(mesh%Np), Mz_x(mesh%Np), E_x(mesh%Np) )
    allocate( rho_y(mesh%Np), Mx_y(mesh%Np), My_y(mesh%Np), Mz_y(mesh%Np), E_y(mesh%Np) )
    allocate( rho_z(mesh%Np), Mx_z(mesh%Np), My_z(mesh%Np), Mz_z(mesh%Np), E_z(mesh%Np) )
    
    allocate( velx(mesh%Np), vely(mesh%Np), velz(mesh%Np) )
    
    allocate( velx_x(mesh%Np), velx_y(mesh%Np), velx_z(mesh%Np) )
    allocate( vely_x(mesh%Np), vely_y(mesh%Np), vely_z(mesh%Np) )
    allocate( velz_x(mesh%Np), velz_y(mesh%Np), velz_z(mesh%Np) )
    
    allocate( T_x(mesh%Np), T_y(mesh%Np), T_z(mesh%Np) )
    
    allocate(xr(mesh%Np), xs(mesh%Np), xt(mesh%Np))
    allocate(yr(mesh%Np), ys(mesh%Np), yt(mesh%Np))
    allocate(zr(mesh%Np), zs(mesh%Np), zt(mesh%Np))
    
    Hd = ZERO
    Hv = ZERO
    
    !$omp parallel do private(var, elem_index, &
    !$omp                     rho, Mx, My, Mz, E, P, &
    !$omp                     rho_x, Mx_x, My_x, Mz_x, E_x, &
    !$omp                     rho_y, Mx_y, My_y, Mz_y, E_y, &
    !$omp                     rho_z, Mx_z, My_z, Mz_z, E_z, &
    !$omp                     velx, vely, velz, &
    !$omp                     velx_x, velx_y, velx_z, &
    !$omp                     vely_x, vely_y, vely_z, &
    !$omp                     velz_x, velz_y, velz_z, &
    !$omp                     T_x, T_y, T_z, &
    !$omp                     xr, xs, xt, yr, ys, yt, zr, zs, zt, &
    !$omp                     Fd, Gd, Hd, Fv, Gv, Hv) & 
    !$omp schedule(dynamic,2)
    do elem_index = 1, mesh%nelements
       rho = data%u(:, 1, elem_index)
       Mx  = data%u(:, 2, elem_index)
       My  = data%u(:, 3, elem_index)
       E   = data%u(:, 4, elem_index)
       Mz  = ZERO

#ifdef DIM3
       Mz = data%u(:, 4, elem_index)
       E  = data%u(:, 5, elem_index)
#endif

       ! Primitive variables
       velx = Mx/rho; vely = My/rho; velz = Mz/rho
       P  = (gamma - ONE)*(E - HALF*rho*(velx**2 + vely**2 + velz**2))

       rho_x = data%ux(:, 1, elem_index)
       rho_y = data%uy(:, 1, elem_index)
       rho_z = data%uz(:, 1, elem_index)

       Mx_x  = data%ux(:, 2, elem_index)
       Mx_y  = data%uy(:, 2, elem_index)
       Mx_z  = data%uz(:, 2, elem_index)

       My_x  = data%ux(:, 3, elem_index)
       My_y  = data%uy(:, 3, elem_index)
       My_z  = data%uz(:, 3, elem_index)

#ifdef DIM3
       Mz_x = data%ux(:, 4, elem_index)
       Mz_y = data%uy(:, 4, elem_index)
       Mz_z = data%uz(:, 4, elem_index)
         
       E_x = data%ux(:, 5, elem_index)
       E_y = data%uy(:, 5, elem_index)
       E_z = data%uz(:, 5, elem_index)
#else
       rho_z = ZERO
       Mx_z  = ZERO; My_z = ZERO
       Mz_x  = ZERO; Mz_y = ZERO; Mz_z = ZERO
       E_z   = ZERO
       
       E_x   = data%ux(:, 4, elem_index)
       E_y   = data%uy(:, 4, elem_index)
#endif

       ! velocity derivatives
       velx_x = ONE/rho*(Mx_x - velx*rho_x)
       velx_y = ONE/rho*(Mx_y - velx*rho_y)
       velx_z = ZERO

       vely_x = ONE/rho*(My_x - vely*rho_x)
       vely_y = ONE/rho*(My_y - vely*rho_y)
       vely_z = ZERO

       velz_x = ZERO; velz_y = ZERO; velz_z = ZERO

#ifdef DIM3
       velx_z = ONE/rho*(Mx_z - velx*rho_z)
       vely_z = ONE/rho*(My_z - vely*rho_z)

       velz_x = ONE/rho*(Mz_x - velz*rho_x)
       velz_y = ONE/rho*(Mz_y - velz*rho_y)
       velz_z = ONE/rho*(Mz_z - velz*rho_z)
#endif

       T_x = ONE/(rho*Cv)*(E_x - E/rho*rho_x - rho*(velx*velx_x + vely*vely_x + velz*velz_x))
       T_y = ONE/(rho*Cv)*(E_y - E/rho*rho_y - rho*(velx*velx_y + vely*vely_y + velz*velz_y))
       T_z = ONE/(rho*Cv)*(E_z - E/rho*rho_z - rho*(velx*velx_z + vely*vely_z + velz*velz_z))

       ! store the divergence and vorticity for this element. FIXME:
       ! For the 3D case the vorticity needs to be defined as a
       ! tensor
       !mesh%div( :, elem_index) = velx_x + vely_y
       !mesh%vort(:, elem_index) = vely_x - velx_y

       call evaluate_flux(mesh%Np, mesh%Nvar, &
            rho, Mx, My, Mz, E, &
            rho_x, Mx_x, My_x, Mz_x, E_x, &
            rho_y, Mx_y, My_y, Mz_y, E_y, &
            rho_z, Mx_z, My_z, Mz_z, E_z, &
            velx, vely, velz, &
            velx_x, velx_y, velx_z, &
            vely_x, vely_y, vely_z, &
            velz_x, velz_y, velz_z, &
            T_x, T_y, T_z, &
            Fd, Gd, Hd, &
            Fv, Gv, Hv, &
            gamma, mu, data%avisc(elem_index), system%Cp, system%Pr)

       ! element transformation factors
       xr = mesh%xr(:, elem_index); xs = mesh%xs(:, elem_index); xt = mesh%xt(:, elem_index)
       yr = mesh%yr(:, elem_index); ys = mesh%ys(:, elem_index); yt = mesh%yt(:, elem_index)
       zr = mesh%zr(:, elem_index); zs = mesh%zs(:, elem_index); zt = mesh%zt(:, elem_index)
       
       ! store the TRANSFORMED discontinuous flux at the solution points
       do var = 1, mesh%Nvar
          ! mesh%Fd(:,var,elem_index) = &
          !      mesh%ys(:, elem_index)*(Fd_solution(:, var) + Fv_solution(:, var)) - &
          !      mesh%xs(:, elem_index)*(Gd_solution(:, var) + Gv_solution(:, var))
          
          ! mesh%Gd(:,var,elem_index) = &
          !      -mesh%yr(:, elem_index)*(Fd_solution(:, var) + Fv_solution(:, var)) + &
          !       mesh%xr(:, elem_index)*(Gd_solution(:, var) + Gv_solution(:, var))
          
          
          ! mesh%Fdiscont(:, (elem_index-1)*2*mesh%Nvar+var) = &
          !      mesh%ys(:, elem_index)*(Fd(:, var) + Fv(:, var)) - &
          !      mesh%xs(:, elem_index)*(Gd(:, var) + Gv(:, var))
          
          ! mesh%Fdiscont(:, (elem_index-1)*2*mesh%Nvar+mesh%Nvar+var) = &
          !      -mesh%yr(:, elem_index)*(Fd(:, var) + Fv(:, var)) + &
          !       mesh%xr(:, elem_index)*(Gd(:, var) + Gv(:, var))
          
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar+var) = &
               (ys*zt - yt*zs)*(Fd(:, var) + Fv(:, var)) + &
               (xt*zs - xs*zt)*(Gd(:, var) + Gv(:, var)) + &
               (xs*yt - xt*ys)*(Hd(:, var) + Hv(:, var))
          
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar + mesh%Nvar+var) = &
               (yt*zr - yr*zt)*(Fd(:, var) + Fv(:, var)) + &
               (xr*zt - xt*zr)*(Gd(:, var) + Gv(:, var)) + &
               (xt*yr - xr*yt)*(Hd(:, var) + Hv(:, var))
          
#ifdef DIM3
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar + 2*mesh%Nvar+var) = &
               (yr*zs - ys*zr)*(Fd(:, var) + Fv(:, var)) + &
               (xs*zr - xr*zs)*(Gd(:, var) + Gv(:, var)) + &
               (xr*ys - xs*yr)*(Hd(:, var) + Hv(:, var))
#endif
          
       end do

    end do
    !$omp end parallel do
         
    deallocate( rho, Mx, My, Mz, E )
    deallocate( P, cs )
    
    deallocate( rho_x, Mx_x, My_x, Mz_x, E_x )
    deallocate( rho_y, Mx_y, My_y, Mz_y, E_y )
    deallocate( rho_z, Mx_z, My_z, Mz_z, E_z )
    
    deallocate( velx, vely, velz )
    
    deallocate( velx_x, velx_y, velx_z )
    deallocate( vely_x, vely_y, vely_z )
    deallocate( velz_x, velz_y, velz_z )
    
    deallocate( T_x, T_y, T_z )
    
    deallocate(xr, xs, xt)
    deallocate(yr, ys, yt)
    deallocate(zr, zs, zt)
    
  end subroutine get_discontinuous_flux_at_solution_points

  subroutine get_interaction_flux(system, mesh, data)
      use input_module, only: ldg_tau, ldg_beta, avisc, vdiff

      type(NavierStokesSystem), intent(inout) :: system
      type(mesh2d),             intent(inout) :: mesh
      type(FieldData_t),        intent(inout) :: data
      
      ! locals
      integer(c_int) :: j, var, elem_num_faces
      integer(c_int) :: face2elem(2), left, rght
      integer(c_int) :: tmp_index, glb_face_index, left_face_index, rght_face_index
      integer(c_int) :: left_indices(2), rght_indices(2)
      
      real(c_double) :: uflux_left(mesh%P1, mesh%Nvar)
      real(c_double) :: uflux_rght(mesh%P1, mesh%Nvar)

      !real(c_double) :: tmpi_left(mesh%P1, mesh%Nvar)
      !real(c_double) :: tmpi_rght(mesh%P1, mesh%Nvar)

      integer(c_int) :: sfi_left(mesh%P1)
      integer(c_int) :: sfi_rght(mesh%P1)

      real(c_double) :: Fd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Fd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Fv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Fv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Gd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Gd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Hd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Hd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Fi(mesh%P1, mesh%Nvar)
      real(c_double) :: Gi(mesh%P1, mesh%Nvar)
      real(c_double) :: Hi(mesh%P1, mesh%Nvar)

      real(c_double) :: Fv(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv(mesh%P1, mesh%Nvar)

      real(c_double) :: ilambda, ibeta_viscous, itau

      real(c_double) :: mu, Cp, Cv, Pr, Rs, gamma
      real(c_double) :: nx, ny, nz

      real(c_double) :: viscous_prefactor

      !mesh = system%mesh
      elem_num_faces = mesh%elem_num_faces

      mu = system%mu
      Cp = system%Cp
      Cv = system%Cv
      Pr = system%Pr
      Rs = system%Rs
      gamma = system%gamma

      ilambda = HALF
      ibeta_viscous = ZERO!ldg_beta
      itau = ldg_tau

      left_face_index = -1
      rght_face_index = -1
      
      ! if either of avisc of vdiff is non-zero, the viscous fluxes
      ! will be added to the fluxes
      viscous_prefactor = max(avisc, vdiff)

      !$omp parallel do private(j, var, tmp_index, glb_face_index, face2elem, left, rght, &
      !$omp                     left_face_index, rght_face_index, left_indices, rght_indices, &
      !$omp                     uflux_left, uflux_rght, &
      !$omp                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
      !$omp                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
      !$omp                     Hd_left, Hd_rght, Hv_left, Hv_rght, &
      !$omp                     sfi_left, sfi_rght, &
      !$omp                     Fi, Gi, Hi, Fv, Gv, Hv, nx, ny, nz) &
      !$omp schedule(static)
      do tmp_index = 1, mesh%Ninterior_faces
         glb_face_index = mesh%interior_faces(tmp_index)
         face2elem      = mesh%face2elem(glb_face_index, :); 

         left = face2elem(1)
         rght = face2elem(2)

         nx = mesh%normals(glb_face_index, 1)
         ny = mesh%normals(glb_face_index, 2)
         nz = ZERO

#ifdef DIM3
         nz = mesh%normals(glb_face_index, 3)
#endif         

         ! get the face indices for each element abutting this face
         left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
         rght_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 2)

         left_indices(1) = mesh%P1*(left_face_index-1)+1
         left_indices(2) = mesh%P1*left_face_index
            
         rght_indices(1) = mesh%P1*(rght_face_index-1)+1
         rght_indices(2) = mesh%P1*rght_face_index

         ! get the sorted face indices
         sfi_left = mesh%sfi(:, left_face_index, left)
         sfi_rght = mesh%sfi(:, rght_face_index, rght)

         ! inviscid flux to the left
         call euler_inviscid_flux(&
              mesh%P1, mesh%Nvar, gamma, &
              data%uflux(left_indices(1):left_indices(2), :, left), &
              left_indices(1), sfi_left, &
              Fd_left, Gd_left, Hd_left)

         ! viscous flux to the left
         call ns_viscous_flux(&
              mesh%P1, mesh%Nvar, gamma, mu, data%avisc(left), Cp, Cv, Pr, &
              data%uflux(  left_indices(1):left_indices(2), :, left), &
              data%ux_flux(left_indices(1):left_indices(2), :, left), &
              data%uy_flux(left_indices(1):left_indices(2), :, left), &
              data%uz_flux(left_indices(1):left_indices(2), :, left), &
              left_indices(1), sfi_left, &
              Fv_left, Gv_left, Hv_left)

         ! right-inviscid flux
         call euler_inviscid_flux(&
              mesh%P1, mesh%Nvar, gamma, &
              data%uflux(rght_indices(1):rght_indices(2), :, rght), &
              rght_indices(1), sfi_rght, &
              Fd_rght, Gd_rght, Hd_rght)

         ! right-viscous flux
         call ns_viscous_flux(&
              mesh%P1, mesh%Nvar, gamma, mu, data%avisc(rght), Cp, Cv, Pr, &
              data%uflux(  rght_indices(1):rght_indices(2), :, rght), &
              data%ux_flux(rght_indices(1):rght_indices(2), :, rght), &
              data%uy_flux(rght_indices(1):rght_indices(2), :, rght), &
              data%uz_flux(rght_indices(1):rght_indices(2), :, rght), &
              rght_indices(1), sfi_rght, &
              Fv_rght, Gv_rght, Hv_rght)

         ! get the left and right values
          do var = 1, mesh%Nvar
             do j = 1, mesh%P1
                uflux_left(j, var) = data%uflux(left_indices(1) + (sfi_left(j)-1), var, left)
                uflux_rght(j, var) = data%uflux(rght_indices(1) + (sfi_rght(j)-1), var, rght)
             end do
          end do
          
         ! Evaluate the interaction flux for this face
         call evaluate_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
              mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
              uflux_left, uflux_rght, &
              Fd_left, Fd_rght, Fv_left, Fv_rght, &
              Gd_left, Gd_rght, Gv_left, Gv_rght, &
              Hd_left, Hd_rght, Hv_left, Hv_rght, &
              Fi, Gi, Hi, &
              Fv, Gv, Hv, &
              system%gamma, viscous_prefactor, &
              mesh%face_markers(glb_face_index), &
              ilambda, ibeta_viscous, itau, &
              glb_face_index, ONE, ONE)

         do var = 1, mesh%Nvar
            do j = 1, mesh%P1
               ! old-style indexing
               ! mesh%Fi(left_indices(1) + (j-1), :, left) = Fi(j, :) + Fv(j, :) 

               data%Fi(left_indices(1) + (sfi_left(j)-1), var, left) = Fi(j, var) + Fv(j, var)
               data%Fi(rght_indices(1) + (sfi_rght(j)-1), var, rght) = Fi(j, var) + Fv(j, var) 

               data%Gi(left_indices(1) + (sfi_left(j)-1), var, left) = Gi(j, var) + Gv(j, var) 
               data%Gi(rght_indices(1) + (sfi_rght(j)-1), var, rght) = Gi(j, var) + Gv(j, var) 

               data%Hi(left_indices(1) + (sfi_left(j)-1), var, left) = Hi(j, var) + Hv(j, var) 
               data%Hi(rght_indices(1) + (sfi_rght(j)-1), var, rght) = Hi(j, var) + Hv(j, var) 
            end do
         end do

      end do
      !$omp end parallel do

    end subroutine get_interaction_flux   

    subroutine get_maxvel(mesh, data, gamma)
      type(mesh2d),      intent(inout) :: mesh
      real(c_double),    intent(in   ) :: gamma
      type(FieldData_t), intent(in   ) :: data

      ! locals
      integer(c_int) :: elem_index
      real(c_double) :: v2(mesh%Np), rho(mesh%Np), P(mesh%Np)
      real(c_double) :: elem_smax, smax

      ! initialize smax
      smax = -ONE

      !$omp parallel do private(elem_index, rho, v2, P, elem_smax) reduction(max:smax)
      do elem_index = 1, mesh%Nelements
         rho  = data%u(:, 1, elem_index)
         v2   = (data%u(:, 2, elem_index)/rho)**2 + (data%u(:, 3, elem_index)/rho)**2

#ifdef DIM3
         v2 = v2 + (data%u(:, 4, elem_index)/rho)**2
         P = (gamma-ONE)*(data%u(:, 5, elem_index) - HALF*rho*v2)
#else
         P = (gamma-ONE)*(data%u(:, 4, elem_index) - HALF*rho*v2)
#endif

         elem_smax = maxval( sqrt(v2) + sqrt(gamma*P/rho) )
         smax      = max(smax, elem_smax)
      end do
      !$omp end parallel do

      ! set the maximum velocity for the mesh
      mesh%maxvel = smax
      
    end subroutine get_maxvel

    subroutine primitive_variable_min_max(mesh, data, Wmin, Wmax)
      type(mesh2d),      intent(in ) :: mesh
      type(FieldData_t), intent(in)  :: data
      real(c_double), intent(out) :: Wmin(mesh%Nvar)
      real(c_double), intent(out) :: Wmax(mesh%Nvar)

      ! locals
      integer(c_int) :: index

      Wmin = +LARGE
      Wmax = -LARGE

      !$omp parallel do private(index) reduction(max:Wmax) reduction(min:Wmin)
      do index = 1, mesh%Nelements_local

            Wmin(1) = min(Wmin(1), minval(data%u(:, 1, index)))
            Wmax(1) = max(Wmax(1), maxval(data%u(:, 1, index)))

            Wmin(2) = min(Wmin(2), minval(data%u(:, 2, index)/data%u(:, 1, index)))
            Wmax(2) = max(Wmax(2), maxval(data%u(:, 2, index)/data%u(:, 1, index)))

            Wmin(3) = min(Wmin(3), minval(data%u(:, 3, index)/data%u(:, 1, index)))
            Wmax(3) = max(Wmax(3), maxval(data%u(:, 3, index)/data%u(:, 1, index)))

#ifdef DIM3 
            Wmin(4) = min(Wmin(4), minval(data%u(:, 4, index)/data%u(:, 1, index)))
            Wmax(4) = max(Wmax(4), maxval(data%u(:, 4, index)/data%u(:, 1, index)))
#endif
      end do
      !$omp end parallel do
            
    end subroutine primitive_variable_min_max

  subroutine checkbounds(mesh, data)
    type(mesh2d),      intent(in)  :: mesh
    type(FieldData_t), intent(in) :: data

    ! locals
    integer(c_int) :: elem_index
    integer(c_int) :: rho_comp, E_comp

    rho_comp = 1; E_comp   = 4
    if (mesh%DIM .eq. 3) then
       E_comp = 5
    end if
    
    !$omp parallel do private(elem_index)
    do elem_index = 1, mesh%Nelements
       if(mesh%elem_type(elem_index) .eq. 1) then
          if ( any(data%u(:, rho_comp, elem_index) .lt. EPSILON) .or. &
               any(data%u(:,   E_comp, elem_index) .lt. EPSILON) ) then
             print*, 'Non physical values encountered!', elem_index
             stop
          end if
       end if
    end do
    !$omp end parallel do
  end subroutine checkbounds

!   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   ! Deprecated (will not work)
!   subroutine get_discontinuous_flux_at_flux_points(system, mesh, data)
!       type(NavierStokesSystem), intent(inout) :: system
!       type(mesh2d),             intent(inout) :: mesh
!       type(FieldData_t),        intent(inout) :: data

!       ! locals
!       integer(c_int) :: Nflux, elem_index
!       real(c_double) :: gamma, mu, Rs, Cv

!       real(c_double), allocatable :: rho(:), Mx(:), My(:), Mz(:), E(:)

!       real(c_double), allocatable :: rho_x(:), Mx_x(:), My_x(:), Mz_x(:), E_x(:)
!       real(c_double), allocatable :: rho_y(:), Mx_y(:), My_y(:), Mz_y(:), E_y(:)
!       real(c_double), allocatable :: rho_z(:), Mx_z(:), My_z(:), Mz_z(:), E_z(:)

!       real(c_double), allocatable :: velx(:), vely(:), velz(:)
!       real(c_double), allocatable :: velx_x(:), velx_y(:), velx_z(:)
!       real(c_double), allocatable :: vely_x(:), vely_y(:), vely_z(:)
!       real(c_double), allocatable :: velz_x(:), velz_y(:), velz_z(:)
!       real(c_double), allocatable :: T_x(:), T_y(:), T_z(:)

!       real(c_double) :: Fd(mesh%Nflux, mesh%Nvar)
!       real(c_double) :: Fv(mesh%Nflux, mesh%Nvar)

!       real(c_double) :: Gd(mesh%Nflux, mesh%Nvar)
!       real(c_double) :: Gv(mesh%Nflux, mesh%Nvar)

!       real(c_double) :: Hd(mesh%Nflux, mesh%Nvar)
!       real(c_double) :: Hv(mesh%Nflux, mesh%Nvar)

!       !mesh  = system%mesh
!       gamma = system%gamma
!       mu    = system%mu
!       Rs    = system%Rs
!       Cv    = system%Cv

!       Nflux = mesh%Nflux

!       allocate( rho(Nflux), Mx(Nflux), My(Nflux), Mz(Nflux), E(Nflux) )

!       allocate( rho_x(Nflux), Mx_x(Nflux), My_x(Nflux), Mz_x(Nflux), E_x(Nflux) )
!       allocate( rho_y(Nflux), Mx_y(Nflux), My_y(Nflux), Mz_y(Nflux), E_y(Nflux) )
!       allocate( rho_z(Nflux), Mx_z(Nflux), My_z(Nflux), Mz_z(Nflux), E_z(Nflux) )

!       allocate( velx(Nflux), vely(Nflux), velz(Nflux) )

!       allocate( velx_x(Nflux), vely_x(Nflux), velz_x(Nflux) )
!       allocate( velx_y(Nflux), vely_y(Nflux), velz_y(Nflux) )
!       allocate( velx_z(Nflux), vely_z(Nflux), velz_z(Nflux) )

!       allocate( T_x(Nflux), T_y(Nflux), T_z(Nflux) )

!       Hd = ZERO
!       Hv = ZERO

!       !$omp parallel do private(elem_index, &
!       !$omp                     rho, Mx, My, Mz, E, &
!       !$omp                     rho_x, Mx_x, My_x, Mz_x, E_x, &
!       !$omp                     rho_y, Mx_y, My_y, Mz_y, E_y, &
!       !$omp                     rho_z, Mx_z, My_z, Mz_z, E_z, &
!       !$omp                     velx, vely, velz, &
!       !$omp                     velx_x, velx_y, velx_z, &
!       !$omp                     vely_x, vely_y, vely_z, &
!       !$omp                     velz_x, velz_y, velz_z, &
!       !$omp                     T_x, T_y, T_z, &
!       !$omp                     Fd, Gd, Hd, Fv, Gv, Hv) &
!       !$omp schedule(dynamic,2)
!       do elem_index = 1, mesh%nelements

!          rho = data%uflux(:, 1, elem_index)
!          Mx  = data%uflux(:, 2, elem_index)
!          My  = data%uflux(:, 3, elem_index)
!          E   = data%uflux(:, 4, elem_index)
!          Mz  = ZERO

! #ifdef DIM3
!          Mz = data%uflux(:, 4, elem_index)
!          E  = data%uflux(:, 5, elem_index)
! #endif

!          rho_x = data%ux_flux(:, 1, elem_index)
!          rho_y = data%uy_flux(:, 1, elem_index)
!          rho_z = data%uz_flux(:, 1, elem_index)

!          Mx_x  = data%ux_flux(:, 2, elem_index)
!          Mx_y  = data%uy_flux(:, 2, elem_index)
!          Mx_z  = data%uz_flux(:, 2, elem_index)

!          My_x  = data%ux_flux(:, 3, elem_index)
!          My_y  = data%uy_flux(:, 3, elem_index)
!          My_z  = data%uz_flux(:, 3, elem_index)

! #ifdef DIM3
!          Mz_x  = data%ux_flux(:, 4, elem_index)
!          Mz_y  = data%uy_flux(:, 4, elem_index)
!          Mz_z  = data%uz_flux(:, 4, elem_index)

!          E_x   = data%ux_flux(:, 5, elem_index)
!          E_y   = data%uy_flux(:, 5, elem_index)
!          E_z   = data%uz_flux(:, 5, elem_index)
! #else
!          rho_z = ZERO
!          Mx_z  = ZERO; My_z = ZERO
!          Mz_x  = ZERO; Mz_y = ZERO; Mz_z = ZERO

!          E_x   = data%ux_flux(:, 4, elem_index)
!          E_y   = data%uy_flux(:, 4, elem_index)
!          E_z   = ZERO
! #endif         

!          ! velocity derivatives
!          velx = Mx/rho; vely = My/rho; velz = Mz/rho

!          velx_x = ONE/rho*(Mx_x - velx*rho_x)
!          velx_y = ONE/rho*(Mx_y - velx*rho_y)         
!          velx_z = ZERO

!          vely_x = ONE/rho*(My_x - vely*rho_x)
!          vely_y = ONE/rho*(My_y - vely*rho_y)
!          vely_z = ZERO

!          velz_x = ZERO; velz_y = ZERO; velz_z = ZERO

! #ifdef DIM3
!          velx_z = ONE/rho*(Mx_z - velx*rho_z)
!          vely_z = ONE/rho*(My_z - vely*rho_z)

!          velz_x = ONE/rho*(Mz_x - velz*rho_x)
!          velz_y = ONE/rho*(Mz_y - velz*rho_y)
!          velz_z = ONE/rho*(Mz_z - velz*rho_z)
! #endif

!          T_x = ONE/(rho*Cv)*(E_x - E/rho*rho_x - rho*(velx*velx_x + vely*vely_x + velz*velz_x))
!          T_y = ONE/(rho*Cv)*(E_y - E/rho*rho_y - rho*(velx*velx_y + vely*vely_y + velz*velz_y))
!          T_z = ONE/(rho*Cv)*(E_z - E/rho*rho_z - rho*(velx*velx_z + vely*vely_z + velz*velz_z))

!          call evaluate_flux(Nflux, mesh%Nvar, &
!               rho, Mx, My, Mz, E, &
!               rho_x, Mx_x, My_x, Mz_x, E_x, &
!               rho_y, Mx_y, My_y, Mz_y, E_y, &
!               rho_z, Mx_z, My_z, Mz_z, E_z, &
!               velx, vely, velz, &
!               velx_x, velx_y, velx_z, &
!               vely_x, vely_y, vely_z, &
!               velz_x, velz_y, velz_z, &
!               T_x, T_y, T_z, &
!               Fd, Gd, Hd, &
!               Fv, Gv, Hv, &
!               gamma, mu, data%avisc(elem_index), system%Cp, system%Pr)

!          ! store the PHYSICAL flux at the flux points
!          data%Fd_flux(:, :, elem_index) = Fd
!          data%Gd_flux(:, :, elem_index) = Gd
!          data%Hd_flux(:, :, elem_index) = Hd

!          data%Fv_flux(:, :, elem_index) = Fv
!          data%Gv_flux(:, :, elem_index) = Gv
!          data%Hv_flux(:, :, elem_index) = Hv

!       end do
!       !$omp end parallel do

!       deallocate( rho, Mx, My, Mz, E )
!       deallocate( rho_x, Mx_x, My_x, Mz_x, E_x )
!       deallocate( rho_y, Mx_y, My_y, Mz_y, E_y )
!       deallocate( rho_z, Mx_z, My_z, Mz_z, E_z )

!       deallocate( velx, vely, velz )
!       deallocate( velx_x, velx_y, velx_z )
!       deallocate( vely_x, vely_y, vely_z )
!       deallocate( velz_x, velz_y, velz_z )
!       deallocate( T_x, T_y, T_z )

!     end subroutine get_discontinuous_flux_at_flux_points

end module system_module
