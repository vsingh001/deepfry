module userstats_module
  use input_module
  use deepfry_constants_module
  use iso_c_binding, only: c_double, c_int
  
  use mesh_module
  use fieldvar_module

  implicit none

contains

  subroutine get_user_stats(time, mesh, data, myRank, numProcs)
    integer(c_int), intent(in) :: myRank, numProcs
    real(c_double), intent(in) :: time
    type(mesh2d),   intent(in) :: mesh
    type(FieldData_t), intent(in) :: data

  end subroutine get_user_stats

end module userstats_module
