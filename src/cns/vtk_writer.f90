module vtk_writer_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use input_module
  use vtk_headers_module
  use mesh_module
  use fieldvar_module
  implicit none

contains

  subroutine interpolate_to_plot_points(Np, Npoints, Nvar, Nelements, Lf, u, uinterp)
    use fr_module, only: interpolate_to_flux_points

    integer(c_int), intent(in   ) :: Np, Npoints, Nvar, Nelements
    real(c_double), intent(in   ) :: u(Np, Nvar, Nelements)
    real(c_double), intent(in   ) :: Lf(Npoints, Np)
    real(c_double), intent(inout) :: uinterp(Npoints, Nvar, Nelements)

    call interpolate_to_flux_points(Np, Npoints, Nvar, Nelements, Lf, u, uinterp)

  end subroutine interpolate_to_plot_points

  subroutine write_vtk_file(filename, mesh, data, vtk_header, gamma, step, time)
    use polynomials_module, only: rs2xy

    type(mesh2d),            intent(inout)  :: mesh
    type(FieldData_t),       intent(inout)  :: data
    type(vtk_mesh_header_t), intent(in)     :: vtk_header
    character(len=256),      intent(in)     :: filename
    real(c_double),          intent(in)     :: time
    real(c_double),          intent(in)     :: gamma
    integer(c_int),          intent(in)     :: step

    integer :: io
    integer(c_int) :: elem_index, j

    real(c_double), allocatable :: xp(:), yp(:), zp(:)
    real(c_double), allocatable :: xv(:), yv(:), zv(:)

    real(c_double), allocatable :: rho(:), rhoinv(:)
    real(c_double), allocatable :: velx(:), vely(:), velz(:), E(:), P(:)

    real(c_double), allocatable :: rho_x(:), rho_y(:), rho_z(:)

    real(c_double), allocatable :: velx_x(:), velx_y(:), velx_z(:)
    real(c_double), allocatable :: vely_x(:), vely_y(:), vely_z(:)
    real(c_double), allocatable :: velz_x(:), velz_y(:), velz_z(:)

    real(c_double), allocatable :: s12(:), s13(:), s23(:)
    real(c_double), allocatable :: a12(:), a13(:), a23(:)
    
    real(c_double), allocatable :: omega(:), s2(:), wx(:), wy(:), wz(:)
    real(c_double), allocatable :: qc(:), l2(:)

    allocate(xp(mesh%vtk_header%PlotNumberOfPoints))
    allocate(yp(mesh%vtk_header%PlotNumberOfPoints))
    allocate(zp(mesh%vtk_header%PlotNumberOfPoints))

    allocate(xv(mesh%vtk_header%PlotNumberOfPoints))
    allocate(yv(mesh%vtk_header%PlotNumberOfPoints))
    allocate(zv(mesh%vtk_header%PlotNumberOfPoints))

    allocate(rho(mesh%vtk_header%PlotNumberOfPoints))
    allocate(rhoinv(mesh%vtk_header%PlotNumberOfPoints))

    allocate(velx(mesh%vtk_header%PlotNumberOfPoints))
    allocate(vely(mesh%vtk_header%PlotNumberOfPoints))
    allocate(velz(mesh%vtk_header%PlotNumberOfPoints))

    allocate(E(mesh%vtk_header%PlotNumberOfPoints))
    allocate(P(mesh%vtk_header%PlotNumberOfPoints))

    allocate(rho_x(mesh%vtk_header%PlotNumberOfPoints))
    allocate(rho_y(mesh%vtk_header%PlotNumberOfPoints))
    allocate(rho_z(mesh%vtk_header%PlotNumberOfPoints))

    allocate(velx_x(mesh%vtk_header%PlotNumberOfPoints))
    allocate(velx_y(mesh%vtk_header%PlotNumberOfPoints))
    allocate(velx_z(mesh%vtk_header%PlotNumberOfPoints))

    allocate(vely_x(mesh%vtk_header%PlotNumberOfPoints))
    allocate(vely_y(mesh%vtk_header%PlotNumberOfPoints))
    allocate(vely_z(mesh%vtk_header%PlotNumberOfPoints))

    allocate(velz_x(mesh%vtk_header%PlotNumberOfPoints))
    allocate(velz_y(mesh%vtk_header%PlotNumberOfPoints))
    allocate(velz_z(mesh%vtk_header%PlotNumberOfPoints))

    allocate(s12(mesh%vtk_header%PlotNumberOfPoints))
    allocate(s13(mesh%vtk_header%PlotNumberOfPoints))
    allocate(s23(mesh%vtk_header%PlotNumberOfPoints))

    allocate(a12(mesh%vtk_header%PlotNumberOfPoints))
    allocate(a13(mesh%vtk_header%PlotNumberOfPoints))
    allocate(a23(mesh%vtk_header%PlotNumberOfPoints))

    allocate(omega(mesh%vtk_header%PlotNumberOfPoints))
    allocate(s2(   mesh%vtk_header%PlotNumberOfPoints))

    allocate(qc(mesh%vtk_header%PlotNumberOfPoints))
    allocate(l2(mesh%vtk_header%PlotNumberOfPoints))

    allocate(wx(mesh%vtk_header%PlotNumberOfPoints))
    allocate(wy(mesh%vtk_header%PlotNumberOfPoints))
    allocate(wz(mesh%vtk_header%PlotNumberOfPoints))

    io = 111

    ! interpolate to the plot points
    call interpolate_to_plot_points(&
         mesh%Np, vtk_header%PlotNumberOfPoints, mesh%Nvar, mesh%Nelements_local, &
         mesh%Lplot, data%u, data%u_plot)

    call interpolate_to_plot_points(&
         mesh%Np, vtk_header%PlotNumberOfPoints, mesh%Nvar, mesh%Nelements_local, &
         mesh%Lplot, data%ux, data%ux_plot)

    call interpolate_to_plot_points(&
         mesh%Np, vtk_header%PlotNumberOfPoints, mesh%Nvar, mesh%Nelements_local, &
         mesh%Lplot, data%uy, data%uy_plot)

#ifdef DIM3
    call interpolate_to_plot_points(&
         mesh%Np, vtk_header%PlotNumberOfPoints, mesh%Nvar, mesh%Nelements_local, &
         mesh%Lplot, data%uz, data%uz_plot)
#endif

    !open(io,file=trim(plot_filename),status='replace',action='write')
    open(io,file=filename,status='replace',action='write')

    write(io,'(A)') '<?xml version="1.0"?>'
    write(io,*) '<VTKFile type="UnstructuredGrid" version="0.1" &
         &byte_order="LittleEndian" header_type="UInt32" &
         &compressor="vtkZLibDataCompressor">'

    write(io,*) '<UnstructuredGrid>'
    
    do elem_index = 1, mesh%Nelements_local       

       ! get the physical location of the plot points
       call get_element_vertices(mesh, elem_index, xv, yv, zv)
       call rs2xy(mesh%vtk_header%r_interp, mesh%vtk_header%s_interp, mesh%vtk_header%t_interp, &
            mesh%rv, mesh%sv, mesh%tv, xv, yv, zv, xp, yp, zp)
       
       ! auxillary data
       rho    = data%u_plot(:, 1, elem_index)
       rhoinv = ONE/rho

#ifdef DIM3
       velx = rhoinv*data%u_plot(:, 2, elem_index)
       vely = rhoinv*data%u_plot(:, 3, elem_index)
       velz = rhoinv*data%u_plot(:, 4, elem_index)
       E    =        data%u_plot(:, 5, elem_index)

       rho_x = data%ux_plot(:, 1, elem_index)
       rho_y = data%uy_plot(:, 1, elem_index)
       rho_z = data%uz_plot(:, 1, elem_index)

       velx_x = rhoinv*(data%ux_plot(:, 2, elem_index) - velx*rho_x)
       velx_y = rhoinv*(data%uy_plot(:, 2, elem_index) - velx*rho_y)
       velx_z = rhoinv*(data%uz_plot(:, 2, elem_index) - velx*rho_z)

       vely_x = rhoinv*(data%ux_plot(:, 3, elem_index) - vely*rho_x)
       vely_y = rhoinv*(data%uy_plot(:, 3, elem_index) - vely*rho_y)
       vely_z = rhoinv*(data%uz_plot(:, 3, elem_index) - vely*rho_z)

       velz_x = rhoinv*(data%ux_plot(:, 4, elem_index) - velz*rho_x)
       velz_y = rhoinv*(data%uy_plot(:, 4, elem_index) - velz*rho_y)
       velz_z = rhoinv*(data%uz_plot(:, 4, elem_index) - velz*rho_z)
#else
       velx = rhoinv*data%u_plot(:, 2, elem_index)
       vely = rhoinv*data%u_plot(:, 3, elem_index)
       velz = ZERO
       E    = data%u_plot(:, 4, elem_index)

       rho_x = data%ux_plot(:, 1, elem_index)
       rho_y = data%uy_plot(:, 1, elem_index)
       rho_z = ZERO

       velx_x = rhoinv*(data%ux_plot(:, 2, elem_index) - velx*rho_x)
       velx_y = rhoinv*(data%uy_plot(:, 2, elem_index) - velx*rho_y)
       velx_z = ZERO

       vely_x = rhoinv*(data%ux_plot(:, 3, elem_index) - vely*rho_x)
       vely_y = rhoinv*(data%uy_plot(:, 3, elem_index) - vely*rho_y)
       vely_z = ZERO

       velz_x = ZERO
       velz_y = ZERO
       velz_z = ZERO

#endif

       P = (gamma - ONE)*(E - HALF*rho*(velx*velx + vely*vely + velz*velz))

       s12 = HALF * (velx_y + vely_x)
       s13 = HALF * (velx_z + velz_x)
       s23 = HALF * (vely_z + velz_y)

       a12 = HALF * (velx_y - vely_x)
       a13 = HALF * (velx_z - velz_x)
       a23 = HALF * (vely_z - velz_y)

       omega = TWO * (a12*a12 + a13*a13 + a23*a23)
       s2    = TWO * (s12*s12 + s13*s13 + s23*s23)

       s2 = s2 + (velx_x*velx_x + vely_y*vely_y + velz_z*velz_z)

       qc = max(ZERO, HALF*(omega-s2))
       wx = velz_y - vely_z
       wy = velx_z - velz_x
       wz = vely_x - velx_y

       write(io,*)'<Piece NumberOfPoints="', vtk_header%PlotNumberOfPoints,'" NumberOfCells="', &
            vtk_header%PlotNumberOfCells,'">'

       write(io,*) '<PointData>'

       ! write out the individial variables
       write(io,*) '<DataArray type="Float32" Name="rho" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) data%u_plot(j, 1, elem_index)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="velx" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) velx(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="vely" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) vely(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="velz" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) velz(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="P" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) P(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="omegax" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io, *) wx(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="omegay" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io,*) wy(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="omegaz" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io,*) wz(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Float32" Name="qcriterion" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io,*) qc(j)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '<DataArray type="Int32" Name="rank" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io,*) mesh%Proc(elem_index)
       end do
       !write(io,*)
       write(io,*) '</DataArray>'

       write(io,*) '</PointData>'
       
       write(io,*) '<Points>'
       write(io,*) '<DataArray type="Float32" NumberOfComponents="3" format="ascii">'
       do j = 1, mesh%vtk_header%PlotNumberOfPoints
          write(io,*) xp(j), yp(j), zp(j)
       end do
       write(io,*) '</DataArray>'
       write(io,*) '</Points>'

       ! cell-connectivity
       write(io,*) '<Cells>'
       write(io,*) '<DataArray type="Int32" Name="connectivity" format="ascii">'
       write(io,*) trim(mesh%vtk_header%PlotConnectivity)
       write(io,*) '</DataArray>'
       write(io,*) '<DataArray type="Int32" Name="offsets" format="ascii">'
       write(io,*) trim(mesh%vtk_header%PlotOffsets)
       write(io,*) '</DataArray>'
       write(io,*) '<DataArray type="Int32" Name="types" format="ascii">'
       write(io,*) trim(mesh%vtk_header%PlotEtype)
       write(io,*) '</DataArray>'
       write(io,*) '</Cells>'
       write(io,*) '</Piece>'
    end do
    
    write(io,*) '</UnstructuredGrid>'
    write(io,*) '</VTKFile>'

    ! close the file
    close(io)

    deallocate(xp, yp, zp)
    deallocate(xv, yv, zv)

    deallocate(rho, rhoinv)
    deallocate(rho_x, rho_y, rho_z)
    
    deallocate(velx, vely, velz)
    deallocate(E, P)

    deallocate(velx_x, velx_y, velx_z)
    deallocate(vely_x, vely_y, vely_z)
    deallocate(velz_x, velz_y, velz_z)

    deallocate(s12, s13, s23)
    deallocate(a12, a13, a23)

    deallocate(omega, s2)
    deallocate(wx, wy, wz)

    deallocate(qc, l2)

  end subroutine write_vtk_file

end module vtk_writer_module
