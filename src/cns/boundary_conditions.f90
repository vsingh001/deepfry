module df_bc_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use flux_module
  use fieldvar_module

  implicit none

contains

  subroutine allocate_bface_data(mesh, data)
    type(mesh2d),       intent(in) :: mesh
    type(FieldData_t),  intent(inout) :: data

    allocate(data%bface_data%local_map_available(mesh%Nbfaces))
    allocate(data%bface_data%local_periodic_elem(mesh%Nbfaces))
    allocate(data%bface_data%local_periodic_face(mesh%Nbfaces))

    allocate(data%bface_data%sfi_rght(  mesh%P1, mesh%Nbfaces))
    allocate(data%bface_data%uflux_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))

    allocate(data%bface_data%ux_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%uy_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%uz_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))

    allocate(data%bface_data%Fd_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%Gd_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%Hd_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))

    allocate(data%bface_data%Fv_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%Gv_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))
    allocate(data%bface_data%Hv_rght(mesh%P1, mesh%Nvar, mesh%Nbfaces))

  end subroutine allocate_bface_data

  subroutine deallocate_bface_data(data)
    type(FieldData_t),  intent(inout) :: data

    deallocate(data%bface_data%local_map_available)
    deallocate(data%bface_data%local_periodic_elem)
    deallocate(data%bface_data%local_periodic_face)

    deallocate(data%bface_data%sfi_rght)
    deallocate(data%bface_data%uflux_rght)

    deallocate(data%bface_data%ux_rght)
    deallocate(data%bface_data%uy_rght)
    deallocate(data%bface_data%uz_rght)

    deallocate(data%bface_data%Fd_rght)
    deallocate(data%bface_data%Gd_rght)
    deallocate(data%bface_data%Hd_rght)

    deallocate(data%bface_data%Fv_rght)
    deallocate(data%bface_data%Gv_rght)
    deallocate(data%bface_data%Hv_rght)
    
  end subroutine deallocate_bface_data

  subroutine copy_locally_mapped_elements(mesh, data)
    use input_module
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int) :: tmp_index, fm, face_index
    integer(c_int) :: j, var
    integer(c_int) :: mapped_elem, mapped_face
    integer(c_int) :: sfi(mesh%P1), data_indices(mesh%P1)

    real(c_double) :: uflux_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: ux_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: uy_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: uz_tmp(mesh%P1, mesh%Nvar)

    real(c_double) :: Fd_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: Gd_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: Hd_tmp(mesh%P1, mesh%Nvar)

    real(c_double) :: Fv_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: Gv_tmp(mesh%P1, mesh%Nvar)
    real(c_double) :: Hv_tmp(mesh%P1, mesh%Nvar)

    !$omp parallel do private(tmp_index, face_index, &
    !$omp                     fm, j, var, mapped_elem, mapped_face, &
    !$omp                     data_indices, sfi, &
    !$omp                     uflux_tmp, ux_tmp, uy_tmp, uz_tmp) &
    !$omp schedule (dynamic,2)
    do tmp_index = 1, mesh%Nbfaces
       face_index = mesh%bfaces(tmp_index)
       fm         = mesh%face_markers(face_index)
       
       periodic_face: if (fm .eq. periodic_lox_marker .or. fm .eq. periodic_hix_marker .or. &
                          fm .eq. periodic_loy_marker .or. fm .eq. periodic_hiy_marker .or. &
                          fm .eq. periodic_loz_marker .or. fm .eq. periodic_hiz_marker) then
         
          local_mapping: if (data%bface_data%local_map_available(tmp_index)) then
             mapped_elem = data%bface_data%local_periodic_elem(tmp_index)
             mapped_face = data%bface_data%local_periodic_face(tmp_index)

             mesh%bc_map( mesh%face2elem(face_index, 1) ) = mapped_elem

             data_indices(1) = mesh%P1*(mapped_face-1) + 1
             data_indices(2) = mesh%P1*(mapped_face)

             ! get the data along the mapped face 
             do var = 1, mesh%Nvar
                uflux_tmp(:, var) = data%uflux(  data_indices(1):data_indices(2), var, mapped_elem)

                ux_tmp(:, var) = data%ux_flux(data_indices(1):data_indices(2), var, mapped_elem)
                uy_tmp(:, var) = data%uy_flux(data_indices(1):data_indices(2), var, mapped_elem)
                uz_tmp(:, var) = data%uz_flux(data_indices(1):data_indices(2), var, mapped_elem)
                
             end do
          
             ! re-arrange the data as per the sorted face indices
             sfi = data%bface_data%sfi_rght(:, tmp_index)
             do var = 1, mesh%Nvar
                do j = 1, mesh%P1
                   data%bface_data%uflux_rght(j, var, tmp_index) = uflux_tmp(sfi(j), var)

                   data%bface_data%ux_rght(j, var, tmp_index) = ux_tmp(sfi(j), var)
                   data%bface_data%uy_rght(j, var, tmp_index) = uy_tmp(sfi(j), var)
                   data%bface_data%uz_rght(j, var, tmp_index) = uz_tmp(sfi(j), var)

                end do
             end do

          end if local_mapping
       end if periodic_face
    end do
    !$omp end parallel do

  end subroutine copy_locally_mapped_elements

  subroutine get_boundary_common_solution_values(mesh, data, time, beta)
    use input_module, only: inlet_marker, outlet_marker, wall_marker, zerogradient_marker, &
                            reflective_wall_marker, supersonic_outflow_marker, &
                            rho_inlet, velx_inlet, vely_inlet, velz_inlet, p_inlet, gamma, &
                            periodic_lox_marker, periodic_hix_marker, & 
                            periodic_loy_marker, periodic_hiy_marker, &
                            periodic_loz_marker, periodic_hiz_marker, &
                            unsteady_inlet, velx_ampl, velx_freq, velx_angle, ldg_beta

    real(c_double),    intent(in)    :: time
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data
    real(c_double), optional,       intent(in   ) :: beta

    ! locals
    integer(c_int) :: j, var, tmp_index, glb_face_index, left_face_index, left
    integer(c_int) :: left_indices(2)
    real(c_double) :: rhofluid, Vxfluid, Vyfluid, Vzfluid, Efluid, Pfluid, c_fluid
    real(c_double) :: rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost
    real(c_double) :: Rl, Rr, ul, ur, Vb, cb
    real(c_double) :: c_inlet, s, nx, ny, nz

    integer(c_int) :: sfi_left(mesh%P1)

    real(c_double) :: uflux_avg(mesh%P1, mesh%Nvar)
    real(c_double), allocatable :: uflux_left(:,:), uflux_rght(:,:)
    real(c_double), allocatable :: ucommon(:, :)

    real(c_double) :: vin, inlet_velx, inlet_vely, inlet_velz

    real(c_double) :: ibeta
    ibeta = ldg_beta; if (present(beta)) ibeta = beta

    c_inlet = sqrt(gamma*p_inlet/rho_inlet)
    s = p_inlet/rho_inlet**gamma

    left_face_index = -1

    allocate(uflux_left(mesh%P1, mesh%Nvar), uflux_rght(mesh%P1, mesh%Nvar))
    allocate(ucommon(mesh%P1, mesh%Nvar))

    inlet_velx = velx_inlet
    inlet_vely = vely_inlet
    inlet_velz = velz_inlet

    if (unsteady_inlet) then
       vin = velx_inlet*(ONE + velx_ampl*sin(TWO*M_PI*velx_freq*time))

       inlet_velx = vin * cos( velx_angle * M_PI/180_c_double )
       inlet_vely = vin * sin( velx_angle * M_PI/180_c_double )
       inlet_velz = ZERO
       
    end if

    !$omp parallel do private(j, var, tmp_index, glb_face_index, left, &
    !$omp                     left_face_index, left_indices, ucommon, &
    !$omp                     sfi_left, uflux_left, uflux_rght, uflux_avg, &
    !$omp                     rhofluid, Vxfluid, Vyfluid, Vzfluid, Efluid, Pfluid, c_fluid, &
    !$omp                     rhoghost, Vxghost, Vyghost, Vzghost, Eghost, Pghost, &
    !$omp                     Rl, Rr, ul, ur, Vb, cb, nx, ny, nz) &
    !$omp schedule (dynamic,2)
    do tmp_index = 1, mesh%Nbfaces
       glb_face_index    = mesh%bfaces(tmp_index)
       left              = mesh%face2elem(glb_face_index, 1)
       
       left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
       left_indices(1) = mesh%P1*(left_face_index-1)+1
       left_indices(2) = mesh%P1*left_face_index

       nx = mesh%normals(glb_face_index,1)
       ny = mesh%normals(glb_face_index,2)
       nz = ZERO

#ifdef DIM3
       nz = mesh%normals(glb_face_index,3)
#endif

       uflux_left(:, :) = data%uflux(left_indices(1):left_indices(2), :, left)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Periodic faces !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       if (mesh%face_markers(glb_face_index) .eq. periodic_lox_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_loy_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_loz_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hix_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hiy_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hiz_marker) then
          
          ! right interpolated values. Note that this is already
          ! sorted according to the sorted face indices in the copy
          ! stage
          uflux_rght(:, :) = data%bface_data%uflux_rght(:, :, tmp_index)

          ! match up the data using the sorted face indices
          sfi_left = mesh%sfi(:, left_face_index, left)
          do j = 1, mesh%P1
             do var = 1, mesh%Nvar
                uflux_left(j, var) = data%uflux(left_indices(1) + (sfi_left(j)-1), var, left)
             end do
          end do

          uflux_avg = HALF * (uflux_left + uflux_rght)
          do j = 1, mesh%P1
             do var = 1, mesh%Nvar
                data%ucommon(left_indices(1) + (sfi_left(j)-1), var, left) = uflux_avg(j, var) - &
                     ibeta * (uflux_left(j, var) - uflux_rght(j, var))
             end do
          end do
          
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Zerograd !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       else if (mesh%face_markers(glb_face_index) .eq. zerogradient_marker .or. &
                mesh%face_markers(glb_face_index) .eq. supersonic_outflow_marker) then

          ! the ghost-state has the same values as the fluid element
          do var = 1, mesh%Nvar
             do j = 1, mesh%P1
                data%ucommon(left_indices(1) + (j-1), var, left) = &
                     data%uflux(left_indices(1) + (j-1), var, left)
             end do
          end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Reflective walls !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       else if (mesh%face_markers(glb_face_index) .eq. reflective_wall_marker) then
          do j = 1, mesh%P1
             rhofluid = uflux_left(j, 1)
             Vxfluid  = uflux_left(j, 2)/rhofluid
             Vyfluid  = uflux_left(j, 3)/rhofluid
             Vzfluid  = ZERO
             Efluid   = uflux_left(j, 4)

#ifdef DIM3
             Vzfluid = uflux_left(j, 4)/rhofluid
             Efluid  = uflux_left(j, 5)
#endif
             rhoghost = rhofluid
             Vxghost  = Vxfluid - TWO*nx*(Vxfluid*nx + Vyfluid*ny + Vzfluid*nz)
             Vyghost  = Vyfluid - TWO*ny*(Vxfluid*nx + Vyfluid*ny + Vzfluid*nz)
             Vzghost  = Vzfluid - TWO*nz*(Vxfluid*nx + Vyfluid*ny + Vzfluid*nz)
             Eghost   = Efluid

             data%ucommon(left_indices(1) + (j-1), 1, left) = rhoghost
             data%ucommon(left_indices(1) + (j-1), 2, left) = rhoghost*Vxghost
             data%ucommon(left_indices(1) + (j-1), 3, left) = rhoghost*Vyghost
             data%ucommon(left_indices(1) + (j-1), 4, left) = Eghost             

#ifdef DIM3
             data%ucommon(left_indices(1) + (j-1), 4, left) = rhoghost*Vzghost
             data%ucommon(left_indices(1) + (j-1), 5, left) = Eghost 
#endif
          end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! No-slip wall !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       else if (mesh%face_markers(glb_face_index) .eq. wall_marker) then
          do j = 1, mesh%P1
             rhofluid = data%uflux(left_indices(1) + (j-1), 1, left)
             Vxfluid  = data%uflux(left_indices(1) + (j-1), 2, left)/rhofluid
             Vyfluid  = data%uflux(left_indices(1) + (j-1), 3, left)/rhofluid
             Vzfluid  = ZERO
             Efluid   = data%uflux(left_indices(1) + (j-1), 4, left)

#ifdef DIM3
             Vzfluid  = data%uflux(left_indices(1) + (j-1), 4, left)/rhofluid
             Efluid   = data%uflux(left_indices(1) + (j-1), 5, left)
#endif
             
             Pfluid = (gamma-ONE) * (Efluid - &
                  HALF*rhofluid*(Vxfluid*Vxfluid + Vyfluid*Vyfluid + Vzfluid*Vzfluid))

             rhoghost = rhofluid
             Vxghost  = ZERO
             Vyghost  = ZERO
             Vzghost  = ZERO
             Pghost   = Pfluid
             
             Eghost   = Pghost/(gamma-ONE) + &
                  HALF*rhoghost*(Vxghost*Vxghost + Vyghost*Vyghost + Vzghost*Vzghost)
             
             !Eghost   = Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2)
             
             data%ucommon(    left_indices(1) + (j-1), 1, left) = rhofluid
             data%ucommon(    left_indices(1) + (j-1), 2, left) = HALF*rhofluid*(Vxfluid + Vxghost)
             data%ucommon(    left_indices(1) + (j-1), 3, left) = HALF*rhofluid*(Vyfluid + Vyghost)
             data%ucommon(    left_indices(1) + (j-1), 4, left) = HALF*(Efluid + Eghost)
#ifdef DIM3
             data%ucommon(    left_indices(1) + (j-1), 4, left) = HALF*rhofluid*(Vzfluid + Vzghost)
             data%ucommon(    left_indices(1) + (j-1), 5, left) = HALF*(Efluid + Eghost)
#endif
          end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Inlets/Outlets !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
       else  if (mesh%face_markers(glb_face_index) .eq. inlet_marker .or. &
                 mesh%face_markers(glb_face_index) .eq. outlet_marker) then

          do j = 1, mesh%P1
             rhofluid = uflux_left(j, 1)
             Vxfluid  = uflux_left(j, 2)/rhofluid
             Vyfluid  = uflux_left(j, 3)/rhofluid
             Vzfluid  = ZERO
             Efluid   = uflux_left(j, 4)

#ifdef DIM3
             Vzfluid = uflux_left(j, 4)/rhofluid
             Efluid  = uflux_left(j, 5)
#endif
             Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2))
             c_fluid = sqrt(gamma*Pfluid/rhofluid)

             ul = Vxfluid*nx + Vyfluid*ny + Vzfluid*nz
             !ur = velx_inlet*nx + vely_inlet*ny + velz_inlet*nz
             ur = inlet_velx*nx + inlet_vely*ny + inlet_velz*nz
                  
             if ( abs(ur) .ge. c_inlet .and. ul .ge. ZERO) then
                Rr = ul - TWO*c_fluid/(gamma-ONE)
             else
                Rr = ur - TWO*c_inlet/(gamma-ONE)
             end if
             
             if (abs(ur) .ge. c_inlet .and. ul .lt. ZERO) then
                Rl = ur + TWO*c_inlet/(gamma-ONE)
             else
                Rl = ul + TWO*c_fluid/(gamma-ONE)
             end if
             
             Vb = HALF*(Rl + Rr)
             cb = FOURTH*(gamma-ONE)*(Rl - Rr)

             if (ul .lt. ZERO) then
                rhoghost = (ONE/(gamma*s)*cb**2)**(ONE/(gamma-ONE))
             else
                rhoghost = rhofluid* (rhofluid*cb**2/(gamma*Pfluid))**(ONE/(gamma-ONE))
             end if
             
             Pghost = ONE/gamma*rhoghost*cb**2
             
             if (ul .ge. ZERO) then
                Vxghost = Vxfluid + (Vb - ul)*nx
                Vyghost = Vyfluid + (Vb - ul)*ny
                Vzghost = Vzfluid + (Vb - ul)*nz
             else
                ! Vxghost = velx_inlet + (Vb - ur)*nx
                ! Vyghost = vely_inlet + (Vb - ur)*ny
                ! Vzghost = velz_inlet + (Vb - ur)*nz
                Vxghost = inlet_velx + (Vb - ur)*nx
                Vyghost = inlet_vely + (Vb - ur)*ny
                Vzghost = inlet_velz + (Vb - ur)*nz
             end if
             
             Eghost = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2 + Vzghost**2)

             ! set the common solution values
             ucommon(j, 1) = rhoghost
             ucommon(j, 2) = rhoghost*Vxghost
             ucommon(j, 3) = rhoghost*Vyghost
             ucommon(j, 4) = Eghost

#ifdef DIM3
             ucommon(j, 4) = rhoghost*Vzghost
             ucommon(j, 5) = Eghost
#endif
          end do
          data%ucommon(left_indices(1):left_indices(2),:,left) = ucommon(:,:)
       end if
    end do
    !$omp end parallel do

    deallocate(uflux_left, uflux_rght)
    deallocate(ucommon)

  end subroutine get_boundary_common_solution_values

  subroutine get_boundary_interaction_flux(mesh, data, time, mu, Cp, Cv, Pr, Rs)
    use input_module, only: inlet_marker, outlet_marker, wall_marker, zerogradient_marker, &
                            reflective_wall_marker, supersonic_outflow_marker, &
                            rho_inlet, velx_inlet, vely_inlet, velz_inlet, p_inlet, gamma, ldg_tau, ldg_beta, &
                            periodic_lox_marker, periodic_hix_marker, & 
                            periodic_loy_marker, periodic_hiy_marker, &
                            periodic_loz_marker, periodic_hiz_marker, &
                            unsteady_inlet, velx_ampl, velx_freq, velx_angle, &
                            vdiff, avisc

    real(c_double),    intent(in   ) :: time, mu, Cp, Cv, Pr, Rs
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data
    
    ! locals
    integer(c_int) :: j, var, left
    integer(c_int) :: tmp_index, glb_face_index, left_face_index
    integer(c_int) :: left_indices(2)
    
    real(c_double) :: c_inlet
    real(c_double) :: rhofluid, Vxfluid, Vyfluid, Vzfluid, Pfluid, Efluid
    real(c_double) :: rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost
    
    real(c_double) :: rho_x, rho_y, rho_z
    real(c_double) :: velx_x, velx_y, velx_z
    real(c_double) :: vely_x, vely_y, vely_z
    real(c_double) :: velz_x, velz_y, velz_z

    real(c_double) :: T_x, T_y, T_z, T_x_ghost, T_y_ghost, T_z_ghost
    real(c_double) :: E_x, E_y, E_z
    real(c_double) :: c_fluid

    real(c_double) :: div
    real(c_double) :: tauxx, tauxy, tauxz
    real(c_double) :: tauyy, tauyz
    real(c_double) :: tauzz    

    real(c_double) :: uflux_left(mesh%P1, mesh%Nvar)
    real(c_double) :: ux_left(mesh%P1, mesh%Nvar)
    real(c_double) :: uy_left(mesh%P1, mesh%Nvar)
    real(c_double) :: uz_left(mesh%P1, mesh%Nvar)

    real(c_double) :: uflux_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: ux_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: uy_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: uz_rght(mesh%P1, mesh%Nvar)

    real(c_double) :: Fd_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Fd_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: Fv_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Fv_rght(mesh%P1, mesh%Nvar)
    
    real(c_double) :: Gd_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Gd_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: Gv_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Gv_rght(mesh%P1, mesh%Nvar)
    
    real(c_double) :: Hd_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Hd_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: Hv_left(mesh%P1, mesh%Nvar)
    real(c_double) :: Hv_rght(mesh%P1, mesh%Nvar)
    
    real(c_double) :: Fi(mesh%P1, mesh%Nvar)
    real(c_double) :: Gi(mesh%P1, mesh%Nvar)
    real(c_double) :: Hi(mesh%P1, mesh%Nvar)
    
    real(c_double) :: Fv(mesh%P1, mesh%Nvar)
    real(c_double) :: Gv(mesh%P1, mesh%Nvar)
    real(c_double) :: Hv(mesh%P1, mesh%Nvar)

    integer(c_int) :: sfi_left(mesh%P1)
    integer(c_int) :: sfi_rght(mesh%P1)
    
    real(c_double) :: ilambda, ibeta_viscous, itau
    
    real(c_double) :: nx, ny, nz
    real(c_double) :: Rl, Rr, ul, ur, Vb, cb
    real(c_double) :: s

    real(c_double) :: vin, inlet_velx, inlet_vely, inlet_velz
    real(c_double) :: viscous_prefactor
    real(c_double) :: vleft_prefactor, vrght_prefactor

    logical :: evaluate_flux
    
    c_inlet = sqrt(gamma*p_inlet/rho_inlet)
    s = p_inlet/rho_inlet**gamma

    ! initialize H-fluxes to 0 for the 2d case
    Hd_left = ZERO; Hd_rght = ZERO
    Hv_left = ZERO; Hv_rght = ZERO

    ilambda = HALF
    ibeta_viscous = ldg_beta
    itau = ldg_tau

    left_face_index = -1
    
    evaluate_flux = .false.

    inlet_velx = velx_inlet
    inlet_vely = vely_inlet
    inlet_velz = velz_inlet

    if (unsteady_inlet) then
       vin = velx_inlet*(ONE + velx_ampl*sin(2*M_PI*velx_freq*time))

       inlet_velx = vin * cos( velx_angle * M_PI/180_c_double )
       inlet_vely = vin * sin( velx_angle * M_PI/180_c_double )
       inlet_velz = ZERO

    end if

    viscous_prefactor = max(avisc, vdiff)

    !$omp parallel do private(tmp_index, glb_face_index, j, var, nx, ny, nz, left, &
    !$omp                     left_face_index, left_indices, &
    !$omp                     rhofluid, Vxfluid, Vyfluid, Vzfluid, Pfluid, Efluid, c_fluid, &
    !$omp                     rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost, &
    !$omp                     rho_x, rho_y, rho_z, &
    !$omp                     velx_x, velx_y, velx_z, &
    !$omp                     vely_x, vely_y, vely_z, &
    !$omp                     velz_x, velz_y, velz_z, &
    !$omp                     E_x, E_y, E_z, &
    !$omp                     T_x, T_y, T_z, &
    !$omp                     T_x_ghost, T_y_ghost, T_z_ghost, div, &
    !$omp                     tauxx, tauxy, tauxz, tauyy, tauyz, tauzz, &
    !$omp                     sfi_left, sfi_rght, &
    !$omp                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
    !$omp                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
    !$omp                     Hd_left, Hd_rght, Hv_left, Hv_rght, &
    !$omp                     uflux_left, uflux_rght, &
    !$omp                     ux_left, uy_left, uz_left, &
    !$omp                     ux_rght, uy_rght, uz_rght, &
    !$omp                     Fi, Gi, Hi, &
    !$omp                     Fv, Gv, Hv, &
    !$omp                     Rl, Rr, ul, ur, Vb, cb, ilambda, ibeta_viscous, itau, &
    !$omp                     evaluate_flux, vleft_prefactor, vrght_prefactor) &
    !$omp schedule(dynamic,2)
    do tmp_index = 1, mesh%Nbfaces
       glb_face_index    = mesh%bfaces(tmp_index)
       left              = mesh%face2elem(glb_face_index, 1)

       evaluate_flux = .false.
             
       left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
       left_indices(1) = mesh%P1*(left_face_index-1)+1
       left_indices(2) = mesh%P1*left_face_index
       
       nx = mesh%normals(glb_face_index,1)
       ny = mesh%normals(glb_face_index,2)
       nz = ZERO

#ifdef DIM3
       nz = mesh%normals(glb_face_index,3)
#endif

       ! initialize sfi right
       do j = 1, mesh%P1 
          sfi_rght(j) = j
          sfi_left(j) = j
       end do
         
       ! get the left discontinuous flux and solution values
       do var = 1, mesh%Nvar
          uflux_left(:, var) = data%uflux(left_indices(1):left_indices(2), var, left)

          ux_left(:, var) = data%ux_flux(left_indices(1):left_indices(2), var, left)
          uy_left(:, var) = data%uy_flux(left_indices(1):left_indices(2), var, left)
          uz_left(:, var) = data%uz_flux(left_indices(1):left_indices(2), var, left)
       end do

       ! interior (left state inviscid flux)
       call euler_inviscid_flux(mesh%P1, mesh%Nvar, gamma, uflux_left, left_indices(1), sfi_left, &
                                Fd_left, Gd_left, Hd_left)

       ! default values
       uflux_rght = uflux_left
       ux_rght    = ux_left
       uy_rght    = uy_left
       uz_rght    = uz_left

       Fd_rght = Fd_left
       Gd_rght = Gd_left
       Hd_rght = Hd_left

       ! zeroed out viscous terms
       Fv_left = ZERO
       Fv_rght = ZERO
       
       Gv_left = ZERO
       Gv_rght = ZERO

       Hv_left = ZERO
       Hv_rght = ZERO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Periodic faces !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       if (mesh%face_markers(glb_face_index) .eq. periodic_lox_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_loy_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_loz_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hix_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hiy_marker .or. &
           mesh%face_markers(glb_face_index) .eq. periodic_hiz_marker) then
          evaluate_flux = .false.

          ilambda = HALF; ibeta_viscous = ZERO; itau = ldg_tau
          
          ! get the aligned data for the right element
          uflux_rght(:, :) = data%bface_data%uflux_rght(:, :, tmp_index)

          ux_rght(:, :) = data%bface_data%ux_rght(:, :, tmp_index)
          uy_rght(:, :) = data%bface_data%uy_rght(:, :, tmp_index)
          uz_rght(:, :) = data%bface_data%uz_rght(:, :, tmp_index)
          
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! interior fluxes (left)
          sfi_left = mesh%sfi(:, left_face_index, left)
          call euler_inviscid_flux(mesh%P1, mesh%Nvar, gamma, uflux_left, left_indices(1), sfi_left, &
                                   Fd_left, Gd_left, Hd_left)
          
          
          call ns_viscous_flux(mesh%P1, mesh%Nvar, gamma, mu, data%avisc(left), Cp, Cv, Pr, &
                               uflux_left, ux_left, uy_left, uz_left, left_indices(1), sfi_left, &
                               Fv_left, Gv_left, Hv_left)

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! exterior fluxes (right)
         call euler_inviscid_flux(mesh%P1, mesh%Nvar, gamma, uflux_rght, 1, sfi_rght, &
                                  Fd_rght, Gd_rght, Hd_rght)

         call ns_viscous_flux(mesh%P1, mesh%Nvar, gamma, mu, data%avisc(left), Cp, Cv, Pr, &
                              uflux_rght, ux_rght, uy_rght, uz_rght, 1, sfi_rght, &
                              Fv_rght, Gv_rght, Hv_rght)             

          ! ! get the aligned data for the left element
          do var = 1, mesh%Nvar
             do j = 1, mesh%P1
                uflux_left(j, var) = data%uflux(left_indices(1) + (sfi_left(j)-1), var, left)
             end do
          end do
          
          ! explicitly evaluate the interaction flux. We need to do
          ! this because all other "internal" boundary conditions
          ! don't use the sorted face indexing
          call evaluate_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
               mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
               uflux_left, uflux_rght, &
               Fd_left, Fd_rght, Fv_left, Fv_rght, &
               Gd_left, Gd_rght, Gv_left, Gv_rght, &
               Hd_left, Hd_rght, Hv_left, Hv_rght, &
               Fi, Gi, Hi, &
               Fv, Gv, Hv, &
               gamma, viscous_prefactor, mesh%face_markers(glb_face_index), &
               ilambda, ibeta_viscous, itau, &
               glb_face_index, ONE, ONE)

          ! store the physical interaction flux for the element
          do var = 1, mesh%Nvar
             do j = 1, mesh%P1
                data%Fi(left_indices(1) + (sfi_left(j)-1), var, left) = Fi(j, var) + Fv(j, var) 
                data%Gi(left_indices(1) + (sfi_left(j)-1), var, left) = Gi(j, var) + Gv(j, var) 
                data%Hi(left_indices(1) + (sfi_left(j)-1), var, left) = Hi(j, var) + Hv(j, var) 
             end do
          end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Zerograd BC !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!       
       else if (mesh%face_markers(glb_face_index) .eq. zerogradient_marker .or. &
                mesh%face_markers(glb_face_index) .eq. supersonic_outflow_marker) then
          ilambda = ZERO; ibeta_viscous = ZERO; itau = ZERO
          vleft_prefactor = ONE; vrght_prefactor = ONE
          evaluate_flux = .true.
          
          ! copy values to the ghost state
          uflux_rght = uflux_left
          
          ! Copy the inviscid fluxes for the ghost state
          Fd_rght = Fd_left
          Gd_rght = Gd_left
          Hd_rght = Hd_left
          
          ! set the viscous fluxes to 0
          Fv_left = ZERO; Fv_rght = ZERO
          Gv_left = ZERO; Gv_rght = ZERO
          Hv_left = ZERO; Hv_rght = ZERO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Reflective walls !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!          
       else if (mesh%face_markers(glb_face_index) .eq. reflective_wall_marker) then
          ilambda = HALF; ibeta_viscous = ZERO; itau = ZERO
          vleft_prefactor = ONE; vrght_prefactor = ONE
          evaluate_flux = .true.

          do j = 1, mesh%P1
             
             ! set the ghost states (Hesthaven & Warburton pg. 234)
             rhoghost =  uflux_left(j, 1)

#ifdef DIM3
             Vxghost = uflux_left(j, 2) - TWO*nx*(&
                  nx*uflux_left(j,2) + ny*uflux_left(j,3) + nz*uflux_left(j,4))

             Vyghost = uflux_left(j, 3) - TWO*ny*(&
                  nx*uflux_left(j,2) + ny*uflux_left(j,3) + nz*uflux_left(j,4))

             Vzghost = uflux_left(j, 4) - TWO*nz*(&
                  nx*uflux_left(j,2) + ny*uflux_left(j,3) + nz*uflux_left(j,4))

             Eghost   = uflux_left(j, 5)
             
#else
             Vxghost = uflux_left(j, 2) - TWO*nx*(&
                  nx*uflux_left(j,2) + ny*uflux_left(j,3))

             Vyghost = uflux_left(j, 3) - TWO*ny*(&
                  nx*uflux_left(j,2) + ny*uflux_left(j,3))

             Vzghost = ZERO
             Eghost   = uflux_left(j, 4)
#endif
             Vxghost = Vxghost/rhoghost
             Vyghost = Vyghost/rhoghost
             Vzghost = Vzghost/rhoghost

             Pghost   = (gamma-ONE)*(Eghost - HALF*rhoghost*(Vxghost**2 + &
                                                             Vyghost**2 + &
                                                             Vzghost**2))
             
             uflux_rght(j, 1) = rhoghost
             uflux_rght(j, 2) = rhoghost*Vxghost
             uflux_rght(j, 3) = rhoghost*Vyghost

#ifdef DIM3
             uflux_rght(j, 4) = rhoghost*Vzghost
             uflux_rght(j, 5) = Eghost
#else
             uflux_rght(j, 4) = Eghost
#endif
             
             ! set the ghost state fluxes
             Fd_rght(j, 1) = rhoghost*Vxghost
             Fd_rght(j, 2) = rhoghost*Vxghost*Vxghost + Pghost
             Fd_rght(j, 3) = rhoghost*Vxghost*Vyghost
             Fd_rght(j, 4) = Vxghost*(Eghost + Pghost)
             
             Gd_rght(j, 1) = rhoghost*Vyghost
             Gd_rght(j, 2) = rhoghost*Vxghost*Vyghost
             Gd_rght(j, 3) = rhoghost*Vyghost*Vyghost + Pghost
             Gd_rght(j, 4) = Vyghost*(Eghost + Pghost)
             
             Hd_rght(j, 1) = ZERO
             Hd_rght(j, 2) = ZERO
             Hd_rght(j, 3) = ZERO
             Hd_rght(j, 4) = ZERO

#ifdef DIM3
             Fd_rght(j, 4) = rhoghost*Vxghost*Vzghost
             Fd_rght(j, 5) = Vxghost*(Eghost + Pghost)
             
             Gd_rght(j, 4) = rhoghost*Vyghost*Vzghost
             Gd_rght(j, 5) = Vyghost*(Eghost + Pghost)
             
             Hd_rght(j, 1) = rhoghost*Vzghost
             Hd_rght(j, 2) = rhoghost*Vzghost*Vxghost
             Hd_rght(j, 3) = rhoghost*Vzghost*Vyghost
             Hd_rght(j, 4) = rhoghost*Vzghost*Vzghost + Pghost
             Hd_rght(j, 5) = Vzghost*(Eghost + Pghost)
#endif

          end do
          ! set the viscous fluxes to 0
          Fv_left = ZERO; Fv_rght = ZERO
          Gv_left = ZERO; Gv_rght = ZERO
          Hv_left = ZERO; Hv_rght = ZERO
          
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! INLETS/OUTLETS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       else if (mesh%face_markers(glb_face_index) .eq. outlet_marker .or. &
                mesh%face_markers(glb_face_index) .eq. inlet_marker) then
          ilambda = HALF; ibeta_viscous = ZERO; itau = ldg_tau
          vleft_prefactor = ONE; vrght_prefactor = ONE
          evaluate_flux = .true.

          do j = 1, mesh%P1
             rhofluid = data%uflux(left_indices(1) + (j-1), 1, left)
             Vxfluid  = data%uflux(left_indices(1) + (j-1), 2, left)/rhofluid
             Vyfluid  = data%uflux(left_indices(1) + (j-1), 3, left)/rhofluid
             Efluid   = data%uflux(left_indices(1) + (j-1), 4, left)
             Vzfluid  = ZERO

#ifdef DIM3
             Vzfluid  = data%uflux(left_indices(1) + (j-1), 4, left)/rhofluid
             Efluid   = data%uflux(left_indices(1) + (j-1), 5, left)
#endif
             
             Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2))
             c_fluid = sqrt(gamma*Pfluid/rhofluid)

             ul = Vxfluid*nx + Vyfluid*ny + Vzfluid*nz
             ur = inlet_velx*nx + inlet_vely*ny + inlet_velz*nz
             
             if ( abs(ur) .ge. c_inlet .and. ul .ge. ZERO) then
                Rr = ul - TWO*c_fluid/(gamma-ONE)
             else
                Rr = ur - TWO*c_inlet/(gamma-ONE)
             end if
             
             if (abs(ur) .ge. c_inlet .and. ul .lt. ZERO) then
                Rl = ur + TWO*c_inlet/(gamma-ONE)
             else
                Rl = ul + TWO*c_fluid/(gamma-ONE)
             end if
             
             Vb = HALF*(Rl + Rr)
             cb = FOURTH*(gamma-ONE)*(Rl - Rr)
             
             if (ul .lt. ZERO) then
                rhoghost = (ONE/(gamma*s)*cb**2)**(ONE/(gamma-ONE))
             else
                rhoghost = rhofluid* (rhofluid*cb**2/(gamma*Pfluid))**(ONE/(gamma-ONE))
             end if
             
             Pghost = ONE/gamma*rhoghost*cb**2
             
             if (ul .ge. ZERO) then
                Vxghost = Vxfluid + (Vb - ul)*nx
                Vyghost = Vyfluid + (Vb - ul)*ny
                Vzghost = Vzfluid + (Vb - ul)*nz
             else
                Vxghost = inlet_velx + (Vb - ur)*nx
                Vyghost = inlet_vely + (Vb - ur)*ny
                Vzghost = inlet_velz + (Vb - ur)*nz
             end if
           
             Eghost = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2 + Vzghost**2)
             
             ! set the right states for the flux
             uflux_rght(j, 1) = rhoghost
             uflux_rght(j, 2) = rhoghost*Vxghost
             uflux_rght(j, 3) = rhoghost*Vyghost             

#ifdef DIM3
             uflux_rght(j, 4) = rhoghost*Vzghost
             uflux_rght(j, 5) = Eghost
#else
             uflux_rght(j, 4) = Eghost
#endif
             
             ! F-inviscid fluxes for the ghost-state
             Fd_rght(j, 1) = rhoghost*Vxghost
             Fd_rght(j, 2) = rhoghost*Vxghost*Vxghost + Pghost
             Fd_rght(j, 3) = rhoghost*Vxghost*Vyghost             

#ifdef DIM3
             Fd_rght(j, 4) = rhoghost*Vxghost*Vzghost
             Fd_rght(j, 5) = Vxghost*(Eghost + Pghost)
#else
             Fd_rght(j, 4) = Vxghost*(Eghost + Pghost)
#endif
             
             ! G-inviscid fluxes for the ghost-state
             Gd_rght(j, 1) = rhoghost*Vyghost
             Gd_rght(j, 2) = rhoghost*Vyghost*Vxghost
             Gd_rght(j, 3) = rhoghost*Vyghost*Vyghost + Pghost
             
#ifdef DIM3
             Gd_rght(j, 4) = rhoghost*Vyghost*Vzghost
             Gd_rght(j, 5) = Vyghost*(Eghost + Pghost)
#else
             Gd_rght(j, 4) = Vyghost*(Eghost + Pghost)
#endif

#ifdef DIM3  
             ! H-inviscid fluxes for the ghost state
             Hd_rght(j, 1) = rhoghost*Vzghost
             Hd_rght(j, 2) = rhoghost*Vzghost*Vxghost
             Hd_rght(j, 3) = rhoghost*Vzghost*Vyghost
             Hd_rght(j, 4) = rhoghost*Vzghost*Vzghost + Pghost
             Hd_rght(j, 5) = Vzghost*(Eghost + Pghost)
#else
             Hd_rght(j,:) = ZERO
#endif
             
             ! viscous fluid states
             rho_x = data%ux_flux(left_indices(1) + (j-1), 1, left)
             rho_y = data%uy_flux(left_indices(1) + (j-1), 1, left)
             rho_z = ZERO
             
             E_x = data%ux_flux(left_indices(1) + (j-1), 4, left)
             E_y = data%uy_flux(left_indices(1) + (j-1), 4, left)
             E_z = ZERO
             
             velx_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_x)
             velx_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_y)
             velx_z = ZERO
             
             vely_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_x)
             vely_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_y)
             vely_z = ZERO

             velz_x = ZERO
             velz_y = ZERO
             velz_z = ZERO

#ifdef DIM3
             rho_z = data%uz_flux(left_indices(1) + (j-1), 1, left)

             E_x   = data%ux_flux(left_indices(1) + (j-1), 5, left)
             E_y   = data%uy_flux(left_indices(1) + (j-1), 5, left)
             E_z   = data%uz_flux(left_indices(1) + (j-1), 5, left)

             velx_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_z)
             vely_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_z)

             velz_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_x)
             velz_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_y)
             velz_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_z)
#endif

             ! temperature gradients
             T_x = ONE/(rhofluid*Cv)*(E_x - Efluid/rhofluid*rho_x - &
                                      rhofluid*(Vxfluid*velx_x + Vyfluid*vely_x + Vzfluid*velz_x))

             T_y = ONE/(rhofluid*Cv)*(E_y - Efluid/rhofluid*rho_y - &
                                      rhofluid*(Vxfluid*velx_y + Vyfluid*vely_y + Vzfluid*velz_y))

             T_z = ONE/(rhofluid*Cv)*(E_z - Efluid/rhofluid*rho_z - &
                                      rhofluid*(Vxfluid*velx_z + Vyfluid*vely_z + Vzfluid*velz_z))
             
             div = velx_x + vely_y + velz_z

             tauxx = mu*(TWO*velx_x - TWO3RD*div)
             tauyy = mu*(TWO*vely_y - TWO3RD*div)
             tauzz = mu*(TWO*velz_z - TWO3RD*div)

             tauxy = mu*(velx_y + vely_x)
             tauxz = mu*(velx_z + velz_x)
             tauyz = mu*(vely_z + velz_y)             
             
             ! F-viscous fluxes
             Fv_left(j, 1) =  ZERO ; Fv_rght(j, 1) = ZERO
             Fv_left(j, 2) = -tauxx; Fv_rght(j, 2) = ZERO
             Fv_left(j, 3) = -tauxy; Fv_rght(j, 3) = ZERO

             Fv_left(j, 4) = -(Vxfluid*tauxx + Vyfluid*tauxy) - mu*Cp/Pr*T_x
             Fv_rght(j, 4) = ZERO

             ! G-viscous fluxes
             Gv_left(j, 1) =  ZERO;  Gv_rght(j, 1) = ZERO
             Gv_left(j, 2) = -tauxy; Gv_rght(j, 2) = ZERO
             Gv_left(j, 3) = -tauyy; Gv_rght(j, 3) = ZERO

             Gv_left(j, 4) = -(Vxfluid*tauxy + Vyfluid*tauyy) - mu*Cp/Pr*T_y
             Gv_rght(j, 4) = ZERO

#ifdef DIM3
             Fv_left(j, 4) = -tauxz; Fv_rght(j, 4) = ZERO

             Fv_left(j, 5) = -(Vxfluid*tauxx + Vyfluid*tauxy + Vzfluid*tauxz) - mu*Cp/Pr*T_x
             Fv_rght(j, 5) = ZERO

             Gv_left(j, 4) = -tauyz; Gv_rght(j, 4) = ZERO

             Gv_left(j, 5) = -(Vxfluid*tauxy + Vyfluid*tauyy + Vzfluid*tauyz) - mu*Cp/Pr*T_y
             Gv_rght(j, 5) = ZERO

             Hv_left(j, 1) = ZERO;   Hv_rght(j, 1) = ZERO
             Hv_left(j, 2) = -tauxz; Hv_rght(j, 2) = ZERO
             Hv_left(j, 3) = -tauyz; Hv_rght(j, 3) = ZERO
             Hv_left(j, 4) = -tauzz; Hv_left(j, 4) = ZERO

             Hv_left(j, 5) = -(Vxfluid*tauxz + Vyfluid*tauyz + Vzfluid*tauzz) - mu*Cp/Pr*T_z
             Hv_rght = ZERO
#else
             Hv_left(j, :) = ZERO
             Hv_rght(j, :) = ZERO
#endif
          end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! No-slip walls !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       else if (mesh%face_markers(glb_face_index) .eq. wall_marker) then 
          ilambda = ZERO; ibeta_viscous = ZERO; itau = ldg_tau
          vleft_prefactor = ZERO; vrght_prefactor = TWO
          evaluate_flux = .true.

          do j = 1, mesh%P1                 

             ! Get the fluid and ghost states for the inviscid wall flux
             rhofluid = uflux_left(j, 1)
             Vxfluid  = uflux_left(j, 2)/rhofluid
             Vyfluid  = uflux_left(j, 3)/rhofluid
             Efluid   = uflux_left(j, 4)
             Vzfluid  = ZERO

#ifdef DIM3
             Vzfluid = uflux_left(j, 4)/rhofluid
             Efluid  = uflux_left(j, 5)
#endif
             Pfluid = (gamma - ONE)*(Efluid - &
                                     HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2))
             
             rhoghost = rhofluid
             Vxghost =  -Vxfluid
             Vyghost =  -Vyfluid
             Vzghost =  -Vzfluid
             
             Pghost = Pfluid
             Eghost = Pghost/(gamma-ONE)  + &
                             HALF*rhoghost*(Vxghost**2 + Vyghost**2 + Vzghost**2)
             
             ! inviscid fluxes
             Fd_left(j, 1) = ZERO;   Fd_rght(j, 1) = ZERO
             Fd_left(j, 2) = Pfluid; Fd_rght(j, 2) = Pghost
             Fd_left(j, 3) = ZERO;   Fd_rght(j, 3) = ZERO
             Fd_left(j, 4) = ZERO;   Fd_rght(j, 4) = ZERO
             
             Gd_left(j, 1) = ZERO;   Gd_rght(j, 1) = ZERO
             Gd_left(j, 2) = ZERO  ; Gd_rght(j, 2) = ZERO
             Gd_left(j, 3) = Pfluid; Gd_rght(j, 3) = Pghost
             Gd_left(j, 4) = ZERO;   Gd_rght(j, 4) = ZERO

#ifdef DIM3
             Fd_left(j, 4) = ZERO; Fd_rght(j, 4) = ZERO
             Fd_left(j, 5) = ZERO; Fd_rght(j, 5) = ZERO
             
             Gd_left(j, 4) = ZERO; Gd_rght(j, 4) = ZERO
             Gd_left(j, 5) = ZERO; Gd_rght(j, 5) = ZERO

             Hd_left(j, 1) = ZERO;   Hd_rght(j, 1) = ZERO
             Hd_left(j, 2) = ZERO;   Hd_rght(j, 2) = ZERO
             Hd_left(j, 3) = ZERO;   Hd_rght(j, 3) = ZERO
             Hd_left(j, 4) = Pfluid; Hd_rght(j, 4) = Pghost
             Hd_left(j, 5) = ZERO;   Hd_rght(j, 5) = ZERO
#endif
             
             ! viscous fluid states
             rho_x = data%ux_flux(left_indices(1) + (j-1), 1, left)
             rho_y = data%uy_flux(left_indices(1) + (j-1), 1, left)
             rho_z = ZERO
             
             E_x = data%ux_flux(left_indices(1) + (j-1), 4, left)
             E_y = data%uy_flux(left_indices(1) + (j-1), 4, left)
             E_z   = ZERO
             
             velx_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_x)
             velx_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_y)
             
             vely_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_x)
             vely_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_y)

             velx_z = ZERO; vely_z = ZERO
             velz_x = ZERO; velz_y = ZERO; velz_z = ZERO

#ifdef DIM3
             rho_z = data%uz_flux(left_indices(1) + (j-1), 1, left)

             E_x   = data%ux_flux(left_indices(1) + (j-1), 5, left)
             E_y   = data%uy_flux(left_indices(1) + (j-1), 5, left)
             E_z   = data%uz_flux(left_indices(1) + (j-1), 5, left)

             velx_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_z)
             vely_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_z)

             velz_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_x)
             velz_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_y)
             velz_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_z)
#endif             

             ! fluid-side temperature gradients
             T_x = ONE/(rhofluid*Cv)*(E_x - Efluid/rhofluid*rho_x - &
                                      rhofluid*(Vxfluid*velx_x + Vyfluid*vely_x + Vzfluid*velz_x))

             T_y = ONE/(rhofluid*Cv)*(E_y - Efluid/rhofluid*rho_y - &
                                      rhofluid*(Vxfluid*velx_y + Vyfluid*vely_y + Vzfluid*velz_y))

             T_z = ONE/(rhofluid*Cv)*(E_z - Efluid/rhofluid*rho_z - &
                                      rhofluid*(Vxfluid*velx_z + Vyfluid*vely_z + Vzfluid*velz_z))

             ! Viscous wall states and fluxes
             rhoghost = rhofluid
             Vxghost  = ZERO
             Vyghost  = ZERO
             Vzghost  = ZERO
             Pghost   = Pfluid
             !Eghost   = Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2)
             Eghost = Pghost/(gamma-ONE) + &
                  HALF*rhoghost*(Vxghost*Vxghost + Vyghost*Vyghost + Vzghost*Vzghost)
             
             ! correct the wall gradients for T
             T_x_ghost = T_x - nx*(nx*T_x + ny*T_y + nz*T_z)
             T_y_ghost = T_y - ny*(nx*T_x + ny*T_y + nz*T_z)
             T_z_ghost = T_z - nz*(nx*T_x + ny*T_y + nz*T_z)

             ! divergence & gradient tensor
             div = velx_x + vely_y + velz_z

             tauxx = mu*(TWO*velx_x - TWO3RD*div)
             tauxy = mu*(velx_y + vely_x)
             tauxz = mu*(velx_z + velz_x)
             
             tauyy = mu*(TWO*vely_y - TWO3RD*div)
             tauyz = mu*(vely_z + velz_y)
             
             tauzz = mu*(TWO*velz_z - TWO3RD*div)
             
             ! set the right states
             uflux_rght(j, 1) = rhoghost
             uflux_rght(j, 2) = rhoghost*Vxghost
             uflux_rght(j, 3) = rhoghost*Vyghost
             uflux_rght(j, 4) = Eghost

#ifdef DIM3
             uflux_rght(j, 4) = rhoghost*Vzghost
             uflux_rght(j, 5) = Eghost
#endif
             
             ! viscous fluxes
             Fv_left(j, 1) =  ZERO;  Fv_rght(j, 1) = ZERO
             Fv_left(j, 2) = -tauxx; Fv_rght(j, 2) = -tauxx
             Fv_left(j, 3) = -tauxy; Fv_rght(j, 3) = -tauxy

             Fv_left(j, 4) = -(Vxfluid*tauxx + Vyfluid*tauxy) - mu*Cp/Pr*T_x
             Fv_rght(j, 4) = -(Vxghost*tauxx + Vyghost*tauxy) - mu*Cp/Pr*T_x_ghost
             
             Gv_left(j, 1) = ZERO;   Gv_rght(j, 1) = ZERO
             Gv_left(j, 2) = -tauxy; Gv_rght(j, 2) = -tauxy
             Gv_left(j, 3) = -tauyy; Gv_rght(j, 3) = -tauyy

             Gv_left(j, 4) = -mu*(Vxfluid*tauxy + Vyfluid*tauyy) - mu*Cp/Pr*T_y
             Gv_rght(j, 4) = -mu*(Vxghost*tauxy + Vyghost*tauyy) - mu*Cp/Pr*T_y_ghost
             
#ifdef DIM3
             Fv_left(j, 4) = -tauxz
             Fv_rght(j, 4) = -tauxz

             Fv_left(j, 5) = -(Vxfluid*tauxx + Vyfluid*tauxy + Vzfluid*tauxz) - mu*Cp/Pr*T_x
             Fv_rght(j, 5) = -(Vxghost*tauxx + Vyghost*tauxy + Vzghost*tauxz) - mu*Cp/Pr*T_x_ghost

             Gv_left(j, 4) = -tauyz
             Gv_rght(j, 4) = -tauyz

             Gv_left(j, 5) = -(Vxfluid*tauxy + Vyfluid*tauyy + Vzfluid*tauyz) - mu*Cp/Pr*T_y
             Gv_rght(j, 5) = -(Vxghost*tauxy + Vyghost*tauyy + Vzghost*tauyz) - mu*Cp/Pr*T_y_ghost

             Hv_left(j, 1) =  ZERO;  Hv_rght(j, 1) = ZERO
             Hv_left(j, 2) = -tauxz; Hv_rght(j, 2) = -tauxz
             Hv_left(j, 3) = -tauyz; Hv_rght(j, 3) = -tauyz
             Hv_left(j, 4) = -tauzz; Hv_rght(j, 4) = -tauzz

             Hv_left(j, 5) = -(Vxfluid*tauxz + Vyfluid*tauyz + Vzfluid*tauzz) - mu*Cp/Pr*T_z
             Hv_rght(j, 5) = -(Vxghost*tauxz + Vyghost*tauyz + Vzghost*tauzz) - mu*Cp/Pr*T_z_ghost
#endif
          end do
       end if

       ! Evaluate the interaction flux for this face
       if (evaluate_flux) then

          call evaluate_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
               mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
               uflux_left, uflux_rght, &
               Fd_left, Fd_rght, Fv_left, Fv_rght, &
               Gd_left, Gd_rght, Gv_left, Gv_rght, &
               Hd_left, Hd_rght, Hv_left, Hv_rght, &
               Fi, Gi, Hi, &
               Fv, Gv, Hv, &
               gamma, viscous_prefactor, mesh%face_markers(glb_face_index), &
               ilambda, ibeta_viscous, itau, &
               glb_face_index, vleft_prefactor, vrght_prefactor)

          ! store the PHYSICAL interaction flux for the element
          ! abutting this face
          do j = 1, mesh%P1
             data%Fi(left_indices(1) + (j-1), :, left) = Fi(j, :) + Fv(j, :)
             data%Gi(left_indices(1) + (j-1), :, left) = Gi(j, :) + Gv(j, :)
             data%Hi(left_indices(1) + (j-1), :, left) = Hi(j, :) + Hv(j, :)
          end do
       end if

    end do
    !$omp end parallel do
    
  end subroutine get_boundary_interaction_flux

  subroutine setup_local_periodic_maps(mesh, data)
    use input_module
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int) :: i, i_tmp_index, j_tmp_index, i_fm, j_fm
    integer(c_int) :: i_bface, j_bface

    real(c_double) :: xf1(mesh%DIM, mesh%face_nnodes)
    real(c_double) :: xf2(mesh%DIM, mesh%face_nnodes)
    real(c_double) :: xc1(3)
    real(c_double) :: xc2(3)
    real(c_double) :: diff

    integer :: counter

    data%bface_data%local_periodic_face = -1
    data%bface_data%local_periodic_elem = -1
    data%bface_data%local_map_available = .false.
    data%bface_data%sfi_rght = -1

    counter = 0

    xc1 = ZERO; xc2 = ZERO
    do i_tmp_index = 1, mesh%Nbfaces
       i_bface = mesh%bfaces(i_tmp_index)
       i_fm    = mesh%face_markers(i_bface)

       ! only setup data for the periodic faces
       if (i_fm .eq. periodic_lox_marker .or. i_fm .eq. periodic_hix_marker .or. &
           i_fm .eq. periodic_loy_marker .or. i_fm .eq. periodic_hiy_marker .or. &
           i_fm .eq. periodic_loz_marker .or. i_fm .eq. periodic_hiz_marker) then

          ! get the face centroid for the face we're matching
          do i = 1, mesh%face_nnodes
             xf1(:, i) = mesh%points(mesh%xfaces(i_bface, i), :)
          end do
          xc1(1:mesh%DIM) = ONE/mesh%face_nnodes*sum(xf1, dim=2)
          
          secrch_matching_faces: do j_tmp_index = 1, mesh%Nbfaces
             j_bface = mesh%bfaces(j_tmp_index)
             j_fm    = mesh%face_markers(j_bface)
             
             ! get the other face centroid
             do i = 1, mesh%face_nnodes
                xf2(:, i) = mesh%points(mesh%xfaces(j_bface, i), :)
             end do
             xc2(1:mesh%DIM) = ONE/mesh%face_nnodes*sum(xf2, dim=2)

             diff = ONE
             ! test for 6 matching cases
             if (i_fm .eq. periodic_lox_marker .and. j_fm .eq. periodic_hix_marker) then
                diff = (xc1(2)-xc2(2))**2 + (xc1(3)-xc2(3))**2

             else if (i_fm .eq. periodic_hix_marker .and. j_fm .eq. periodic_lox_marker) then
                diff = (xc1(2)-xc2(2))**2 + (xc1(3)-xc2(3))**2

             else if (i_fm .eq. periodic_loy_marker .and. j_fm .eq. periodic_hiy_marker) then
                diff = (xc1(1)-xc2(1))**2 + (xc1(3)-xc2(3))**2

             else if (i_fm .eq. periodic_hiy_marker .and. j_fm .eq. periodic_loy_marker) then
                diff = (xc1(1)-xc2(1))**2 + (xc1(3)-xc2(3))**2

             else if (i_fm .eq. periodic_loz_marker .and. j_fm .eq. periodic_hiz_marker) then
                diff = (xc1(1)-xc2(1))**2 + (xc1(2)-xc2(2))**2

             else if (i_fm .eq. periodic_hiz_marker .and. j_fm .eq. periodic_loz_marker) then
                diff = (xc1(1)-xc2(1))**2 + (xc1(2)-xc2(2))**2
             end if

             if (diff .lt. SMALL) then
                data%bface_data%local_periodic_elem(i_tmp_index) = mesh%face2elem(j_bface,1)
                data%bface_data%local_periodic_face(i_tmp_index) = mesh%faceindex2elemfaceindex(j_bface,1)
                data%bface_data%local_map_available(i_tmp_index) = .true.
                
                data%bface_data%sfi_rght(:, i_tmp_index) = mesh%sfi(:, &
                     data%bface_data%local_periodic_face(i_tmp_index), &
                     data%bface_data%local_periodic_elem(i_tmp_index))

                counter = counter + 1
                exit
             end if
          end do secrch_matching_faces
       end if
    end do

  end subroutine setup_local_periodic_maps

end module df_bc_module
