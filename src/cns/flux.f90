module flux_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module

implicit none

contains

  subroutine euler_inviscid_flux(P1, Nvar, gamma, u, istart, sfi, F, G, H)
    integer(c_int), intent(in   ) :: P1, Nvar
    integer(c_int), intent(in   ) :: istart, sfi(P1)
    real(c_double), intent(in   ) :: gamma
    real(c_double), intent(in   ) :: u(P1, Nvar)
    real(c_double), intent(inout) :: F(P1, Nvar), G(P1, Nvar), H(P1, Nvar)
       
    ! locals
    integer(c_int) :: j
    real(c_double) :: rho, rhoinv, Mx, My, Mz, E, P
    
    do j = 1, P1
       rho = u(sfi(j), 1)
       Mx  = u(sfi(j), 2)
       My  = u(sfi(j), 3)
       
#ifdef DIM3
       Mz  = u(sfi(j), 4)
       E   = u(sfi(j), 5)
#else
       E   = u(sfi(j), 4)
       Mz  = ZERO
#endif
       
       rhoinv = ONE/rho
       P      = (gamma-ONE)*(E - rhoinv*HALF*(Mx*Mx + My*My + Mz*Mz))

       ! F-fluxes
       F(j, 1) = Mx
       F(j, 2) = Mx*(Mx*rhoinv) + P
       F(j, 3) = Mx*(My*rhoinv)

#ifdef DIM3
       F(j, 4) = Mx*(Mz*rhoinv)
       F(j, 5) = (Mx*rhoinv)*(E + P)
#else
       F(j, 4) = (Mx*rhoinv)*(E + P)
#endif

       ! G-Fluxes
       G(j, 1) = My
       G(j, 2) = My*(Mx*rhoinv)
       G(j, 3) = My*(My*rhoinv) + P

#ifdef DIM3
       G(j, 4) = My*(Mz*rhoinv)
       G(j, 5) = (My*rhoinv)*(E + P)
#else
       G(j, 4) = (My*rhoinv)*(E + P)
#endif

#ifdef DIM3
       ! H-Fluxes
       H(j, 1) = Mz
       H(j, 2) = Mz*(Mx*rhoinv)
       H(j, 3) = Mz*(My*rhoinv)
       H(j, 4) = Mz*(Mz*rhoinv) + P
       H(j, 5) = Mz*rhoinv*(E + P)
#else
       H(j, :) = ZERO
#endif

    end do
                                
  end subroutine euler_inviscid_flux

  subroutine ns_viscous_flux(P1, Nvar, gamma, mu, lamu, Cp, Cv, Pr,&
                             u, ux, uy, uz, istart, sfi, &
                             Fv, Gv, Hv)
    use input_module, only: vdiff, avisc
    
    integer(c_int), intent(in   ) :: P1, Nvar
    integer(c_int), intent(in   ) :: istart, sfi(P1)
    real(c_double), intent(in   ) :: gamma, mu, Cp, Cv, Pr, lamu
    real(c_double), intent(in   ) :: u( P1, Nvar)
    real(c_double), intent(in   ) :: ux(P1, Nvar)
    real(c_double), intent(in   ) :: uy(P1, Nvar)
    real(c_double), intent(in   ) :: uz(P1, Nvar)
    real(c_double), intent(inout) :: Fv(P1, Nvar), Gv(P1, Nvar), Hv(P1, Nvar)

    !locals
    integer(c_int) :: j
    real(c_double) :: rho, rhoinv, Mx, My, Mz, E, P
    real(c_double) :: velx, vely, velz

    real(c_double) :: rho_x, Mx_x, My_x, Mz_x, E_x
    real(c_double) :: rho_y, Mx_y, My_y, Mz_y, E_y
    real(c_double) :: rho_z, Mx_z, My_z, Mz_z, E_z

    real(c_double) :: velx_x, velx_y, velx_z
    real(c_double) :: vely_x, vely_y, vely_z
    real(c_double) :: velz_x, velz_y, velz_z

    real(c_double) :: T_x, T_y, T_z
    real(c_double) :: tauxx, tauxy, tauxz
    real(c_double) :: tauyy, tauyz
    real(c_double) :: tauzz

    real(c_double) :: div

    do j = 1, P1

       ! solution values
       rho = u(sfi(j), 1)
       Mx  = u(sfi(j), 2)
       My  = u(sfi(j), 3)
       
#ifdef DIM3
       Mz  = u(sfi(j), 4)
       E   = u(sfi(j), 5)
#else
       E   = u(sfi(j), 4)
       Mz  = ZERO
#endif
       rhoinv = ONE/rho
       P      = (gamma-ONE)*(E - rhoinv*HALF*(Mx*Mx + My*My + Mz*Mz))

       ! x-gradients
       rho_x = ux(sfi(j), 1)
       Mx_x  = ux(sfi(j), 2)
       My_x  = ux(sfi(j), 3)
       
#ifdef DIM3
       Mz_x  = ux(sfi(j), 4)
       E_x   = ux(sfi(j), 5)
#else
       E_x   = ux(sfi(j), 4)
       Mz_x  = ZERO
#endif

       ! y-gradients
       rho_y = uy(sfi(j), 1)
       Mx_y  = uy(sfi(j), 2)
       My_y  = uy(sfi(j), 3)
       
#ifdef DIM3
       Mz_y  = uy(sfi(j), 4)
       E_y   = uy(sfi(j), 5)
#else
       E_y   = uy(sfi(j), 4)
       Mz_y  = ZERO
#endif

#ifdef DIM3
       ! z-gradients
       rho_z = uz(sfi(j), 1)
       Mx_z  = uz(sfi(j), 2)
       My_z  = uz(sfi(j), 3)
       Mz_z  = uz(sfi(j), 4)
       E_z   = uz(sfi(j), 5)
#else
       rho_z = ZERO; Mx_z = ZERO; My_z = ZERO; Mz_z = ZERO; E_z=ZERO
#endif

       ! velocity gradients
       velx = rhoinv*Mx; vely = rhoinv*My; velz = rhoinv*Mz

       velx_x = rhoinv*(Mx_x - velx*rho_x)
       velx_y = rhoinv*(Mx_y - velx*rho_y)
       velx_z = rhoinv*(Mx_z - velx*rho_z)

       vely_x = rhoinv*(My_x - vely*rho_x)
       vely_y = rhoinv*(My_y - vely*rho_y)
       vely_z = rhoinv*(My_z - vely*rho_z)

       velz_x = rhoinv*(Mz_x - velz*rho_x)
       velz_y = rhoinv*(Mz_y - velz*rho_y)
       velz_z = rhoinv*(Mz_z - velz*rho_z)

       div = velx_x + vely_y + velz_z

       ! stress terms
       tauxx = mu*(TWO*velx_x - TWO3RD*div)
       tauxy = mu*(velx_y + vely_x)
       tauxz = mu*(velx_z + velz_x)

       tauyy = mu*(TWO*vely_y - TWO3RD*div)
       tauyz = mu*(vely_z + velz_y)
       
       tauzz = mu*(TWO*velz_z - TWO3RD*div)

       ! temperature gradients
       T_x = ONE/(rho*Cv)*(E_x - E/rho*rho_x - rho*(velx*velx_x + vely*vely_x + velz*velz_x))
       T_y = ONE/(rho*Cv)*(E_y - E/rho*rho_y - rho*(velx*velx_y + vely*vely_y + velz*velz_y))
       T_z = ONE/(rho*Cv)*(E_z - E/rho*rho_z - rho*(velx*velx_z + vely*vely_z + velz*velz_z))

       ! F-viscous fluxes
       Fv(j, 1) = ZERO
       Fv(j, 2) = -tauxx
       Fv(j, 3) = -tauxy
       Fv(j, 4) = -(velx*tauxx + vely*tauxy) - mu*Cp/Pr*T_x

#ifdef DIM3
      Fv(j, 4) = -tauxz
      Fv(j, 5) = -(velx*tauxx + vely*tauxy + velz*tauxz) - mu*Cp/Pr*T_x
#endif

      ! G-viscous fluxes
      Gv(j, 1) = ZERO
      Gv(j, 2) = -tauxy
      Gv(j, 3) = -tauyy
      Gv(j, 4) = -(velx*tauxy + vely*tauyy) - mu*Cp/Pr*T_y

#ifdef DIM3
      Gv(j, 4) = -tauyz
      Gv(j, 5) = -(velx*tauxy + vely*tauyy + velz*tauyz) - mu*Cp/Pr*T_y
#endif

#ifdef DIM3
      ! Z-viscous fluxes 
      Hv(j, 1) = ZERO
      Hv(j, 2) = -tauxz
      Hv(j, 3) = -tauyz
      Hv(j, 4) = -tauzz
      Hv(j, 5) = -(velx*tauxz + vely*tauyz + velz*tauzz) - mu*Cp/Pr*T_z
#else
      Hv(j, :) = ZERO
#endif

   end do
   
   Fv = vdiff*Fv
   Gv = vdiff*Gv
   Hv = vdiff*Hv
   
 end subroutine ns_viscous_flux
    

    subroutine evaluate_flux(n, nvar, &
                        rho, Mx, My, Mz, E, &
                        rho_x, Mx_x, My_x, Mz_x, E_x, &
                        rho_y, Mx_y, My_y, Mz_y, E_y, &
                        rho_z, Mx_z, My_z, Mz_z, E_z, &
                        velx, vely, velz, &
                        velx_x, velx_y, velx_z, &
                        vely_x, vely_y, vely_z, &
                        velz_x, velz_y, velz_z, &
                        T_x, T_y, T_z, &
                        F, G, H, Fv, Gv, Hv, &
                        gamma, mu, lamu, Cp, Pr)
      use input_module, only: vdiff, avisc

      integer(c_int), intent(in   ) :: n, nvar
      real(c_double), intent(in   ) :: rho(n), rho_x(n), rho_y(n), rho_z(n)
      real(c_double), intent(in   ) :: Mx(n),   Mx_x(n),  Mx_y(n), Mx_z(n)
      real(c_double), intent(in   ) :: My(n),   My_x(n),  My_y(n), My_z(n)
      real(c_double), intent(in   ) :: Mz(n),   Mz_x(n),  Mz_y(n), Mz_z(n)

      real(c_double), intent(in   ) :: E(n), E_x(n), E_y(n), E_z(n)
      real(c_double), intent(in   ) :: velx(n), vely(n), velz(n)

      real(c_double), intent(in   ) :: velx_x(n), velx_y(n), velx_z(n)
      real(c_double), intent(in   ) :: vely_x(n), vely_y(n), vely_z(n)
      real(c_double), intent(in   ) :: velz_x(n), velz_y(n), velz_z(n)

      real(c_double), intent(in   ) :: T_x(n), T_y(n), T_z(n)
      real(c_double), intent(inout) :: F(n, nvar), G(n, nvar), H(n, nvar)
      real(c_double), intent(inout) :: Fv(n, nvar), Gv(n, nvar), Hv(n, nvar)
      real(c_double), intent(in   ) :: gamma, mu, Cp, Pr, lamu

      ! locals
      real(c_double) :: p(n), div(n)
      real(c_double) :: tauxx(n), tauxy(n), tauxz(n)
      real(c_double) :: tauyy(n), tauyz(n)
      real(c_double) :: tauzz(n)

      ! Pressure
      P = (gamma-ONE)*(E - HALF*rho*(velx**2 + vely**2 + velz**2))

      ! Velocity divergence
      div = velx_x + vely_y + velz_z

      ! stress tensor components
      tauxx = mu*(TWO*velx_x - TWO3RD*div)
      tauxy = mu*(velx_y + vely_x)
      tauxz = mu*(velx_z + velz_x)

      tauyy = mu*(TWO*vely_y - TWO3RD*div)
      tauyz = mu*(vely_z + velz_y)

      tauzz = mu*(TWO*velz_z - TWO3RD*div)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! X-inviscid fluxes
      F(:, 1) = Mx
      F(:, 2) = Mx*velx + P
      F(:, 3) = Mx*vely
      F(:, 4) = velx*(E + P)

      ! X-viscous fluxes
      Fv(:, 1) = ZERO
      Fv(:, 2) = -tauxx
      Fv(:, 3) = -tauxy
      Fv(:, 4) = -(velx*tauxx + vely*tauxy) - mu*Cp/Pr*T_x

#ifdef DIM3
      F(:, 4) = Mx*velz
      F(:, 5) = velx*(E + P)

      Fv(:, 4) = -tauxz
      Fv(:, 5) = -(velx*tauxx + vely*tauxy + velz*tauxz) - mu*Cp/Pr*T_x
#endif
 
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Y-inviscid fluxes
      G(:, 1) = My
      G(:, 2) = My*velx
      G(:, 3) = My*vely + P
      G(:, 4) = vely*(E + P)

      ! Y-viscous fluxes
      Gv(:, 1) = ZERO
      Gv(:, 2) = -tauxy
      Gv(:, 3) = -tauyy
      Gv(:, 4) = -(velx*tauxy + vely*tauyy) - mu*Cp/Pr*T_y

#ifdef DIM3
      G(:, 4) = My*velz
      G(:, 5) = vely*(E + P)

      Gv(:, 4) = -tauyz
      Gv(:, 5) = -(velx*tauxy + vely*tauyy + velz*tauyz) - mu*Cp/Pr*T_y
#endif

#ifdef DIM3
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Z-inviscid fluxes
      H(:, 1) = Mz
      H(:, 2) = Mz*velx
      H(:, 3) = Mz*vely
      H(:, 4) = Mz*velz + P
      H(:, 5) = velz*(E + P)

      ! Z-viscous fluxes 
      Hv(:, 1) = ZERO
      Hv(:, 2) = -tauxz
      Hv(:, 3) = -tauyz
      Hv(:, 4) = -tauzz
      Hv(:, 5) = -(velx*tauxz + vely*tauyz + velz*tauzz) - mu*Cp/Pr*T_z
#else
      H = ZERO; Hv = ZERO
#endif

      Fv = vdiff*Fv
      Gv = vdiff*Gv
      Hv = vdiff*Hv

      ! Artificial viscous flux
      Fv(:, 1) = Fv(:, 1) -avisc*lamu*rho_x
      Gv(:, 1) = Gv(:, 1) -avisc*lamu*rho_y
      Hv(:, 1) = Hv(:, 1) -avisc*lamu*rho_z

      Fv(:, 2) = Fv(:, 2) -avisc*lamu*Mx_x
      Gv(:, 2) = Gv(:, 2) -avisc*lamu*Mx_y
      Hv(:, 2) = Hv(:, 2) -avisc*lamu*Mx_z

      Fv(:, 3) = Fv(:, 3) -avisc*lamu*My_x
      Gv(:, 3) = Gv(:, 3) -avisc*lamu*My_y
      Hv(:, 3) = Hv(:, 3) -avisc*lamu*My_z

#ifdef DIM3
      Fv(:, 4) = Fv(:, 4) -avisc*(lamu*Mz_x)
      Gv(:, 4) = Gv(:, 4) -avisc*(lamu*Mz_y)
      Hv(:, 4) = Hv(:, 4) -avisc*(lamu*Mz_z)

      Fv(:, 5) = Fv(:, 5) -avisc*lamu*E_x
      Gv(:, 5) = Gv(:, 5) -avisc*lamu*E_y
      Hv(:, 5) = Hv(:, 5) -avisc*lamu*E_z

#else
      H = ZERO; Hv = ZERO
      Fv(:, 4) = Fv(:, 4) -avisc*(lamu*E_x)
      Gv(:, 4) = Gv(:, 4) -avisc*(lamu*E_y)
#endif

    end subroutine evaluate_flux

    subroutine evaluate_inviscid_interaction_flux(&
         DIM, p1, nvar, &
         nminus, pnminus, &
         uminus, uplus,  &
         Fdminus, Fdplus, &
         Gdminus, Gdplus, &
         Hdminus, Hdplus, &
         Fi, Gi, Hi, &
         gamma, ilambda, face_type, glb_face_index)

      integer(c_int), intent(in ) :: DIM, p1, nvar
      real(c_double), intent(in ) :: nminus(DIM), pnminus(DIM)
      real(c_double), intent(in ) :: uminus(p1, nvar), uplus( p1, nvar)

      real(c_double), intent(in ) :: Fdminus(p1, nvar), Fdplus(p1, nvar)
      real(c_double), intent(in ) :: Gdminus(p1, nvar), Gdplus(p1, nvar)
      real(c_double), intent(in ) :: Hdminus(p1, nvar), Hdplus(p1, nvar)

      real(c_double), intent(out) :: Fi(p1, nvar), Gi(p1, nvar), Hi(p1, nvar)

      real(c_double), intent(in) :: gamma

      integer(c_int), optional, intent(in) :: face_type, glb_face_index
      real(c_double), optional, intent(in) :: ilambda

      ! locals
      integer(c_int) :: iface
      integer(c_int) :: j, var
      real(c_double) :: lambda
      real(c_double) :: nplus(DIM)

      real(c_double) :: ujumpx(p1, nvar), ujumpy(p1, nvar), ujumpz(P1, Nvar)

      real(c_double) :: rho_minus(P1), Mx_minus(P1), My_minus(P1), Mz_minus(P1), E_minus(P1)
      real(c_double) :: rho_plus( P1), Mx_plus( P1), My_plus( P1), Mz_plus( P1), E_plus( P1)

      real(c_double) :: v2_minus(P1), v2_plus(P1)
      real(c_double) :: p_minus( P1), p_plus( P1)
      real(c_double) :: cmax(P1), csmax

      real(c_double) :: vmin_dot_n(P1)
      real(c_double) :: vpls_dot_n(P1)

      real(c_double) :: nx, ny, nz, pnx, pny, pnz

      lambda = HALF;           if (present(ilambda)) lambda = ilambda

      iface = -1; if (present(glb_face_index)) iface = glb_face_index

      ! get the inward pointing normal for this face
      nplus = -nminus

      ! physical and unit normals
      pnx = pnminus(1)
      pny = pnminus(2)
      pnz = ZERO

      nx = nminus(1)
      ny = nminus(2)
      nz = ZERO

#ifdef DIM3
      nz  = nminus(3)
      pnz = pnminus(3)
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Inviscid interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! get the centered average flux
      Fi = HALF * (Fdminus + Fdplus)
      Gi = HALF * (Gdminus + Gdplus)
      Hi = HALF * (Hdminus + Hdplus)

      ! get the solution values abutting this element
      rho_minus = uminus(:,1)
      rho_plus  = uplus(:, 1)

      Mx_minus  = uminus(:,2)
      Mx_plus   = uplus(:, 2)

      My_minus  = uminus(:,3)
      My_plus   = uplus(:, 3)

#ifdef DIM3
      Mz_minus = uminus(:, 4)
      Mz_plus  = uplus( :, 4)

      E_minus  = uminus(:, 5)
      E_plus   = uplus( :, 5)
#else
      Mz_minus = ZERO
      Mz_plus  = ZERO

      E_minus   = uminus(:,4)
      E_plus    = uplus(:, 4)
#endif

      v2_minus = (Mx_minus/rho_minus)**2 + (My_minus/rho_minus)**2 + (Mz_minus/rho_minus)**2
      v2_plus  = (Mx_plus/rho_plus)**2   + (My_plus/rho_plus)**2   + (Mz_plus/rho_plus)**2

      p_minus = (gamma - ONE)*(E_minus - HALF*rho_minus*v2_minus)
      p_plus  = (gamma - ONE)*(E_plus  - HALF*rho_plus*v2_plus)

      vmin_dot_n = abs(nx*Mx_minus/rho_minus + ny*My_minus/rho_minus + nz*Mz_minus/rho_minus)
      vpls_dot_n = abs(nx*Mx_plus/rho_plus + ny*My_plus/rho_plus + nz*Mz_plus/rho_plus)

      cmax = sqrt( gamma*(p_minus + p_plus)/(rho_minus + rho_plus) ) + HALF * (vmin_dot_n + vpls_dot_n)
      csmax = maxval(cmax)

      ujumpx = nx*(uminus - uplus)
      ujumpy = ny*(uminus - uplus)
      ujumpz = nz*(uminus - uplus)

      ! store the inviscid interaction flux
      do var = 1, Nvar
         Fi(:, var) = Fi(:, var) + lambda*csmax*ujumpx(:, var)
         Gi(:, var) = Gi(:, var) + lambda*csmax*ujumpy(:, var)

#ifdef DIM3
         Hi(:, var) = Hi(:, var) + lambda*csmax*ujumpz(:, var)
#else
         Hi(:, var) = ZERO
#endif
      end do

    end subroutine evaluate_inviscid_interaction_flux

    subroutine evaluate_viscous_interaction_flux(&
         DIM, p1, nvar, &
         nminus, pnminus, &
         uminus, uplus,  &
         Fvminus, Fvplus, &
         Gvminus, Gvplus, &
         Hvminus, Hvplus, &
         Fv, Gv, Hv, &
         gamma, viscous_prefactor, &
         ibeta_viscous, itau, &
         face_type, glb_face_index, &
         vleft_prefactor, vrght_prefactor)

      use input_module, only: ldg_tau, ldg_beta

      real(c_double), intent(in ) :: viscous_prefactor
      integer(c_int), intent(in ) :: DIM, p1, nvar
      real(c_double), intent(in ) :: nminus(DIM), pnminus(DIM)
      real(c_double), intent(in ) :: uminus(p1, nvar), uplus( p1, nvar)

      real(c_double), intent(in ) :: Fvminus(p1, nvar), Fvplus(p1, nvar)
      real(c_double), intent(in ) :: Gvminus(p1, nvar), Gvplus(p1, nvar)
      real(c_double), intent(in ) :: Hvminus(p1, nvar), Hvplus(p1, nvar)

      real(c_double), intent(out) :: Fv(p1, nvar), Gv(p1, nvar), Hv(p1, nvar)

      real(c_double), intent(in) :: gamma

      integer(c_int), intent(in) :: face_type, glb_face_index
      real(c_double), intent(in) :: ibeta_viscous
      real(c_double), intent(in) :: itau
      real(c_double), intent(in) :: vleft_prefactor, vrght_prefactor

      ! locals
      integer(c_int) :: iface
      integer(c_int) :: j, var
      real(c_double) :: beta_viscous, tau_penalty

      real(c_double) :: nx, ny, nz

      beta_viscous = ibeta_viscous
      tau_penalty = itau

      nx = nminus(1)
      ny = nminus(2)
      nz = ZERO

#ifdef DIM3
      nz  = nminus(3)
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Viscous interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! get the centered average flux
      Fv = HALF * (vleft_prefactor*Fvminus + vrght_prefactor*Fvplus)
      Gv = HALF * (vleft_prefactor*Gvminus + vrght_prefactor*Gvplus)
      Hv = HALF * (vleft_prefactor*Hvminus + vrght_prefactor*Hvplus)

      ! get the penalty term
      Fv = Fv + tau_penalty*nx*( uminus - uplus )
      Gv = Gv + tau_penalty*ny*( uminus - uplus )
      Hv = Hv + tau_penalty*nz*( uminus - uplus )

      ! The ldg_beta term will be computed in the calling function !

      ! the input parameter 'vdiff' & 'avisc' control whether the
      ! viscous contribution is used (default vdiff=0, avisc=0)
      Fv = viscous_prefactor*Fv
      Gv = viscous_prefactor*Gv

#ifdef DIM3
      Hv = viscous_prefactor*Hv
#else
      Hv = ZERO
#endif
      
    end subroutine evaluate_viscous_interaction_flux

    subroutine evaluate_interaction_flux(DIM, p1, nvar, &
      nminus, pnminus, &
      uminus, uplus,  &
      Fdminus, Fdplus, Fvminus, Fvplus, &
      Gdminus, Gdplus, Gvminus, Gvplus, &
      Hdminus, Hdplus, Hvminus, Hvplus, &
      Fi, Gi, Hi, &
      Fv, Gv, Hv, &
      gamma, viscous_prefactor, face_type, &
      ilambda, ibeta_viscous, itau, &
      glb_face_index, vleft_prefactor, vrght_prefactor)

      use input_module, only: ldg_tau, ldg_beta, vdiff, avisc

      real(c_double), intent(in ) :: viscous_prefactor
      integer(c_int), intent(in ) :: DIM, p1, nvar
      real(c_double), intent(in ) :: nminus(DIM), pnminus(DIM)
      real(c_double), intent(in ) :: uminus(p1, nvar), uplus( p1, nvar)

      real(c_double), intent(in ) :: Fdminus(p1, nvar), Fdplus(p1, nvar)
      real(c_double), intent(in ) :: Fvminus(p1, nvar), Fvplus(p1, nvar)

      real(c_double), intent(in ) :: Gdminus(p1, nvar), Gdplus(p1, nvar)
      real(c_double), intent(in ) :: Gvminus(p1, nvar), Gvplus(p1, nvar)

      real(c_double), intent(in ) :: Hdminus(p1, nvar), Hdplus(p1, nvar)
      real(c_double), intent(in ) :: Hvminus(p1, nvar), Hvplus(p1, nvar)

      real(c_double), intent(out) :: Fi(p1, nvar), Gi(p1, nvar), Hi(p1, nvar)
      real(c_double), intent(out) :: Fv(p1, nvar), Gv(p1, nvar), Hv(p1, nvar)

      real(c_double), intent(in) :: gamma
      integer(c_int), intent(in) :: face_type

      integer(c_int), intent(in) :: glb_face_index
      real(c_double), intent(in) :: ilambda
      real(c_double), intent(in) :: ibeta_viscous
      real(c_double), intent(in) :: itau
      real(c_double), intent(in) :: vleft_prefactor, vrght_prefactor

      ! locals
      integer(c_int) :: iface
      integer(c_int) :: j, var
      real(c_double) :: lambda, beta_viscous, tau_penalty
      real(c_double) :: nplus(DIM)

      real(c_double) :: ujumpx(p1, nvar), ujumpy(p1, nvar), ujumpz(P1, Nvar)

      real(c_double) :: rho_minus(P1), Mx_minus(P1), My_minus(P1), Mz_minus(P1), E_minus(P1)
      real(c_double) :: rho_plus( P1), Mx_plus( P1), My_plus( P1), Mz_plus( P1), E_plus( P1)

      real(c_double) :: v2_minus(P1), v2_plus(P1)
      real(c_double) :: p_minus( P1), p_plus( P1)
      real(c_double) :: cmax(P1), csmax

      real(c_double) :: vmin_dot_n(P1)
      real(c_double) :: vpls_dot_n(P1)

      real(c_double) :: Fv_dot_n(P1, Nvar)

      real(c_double) :: nx, ny, nz, pnx, pny, pnz

      lambda       = ilambda          
      beta_viscous = ibeta_viscous
      tau_penalty  = itau

      ! get the inward pointing normal for this face
      nplus = -nminus

      ! physical and unit normals
      pnx = pnminus(1)
      pny = pnminus(2)
      pnz = ZERO

      nx = nminus(1)
      ny = nminus(2)
      nz = ZERO

#ifdef DIM3
      nz  = nminus(3)
      pnz = pnminus(3)
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Inviscid interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! get the centered average flux
      Fi = HALF * (Fdminus + Fdplus)
      Gi = HALF * (Gdminus + Gdplus)
      Hi = HALF * (Hdminus + Hdplus)

      ! get the solution values abutting this element
      rho_minus = uminus(:,1)
      rho_plus  = uplus(:, 1)

      Mx_minus  = uminus(:,2)
      Mx_plus   = uplus(:, 2)

      My_minus  = uminus(:,3)
      My_plus   = uplus(:, 3)

#ifdef DIM3
      Mz_minus = uminus(:, 4)
      Mz_plus  = uplus( :, 4)

      E_minus  = uminus(:, 5)
      E_plus   = uplus( :, 5)
#else
      Mz_minus = ZERO
      Mz_plus  = ZERO

      E_minus   = uminus(:,4)
      E_plus    = uplus(:, 4)
#endif

      v2_minus = (Mx_minus/rho_minus)**2 + (My_minus/rho_minus)**2 + (Mz_minus/rho_minus)**2
      v2_plus  = (Mx_plus/rho_plus)**2   + (My_plus/rho_plus)**2   + (Mz_plus/rho_plus)**2

      p_minus = (gamma - ONE)*(E_minus - HALF*rho_minus*v2_minus)
      p_plus  = (gamma - ONE)*(E_plus  - HALF*rho_plus*v2_plus)

      vmin_dot_n = abs(nx*Mx_minus/rho_minus + ny*My_minus/rho_minus + nz*Mz_minus/rho_minus)
      vpls_dot_n = abs(nx*Mx_plus/rho_plus + ny*My_plus/rho_plus + nz*Mz_plus/rho_plus)

      cmax = sqrt( gamma*(p_minus + p_plus)/(rho_minus + rho_plus) ) + HALF * (vmin_dot_n + vpls_dot_n)
      csmax = maxval(cmax)

      ujumpx = nx*(uminus - uplus)
      ujumpy = ny*(uminus - uplus)
      ujumpz = nz*(uminus - uplus)

      ! store the inviscid interaction flux
      do var = 1, Nvar
         Fi(:, var) = Fi(:, var) + lambda*csmax*ujumpx(:, var)
         Gi(:, var) = Gi(:, var) + lambda*csmax*ujumpy(:, var)

#ifdef DIM3
         Hi(:, var) = Hi(:, var) + lambda*csmax*ujumpz(:, var)
#else
         Hi(:, var) = ZERO
#endif
      end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Viscous interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! get the centered average flux
      Fv = HALF * (vleft_prefactor*Fvminus + vrght_prefactor*Fvplus)
      Gv = HALF * (vleft_prefactor*Gvminus + vrght_prefactor*Gvplus)
      Hv = HALF * (vleft_prefactor*Hvminus + vrght_prefactor*Hvplus)

      ! get the penalty term
      Fv = Fv + tau_penalty*nx*( uminus - uplus )
      Gv = Gv + tau_penalty*ny*( uminus - uplus )
      Hv = Hv + tau_penalty*nz*( uminus - uplus )

      ! The ldg_beta term
      beta_viscous = ZERO ! FIXME
      Fv_dot_n = nx*(Fvminus-Fvplus) + ny*(Gvminus-Gvplus) + nz*(Hvminus-Hvplus)

      Fv = Fv + beta_viscous*nx*Fv_dot_n
      Gv = Gv + beta_viscous*ny*Fv_dot_n
      Hv = Hv + beta_viscous*nz*Fv_dot_n

      ! the input parameter 'vdiff' & 'avisc' control whether the
      ! viscous contribution is used (default vdiff=0, avisc=0)
      Fv = viscous_prefactor*Fv
      Gv = viscous_prefactor*Gv

#ifdef DIM3
      Hv = viscous_prefactor*Hv
#else
      Hv = ZERO
#endif

    end subroutine evaluate_interaction_flux

end module flux_module
