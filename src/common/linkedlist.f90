module linked_list_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use mesh_module

#ifdef BOXLIB
  use parallel
#else
  use mpi
#endif

  implicit none

  type linked_list_data

     ! the box-sort bin size used. This is typically computed from the
     ! mesh data structure
     real(c_double) :: dx

     ! Number of cells in the linked-list data structure
     integer(c_int) :: Ncells, Ncells_x, Ncells_y, Ncells_z

     ! The linked list data structure. Head is of size Ncells and
     ! holds the location of the first object in that cell. Next is of
     ! size nobjects (Nelements) and points to the next item in the
     ! bin. -1 indicates nothing
     integer(c_int), pointer :: Head(:) => Null()
     integer(c_int), pointer :: Next(:) => Null()

     ! When querying for nearest objects, elem_list will give the list
     ! of objects (max_size = linked_list_max_elem_list_size) and
     ! Nnear_elements gives the number currently held. Thus
     ! elem_list(1:Nnear_elements) is the valid range
     integer(c_int)  :: Nnear_elements
     integer(c_int), pointer :: elem_list(:) => Null()

     ! Data to be interfaced with DG type limiters. The SVj & Stj sets
     ! are defined as the set of all elements sharing a given vertex
     ! and the union of it respectively. 
     integer(c_int), pointer :: nsvj(:)
     integer(c_int), pointer :: SVj(:, :) => Null()

     integer(c_int)          :: nstj
     integer(c_int), pointer :: STj(:) => Null()

  end type linked_list_data

contains

  subroutine initialize_linked_list(mesh, linked_list)
    type(mesh2d),           intent(inout) :: mesh
    type(linked_list_data), intent(inout) :: linked_list

    ! locals
    integer(c_int) :: elem_index

    call allocate_linked_list(mesh, linked_list)
    call setup_linked_list(mesh, linked_list)

    do elem_index = 1, mesh%Nelements
       call get_SVj(mesh, elem_index, linked_list)
    end do

  end subroutine initialize_linked_list
    
  subroutine allocate_linked_list(mesh, linked_list)
    use input_module, only: linked_list_max_elem_list_size, MAX_SVJ

    type(mesh2d),    intent(inout   )     :: mesh
    type(linked_list_data), intent(inout) :: linked_list

    integer(c_int) :: myRank, numProcs, err
    
    ! Set the macro cell spacing for the linked list
    call get_mesh_characteristic_length(mesh)
    linked_list%dx = TWO*mesh%hmax

    ! compute the total number of cells
    linked_list%Ncells_x = int((mesh%bbox%xmax - mesh%bbox%xmin)/linked_list%dx) + 1
    linked_list%Ncells_y = int((mesh%bbox%ymax - mesh%bbox%ymin)/linked_list%dx) + 1
    linked_list%Ncells_z = int((mesh%bbox%zmax - mesh%bbox%zmin)/linked_list%dx) + 1

    linked_list%Ncells = linked_list%Ncells_x * linked_list%Ncells_y * linked_list%Ncells_z

    ! now allocate the Head and Next arrays and initialize them
    allocate(linked_list%Head(linked_list%Ncells))
    allocate(linked_list%Next(mesh%Nelements))

    linked_list%Head = -1
    linked_list%Next = -1

    ! allocate the linked list max elem list size
    allocate(linked_list%elem_list(linked_list_max_elem_list_size))

    ! Allocate limiter data
    allocate(linked_list%SVj(mesh%elem_num_verts, MAX_SVJ))
    allocate(linked_list%nsvj(mesh%elem_num_verts))

    allocate(linked_list%STj(mesh%elem_num_verts*MAX_SVJ))

  end subroutine allocate_linked_list

  subroutine setup_linked_list(mesh, linked_list)
    type(mesh2d),    intent(inout   )     :: mesh
    type(linked_list_data), intent(inout) :: linked_list

    ! locals
    integer(c_int) :: head
    integer(c_int) :: elem_index, cx, cy, cz, cell_index
    real(c_double) :: dx, xc, yc, zc

    linked_list%Head = -1
    linked_list%Next = -1

    dx = linked_list%dx

    do elem_index = 1, mesh%Nelements
       xc = mesh%xc(elem_index)
       yc = mesh%yc(elem_index)
       zc = mesh%zc(elem_index)

       cx = int((xc - mesh%bbox%xmin)/dx) + 1
       cy = int((yc - mesh%bbox%ymin)/dx) + 1
       cz = int((zc - mesh%bbox%zmin)/dx) + 1

       cell_index = get_linear_cell_index(&
            cx, cy, cz, linked_list%Ncells_x, linked_list%Ncells_y, linked_list%Ncells_z)

       ! set the Head and Next arrays
       head = linked_list%Head(cell_index)

       linked_list%Head(cell_index) = elem_index
       linked_list%Next(elem_index) = head

    end do

  end subroutine setup_linked_list

  subroutine get_elements_in_cell(cell_index, linked_list)
    use input_module, only: linked_list_max_elem_list_size

    integer(c_int),         intent(in   )   :: cell_index
    type(linked_list_data), intent(inout)   :: linked_list

    ! locals
    integer(c_int) :: Nnear_elements, next

    Nnear_elements = 0
    next = linked_list%Head(cell_index)
    do while( next .ne. -1 )
       Nnear_elements = Nnear_elements + 1
       
       if (Nnear_elements .gt. linked_list_max_elem_list_size) then
          write(*, *) 'Number of elements too large', Nnear_elements, linked_list_max_elem_list_size, &
                      linked_list%dx
          stop
       end if

       linked_list%elem_list(Nnear_elements) = next
       next = linked_list%Next(next)
    end do

    linked_list%Nnear_elements = Nnear_elements
    
  end subroutine get_elements_in_cell

  subroutine  get_neighboring_cell_indices(cx, cy, cz, &
       Ncells_x, Ncells_y, Ncells_z, Nindices, indices)
    integer(c_int), intent(in)  :: cx, cy, cz, Ncells_x, Ncells_y, Ncells_z
    integer(c_int), intent(out) :: Nindices
    integer(c_int), intent(out) :: indices(27)

    ! locals
    integer(c_int) :: i, j, k, idx, idy, idz, cell_index
    integer(c_int) :: tmp_index
    logical        :: has_index
    
    indices = -1
    Nindices = 0

    do i = -1, 1
       do j = -1, 1
          do k = -1, 1
             idx = max(1, cx+i)
             idy = max(1, cy+j)
             idz = max(1, cz+k)
             
             cell_index = get_linear_cell_index(idx, idy, idz, Ncells_x, Ncells_y, Ncells_z)
             
             if ( cell_index .ge. 1 .and. cell_index .le. Ncells_x*Ncells_y*Ncells_z ) then
                has_index = .false.
                do tmp_index = 1, 27
                   if ( (indices(tmp_index) .eq. cell_index) ) has_index = .true.
                end do
                
                if ( .not. has_index ) then
                   Nindices = Nindices + 1
                   indices(Nindices) = cell_index
                end if
             end if

          end do
       end do
    end do

  end subroutine  get_neighboring_cell_indices

  subroutine get_containing_element(xp, yp, zp, mesh, linked_list, elem_index)
    use polynomials_module
    use input_module, only: MESH_ELEM_TYPE

    real(c_double),         intent(in   ) :: xp, yp, zp
    type(mesh2d),           intent(in   ) :: mesh
    type(linked_list_data), intent(inout) :: linked_list
    integer(c_int),         intent(out  ) :: elem_index

    ! locals
    integer(c_int) :: cx, cy, cz, i, j
    integer(c_int) :: Nindices, indices(27)

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: r, s, t
    logical        :: has_converged, found_containing_element

    ! find the cell to which the given point belongs to
    call get_cell_index(xp, yp, zp, linked_list, mesh, cx, cy, cz)

    found_containing_element = .false.

    ! get unique cell indices for neighboring elements
    call get_neighboring_cell_indices(cx, cy, cz, &
         linked_list%Ncells_x, linked_list%Ncells_y, linked_list%Ncells_z, &
         Nindices, indices)

    ! get a list of potential elements in each cell index around this
    ! point and search for containment
    do j = 1, Nindices
       call get_elements_in_cell(indices(j), linked_list)

       ! iterate over the list of elements and find the one which
       ! contains the point
       do i = 1, linked_list%Nnear_elements
          call get_element_vertices(mesh, linked_list%elem_list(i), xv, yv, zv)

          ! get the r-s coordinates for the physical point
          call xy2rs_inner(xp, yp, zp, xv, yv, zv, mesh%rv, mesh%sv, mesh%tv, &
               r, s, t, has_converged, rguess=ZERO, sguess=ZERO, tguess=ZERO)

          ! check for containment
          if (has_converged .and. MESH_ELEM_TYPE .eq. QUADS) then
             if ( (abs(r) .le. ONE .and. abs(s) .le. ONE) ) then
                write(*, *) 'LL::Point Contained in ', linked_list%elem_list(i)
                elem_index = linked_list%elem_list(i)
                found_containing_element = .true.
                exit
             end if

          else if (MESH_ELEM_TYPE .eq. TRIANGLES) then
             if ( (r+s) .le. ZERO ) then
                write(*, *) 'LL::Point Contained in ', linked_list%elem_list(i)
                elem_index = linked_list%elem_list(i)
                found_containing_element = .true.
                exit
             end if
          end if
       end do
       
       if (found_containing_element) exit

    end do

  end subroutine get_containing_element

  subroutine get_SVj(mesh, mesh_elem_index, linked_list)
    use input_module, only: MAX_SVJ

    integer(c_int),         intent(in   ) :: mesh_elem_index
    type(mesh2d),           intent(inout) :: mesh
    type(linked_list_data), intent(inout) :: linked_list

    ! locals
    integer(c_int) :: vertex_id, nsvj

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: xp, yp, zp
    integer(c_int) :: SVj(MAX_SVJ)

    zv = ZERO
    
    ! get the element vertices
    call get_element_vertices(mesh, mesh_elem_index, xv, yv, zv)
    
    ! get the SVj set for each vertex
    do vertex_id = 1, mesh%elem_num_verts
       xp = xv(vertex_id); yp = yv(vertex_id); zp = zv(vertex_id)
     
       call get_elements_sharing_vertex(xp, yp, zp, mesh, linked_list, SVj, nsvj)
       linked_list%SVj(vertex_id, :) = SVj
       linked_list%Nsvj(vertex_id) = nsvj

       ! store the vertex neighbors for the mesh. This can be done
       ! once at the head of the calculation if the mesh connectivity
       ! does not change
       mesh%NSVj(vertex_id, mesh_elem_index) = Nsvj
       mesh%SVj(vertex_id, 1:Nsvj, mesh_elem_index) = SVj(1:Nsvj)

    end do    

  end subroutine get_SVj

  subroutine get_STj(mesh, mesh_elem_index, linked_list)
    integer(c_int),         intent(in   ) :: mesh_elem_index
    type(mesh2d),           intent(inout) :: mesh
    type(linked_list_data), intent(inout) :: linked_list
    
    ! locals
    integer(c_int) :: i, vertex_id, counter
    integer(c_int) :: elem_index

    ! get the SVj set for each vertex
    call get_SVj(mesh, mesh_elem_index, linked_list)

    linked_list%STj(:) = -1

    counter = 0
    do vertex_id = 1, mesh%elem_num_verts
       do i = 1, linked_list%nsvj(vertex_id)
          elem_index = linked_list%SVj(vertex_id, i)

          if ( .not. any(linked_list%STj(:) .eq. elem_index) ) then
             counter = counter + 1
             if ( counter .gt. size(linked_list%STj) ) then
                write(*, *) 'STj set too large!'
                stop
             end if
             linked_list%STj(counter) = elem_index
          end if

       end do
    end do
    linked_list%Nstj = counter
    
  end subroutine get_STj

  subroutine get_elements_sharing_vertex(xp, yp, zp, mesh, linked_list, SVj, nsvj)
    use polynomials_module
    use input_module, only: MAX_SVJ

    real(c_double),         intent(in   ) :: xp, yp, zp
    type(mesh2d),           intent(in   ) :: mesh
    integer(c_int),         intent(out  ) :: nsvj
    type(linked_list_data), intent(inout) :: linked_list
    integer(c_int),         intent(out  ) :: SVj(:)

    ! locals
    integer(c_int) :: i, j, vert, counter
    integer(c_int) :: cx, cy, cz
    integer(c_int) :: Nindices, indices(27)

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: diff

    ! find the cell to which the given point belongs to
    call get_cell_index(xp, yp, zp, linked_list, mesh, cx, cy, cz)

    ! get unique neighboring cell indices
    call get_neighboring_cell_indices(cx, cy, cz, &
         linked_list%Ncells_x, linked_list%Ncells_y, linked_list%Ncells_z, &
         Nindices, indices)

    ! get a list of potential elements in each cell around this point
    ! and search
    counter = 0
    do j = 1, Nindices
       call get_elements_in_cell(indices(j), linked_list)

       ! iterate over the list of elements
       do i = 1, linked_list%Nnear_elements
          ! get vertices for the neighboring element
          if (mesh%elem_type(linked_list%elem_list(i)) .gt. 0) then
             call get_element_vertices(mesh, linked_list%elem_list(i), xv, yv, zv)

             do vert = 1, mesh%elem_num_verts
                diff = (xv(vert) - xp)**2 + (yv(vert) - yp)**2 + (zv(vert) - zp)**2
                if (diff .lt. EPSILON) then
                   
                   counter = counter + 1
                   if (counter .gt. MAX_SVJ) then
                      write(*, *) 'Number of elements sharing vertex > MAX_SVJ'
                      stop
                   end if
                   SVj(counter) = linked_list%elem_list(i)
                   exit
                end if
             end do
          end if
       end do
    end do
    nsvj = counter

  end subroutine get_elements_sharing_vertex

  subroutine get_cell_index(xp, yp, zp, linked_list, mesh, cx, cy, cz)
    type(linked_list_data), intent(in ) :: linked_list
    type(mesh2d),           intent(in ) :: mesh
    real(c_double),         intent(in ) :: xp, yp, zp
    integer(c_int),         intent(out) :: cx, cy, cz

    cx = int( (xp - mesh%bbox%xmin)/linked_list%dx ) + 1
    cy = int( (yp - mesh%bbox%ymin)/linked_list%dx ) + 1
    cz = int( (zp - mesh%bbox%zmin)/linked_list%dx ) + 1

  end subroutine get_cell_index

  ! pure function get_linear_cell_index(cx, cy, Ncells_x, Ncells_y) result(cell_index)
  !   integer(c_int), intent(in) :: cx, cy
  !   integer(c_int), intent(in) :: Ncells_x, Ncells_y
  !   integer(c_int) :: cell_index

  !   cell_index = (cy-1)*Ncells_x + cx

  ! end functiong et_linear_cell_index

  pure function get_linear_cell_index(cx, cy, cz, &
       Ncells_x, Ncells_y, Ncells_z) result(cell_index)
    integer(c_int), intent(in) :: cx, cy, cz
    integer(c_int), intent(in) :: Ncells_x, Ncells_y, Ncells_z
    integer(c_int) :: cell_index

    cell_index = (cz-1)*Ncells_x*Ncells_y + (cy-1)*Ncells_x + cx

  end function get_linear_cell_index

  subroutine deallocate_linked_list(linked_list)
    type(linked_list_data), intent(inout) :: linked_list

    deallocate(linked_list%Head)
    deallocate(linked_list%Next)
    deallocate(linked_list%elem_list)

    deallocate(linked_list%SVj)
    deallocate(linked_list%nsvj)

    deallocate(linked_list%STj)
    
  end subroutine deallocate_linked_list

end module linked_list_module
