module gpu_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use input_module

  implicit none

  integer, parameter :: size_of_real = 8

  type device_data_t
     integer(c_int) :: Np, Nflux, Nvar, Nelements

     ! constant operator of size Nflux X Np that operates on a matrix
     ! of size Np X *. The result of the operation is to interpolate
     ! values to the flux points
     integer*8 :: device_Lf

     ! cosntant operator of size 2*Np X Np that operates on a matrix
     ! of size Np X *, with the 1:Np entries being the gradient in r
     ! and the next being the gradient in s
     integer*8 :: device_Lgrad 

     ! constant operator of size 2*Np+Nflux X Np that operates on the
     ! discontinuous flux vector (size Np X 2*Nvar*Nelements).
     integer*8 :: device_Fdoperator

     ! output Fd operator matrix
     integer*8 :: device_Fdoutput

     ! output matrix for the gradient of the discontinuous flux of
     ! size Np X 2*Nvar. Entries from 1:Np,1:Nvar are F_r and entries
     ! from Np+1:,Nvar+1: are for G_s. The sum of which gives the
     ! divergence
     integer*8 :: device_Fgrad

     ! output matrix for the gradient of the discontinuous solution of
     ! size Np X Nvar. Where the first Np entries are the gradient in
     ! r and the next Np entries being the gradient in s
     integer*8 :: device_ugrad

     ! input matrix for the current discontinuous flux of size Np X
     ! 2*Nvar, where the first Nvar columns are fd and the next Nvar
     ! columns are gd.
     real(c_double), pointer :: host_Fd(:,:) => Null()
     integer*8 :: device_Fd

     ! input matrix for the current discontinuous solution of size Np X Nvar
     real(c_double), pointer :: host_u(:,:) => Null()
     integer*8 :: device_u

     ! output matrix for the interpolated discontinuous flux at the
     ! flux points of size (Nflux X 2*Nvar*Nelements)
     integer*8 :: device_Fdflux

  end type device_data_t

contains
  
  subroutine initialize_GPU(DIM, data, Np, Nflux, Nvar, Nelements, &
                            Lf, Lgrad)
    integer(c_int),      intent(in   ) :: Np, Nflux, Nvar, Nelements, DIM
    real(c_double),      intent(in   ) :: Lf(Nflux, Np)
    real(c_double),      intent(in   ) :: Lgrad(DIM*Np, Np)
    type(device_data_t), intent(inout) :: data

    ! locals
    real(c_double) :: Fdop(DIM*Np+Nflux, Np)

    
    data%Np        = Np
    data%Nflux     = Nflux
    data%Nvar      = Nvar 
    data%Nelements = Nelements

    ! allcoate device memory and set the interpolation matrix
    call cublas_Alloc(Nflux*Np, size_of_real, data%device_Lf)
    call cublas_Set_Matrix(Nflux, Np, size_of_real, Lf, Nflux, data%device_Lf, Nflux)

    ! allocate device memory and set the gradient matrix
    call cublas_Alloc(DIM*Np*Np, size_of_real, data%device_Lgrad)
    call cublas_Set_Matrix(DIM*Np, Np, size_of_real, Lgrad, DIM*Np, &
                           data%device_Lgrad, DIM*Np)

    ! allocate and set memory for the Fd operator matrix
    call cublas_Alloc((DIM*Np+Nflux)*Np, size_of_real, data%device_Fdoperator)

    Fdop(1:DIM*Np,  :) = Lgrad(1:DIM*Np, 1:Np)
    Fdop(DIM*Np+1:, :) = Lf(1:Nflux, 1:Np)

    call cublas_Set_Matrix(DIM*Np+Nflux, Np, size_of_real, Fdop, &
         DIM*Np+Nflux, data%device_Fdoperator, DIM*Np+Nflux)

    ! allocate memory for Fd output on the device
    call cublas_Alloc((DIM*Np+Nflux)*DIM*Nvar*Nelements, size_of_real, data%device_Fdoutput)    

    ! allocate memory for the solution vector and its gradient
    allocate(data%host_u(Np, Nvar*Nelements))
    call cublas_Alloc(Np*Nvar*Nelements, size_of_real, data%device_u)
    call cublas_Alloc(DIM*Np*Nvar*Nelements, size_of_real, data%device_ugrad)

    ! allocate memory for the discontinuous flux and its gradient
    allocate(data%host_Fd(Np, DIM*Nvar*Nelements))
    call cublas_Alloc(Np*DIM*Nvar*Nelements, size_of_real, data%device_Fd)

  end subroutine initialize_GPU

  subroutine gpu_ugrad(DIM, data, u)
    integer(c_int),      intent(in   ) :: DIM
    real(c_double),      intent(in   ) :: u(:, :, :)
    type(device_data_t), intent(inout) :: data

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements

    Nelements = data%Nelements
    Nflux     = data%Nflux
    Nvar      = data%Nvar
    Np        = data%Np


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Set the discontinuous solution matrix

    ! copy the host buffer to the GPU device. This is currently
    ! synchronous which means the control will return after the data
    ! has been successfully read from the device
    call cublas_Set_Matrix(Np, Nvar*Nelements, size_of_real, &
         u, Np, data%device_u, Np)

    ! gradient of the discontinuous solution
    call cublas_DGEMM('N', 'N', DIM*Np, Nvar*Nelements, Np, &
         ONE, data%device_Lgrad, DIM*Np, &
         data%device_u, Np, &
         ZERO, data%device_ugrad, DIM*Np)

  end subroutine gpu_ugrad

  subroutine read_host_ugrad(DIM, data, host_ugrad, ux, uy, uz)
    integer(c_int),      intent(in)    :: DIM
    type(device_data_t), intent(inout) :: data
    real(c_double),      intent(inout) :: host_ugrad(DIM*data%Np, data%Nvar*data%Nelements)
    real(c_double),      intent(inout) :: ux(data%Np, data%Nvar, data%Nelements)
    real(c_double),      intent(inout) :: uy(data%Np, data%Nvar, data%Nelements)
    real(c_double),      intent(inout) :: uz(data%Np, data%Nvar, data%Nelements)

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements, elem_index, var
    integer(c_int) :: eis

    ! locals
    Nelements = data%Nelements
    Nflux     = data%Nflux
    Nvar      = data%Nvar
    Np        = data%Np

    ! read GPU memory from device
    call cublas_Get_Matrix(DIM*Np, Nvar*Nelements, size_of_real, &
         data%device_ugrad, DIM*Np, host_ugrad, DIM*Np)

    ! Pack the gradients to the usual arrays (ux, uy, uz) which will
    ! be used in the libfr routines.

    !$omp parallel do private(elem_index, var, eis)
    do elem_index = 1, Nelements
       eis = (elem_index-1)*Nvar

       do var = 1, Nvar
          ux(:, var, elem_index) = host_ugrad(1:Np,      eis+var)
          uy(:, var, elem_index) = host_ugrad(Np+1:2*Np, eis+var)
          uz(:, var, elem_index) = ZERO
#ifdef DIM3
          uz(:, var, elem_index) = host_ugrad(2*Np+1:,   eis+var)
#endif
       end do
    end do
    !$omp end parallel do

  end subroutine read_host_ugrad

  subroutine gpu_Fgrad(DIM, data, Fdiscont)
    integer(c_int),      intent(in   ) :: DIM
    type(device_data_t), intent(inout) :: data
    real(c_double),      intent(in   ) :: Fdiscont(data%Np, DIM*data%Nvar*data%Nelements)

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements

    Nelements = data%Nelements
    Nflux     = data%Nflux
    Nvar      = data%Nvar
    Np        = data%Np

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Set the discontinuous flux matrix
    call cublas_Set_Matrix(Np, DIM*Nvar*Nelements, size_of_real, &
         Fdiscont, Np, data%device_Fd, Np)

    ! gradient and interpolated discontinuous flux
    call cublas_DGEMM('N', 'N', DIM*Np+Nflux, DIM*Nvar*Nelements, Np, &
         ONE, data%device_Fdoperator, DIM*Np+Nflux, &
         data%device_Fd, Np, &
         ZERO, data%device_Fdoutput, DIM*Np+Nflux)

  end subroutine gpu_Fgrad

  subroutine read_host_Fdoutput(DIM, data, host_Fdoutput)
    integer(c_int),      intent(in)    :: DIM
    type(device_data_t), intent(in)    :: data
    real(c_double),      intent(inout) :: host_Fdoutput(DIM*data%Np, DIM*data%Nvar*data%Nelements)

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements
    integer(c_int) :: col_index

    ! locals
    Nelements = data%Nelements
    Nflux     = data%Nflux
    Nvar      = data%Nvar
    Np        = data%Np

    ! read GPU memory into Fieldvar's Fdoutput. This has the first
    ! DIM*NP rows as the gradient of the discontinuous flux and the
    ! last Nflux rows as the discontinuous flux interpolated to the
    ! flux points
    call cublas_Get_Matrix(DIM*Np+Nflux, DIM*Nvar*Nelements, size_of_real, &
                           data%device_Fdoutput, DIM*Np+Nflux, &
                           host_Fdoutput, DIM*Np+Nflux)

    ! !$omp parallel do private(col_index)
    ! do col_index = 1, DIM*Nvar*Nelements
    !    host_Fgrad( 1:DIM*Np, col_index) = data%host_Fdoutput(1:DIM*Np,  col_index)
    !    host_Fdflux(1:Nflux,  col_index) = data%host_Fdoutput(DIM*Np+1:, col_index)
    ! end do
    ! !$omp end parallel do

  end subroutine read_host_Fdoutput

  subroutine shutdown_GPU(data)
    type(device_data_t), intent(inout) :: data

    ! deallocate GPU memory
    call cublas_Free(data%device_Lf)
    call cublas_Free(data%device_Lgrad)

    call cublas_Free(data%device_u)
    call cublas_Free(data%device_ugrad)

    call cublas_Free(data%device_Fd)

    call cublas_Free(data%device_Fdoperator)
    call cublas_Free(data%device_Fdoutput)

    ! deallocate host memory
    deallocate(data%host_u)
    deallocate(data%host_Fd)
    
  end subroutine shutdown_GPU

end module gpu_module
