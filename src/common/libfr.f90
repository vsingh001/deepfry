!Module foe the flux-reconstruction routines
module fr_module
  use iso_c_binding, only: c_double, c_int
  use input_module,  only: MESH_ELEM_TYPE, debug, ldg_beta
  use deepfry_constants_module
  use polynomials_module
  use mesh_module

  implicit none
  
contains

  subroutine initialize_deepfry(mesh, numProcs, myRank)
    use input_module, only: MESH_ELEM_TYPE
    use make_mesh_module
    
    integer(c_int),          intent(in   ) :: numProcs, myRank
    type(mesh2d),            intent(inout) :: mesh
    
    ! set mesh constants based on element type
    if ( MESH_ELEM_TYPE .eq. TRIANGLES ) then
       mesh%DIM = 2
       mesh%elem_num_faces = 3 
       mesh%face_nnodes = 2
       mesh%elem_num_verts = 3
       mesh%reference_area = TWO

    else if ( MESH_ELEM_TYPE .eq. QUADS ) then
       mesh%DIM = 2
       mesh%elem_num_faces = 4
       mesh%face_nnodes = 2
       mesh%elem_num_verts = 4
       mesh%reference_area = FOUR

    else if (MESH_ELEM_TYPE .eq. HEXS) then
       mesh%DIM = 3
       mesh%elem_num_faces = 6
       mesh%face_nnodes    = 4
       mesh%elem_num_verts = 8
       mesh%reference_area = EIGHT

    else if ( MESH_ELEM_TYPE .eq. TETS ) then
       mesh%DIM = 3
       mesh%elem_num_faces = 4
       mesh%face_nnodes = 3
       mesh%elem_num_verts = 4
       mesh%reference_area = FOUR
    end if
    
    ! read the HDF5 mesh
    call read_mesh(mesh, numProcs, myRank)

    ! set the element flux points and centroids
    call set_element_centroids(mesh)
    call set_flux_points(mesh)

    ! set the faceindex2elemfaceindex array
    call setup_faceindex2elemfaceindex(mesh)

    ! setup the vandermode and operator matrices for the
    ! discretization
    call set_matrices(mesh)

    ! sort the flux points for each face based on the physical
    ! coordinates
    call sort_face_indices(mesh)

  end subroutine initialize_deepfry

  subroutine set_matrices(mesh)
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: elem

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: x(mesh%Np), y(mesh%Np), z(mesh%Np)
    real(c_double) :: Js(mesh%Np)

    ! evaluate the modal basis at the solution points. This is the
    ! Vandermode matrix
    call get_vandermode_matrix(mesh%P, mesh%r, mesh%s, mesh%t, mesh%Np, &
         mesh%V, mesh%Vinv, mesh%L, mesh%basis_degrees)

    ! set up the gradient Vandermode matrices
    call get_grad_vandermode_matrix(mesh%P, mesh%r, mesh%s, mesh%t, mesh%Np, &
         mesh%V, mesh%Vinv, mesh%Lr, mesh%Ls, mesh%Lt, mesh%Lgrad, mesh%DIM)

    ! evaluate the basis at the flux points.
    ! call get_flux_point_vandermode_matrix(mesh%P, mesh%rf, mesh%sf, mesh%tf, &
    !      mesh%Np, mesh%elem_num_faces, mesh%V, mesh%Vinv, mesh%Vf, mesh%Lf, mesh%DIM)

    call evaluate_vandermode_matrix(mesh%P, mesh%Np, mesh%Nflux, &
         mesh%rf, mesh%sf, mesh%tf, mesh%V, mesh%Vinv, mesh%Vf, mesh%Lf)

    ! evaluate the interpolation matrix at the vertex points of the mesh
    call evaluate_vandermode_matrix(mesh%P, mesh%Np, mesh%elem_num_verts, &
         mesh%rv, mesh%sv, mesh%tv, mesh%V, mesh%Vinv, mesh%Vv, mesh%Lv)

    ! evaluate the interpolation matrix at the plot points of the mesh
    call evaluate_vandermode_matrix(mesh%P, mesh%Np, mesh%vtk_header%PlotNumberOfPoints, &
         mesh%vtk_header%r_interp, mesh%vtk_header%s_interp, mesh%vtk_header%t_interp, &
         mesh%V, mesh%Vinv, mesh%Vplot, mesh%Lplot)

    ! evaluate the gradient of the basis at the flux points
    call get_flux_point_grad_vandermode_matrix(mesh%Np, mesh%Nflux, mesh%Lf, &
         mesh%Lr, mesh%Ls, mesh%Lt, mesh%Lf_r, mesh%Lf_s, mesh%Lf_t, mesh%Lf_grad, mesh%DIM)

    ! evaluate the element transformation factors
    mesh%xr = ZERO; mesh%xs = ZERO; mesh%xt = ZERO
    mesh%yr = ZERO; mesh%ys = ZERO; mesh%yt = ZERO
    mesh%zr = ZERO; mesh%zs = ZERO; mesh%zt = ONE

    mesh%xfr = ZERO; mesh%xfs = ZERO; mesh%xft = ZERO
    mesh%yfr = ZERO; mesh%yfs = ZERO; mesh%yft = ZERO
    mesh%zfr = ZERO; mesh%zfs = ZERO; mesh%zft = ONE

    do elem = 1, mesh%Nelements
       call get_element_vertices(mesh, elem, xv, yv, zv)
       call rs2xy(mesh%r, mesh%s, mesh%t, mesh%rv, mesh%sv, mesh%tv, xv, yv, zv, x, y, z)

       ! Transformation factors at the solution points
       call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lr, mesh%Np, x, 1, ZERO, mesh%xr(:, elem), 1)
       call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Ls, mesh%Np, x, 1, ZERO, mesh%xs(:, elem), 1)

       call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lr, mesh%Np, y, 1, ZERO, mesh%yr(:, elem), 1)
       call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Ls, mesh%Np, y, 1, ZERO, mesh%ys(:, elem), 1)

       if (mesh%DIM .eq. 3) then
          call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lt, mesh%Np, x, 1, ZERO, mesh%xt(:, elem), 1)
          call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lt, mesh%Np, y, 1, ZERO, mesh%yt(:, elem), 1)
          
          call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lr, mesh%Np, z, 1, ZERO, mesh%zr(:, elem), 1)
          call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Ls, mesh%Np, z, 1, ZERO, mesh%zs(:, elem), 1)
          call DGEMV('N', mesh%Np, mesh%Np, ONE, mesh%Lt, mesh%Np, z, 1, ZERO, mesh%zt(:, elem), 1)

       end if

       ! element Jacobians at the solution points
       if (mesh%DIM .eq. 2) then
          mesh%detJn(:, elem) = mesh%ys(:, elem)*mesh%xr(:, elem) - &
                                mesh%yr(:, elem)*mesh%xs(:, elem)
       else
          mesh%detJn(:, elem) = mesh%xr(:, elem)*(mesh%ys(:, elem)*mesh%zt(:, elem) - &
                                                  mesh%yt(:, elem)*mesh%zs(:, elem))     - &
                                mesh%xs(:, elem)*(mesh%yr(:, elem)*mesh%zt(:, elem) - &
                                                  mesh%yt(:, elem)*mesh%zr(:, elem))     + &
                                mesh%xt(:, elem)*(mesh%yr(:, elem)*mesh%zs(:, elem) - &
                                                  mesh%ys(:, elem)*mesh%zr(:, elem))
       end if
       
       ! inverse of the jacobians which will be useful in getting the
       ! transformed gradients
       Js(:) = ONE/mesh%detJn(:, elem)

       mesh%rx(:, elem) =  Js*(mesh%ys(:, elem)*mesh%zt(:, elem) - mesh%zs(:, elem)*mesh%yt(:, elem))
       mesh%ry(:, elem) = -Js*(mesh%xs(:, elem)*mesh%zt(:, elem) - mesh%zs(:, elem)*mesh%xt(:, elem))
       mesh%rz(:, elem) =  Js*(mesh%xs(:, elem)*mesh%yt(:, elem) - mesh%ys(:, elem)*mesh%xt(:, elem))
       
       mesh%sx(:, elem) = -Js*(mesh%yr(:, elem)*mesh%zt(:, elem) - mesh%zr(:, elem)*mesh%yt(:, elem))
       mesh%sy(:, elem) =  Js*(mesh%xr(:, elem)*mesh%zt(:, elem) - mesh%zr(:, elem)*mesh%xt(:, elem))
       mesh%sz(:, elem) = -Js*(mesh%xr(:, elem)*mesh%yt(:, elem) - mesh%yr(:, elem)*mesh%xt(:, elem))
       
       mesh%tx(:, elem) =  Js*(mesh%yr(:, elem)*mesh%zs(:, elem) - mesh%zr(:, elem)*mesh%ys(:, elem))
       mesh%ty(:, elem) = -Js*(mesh%xr(:, elem)*mesh%zs(:, elem) - mesh%zr(:, elem)*mesh%xs(:, elem))
       mesh%tz(:, elem) =  Js*(mesh%xr(:, elem)*mesh%ys(:, elem) - mesh%yr(:, elem)*mesh%xs(:, elem))

       ! Transformation factors at the flux points
       call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_r, mesh%Nflux, x, 1, ZERO, mesh%xfr(:, elem), 1)
       call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_s, mesh%Nflux, x, 1, ZERO, mesh%xfs(:, elem), 1)

       call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_r, mesh%Nflux, y, 1, ZERO, mesh%yfr(:, elem), 1)
       call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_s, mesh%Nflux, y, 1, ZERO, mesh%yfs(:, elem), 1)

       if (mesh%DIM .eq. 3) then
          call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_t, mesh%Nflux, x, 1, ZERO, &
               mesh%xft(:, elem), 1)
          call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_t, mesh%Nflux, y, 1, ZERO, &
               mesh%yft(:, elem), 1)

          call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_r, mesh%Nflux, z, 1, ZERO, &
               mesh%zfr(:, elem), 1)
          call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_s, mesh%Nflux, z, 1, ZERO, &
               mesh%zfs(:, elem), 1)
          call DGEMV('N', mesh%Nflux, mesh%Np, ONE, mesh%Lf_t, mesh%Nflux, z, 1, ZERO, &
               mesh%zft(:, elem), 1)
       end if
       
    end do

    ! set up the correction function coefficient matrices
    !print*, 'Correction functions'
    call set_correction_function_coefficients_at_solution_points(mesh%P, &
         mesh%Np, mesh%Nflux, mesh%elem_num_faces, mesh%r, mesh%s, mesh%t, mesh%V, mesh%Vf, &
         mesh%phifj_solution_r, mesh%phifj_solution_s, mesh%phifj_solution_t, &
         mesh%phifj_solution_grad, mesh%DIM)

    ! setup filter matrix
    call set_filter_matrix(mesh%P, mesh%Np, mesh%V, mesh%Vinv, mesh%Filter)

  end subroutine set_matrices

  subroutine set_modal_coefficients(Np, Nvar, Nelements, Vinv, u, umodal)
    integer(c_int), intent(in   ) :: Np, Nvar, Nelements
    real(c_double), intent(in   ) :: u(Np, Nvar, Nelements)
    real(c_double), intent(in   ) :: Vinv(Np, Np)
    real(c_double), intent(inout) :: umodal(Np, Nvar, Nelements)

    ! locals
    integer(c_int) :: index

    !$omp parallel do private(index)
    do index = 1, Nelements

       call DGEMM('N', 'N', Np, Nvar, Np, &
            ONE, Vinv, Np, &
            u(:,:,index), Np, &
            ZERO, umodal(:, :, index), Np)
    end do
    !$omp end parallel do

  end subroutine set_modal_coefficients

  subroutine compute_cell_averages(Np, Nvar, Nelements, &
                                   qweight, reference_area, u, uavg)
    use input_module, only: chunksize

    integer(c_int), intent(in ) :: Np, Nvar, Nelements
    real(c_double), intent(in ) :: reference_area
    real(c_double), intent(in ) :: qweight(Np)
    real(c_double), intent(in ) :: u(   Np, nvar, Nelements)
    real(c_double), intent(out) :: uavg(Nvar, Nelements)

    ! locals
    integer(c_int)          :: index, j
    real(c_double)          :: alpha

    real(c_double), allocatable :: utmp(:, :)
    real(c_double), allocatable :: uavgtmp(:)

    alpha = ONE/reference_area

    allocate(utmp(   Np, Nvar*chunksize))
    allocate(uavgtmp(Nvar*chunksize))

    !$omp parallel private(index, j, utmp, uavgtmp)
    !$omp do schedule(static)
    do index = 1, Nelements/chunksize

       ! Pack into local buffer
       do j = 1, chunksize
          utmp(:, (j-1)*Nvar+1:j*Nvar) = u(:, :, (index-1)*chunksize+j)
       end do

       call DGEMV('T', Np, Nvar*chunksize, &
            alpha, utmp, Np, &
            qweight, 1, &
            ZERO, uavgtmp, 1)

       ! Pack into global buffer
       do j = 1, chunksize
          uavg(:, (index-1)*chunksize+j) = uavgtmp((j-1)*Nvar+1:j*Nvar)
       end do

    end do
    !$omp end do

    !$omp do
    do index = chunksize*(Nelements/chunksize)+1, Nelements
       call DGEMV('T', Np, Nvar,  &
            alpha, u(:, :, index), Np, &
            qweight, 1, &
            ZERO, uavg(:, index), 1)
    end do
    !$omp end do
    !$omp end parallel

    deallocate(utmp)
    deallocate(uavgtmp)

  end subroutine compute_cell_averages

  subroutine interpolate_to_flux_points(Np, Nflux, Nvar, Nelements, Lf, u, uflux)
    use input_module, only: chunksize
    integer(c_int), intent(in   ) :: Np, Nflux, Nvar, Nelements
    real(c_double), intent(in   ) :: u(Np, Nvar, Nelements)
    real(c_double), intent(in   ) :: Lf(Nflux, Np)
    real(c_double), intent(inout) :: uflux(Nflux, Nvar, Nelements)

    ! locals
    integer(c_int) :: index
    integer(c_int) :: j

    real(c_double), allocatable :: uflux_tmp(:, :)
    real(c_double), allocatable :: uelem_tmp(:, :)

    allocate(uelem_tmp(Np, Nvar*chunksize))
    allocate(uflux_tmp(Nflux, Nvar*chunksize))

    !$omp parallel private(index, j, uelem_tmp, uflux_tmp)
    !$omp do schedule(static)
    do index = 1, Nelements/chunksize

       ! Pack into local buffer
       do j = 1, chunksize
          uelem_tmp(:, (j-1)*Nvar+1:j*Nvar) = &
               u(:, :, (index-1)*chunksize+j)
       end do

       ! interpolate at flux points
       call DGEMM('N', 'N', Nflux, Nvar*chunksize, Np, &
            ONE, Lf, Nflux, &
            uelem_tmp(:, :), Np, &
            ZERO, uflux_tmp, Nflux)

       ! pack into global buffer
       do j = 1, chunksize
          uflux(:, :, (index-1)*chunksize+j) = &
               uflux_tmp(:, (j-1)*Nvar+1:j*Nvar)
       end do
       
    end do
    !$omp end do

    !$omp do
    do index = chunksize*(Nelements/chunksize)+1, Nelements
       call DGEMM('N', 'N', Nflux, Nvar, Np, &
            ONE, Lf, Nflux, &
            u(:, :, index), Np, &
            ZERO, uflux(:, :, index), Nflux)
    end do
    !$omp end do
    !$omp end parallel

    deallocate(uelem_tmp)
    deallocate(uflux_tmp)

  end subroutine interpolate_to_flux_points

  subroutine get_common_solution_values(mesh, uflux, ucommon, beta)
    type(mesh2d),   intent(in   ) :: mesh
    real(c_double), intent(in   ) :: uflux(  mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: ucommon(mesh%Nflux, mesh%Nvar, mesh%Nelements)

    real(c_double), optional, intent(in   ) :: beta

    ! locals
    integer(c_int) :: j, tmp_index, glb_face_index, left_face_index, rght_face_index, left, rght
    integer(c_int) :: left_indices(2), rght_indices(2)
    integer(c_int) :: face2elem(2)

    real(c_double) :: ibeta
    real(c_double) :: uflux_left(mesh%P1, mesh%Nvar), uflux_rght(mesh%P1, mesh%Nvar)
    real(c_double) :: uflux_avg(mesh%P1, mesh%Nvar)
    real(c_double) :: uflux_tmp_left(mesh%P1, mesh%Nvar)
    real(c_double) :: uflux_tmp_rght(mesh%P1, mesh%Nvar)

    integer(c_int) :: sfi_left(mesh%P1)
    integer(c_int) :: sfi_rght(mesh%P1)

    !real(c_double) :: xf_left(mesh%P1), yf_left(mesh%P1), zf_left(mesh%P1)
    !real(c_double) :: xf_rght(mesh%P1), yf_rght(mesh%P1), zf_rght(mesh%P1)

    ibeta = ldg_beta; if (present(beta)) ibeta = beta

    left_face_index = -1
    rght_face_index = -1

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Iterate over faces in the mesh and store the common solution value

    !$omp parallel do private(j, tmp_index, glb_face_index, face2elem, left, rght, &
    !$omp                     left_face_index, rght_face_index, left_indices, rght_indices, &
    !$omp                     uflux_tmp_left, uflux_tmp_rght, uflux_left, uflux_rght, uflux_avg, &
    !$omp                     sfi_left, sfi_rght) &
    !$omp schedule(static)
    do tmp_index = 1, mesh%Ninterior_faces
       glb_face_index = mesh%interior_faces(tmp_index)

       face2elem = mesh%face2elem(glb_face_index, :)
       left = face2elem(1)
       rght = face2elem(2)

       ! find the face indices for each element abutting this face
       left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
       rght_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 2)

       ! get the sorted face indices for each element
       sfi_left = mesh%sfi(:, left_face_index, left)
       sfi_rght = mesh%sfi(:, rght_face_index, rght)
          
       ! get the start and end indices for the data on each element
       ! abutting this face
       left_indices(1) = mesh%P1*(left_face_index-1)+1
       left_indices(2) = mesh%P1*left_face_index
       
       rght_indices(1) = mesh%P1*(rght_face_index-1)+1
       rght_indices(2) = mesh%P1*rght_face_index

       ! get the vector data for each element abutting this face
       uflux_tmp_left = uflux(left_indices(1):left_indices(2), :, left)
       uflux_tmp_rght = uflux(rght_indices(1):rght_indices(2), :, rght)
       
       ! match up the data using the sorted face indices
       do j = 1, mesh%P1
          uflux_left(j, :) = uflux_tmp_left(sfi_left(j), :)
          uflux_rght(j, :) = uflux_tmp_rght(sfi_rght(j), :)
       end do

       ! call get_face_points(mesh, left, left_face_index, xf_left, yf_left, zf_left)
       ! call get_face_points(mesh, rght, rght_face_index, xf_rght, yf_rght, zf_rght)

       ! do j = 1, mesh%P1
       !    if ( abs(xf_left(sfi_left(j)) - xf_rght(sfi_rght(j))) .gt. EPSILON .or. &
       !         abs(yf_left(sfi_left(j)) - yf_rght(sfi_rght(j))) .gt. EPSILON .or. &
       !         abs(zf_left(sfi_left(j)) - zf_rght(sfi_rght(j))) .gt. EPSILON) then
       !       print*, 'Something is wrong', j, left, rght, left_face_index, rght_face_index
       !       print*, 'Sorted faces:'
       !       print*, sfi_left
       !       print*, sfi_rght
       !       print*, 
       !       print*, xf_left(sfi_left(j)), xf_rght(sfi_rght(j)), xf_left(sfi_left(j)) - xf_rght(sfi_rght(j))
       !       print*, yf_left(sfi_left(j)), yf_rght(sfi_rght(j)), yf_left(sfi_left(j)) - yf_rght(sfi_rght(j))
       !       print*, zf_left(sfi_left(j)), zf_rght(sfi_rght(j)), zf_left(sfi_left(j)) - zf_rght(sfi_rght(j))

       !       print*, 'Xf:'
       !       print*, xf_left
       !       print*, xf_rght
       !       print*, 'Yf:'
       !       print*, yf_left
       !       print*, yf_rght
       !       print*, 'Zf:'
       !       print*, zf_left
       !       print*, zf_rght
       !       print*,
       !       stop

       !    end if
       ! end do
       
       ! store ucommon value for each element abutting this face
       uflux_avg = HALF * (uflux_left + uflux_rght)

       ! store the common solution values 
       do j = 1, mesh%P1
          ucommon(left_indices(1) + (sfi_left(j)-1), :, left) = uflux_avg(j, :) - &
               ibeta * (uflux_left(j, :) - uflux_rght(j, :))

          ucommon(rght_indices(1) + (sfi_rght(j)-1), :, rght) = uflux_avg(j, :) + &
               ibeta * (uflux_left(j, :) - uflux_rght(j, :))

       end do
    end do
    !$omp end parallel do   

  end subroutine get_common_solution_values

  subroutine compute_gradients(mesh, u, uflux, ucommon, &
                               ux, uy, uz, ux_flux, uy_flux, uz_flux)
    use input_module, only: chunksize
    type(mesh2d), intent(in     ) :: mesh
    real(c_double), intent(in   ) :: u( mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in   ) :: uflux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in   ) :: ucommon(mesh%Nflux, mesh%Nvar, mesh%Nelements)

    real(c_double), intent(inout) :: ux(mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uy(mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uz(mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: ux_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uy_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uz_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements
    integer(c_int) :: chunk_index, j, tmp_index, elem_index, var

    real(c_double), allocatable :: ur(:, :), us(:, :), ut(:, :)
    real(c_double), allocatable :: ugrad(:, :)
    real(c_double), allocatable :: ucorr(:, :)
    real(c_double), allocatable :: uelem(:, :)

    real(c_double) :: ur2(mesh%Np, mesh%Nvar), us2(mesh%Np, mesh%Nvar), ut2(mesh%Np, mesh%Nvar)
    real(c_double) :: ugrad2(mesh%Dim*mesh%Np, mesh%Nvar)
    real(c_double) :: ucorr2(mesh%Nflux, mesh%Nvar)
    
    real(c_double) :: rx(mesh%Np), ry(mesh%Np), rz(mesh%Np)
    real(c_double) :: sx(mesh%Np), sy(mesh%Np), sz(mesh%Np)
    real(c_double) :: tx(mesh%Np), ty(mesh%Np), tz(mesh%Np)

    Np        = mesh%Np
    Nflux     = mesh%Nflux
    Nvar      = mesh%Nvar
    Nelements = mesh%Nelements

    allocate(ur(Np, Nvar*chunksize))
    allocate(us(Np, Nvar*chunksize))
    allocate(ut(Np, Nvar*chunksize))

    allocate(ugrad(mesh%DIM*Np, Nvar*chunksize))
    allocate(ucorr(Nflux,       Nvar*chunksize))
    allocate(uelem(Np,          Nvar*chunksize))

    !$omp parallel private(tmp_index, elem_index, chunk_index, j, var, &
    !$omp                  uelem, ucorr, ugrad, ucorr2, ugrad2, &
    !$omp                  ur, us, ut, ur2, us2, ut2, &
    !$omp                  rx, ry, rz, sx, sy, sz, tx, ty, tz)
    !$omp do schedule(static)
    do chunk_index = 1, Nelements/chunksize

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       ! Gradients from the discontinuous solution values

       ! cuBLAS gradients
#ifdef CUDA
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j
          tmp_index  = (j-1)*Nvar
          
          do var = 1, Nvar
             ugrad(1:Np,      tmp_index+var) = ux(:, var, elem_index)
             ugrad(Np+1:2*Np, tmp_index+var) = uy(:, var, elem_index)

#ifdef DIM3
             ugrad(2*Np+1:,   tmp_index+var) = uz(:, var, elem_index)
#endif
          end do
       end do

       ! CPU (BLAS) gradients
#else
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j
          tmp_index  = (j-1)*Nvar

          ! pack into local buffer
          uelem(:, tmp_index+1:tmp_index+Nvar) = u(:, :, elem_index)
       end do

       ! get the gradients
       call DGEMM('N', 'N', mesh%DIM*Np, chunksize*Nvar, Np, &
            ONE, mesh%Lgrad, mesh%DIM*Np, &
            uelem, Np, &
            ZERO, ugrad, mesh%DIM*Np)
#endif

       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       ! Gradients from the correction term
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j       
          tmp_index  = (j-1)*Nvar

          ucorr(:, tmp_index+1:tmp_index+Nvar) = &
                ucommon(:, :, elem_index) - uflux(  :, :, elem_index)

       end do

       call DGEMM('N', 'N', mesh%DIM*Np, chunksize*Nvar, Nflux, &
            ONE, mesh%phifj_solution_grad, mesh%DIM*Np, &
            ucorr, Nflux, &
            ONE, ugrad, mesh%DIM*Np)

       ! get the individual (r, s, t) derivatives
       ur(:, :) = ugrad(1:Np, :)
       us(:, :) = ugrad(Np+1:2*Np, :)
       ut = ZERO

#ifdef DIM3
       ut(:, :) = ugrad(2*Np+1:, :)
#endif

       ! Pack into the global buffer
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j
          tmp_index  = (j-1)*Nvar

          ! inverse jacobian for the local element
          rx =  mesh%rx(:, elem_index); ry = mesh%ry(:, elem_index); rz = mesh%rz(:, elem_index)
          sx =  mesh%sx(:, elem_index); sy = mesh%sy(:, elem_index); sz = mesh%sz(:, elem_index)
          tx =  mesh%tx(:, elem_index); ty = mesh%ty(:, elem_index); tz = mesh%tz(:, elem_index)

          do var = 1, Nvar
             ux(:, var, elem_index) = &
                  rx*ur(:, tmp_index+var) + sx*us(:, tmp_index+var) + tx*ut(:, tmp_index+var)
             
             uy(:, var, elem_index) = &
                  ry*ur(:, tmp_index+var) + sy*us(:, tmp_index+var) + ty*ut(:, tmp_index+var)

             uz(:, var, elem_index) = ZERO

#ifdef DIM3
             uz(:, var, elem_index) = &
                  rz*ur(:, tmp_index+var) + sz*us(:, tmp_index+var) + tz*ut(:, tmp_index+var)
#endif
          end do
       end do
       
    end do
    !$omp end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Do the remainder
    !$omp do
    do elem_index = chunksize*(Nelements/chunksize)+1, Nelements
       
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       ! Gradients from the discontinuous solution

#ifdef CUDA
       do var = 1, mesh%Nvar
          ugrad2(1:Np,      var) = ux(:, var, elem_index)
          ugrad2(Np+1:2*Np, var) = uy(:, var, elem_index)
#ifdef DIM3
          ugrad2(2*Np+1:,   var) = uz(:, var, elem_index)
#endif
       end do

#else
       ! Gradients from the discontinuous solution
       call DGEMM('N', 'N', mesh%DIM*Np, Nvar, Np, &
            ONE, mesh%Lgrad, mesh%DIM*Np, &
            u(:, :, elem_index), Np, &
            ZERO, ugrad2, mesh%DIM*Np)
#endif
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       ! Gradients from the correction term
       ucorr2 = ucommon(:, :, elem_index) - uflux(:, :, elem_index)

       call DGEMM('N', 'N', mesh%DIM*Np, Nvar, Nflux, &
            ONE, mesh%phifj_solution_grad, mesh%DIM*Np, &
            ucorr2, Nflux, &
            ONE, ugrad2, mesh%DIM*Np)
       
       ur2(:, :) = ugrad2(1:Np,  :)
       us2(:, :) = ugrad2(Np+1:2*Np, :)
       ut2(:, :) = ZERO
       
#ifdef DIM3
       ut2(:, :) = ugrad2(2*Np+1:, :)
#endif

       ! inverse jacobian for this element
       rx =  mesh%rx(:, elem_index); ry = mesh%ry(:, elem_index); rz = mesh%rz(:, elem_index)
       sx =  mesh%sx(:, elem_index); sy = mesh%sy(:, elem_index); sz = mesh%sz(:, elem_index)
       tx =  mesh%tx(:, elem_index); ty = mesh%ty(:, elem_index); tz = mesh%tz(:, elem_index)
       
       ! physical gradients by the chain rule
       do var = 1, Nvar
          ux(:,var,elem_index) = rx*ur2(:, var) + sx*us2(:, var) + tx*ut2(:, var)
          uy(:,var,elem_index) = ry*ur2(:, var) + sy*us2(:, var) + ty*ut2(:, var)
          uz(:,var,elem_index) = ZERO

#ifdef DIM3
          uz(:,var,elem_index) = rz*ur2(:, var) + sz*us2(:, var) + tz*ut2(:, var)
#endif
       end do
    end do
    !$omp end do
    !$omp end parallel

    deallocate(ur, us, ut)
    deallocate(ugrad, ucorr, uelem)

    ! interpolate the gradients to the flux points
    call gradients_at_flux_points(mesh, ux, uy, uz, ux_flux, uy_flux, uz_flux)

  end subroutine compute_gradients

  subroutine gradients_at_flux_points(mesh, ux, uy, uz, ux_flux, uy_flux, uz_flux)
    use input_module, only: chunksize
    type(mesh2d), intent(in   ) :: mesh
    real(c_double), intent(in ) :: ux(mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in ) :: uy(mesh%Np, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in ) :: uz(mesh%Np, mesh%Nvar, mesh%Nelements)

    real(c_double), intent(inout) :: ux_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uy_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(inout) :: uz_flux(mesh%Nflux, mesh%Nvar, mesh%Nelements)

    ! locals
    integer(c_int) :: Np, Nflux, Nvar, Nelements
    integer(c_int) :: j, chunk_index, elem_index, tmp_index

    real(c_double), allocatable :: ugrad_tmp(:, :)
    real(c_double), allocatable :: ugrad_flx(:, :)

    real(c_double) :: ugrad_tmp2(   mesh%Np, mesh%DIM*mesh%Nvar)
    real(c_double) :: ugrad_flx2(mesh%Nflux, mesh%DIM*mesh%Nvar)

    Np        = mesh%Np
    Nflux     = mesh%Nflux
    Nvar      = mesh%Nvar
    Nelements = mesh%Nelements

    allocate(ugrad_tmp(Np, mesh%DIM*Nvar*chunksize))
    allocate(ugrad_flx(Nflux, mesh%DIM*Nvar*chunksize))

    !$omp parallel private(j, chunk_index, elem_index, tmp_index, &
    !$omp                  ugrad_tmp, ugrad_tmp2, ugrad_flx, ugrad_flx2)
    !$omp do schedule(static)
    do chunk_index = 1, Nelements/chunksize
       
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j
          tmp_index  = (j-1)*mesh%DIM*Nvar

          ! Pack gradients into local chunking buffer
          ugrad_tmp(:, tmp_index+1:tmp_index+Nvar)          = ux(:, :, elem_index)
          ugrad_tmp(:, tmp_index+1*Nvar+1:tmp_index+2*Nvar) = uy(:, :, elem_index)

#ifdef DIM3
          ugrad_tmp(:, tmp_index+2*Nvar+1:tmp_index+3*Nvar) = uz(:, :, elem_index)
#endif
       end do

       ! interpolate gradients to the flux points
       call DGEMM('N', 'N', Nflux, chunksize*mesh%DIM*Nvar, Np, &
            ONE, mesh%Lf, Nflux, &
            ugrad_tmp, Np, &
            ZERO, ugrad_flx, Nflux)
       
       ! Unpak the computed gradients to the mesh data structure
       do j = 1, chunksize
          elem_index = (chunk_index-1)*chunksize+j
          tmp_index  = (j-1)*mesh%DIM*Nvar
          
          ux_flux(:, :, elem_index) = ugrad_flx(:, tmp_index+1:tmp_index+Nvar)
          uy_flux(:, :, elem_index) = ugrad_flx(:, tmp_index+1*Nvar+1:tmp_index+2*Nvar)
          uz_flux(:, :, elem_index) = ZERO

#ifdef DIM3
          uz_flux(:, :, elem_index) = ugrad_flx(:, tmp_index+2*Nvar+1:tmp_index+3*Nvar)
#endif          
       end do
    end do
    !omp end do

    !$omp do
    do elem_index = chunksize*(Nelements/chunksize)+1, Nelements

       ugrad_tmp2(:, 1:Nvar )         = ux(:, :, elem_index)
       ugrad_tmp2(:, Nvar+1:2*Nvar)   = uy(:, :, elem_index)

#ifdef DIM3
       ugrad_tmp2(:, 2*Nvar+1:) = uz(:, :, elem_index)
#endif

       call DGEMM('N', 'N', Nflux, mesh%DIM*Nvar, Np, &
            ONE, mesh%Lf, Nflux, &
            ugrad_tmp2, Np, &
            ZERO, ugrad_flx2, Nflux)
       
       ux_flux(:, :, elem_index) = ugrad_flx2(:, 1:Nvar )
       uy_flux(:, :, elem_index) = ugrad_flx2(:, Nvar+1:2*Nvar)
       uz_flux(:, :, elem_index) = ZERO

#ifdef DIM3
       uz_flux(:, :, elem_index) = ugrad_flx2(:, 2*Nvar+1:)
#endif
          
    end do
    !$omp end do
    !$omp end parallel

    deallocate(ugrad_tmp)
    deallocate(ugrad_flx)

  end subroutine gradients_at_flux_points

  subroutine compute_flux_divergence(mesh, data, Fdiscont, Fi, Gi, Hi, detJn, k1)
    use fieldvar_module
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data
    real(c_double), intent(in ) :: detJn(mesh%Np, mesh%Nelements)
    real(c_double), intent(in ) :: Fdiscont(mesh%Np, mesh%DIM*mesh%Nvar*mesh%Nelements)

    real(c_double), intent(in ) :: Fi(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in ) :: Gi(mesh%Nflux, mesh%Nvar, mesh%Nelements)
    real(c_double), intent(in ) :: Hi(mesh%Nflux, mesh%Nvar, mesh%Nelements)

    real(c_double), intent(inout) :: k1(mesh%Np, mesh%Nvar, mesh%Nelements)

    ! locals
    integer(c_int) :: Np, Nvar, Nflux, Nelements, DIM
    integer(c_int) :: elem_index, var
    real(c_double) :: fdiv(mesh%Np, mesh%Nvar)

    real(c_double) :: Fd(mesh%Np, mesh%DIM*mesh%Nvar)
    real(c_double) :: Fgrad(mesh%DIM*mesh%Np, mesh%DIM*mesh%Nvar)

    real(c_double) :: Fcorr(mesh%Nflux,     mesh%DIM*mesh%Nvar)

    real(c_double) :: Fd_flux(mesh%Nflux, mesh%Nvar)
    real(c_double) :: Gd_flux(mesh%Nflux, mesh%Nvar)
    real(c_double) :: Hd_flux(mesh%Nflux, mesh%Nvar)

    real(c_double) :: Fi_flux(mesh%Nflux, mesh%Nvar)
    real(c_double) :: Gi_flux(mesh%Nflux, mesh%Nvar)
    real(c_double) :: Hi_flux(mesh%Nflux, mesh%Nvar)

    real(c_double) :: xfr(mesh%Nflux), xfs(mesh%Nflux), xft(mesh%Nflux)
    real(c_double) :: yfr(mesh%Nflux), yfs(mesh%Nflux), yft(mesh%Nflux)
    real(c_double) :: zfr(mesh%Nflux), zfs(mesh%Nflux), zft(mesh%Nflux)

    real(c_double) :: Fd_flux_tmp(mesh%Nflux, mesh%DIM*mesh%Nvar)
    real(c_double) :: detJ(mesh%Np)

    real(c_double) :: Lf(mesh%Nflux, mesh%Np)
    real(c_double) :: Lgrad(mesh%DIM*mesh%Np, mesh%Np)

    real(c_double) :: phifj_solution_grad(mesh%DIM*mesh%Np, mesh%Nflux)

    DIM = mesh%DIM

    Np        = mesh%Np
    Nflux     = mesh%Nflux
    Nvar      = mesh%Nvar
    Nelements = mesh%Nelements

    Lf                  = mesh%Lf
    Lgrad               = mesh%Lgrad
    phifj_solution_grad = mesh%phifj_solution_grad

    !$omp parallel do private(var, elem_index, xfr, xfs, xft, yfr, yfs, yft, zfr, zfs, zft, &
    !$omp                     Fd_flux, Gd_flux, Hd_flux, &
    !$omp                     Fi_flux, Gi_flux, Hi_flux, &
    !$omp                     Fd, Fgrad, fdiv, Fcorr, &
    !$omp                     Fd_flux_tmp, detJ) &
    !$omp schedule(static)
    do elem_index = 1, mesh%Nelements_local

          ! element transformation factors
          xfr = mesh%xfr(:, elem_index); xfs = mesh%xfs(:, elem_index); xft = mesh%xft(:, elem_index)
          yfr = mesh%yfr(:, elem_index); yfs = mesh%yfs(:, elem_index); yft = mesh%yft(:, elem_index)
          zfr = mesh%zfr(:, elem_index); zfs = mesh%zfs(:, elem_index); zft = mesh%zft(:, elem_index)
          
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Divergence of the discontinuous flux (This is already
          ! transformed to the reference space) either read from the
          ! GPU computed values or evaluate on the host

#ifdef CUDA
          ! GPU computed values
          Fgrad(:, :) = data%host_Fdoutput(1:DIM*Np, (elem_index-1)*DIM*Nvar+1:elem_index*DIM*Nvar)

          Fd_flux(:, :) = data%host_Fdoutput(DIM*Np+1:, (elem_index-1)*DIM*Nvar+1:(elem_index-1)*DIM*Nvar+Nvar)
          Gd_flux(:, :) = data%host_Fdoutput(DIM*Np+1:, (elem_index-1)*DIM*Nvar+Nvar+1:(elem_index-1)*DIM*Nvar+2*Nvar)
          Hd_flux(:, :) = ZERO

#ifdef DIM3
          Hd_flux(:, :) = data%host_Fdoutput(DIM*Np+1:, (elem_index-1)*DIM*Nvar+2*Nvar+1:(elem_index-1)*DIM*Nvar+3*Nvar)
#endif

#else
          ! CPU computed values
          Fd(1:Np, :) = Fdiscont(:, (elem_index-1)*DIM*Nvar+1:elem_index*DIM*Nvar)

          call DGEMM('N', 'N', DIM*Np, DIM*Nvar, Np, &
               ONE, Lgrad, DIM*Np, &
               Fd, Np, &
               ZERO, Fgrad, DIM*Np)

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Interpolate the discontinuous flux to the flux points
          ! using the interpolation matrix

          call DGEMM('N', 'N', Nflux, DIM*Nvar, mesh%Np, &
               ONE, Lf, Nflux, &
               Fd, Np, &
               ZERO, Fd_flux_tmp, Nflux)

          Fd_flux(:, :) = Fd_flux_tmp(:, 1:mesh%Nvar )
          Gd_flux(:, :) = Fd_flux_tmp(:, Nvar+1:2*Nvar)

#ifdef DIM3
          ! read the interpolated H-fluxes if working in 3D. Note that
          ! we explicitly set the 2D values to 0
          Hd_flux(:, :) = Fd_flux_tmp(:, 2*Nvar+1:3*Nvar)
#else
          Hd_flux(:, :) = ZERO
#endif

#endif

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Contribution from the interaction flux 
          ! Set the transformed interaction fluxes at the flux points
          do var = 1, mesh%Nvar
             Fi_flux(:, var) = (yfs*zft - yft*zfs)*Fi(:, var, elem_index) + &
                               (xft*zfs - xfs*zft)*Gi(:, var, elem_index) + &
                               (xfs*yft - xft*yfs)*Hi(:, var, elem_index)

             Gi_flux(:, var) = (yft*zfr - yfr*zft)*Fi(:, var, elem_index) + &
                               (xfr*zft - xft*zfr)*Gi(:, var, elem_index) + &
                               (xft*yfr - xfr*yft)*Hi(:, var, elem_index)

             Hi_flux(:, var) = (yfr*zfs - yfs*zfr)*Fi(:, var, elem_index) + &
                               (xfs*zfr - xfr*zfs)*Gi(:, var, elem_index) + &
                               (xfr*yfs - xfs*yfr)*Hi(:, var, elem_index)
          end do

          ! Form the Correction flux
          Fcorr(1:Nflux, 1:Nvar       ) = Fi_flux(:, :) - Fd_flux(:, :)
          Fcorr(1:Nflux, Nvar+1:2*Nvar) = Gi_flux(:, :) - Gd_flux(:, :)

#ifdef DIM3
          Fcorr(1:Nflux, 2*Nvar+1:) = Hi_flux(:, :) - Hd_flux(:, :)
#endif          

          ! Divergence from the correction flux
          call DGEMM('N', 'N', mesh%DIM*Np, mesh%DIM*Nvar, Nflux, &
               ONE, phifj_solution_grad, mesh%DIM*Np, &
               Fcorr, Nflux, &
               ONE, Fgrad, mesh%DIM*Np)

          ! form the divergence from the component gradients
#ifdef DIM3
          fdiv = Fgrad(1:Np, 1:Nvar) + Fgrad(Np+1:2*Np, Nvar+1:2*Nvar) + &
                                       Fgrad(2*Np+1:, 2*Nvar+1:)
#else
          fdiv =  Fgrad(1:Np, 1:Nvar) + Fgrad(Np+1:, Nvar+1:)
#endif

          ! add the divergence correction and transform to physical
          ! space using the element jacobian.
          detJ = ONE/mesh%detJn(:, elem_index)
          do var = 1, Nvar
             k1(:, var, elem_index) = -fdiv(:,var)*detJ
          end do

       !end if
    end do
    !$omp end parallel do
    
  end subroutine compute_flux_divergence

  subroutine apply_filter(Np, Nvar, Nelements, Filter, u)
    integer(c_int), intent(in   ) :: Np, Nvar, Nelements
    real(c_double), intent(in   ) :: Filter(Np, Np)
    real(c_double), intent(inout) :: u(Np, Nvar, Nelements)

    ! locals
    integer(c_int) :: elem_index
    real(c_double) :: utmp(Np, Nvar)

    !$omp parallel do private(elem_index, utmp)
    do elem_index = 1, Nelements
       call DGEMM('N', 'N', Np, Nvar, Np, &
            ONE, Filter(:, :), Np, &
            u(:, :, elem_index), Np, &
            ZERO, utmp(:, :), Np)

       ! copy over to the solution vector
       u(:, :, elem_index) = utmp(:, :)
    end do
    !$omp end parallel do
    
  end subroutine apply_filter

end module fr_module
