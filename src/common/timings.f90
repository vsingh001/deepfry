module timings_module
  use iso_c_binding, only: c_int, c_double
  implicit none

  real(c_double) :: interpolate_at_face_time
  real(c_double) :: get_ucommon_time
  real(c_double) :: boundary_ucommon_time

  real(c_double) :: get_gradients_time
  real(c_double) :: get_interaction_flux_time
  real(c_double) :: boundary_interaction_time

  real(c_double) :: flux_divergence_time
  real(c_double) :: rhs_evaluation_time

  real(c_double) :: discontinuous_flux_sol_time
  real(c_double) :: discontinuous_flux_flux_time

#ifdef CUDA
  real(c_double) :: gpu_ugrad_write_time
  real(c_double) :: gpu_ugrad_read_time

  real(c_double) :: gpu_Fgrad_write_time
  real(c_double) :: gpu_Fgrad_read_time
#endif
  
end module timings_module
