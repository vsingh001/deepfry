module parallel_module
  use iso_c_binding, only: c_int, c_double

#ifdef BOXLIB
  use parallel
#else
  use mpi
#endif

  use mesh_module

  implicit none

  integer(c_int), parameter :: USOL_TAG    = 1
  integer(c_int), parameter :: UFLX_TAG    = 2
  integer(c_int), parameter :: UCOMMON_TAG = 3
  integer(c_int), parameter :: FI_TAG      = 4
  integer(c_int), parameter :: GI_TAG      = 5
  integer(c_int), parameter :: HI_TAG      = 6

  integer(c_int), parameter :: STj_min_TAG = 7
  integer(c_int), parameter :: STj_max_TAG = 8

  integer(c_int), parameter :: TCI_TAG     = 9

  integer(c_int), parameter :: UX_TAG      = 10
  integer(c_int), parameter :: UY_TAG      = 11
  integer(c_int), parameter :: UZ_TAG      = 12
  integer(c_int), parameter :: UXYZ_TAG      = 13

  type mpi_data_t
     integer(c_int) :: Nsend, Nrecv, src, dst

     real(c_double), allocatable :: flux_recv(:)
     real(c_double), allocatable :: flux_send(:)

     real(c_double), allocatable :: sol_recv(:)
     real(c_double), allocatable :: sol_send(:)

     real(c_double), allocatable :: avg_send(:)
     real(c_double), allocatable :: avg_recv(:)

     ! data for periodic faces
     real(c_double), allocatable :: periodic_recvbuf(:)
     real(c_double), allocatable :: periodic_sendbuf(:)

     real(c_double), allocatable :: periodic_recvbuf3(:)
     real(c_double), allocatable :: periodic_sendbuf3(:)

  end type mpi_data_t

contains

subroutine initialize_mpi_data(mesh, data, myRank, numProcs)
  use input_module, only: df_is_periodic

  integer(c_int),   intent(in   ) :: myRank, numProcs
  type(mesh2d),     intent(in   ) :: mesh
  type(mpi_data_t), intent(inout) :: data

  allocate(data%flux_recv(mesh%Nflux*mesh%Nvar*mesh%max_numImport))
  allocate(data%flux_send(mesh%Nflux*mesh%Nvar*mesh%max_numExport))

  allocate(data%sol_recv(mesh%Np*mesh%Nvar*mesh%max_numImport))
  allocate(data%sol_send(mesh%Np*mesh%Nvar*mesh%max_numExport))

  allocate(data%avg_send(mesh%max_numExport))
  allocate(data%avg_recv(mesh%max_numImport))

  if (df_is_periodic) then
     allocate( data%periodic_recvbuf(mesh%P1*mesh%Nvar*mesh%num_import_indices_periodic) )
     allocate( data%periodic_sendbuf(mesh%P1*mesh%Nvar*mesh%num_export_indices_periodic) )

     allocate( data%periodic_recvbuf3(3*mesh%P1*mesh%Nvar*mesh%num_import_indices_periodic) )
     allocate( data%periodic_sendbuf3(3*mesh%P1*mesh%Nvar*mesh%num_export_indices_periodic) )

  end if

end subroutine initialize_mpi_data

subroutine destroy_mpi_data(mesh, data, myRank, numProcs)
  use input_module, only: df_is_periodic

  integer(c_int),   intent(in   ) :: myRank, numProcs
  type(mesh2d),     intent(in   ) :: mesh
  type(mpi_data_t), intent(inout) :: data

  deallocate(data%flux_recv)
  deallocate(data%flux_send)

  deallocate(data%sol_recv)
  deallocate(data%sol_send)

  deallocate(data%avg_send)
  deallocate(data%avg_recv)
  
  if (df_is_periodic) then
     deallocate( data%periodic_recvbuf )
     deallocate( data%periodic_sendbuf )

     deallocate( data%periodic_recvbuf3 )
     deallocate( data%periodic_sendbuf3 )

  end if

end subroutine destroy_mpi_data

subroutine exchange_periodic_data(Nflux, P1, Nvar, mesh, data_buffer_in, data_buffer_out, tag, &
                                  sendbuf, recvbuf, myRank, numProcs, mpi_data_type)
  integer(c_int),   intent(in   ) :: Nflux, P1, Nvar, myRank, numProcs, mpi_data_type, tag
  type(mesh2d),     intent(inout) :: mesh
  real(c_double),   intent(in)    :: data_buffer_in(Nflux, Nvar, mesh%Nelements)
  real(c_double),   intent(inout) :: data_buffer_out(P1, Nvar, mesh%Nbfaces)
  real(c_double),   intent(inout) :: sendbuf(:)
  real(c_double),   intent(inout) :: recvbuf(:)

  ! locals
  integer(c_int) :: mpi_err, mpi_status(MPI_STATUS_SIZE)
  integer(c_int) :: dir, proc, partition

  integer(c_int) :: Nsend, Nrecv

  integer(c_int) :: exportIndices(mesh%num_export_indices_periodic)
  integer(c_int) :: exportFaces(  mesh%num_export_indices_periodic)

  integer(c_int) :: importIndices(mesh%num_import_indices_periodic)
  integer(c_int) :: importFaces(mesh%num_import_indices_periodic)

  integer(c_int) :: sfi(P1)
  
  integer(c_int) :: index, tmp_index, bface_index, count, var, pnt
  integer(c_int) :: elem_index, face_index, local_face_index
  integer(c_int) :: findex_range(2)

  real(c_double) :: tmp_data(P1, Nvar)

  ! wait till all processors have valid values
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

  do dir = 1, 6
     
     first_pass: do proc = 0, numProcs-1
        avoid_self: if (proc .ne. myRank) then

           partition = proc + 1
        
           Nsend = mesh%nexport_per_part_periodic(partition, dir)
           Nrecv = mesh%nimport_per_part_periodic(partition, dir)

           ! send to lower ranked processors
           send_lo: if ( proc .lt. myRank .and. Nsend .gt. 0) then
              exportIndices(1:Nsend) = mesh%exportIndices_periodic(partition, dir, 1:Nsend)
              exportFaces(1:Nsend)   = mesh%exportFaces_periodic  (partition, dir, 1:Nsend)

              ! pack into send buffer
              get_send_buffer: do index = 1, Nsend
                 elem_index = exportIndices(index)
                 face_index = exportFaces(index)

                 local_face_index = mesh%faceindex2elemfaceindex(face_index,1)
                 sfi = mesh%sfi(:, local_face_index, elem_index)
                 
                 findex_range(1) = (local_face_index-1)*mesh%P1 + 1
                 findex_range(2) = local_face_index*mesh%P1

                 ! get the data for the face
                 tmp_data(:, :) = data_buffer_in(findex_range(1):findex_range(2), :, elem_index)
                
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       count = count + 1
                       sendbuf( (index-1)*P1*Nvar+count ) = tmp_data( sfi(pnt), var )
                    end do
                 end do

              end do get_send_buffer

              ! send the data
              call MPI_Send(sendbuf, Nsend*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_err)
              
           else if ( proc .gt. myRank .and. Nrecv .gt. 0 ) then
              importIndices(1:Nrecv) = mesh%importIndices_periodic(partition, dir, 1:Nrecv)
              importFaces(1:Nrecv)   = mesh%importFaces_periodic(  partition, dir, 1:Nrecv)

              ! receive the data
              call MPI_Recv(recvbuf, Nrecv*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the recieved data
              do index = 1, Nrecv
                 elem_index = importIndices(index)
                 face_index = importFaces(index)

                 do tmp_index = 1, mesh%Nbfaces
                    if (mesh%bfaces(tmp_index) .eq. face_index) then
                       bface_index = tmp_index
                       exit
                    end if
                 end do
                 
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       count = count + 1
                       data_buffer_out(pnt, var, bface_index) = &
                            recvbuf( (index-1)*P1*Nvar+count )
                    end do
                 end do
              end do

           end if send_lo
           
        end if avoid_self
     end do first_pass

     second_pass: do proc = 0, numProcs-1
        if (proc .ne. myRank) then
           partition = proc + 1

           Nsend = mesh%nexport_per_part_periodic(partition, dir)
           Nrecv = mesh%nimport_per_part_periodic(partition, dir)

           if ( proc .gt. myRank .and. Nsend .gt. 0 ) then
              exportIndices(1:Nsend) = mesh%exportIndices_periodic(partition, dir, 1:Nsend)
              exportFaces(1:Nsend)   = mesh%exportFaces_periodic  (partition, dir, 1:Nsend)

              ! pack into send buffer
              do index = 1, Nsend
                 elem_index = exportIndices(index)
                 face_index = exportFaces(index)

                 local_face_index = mesh%faceindex2elemfaceindex(face_index,1)
                 sfi = mesh%sfi(:, local_face_index, elem_index)
                 
                 findex_range(1) = (local_face_index-1)*P1+1
                 findex_range(2) = local_face_index*P1

                 ! get the data for the face
                 tmp_data(:, :) = data_buffer_in(findex_range(1):findex_range(2), :, elem_index)
                
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       count = count + 1
                       sendbuf( (index-1)*P1*Nvar+count ) = tmp_data( sfi(pnt), var )
                    end do
                 end do
              end do
              
              ! send the data
              call MPI_Send(sendbuf, Nsend*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_err)

           else if (proc .lt. myRank .and. Nrecv .gt. 0) then
              importIndices(1:Nrecv) = mesh%importIndices_periodic(partition, dir, 1:Nrecv)
              importFaces(1:Nrecv)   = mesh%importFaces_periodic(  partition, dir, 1:Nrecv)

              ! receive the data
              call MPI_Recv(recvbuf, Nrecv*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the recieved data
              do index = 1, Nrecv
                 elem_index = importIndices(index)
                 face_index = importFaces(index)

                 do tmp_index = 1, mesh%Nbfaces
                    if (mesh%bfaces(tmp_index) .eq. face_index) then
                       bface_index = tmp_index
                       exit
                    end if
                 end do
                 
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       count = count + 1
                       data_buffer_out(pnt, var, bface_index) = &
                            recvbuf( (index-1)*P1*Nvar+count )
                    end do
                 end do
              end do

           end if
        end if
     end do second_pass

  end do

  ! wait till all processors have exchanged periodic values
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

end subroutine exchange_periodic_data

subroutine exchange_periodic_flux_point_gradients(&
     Nflux, P1, Nvar, mesh, &
     ux_flux, uy_flux, uz_flux, &
     bface_ux, bface_uy, bface_uz, &
     tag, sendbuf, recvbuf, myRank, numProcs, mpi_data_type)

  integer(c_int),   intent(in   ) :: Nflux, P1, Nvar, myRank, numProcs, mpi_data_type, tag
  type(mesh2d),     intent(inout) :: mesh
  real(c_double),   intent(in)    :: ux_flux(Nflux, Nvar, mesh%Nelements)
  real(c_double),   intent(in)    :: uy_flux(Nflux, Nvar, mesh%Nelements)
  real(c_double),   intent(in)    :: uz_flux(Nflux, Nvar, mesh%Nelements)
  real(c_double),   intent(inout) :: bface_ux(P1, Nvar, mesh%Nbfaces)
  real(c_double),   intent(inout) :: bface_uy(P1, Nvar, mesh%Nbfaces)
  real(c_double),   intent(inout) :: bface_uz(P1, Nvar, mesh%Nbfaces)
  real(c_double),   intent(inout) :: sendbuf(:)
  real(c_double),   intent(inout) :: recvbuf(:)

  ! locals
  integer(c_int) :: mpi_err, mpi_status(MPI_STATUS_SIZE)
  integer(c_int) :: dir, proc, partition

  integer(c_int) :: Nsend, Nrecv

  integer(c_int) :: exportIndices(mesh%num_export_indices_periodic)
  integer(c_int) :: exportFaces(  mesh%num_export_indices_periodic)

  integer(c_int) :: importIndices(mesh%num_import_indices_periodic)
  integer(c_int) :: importFaces(mesh%num_import_indices_periodic)

  integer(c_int) :: sfi(P1)
  
  integer(c_int) :: index, tmp_index, bface_index, count, var, pnt
  integer(c_int) :: elem_index, face_index, local_face_index
  integer(c_int) :: findex_range(2)

  real(c_double) :: tmp_data_x(P1, Nvar)
  real(c_double) :: tmp_data_y(P1, Nvar)
  real(c_double) :: tmp_data_z(P1, Nvar)

  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

  do dir = 1, 6
     
     first_pass: do proc = 0, numProcs-1
        avoid_self: if (proc .ne. myRank) then

           partition = proc + 1
        
           Nsend = mesh%nexport_per_part_periodic(partition, dir)
           Nrecv = mesh%nimport_per_part_periodic(partition, dir)

           ! send to lower ranked processors
           send_lo: if ( proc .lt. myRank .and. Nsend .gt. 0) then
              exportIndices(1:Nsend) = mesh%exportIndices_periodic(partition, dir, 1:Nsend)
              exportFaces(1:Nsend)   = mesh%exportFaces_periodic  (partition, dir, 1:Nsend)

              ! pack into send buffer
              get_send_buffer: do index = 1, Nsend
                 elem_index = exportIndices(index)
                 face_index = exportFaces(index)

                 local_face_index = mesh%faceindex2elemfaceindex(face_index,1)
                 sfi = mesh%sfi(:, local_face_index, elem_index)
                 
                 findex_range(1) = (local_face_index-1)*mesh%P1 + 1
                 findex_range(2) = local_face_index*mesh%P1

                 ! get the data for the face
                 tmp_data_x(:, :) = ux_flux(findex_range(1):findex_range(2), :, elem_index)
                 tmp_data_y(:, :) = uy_flux(findex_range(1):findex_range(2), :, elem_index)
                 tmp_data_z(:, :) = uz_flux(findex_range(1):findex_range(2), :, elem_index)
                
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       !count = count + 1
                       !sendbuf( (index-1)*P1*Nvar+count ) = tmp_data( sfi(pnt), var )
                       
                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_x( sfi(pnt), var )
                       
                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_y( sfi(pnt), var )

                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_z( sfi(pnt), var )

                    end do
                 end do

              end do get_send_buffer

              ! send the data
              call MPI_Send(sendbuf, 3*Nsend*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_err)
              
           else if ( proc .gt. myRank .and. Nrecv .gt. 0 ) then
              importIndices(1:Nrecv) = mesh%importIndices_periodic(partition, dir, 1:Nrecv)
              importFaces(1:Nrecv)   = mesh%importFaces_periodic(  partition, dir, 1:Nrecv)

              ! receive the data
              call MPI_Recv(recvbuf, 3*Nrecv*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the recieved data
              do index = 1, Nrecv
                 elem_index = importIndices(index)
                 face_index = importFaces(index)

                 do tmp_index = 1, mesh%Nbfaces
                    if (mesh%bfaces(tmp_index) .eq. face_index) then
                       bface_index = tmp_index
                       exit
                    end if
                 end do
                 
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       ! count = count + 1
                       ! data_buffer_out(pnt, var, bface_index) = recvbuf( (index-1)*P1*Nvar+count )

                       count = count + 1
                       bface_ux(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )

                       count = count + 1
                       bface_uy(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )

                       count = count + 1
                       bface_uz(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )
                            
                    end do
                 end do
              end do

           end if send_lo
           
        end if avoid_self
     end do first_pass

     second_pass: do proc = 0, numProcs-1
        if (proc .ne. myRank) then
           partition = proc + 1

           Nsend = mesh%nexport_per_part_periodic(partition, dir)
           Nrecv = mesh%nimport_per_part_periodic(partition, dir)

           if ( proc .gt. myRank .and. Nsend .gt. 0 ) then
              exportIndices(1:Nsend) = mesh%exportIndices_periodic(partition, dir, 1:Nsend)
              exportFaces(1:Nsend)   = mesh%exportFaces_periodic  (partition, dir, 1:Nsend)

              ! pack into send buffer
              !$omp parallel do private(index, elem_index, face_index, local_face_index, sfi, findex_range, &
              !$omp                     tmp_data_x, tmp_data_y, tmp_data_z, count, var, pnt) &
              !$omp schedule(dynamic,2)
              do index = 1, Nsend
                 elem_index = exportIndices(index)
                 face_index = exportFaces(index)

                 local_face_index = mesh%faceindex2elemfaceindex(face_index,1)
                 sfi = mesh%sfi(:, local_face_index, elem_index)
                 
                 findex_range(1) = (local_face_index-1)*P1+1
                 findex_range(2) = local_face_index*P1

                 ! get the data for the face
                 tmp_data_x(:, :) = ux_flux(findex_range(1):findex_range(2), :, elem_index)
                 tmp_data_y(:, :) = uy_flux(findex_range(1):findex_range(2), :, elem_index)
                 tmp_data_z(:, :) = uz_flux(findex_range(1):findex_range(2), :, elem_index)
                
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_x( sfi(pnt), var )
                       
                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_y( sfi(pnt), var )

                       count = count + 1
                       sendbuf( (index-1)*3*P1*Nvar+count ) = tmp_data_z( sfi(pnt), var )
                    end do
                 end do

              end do
              !$omp end parallel do
              
              ! send the data
              call MPI_Send(sendbuf, 3*Nsend*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_err)

           else if (proc .lt. myRank .and. Nrecv .gt. 0) then
              importIndices(1:Nrecv) = mesh%importIndices_periodic(partition, dir, 1:Nrecv)
              importFaces(1:Nrecv)   = mesh%importFaces_periodic(  partition, dir, 1:Nrecv)

              ! receive the data
              call MPI_Recv(recvbuf, 3*Nrecv*P1*Nvar, MPI_DATA_TYPE, proc, tag, &
                            MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the recieved data
              do index = 1, Nrecv
                 elem_index = importIndices(index)
                 face_index = importFaces(index)

                 do tmp_index = 1, mesh%Nbfaces
                    if (mesh%bfaces(tmp_index) .eq. face_index) then
                       bface_index = tmp_index
                       exit
                    end if
                 end do
                 
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, P1
                       ! count = count + 1
                       ! data_buffer_out(pnt, var, bface_index) = recvbuf( (index-1)*P1*Nvar+count )

                       count = count + 1
                       bface_ux(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )

                       count = count + 1
                       bface_uy(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )

                       count = count + 1
                       bface_uz(pnt, var, bface_index) = recvbuf( (index-1)*3*P1*Nvar+count )
                            
                    end do
                 end do

              end do

           end if
        end if
     end do second_pass

  end do

  ! wait till all processors have exchanged periodic values
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

end subroutine exchange_periodic_flux_point_gradients

subroutine exchange_data(Np, Nvar, mesh, data_buffer, tag, sendbuf, recvbuf, myRank, numProcs,&
                         mpi_data_type)
  integer(c_int),   intent(in   ) :: Np, Nvar, tag, myRank, numProcs, mpi_data_type
  type(mesh2d),     intent(inout) :: mesh
  real(c_double),   intent(inout) :: data_buffer(Np, Nvar, mesh%Nelements)
  real(c_double),   intent(inout) :: sendbuf(Np*Nvar*mesh%max_numExport)
  real(c_double),   intent(inout) :: recvbuf(Np*Nvar*mesh%max_numImport)

  !locals
  integer(c_int) :: mpi_err, mpi_status(MPI_STATUS_SIZE)
  integer(c_int) :: index, pnt, var, elem_index, count
  integer(c_int) :: i, partition, Nsend, Nrecv

  integer(c_int) :: local_index_start

  integer(c_int) :: exportIndices(mesh%max_numExport)

  ! wait till all processors have valid values
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! first pass. Send low, recieve high
  do i = 0, numProcs-1
     partition = i + 1
     if ( i .ne. myRank ) then
        
        ! send data to all lower id processors
        if ( i .lt. myRank ) then
           Nsend = mesh%Nexport_per_part(partition)
           if (Nsend .gt. 0) then
              exportIndices = mesh%exportIndices(partition, :)
              
              !$omp parallel do private(index, elem_index, count, var, pnt)
              do index = 1, Nsend
                 elem_index = exportIndices(index)

                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, Np
                       count = count + 1
                       
                       sendbuf((index-1)*Np*Nvar+count) = &
                            data_buffer(pnt, var, elem_index)
                    end do
                 end do
              end do
              !$omp end parallel do

              !print*, 'SEND1', myRank, Nsend, i, Nsend*mesh%Nflux*mesh%Nvar
              call MPI_Send(sendbuf, Nsend*Np*Nvar, MPI_DATA_TYPE, i, tag, &
                           MPI_COMM_WORLD, mpi_err)
           end if

        else
           ! recieve data from higher ranked processors
           Nrecv = mesh%Nimport_per_part(partition)

           if (Nrecv .gt. 0) then
              !print*, 'RECV1', myRank, Nrecv, i, Nrecv*mesh%Nflux*mesh%Nvar
              call MPI_Recv(recvbuf, Nrecv*Np*Nvar, MPI_DATA_TYPE, i, tag, &
                   MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the received buffer into the local mesh data structure
              local_index_start = mesh%importShifts(partition)
              
              !$omp parallel do private(index, count, var, pnt)
              do index = 1, Nrecv
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, Np
                       count = count + 1
                       data_buffer(pnt, var, local_index_start+index) = &
                            recvbuf((index-1)*Np*Nvar+count)
                    end do
                 end do
              end do
              !$omp end parallel do

           end if
        end if
     end if
  end do

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Second pass. Send high, recieve low
  do i = 0, numProcs-1
     partition = i + 1
     if (i .ne. myRank) then

        ! send data to all higher-ranked processors
        if (i .gt. myRank) then
           Nsend = mesh%Nexport_per_part(partition)

           if (Nsend .gt. 0) then
              exportIndices = mesh%exportIndices(partition, :)

              !$omp parallel do private(index, elem_index, count, var, pnt)
              do index = 1, Nsend
                 elem_index = exportIndices(index)

                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, Np
                       count = count + 1
                       
                       sendbuf((index-1)*Np*Nvar+count) = &
                            data_buffer(pnt, var, elem_index)
                    end do
                 end do
              end do
              !$omp end parallel do

              !print*, 'SEND2', myRank, Nsend, i, Nsend*mesh%Nflux*mesh%Nvar
              call MPI_Send(sendbuf, Nsend*Np*Nvar, MPI_DATA_TYPE, i, tag, &
                   MPI_COMM_WORLD, mpi_err)
           end if

        else
           ! recieve data from lower-ranked processors
           Nrecv = mesh%Nimport_per_part(partition)

           if (Nrecv .gt. 0) then
              !print*, 'RECV2', myRank, Nrecv, i, Nrecv*mesh%Nflux*mesh%Nvar
              call MPI_Recv(recvbuf, Nrecv*Np*Nvar, MPI_DATA_TYPE, i, tag, &
                   MPI_COMM_WORLD, mpi_status, mpi_err)

              ! pack the received buffer into the local mesh data structure
              local_index_start = mesh%importShifts(partition)

              !$omp parallel do private(index, count, var, pnt)
              do index = 1, Nrecv
                 count = 0
                 do var = 1, Nvar
                    do pnt = 1, Np
                       count = count + 1
                       data_buffer(pnt, var, local_index_start+index) = &
                            recvbuf((index-1)*Np*Nvar+count)
                    end do
                 end do
              end do
              !$omp end parallel do

           end if

        end if
     end if
  end do

  ! wait till all processors have done sharing
  call MPI_Barrier(MPI_COMM_WORLD, mpi_err)

end subroutine exchange_data

end module parallel_module
