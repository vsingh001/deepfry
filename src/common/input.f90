module input_module
  use iso_c_binding
  use deepfry_constants_module

  implicit none
       
  private

  integer(c_int),     save, public :: EqnSystem = 1

  logical,            save, public :: debug = .true.
  integer(c_int),     save, public :: MESH_ELEM_TYPE = 3

  character(len=256), save, public :: plot_dirname  = "test_output"
  character(len=256), save, public :: plot_filename = "test"
  character(len=256), save, public :: mesh_filename = ""
  character(len=256), save, public :: mesh_file_extn = ".hdf5"

  logical,            save, public :: writestats    = .false.
  character(len=256), save, public :: stats_filename = ""
  integer(c_int),     save, public :: n_userstats = 0

  real(c_double),     save, public :: gamma    = 1.4_c_double
  real(c_double),     save, public :: Cp       = 1005_c_double
  real(c_double),     save, public :: Pr       = 0.72_c_double

  ! FR order and solution point specification
  integer(c_int),     save, public :: nvariables = 4
  integer(c_int),     save, public :: solution_order = 1
  integer(c_int),     save, public :: solution_nodes = 1
  integer(c_int),     save, public :: ESFR_ctype = 1

  ! Filter parameters
  integer(c_int),     save, public :: filter_int = -1
  real(c_double),     save, public :: filter_cutoff_percentage = 50_c_double
  real(c_double),     save, public :: alpha_filter = 36_c_double
  real(c_double),     save, public :: s_filter     = 16_c_double

  ! Problem specific inputs
  real(c_double),     save, public :: eps = FIVE
  real(c_double),     save, public :: vmax = ONE

  ! boundary conditions


  ! time step and I/O controls
  integer(c_int),     save, public :: max_step = 100000000
  real(c_double),     save, public :: stop_time = -ONE
  real(c_double),     save, public :: cfl = HALF
  real(c_double),     save, public :: fixed_dt = -ONE
  real(c_double),     save, public :: max_dt   = 1.D-4

  integer(c_int),     save, public :: plot_int = 50
  real(c_double),     save, public :: plot_deltat = ZERO
  logical,            save, public :: visit_output = .false.
  logical,            save, public :: vtk_output   = .false.
  integer(c_int),     save, public :: vtk_resolution = 2
  logical,            save, public :: plot_conservative = .true.
  logical,            save, public :: plot_gradients = .true.
  integer,            save, public :: vtk_plot_resolution=2
  
  integer(c_int),     save, public :: restart_int = 10000
  integer(c_int),     save, public :: restart = -1

  logical,            save, public :: verbose = .true.
  logical,            save, public :: timings = .false.

  ! Linked list parameters
  !real(c_double),     save, public :: linked_list_dx = TENTH
  integer(c_int),     save, public :: linked_list_max_elem_list_size = 4096

  ! CNS Solver parameters
  real(c_double),     save, public :: Machno = 0.2
  real(c_double),     save, public :: mu     = TENTH

  real(c_double),     save, public :: vdiff  = ZERO
  real(c_double),     save, public :: ldg_beta   = HALF
  real(c_double),     save, public :: ldg_tau    = TENTH

  ! Boundary face markers
  integer(c_int),     save, public :: inlet_marker
  integer(c_int),     save, public :: outlet_marker
  integer(c_int),     save, public :: wall_marker
  integer(c_int),     save, public :: zerogradient_marker
  integer(c_int),     save, public :: reflective_wall_marker
  integer(c_int),     save, public :: supersonic_outflow_marker
  integer(c_int),     save, public :: periodic_lox_marker
  integer(c_int),     save, public :: periodic_loy_marker
  integer(c_int),     save, public :: periodic_loz_marker
  integer(c_int),     save, public :: periodic_hix_marker
  integer(c_int),     save, public :: periodic_hiy_marker
  integer(c_int),     save, public :: periodic_hiz_marker

  ! periodic bcs
  logical,            save, public :: is_periodic_x = .false.
  logical,            save, public :: is_periodic_y = .false.
  logical,            save, public :: is_periodic_z = .false.

  logical,            save, public :: df_is_periodic = .false.
  logical,            save, public :: extend_domain = .false.

  integer(c_int),     save, public :: left_marker
  integer(c_int),     save, public :: right_marker
  integer(c_int),     save, public :: top_marker
  integer(c_int),     save, public :: bottom_marker
  integer(c_int),     save, public :: front_marker
  integer(c_int),     save, public :: back_marker
  ! Inlet values
  real(c_double),     save, public :: rho_inlet = ONE
  real(c_double),     save, public :: velx_inlet = ZERO
  real(c_double),     save, public :: vely_inlet = ZERO
  real(c_double),     save, public :: velz_inlet = ZERO
  real(c_double),     save, public :: p_inlet = ONE

  real(c_double),     save, public :: velx_ampl = ONE
  real(c_double),     save, public :: velx_freq = ZERO
  real(c_double),     save, public :: velx_angle = ZERO
  logical       ,     save, public :: unsteady_inlet = .false.

  ! Limiter & shock sensor parameters
  real(c_double),     save, public :: mlp_deactivating_threshold = 0.001_c_double
  integer(c_int),     save, public :: shock_sensor_type = 0
  real(c_double),     save, public :: mnp_sensor_kh = THREE/TWO

  integer(c_int),     save, public :: limiter_type = 0
  integer(c_int),     save, public :: MAX_SVJ = 128
  logical,            save, public :: hierarchial_limiting = .true.
  real(c_double),     save, public :: limiter_scaling_factor = ONE
  logical,            save, public :: use_shock_sensor_as_marker = .false.

  integer(c_int),     save, public :: initial_limit_nsteps = 0
  real(c_double),     save, public :: avisc = ZERO
  real(c_double),     save, public :: avisc_min = 0.0001_c_double
  real(c_double),     save, public :: avisc_alpha = 10000.0_c_double
  real(c_double),     save, public :: avisc_beta  = 0.01_c_double

  ! parameters for performance optimization etc
  logical,            save, public :: cuBLAS    = .false.
  integer(c_int),     save, public :: chunksize = 8_c_int

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Multiphase model alternative parameters
  real(c_double),     save, public :: gamma_gas, gamma_liq
  real(c_double),     save, public :: pinf_gas,  pinf_liq
  real(c_double),     save, public :: q_gas, q_liq
  real(c_double),     save, public :: cv_gas, cv_liq

  logical,            save, public :: relaxation = .true.

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! BoxLib AMR parameters
  integer,            save, public :: dm_in

  real(c_double),     save, public :: prob_lo_x = -ONE
  real(c_double),     save, public :: prob_lo_y = +ONE
  real(c_double),     save, public :: prob_hi_x = -ONE
  real(c_double),     save, public :: prob_hi_y = +ONE
  real(c_double),     save, public :: prob_lo_z = ZERO
  real(c_double),     save, public :: prob_hi_z = ZERO
  
  real(c_double), allocatable, save, public :: prob_lo(:)
  real(c_double), allocatable, save, public :: prob_hi(:)

  integer(c_int), save, public :: max_levs = 1
  integer(c_int), save, public :: max_grid_size = 64
  integer(c_int), save, public :: max_grid_size_1 = -1
  integer(c_int), save, public :: max_grid_size_2 = -1
  integer(c_int), save, public :: max_grid_size_3 = -1
  integer(c_int), save, public :: regrid_int = -1
  integer(c_int), save, public :: amr_buf_width = 2
  integer(c_int), save, public :: ref_ratio = 2
  integer(c_int), save, public :: n_cellx = -1
  integer(c_int), save, public :: n_celly = -1
  integer(c_int), save, public :: n_cellz = -1

  ! 24 corresponds to Boxlib's first order extrapolation
  integer(c_int),     save, public :: bc_x_lo = 24
  integer(c_int),     save, public :: bc_x_hi = 24
  integer(c_int),     save, public :: bc_y_lo = 24
  integer(c_int),     save, public :: bc_y_hi = 24
  integer(c_int),     save, public :: bc_z_lo = 24
  integer(c_int),     save, public :: bc_z_hi = 24

  integer(c_int),     save, public :: minwidth = 8
  integer(c_int),     save, public :: blocking_factor = 8
  real(c_double),     save, public :: min_eff = NINE/TEN
  
  logical,            save, public :: the_knapsack_verbosity = .false.
  integer(c_int),     save, public :: the_layout_verbosity = 0
  integer(c_int),     save, public :: the_sfc_threshold = 5
  integer(c_int),     save, public :: the_ml_layout_strategy = 0
  integer(c_int),     save, public :: the_copy_cache_max = 128

  integer(c_int),     save, public :: nOutFiles = 64
  logical,            save, public :: lUsingNFiles = .true.
  
end module input_module

module runtime_init_module
  use deepfry_constants_module
  use input_module
  implicit none

  namelist /probin/ EqnSystem
  namelist /probin/ debug

  namelist /probin/ MESH_ELEM_TYPE

  namelist /probin/ plot_dirname
  namelist /probin/ plot_filename
  namelist /probin/ mesh_filename
  namelist /probin/ mesh_file_extn

  namelist /probin/ writestats
  namelist /probin/ stats_filename
  namelist /probin/ n_userstats

  namelist /probin/ gamma
  namelist /probin/ Cp
  namelist /probin/ Pr

  namelist /probin/ prob_lo_x
  namelist /probin/ prob_lo_y
  namelist /probin/ prob_hi_x
  namelist /probin/ prob_hi_y
  namelist /probin/ prob_lo_z
  namelist /probin/ prob_hi_z

  namelist /probin/ bc_x_lo
  namelist /probin/ bc_x_hi
  namelist /probin/ bc_y_lo
  namelist /probin/ bc_y_hi
  namelist /probin/ bc_z_lo
  namelist /probin/ bc_z_hi

  namelist /probin/ max_levs
  namelist /probin/ max_grid_size
  namelist /probin/ max_grid_size_1
  namelist /probin/ max_grid_size_2
  namelist /probin/ max_grid_size_3
  namelist /probin/ regrid_int
  namelist /probin/ amr_buf_width
  namelist /probin/ ref_ratio
  namelist /probin/ n_cellx
  namelist /probin/ n_celly
  namelist /probin/ n_cellz

  namelist /probin/ minwidth
  namelist /probin/ blocking_factor
  namelist /probin/ min_eff
  namelist /probin/ the_knapsack_verbosity
  namelist /probin/ the_layout_verbosity
  namelist /probin/ the_sfc_threshold
  namelist /probin/ the_ml_layout_strategy
  namelist /probin/ the_copy_cache_max

  namelist /probin/ nOutFiles
  namelist /probin/ lUsingNFiles

  namelist /probin/ nvariables
  namelist /probin/ solution_order
  namelist /probin/ solution_nodes
  namelist /probin/ ESFR_ctype

  namelist /probin/ filter_int
  namelist /probin/ filter_cutoff_percentage
  namelist /probin/ alpha_filter
  namelist /probin/ s_filter

  namelist /probin/ eps
  namelist /probin/ vmax

  namelist /probin/ is_periodic_x
  namelist /probin/ is_periodic_y
  namelist /probin/ is_periodic_z
  namelist /probin/ df_is_periodic
  namelist /probin/ extend_domain

  namelist /probin/ max_step
  namelist /probin/ stop_time
  namelist /probin/ cfl
  namelist /probin/ fixed_dt
  namelist /probin/ max_dt

  namelist /probin/ plot_int
  namelist /probin/ plot_deltat

  namelist /probin/ visit_output
  namelist /probin/ vtk_output
  namelist /probin/ vtk_resolution
  namelist /probin/ plot_conservative
  namelist /probin/ plot_gradients
  namelist /probin/ vtk_plot_resolution

  namelist /probin/ restart_int
  namelist /probin/ restart

  namelist /probin/ verbose
  namelist /probin/ timings

  !namelist /probin/ linked_list_dx
  namelist /probin/ linked_list_max_elem_list_size

  namelist /probin/ Machno
  namelist /probin/ mu

  namelist /probin/ vdiff
  namelist /probin/ ldg_beta
  namelist /probin/ ldg_tau

  namelist /probin/ inlet_marker
  namelist /probin/ outlet_marker
  namelist /probin/ wall_marker
  namelist /probin/ zerogradient_marker
  namelist /probin/ reflective_wall_marker
  namelist /probin/ supersonic_outflow_marker

  namelist /probin/ periodic_lox_marker
  namelist /probin/ periodic_loy_marker
  namelist /probin/ periodic_loz_marker
  namelist /probin/ periodic_hix_marker
  namelist /probin/ periodic_hiy_marker
  namelist /probin/ periodic_hiz_marker

  namelist /probin/ left_marker
  namelist /probin/ right_marker
  namelist /probin/ top_marker
  namelist /probin/ bottom_marker
  namelist /probin/ front_marker
  namelist /probin/ back_marker

  namelist /probin/ rho_inlet
  namelist /probin/ velx_inlet
  namelist /probin/ vely_inlet
  namelist /probin/ velz_inlet
  namelist /probin/ p_inlet
  namelist /probin/ velx_ampl
  namelist /probin/ velx_freq
  namelist /probin/ velx_angle
  namelist /probin/ unsteady_inlet

  namelist /probin/ limiter_type
  namelist /probin/ mlp_deactivating_threshold
  namelist /probin/ MAX_SVJ
  namelist /probin/ hierarchial_limiting
  namelist /probin/ limiter_scaling_factor
  namelist /probin/ use_shock_sensor_as_marker

  namelist /probin/ shock_sensor_type
  namelist /probin/ mnp_sensor_kh

  namelist /probin/ initial_limit_nsteps
  namelist /probin/ avisc
  namelist /probin/ avisc_alpha
  namelist /probin/ avisc_beta
  namelist /probin/ avisc_min

  namelist /probin/ chunksize
  namelist /probin/ cuBLAS

  ! multi-phase model parameters
  namelist /probin/ gamma_gas
  namelist /probin/ gamma_liq
  namelist /probin/ pinf_gas
  namelist /probin/ pinf_liq
  namelist /probin/ q_gas
  namelist /probin/ q_liq
  namelist /probin/ cv_gas
  namelist /probin/ cv_liq

  namelist /probin/ relaxation

  public :: probin
  public :: runtime_init

contains

  subroutine runtime_init(rank)
    use iso_c_binding

    integer(c_int), intent(in) :: rank

    character(len=128) :: fname
    logical            :: lexist
    integer            :: narg, farg

    integer(c_int) :: dir

    narg = command_argument_count()
    
    farg = 1
    if (narg .ge. 1) then
       call get_command_argument(farg, value=fname)
       inquire(file=fname, exist=lexist)
       if (lexist) then
          farg = farg + 1
          open(unit=100, file=fname, status='old', action='read')
          read(unit=100, nml=probin)
          close(unit=100)
       else
          write(*, *) 'ERROR: inputs file does not exist'
       end if
    else
       write(*, *) 'ERROR: no inputs file specified'
    end if

    ! if (bc_x_lo .eq. -1 .and. bc_x_hi .eq. -1) then
    !    is_periodic_x = .true.
    ! end if

    ! if (bc_y_lo .eq. -1 .and. bc_y_hi .eq. -1) then
    !    is_periodic_y = .true.
    ! end if

    if ( (is_periodic_x .eqv. .true.) .or. &
         (is_periodic_y .eqv. .true.) .or. &
         (is_periodic_z .eqv. .true.) ) then
       df_is_periodic = .true.
    end if

    ! sanity checks on the input parameters
    if (use_shock_sensor_as_marker) then
       if (limiter_type .lt. 1) then
          print*, 'Warning: No limiter selected...', limiter_type
       end if

       if (shock_sensor_type .lt. 1) then
          print*, 'Warning: No shock sensor selected', shock_sensor_type
       end if
    end if

    ! save the problem dimensionality
    if (MESH_ELEM_TYPE .eq. QUADS .or. MESH_ELEM_TYPE .eq. TRIANGLES) then
       dm_in = 2
    else if(MESH_ELEM_TYPE .eq. HEXS .or. MESH_ELEM_TYPE .eq. TETS) then
       dm_in = 3
    end if

    ! initialize prob_lo and probl_hi
    allocate(prob_lo(dm_in))
    allocate(prob_hi(dm_in))
    prob_lo(1) = prob_lo_x
    prob_lo(2) = prob_lo_y

    prob_hi(1) = prob_hi_x
    prob_hi(2) = prob_hi_y
    
    if (dm_in .eq. 3) then
       prob_lo(3) = prob_lo_z
       prob_hi(3) = prob_hi_z
    end if

    if ( (visit_output .and. vtk_output) .and. (rank .eq. 0) ) then
       print*, 'Choose either VTK or VISIT output...'
       stop
    end if


#ifdef BOXLIB

    if (max_grid_size_1 .eq. -1) then
       max_grid_size_1 = max_grid_size
    end if

    if (max_grid_size_2 .eq. -1) then
       max_grid_size_2 = max_grid_size
    end if

    if (max_grid_size_3 .eq. -1) then
       max_grid_size_3 = max_grid_size
    end if

#endif
    
  end subroutine runtime_init

  subroutine runtime_close()

    deallocate(prob_lo)
    deallocate(prob_hi)

  end subroutine runtime_close

end module runtime_init_module
