module vtk_headers_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use input_module
  implicit none


  type vtk_mesh_header_t
     integer(c_int) :: PlotNumberOfCells, PlotNumberOfPoints

     character(len=256) :: PlotConnectivity, PlotOffsets, PlotEtype
     real(c_double), allocatable :: r_interp(:), s_interp(:), t_interp(:)

  end type vtk_mesh_header_t

contains

  subroutine allocate_vtk_header(vtk_header)
    type(vtk_mesh_header_t), intent(inout) :: vtk_header

    if (MESH_ELEM_TYPE .eq. HEXS) then
       vtk_header%PlotConnectivity  = "0 1 2 3 4 5 6 7"
       vtk_header%PlotEtype         = "12"
       vtk_header%PlotOffsets       = "8"

       vtk_header%PlotNumberOfCells = 1
       vtk_header%PlotNumberOfPoints = 8

       allocate(vtk_header%r_interp(8))
       allocate(vtk_header%s_interp(8))
       allocate(vtk_header%t_interp(8))
       
       ! the location of the plot points in reference space
       vtk_header%r_interp = (/ -ONE, ONE, ONE, -ONE, -ONE, ONE, ONE, -ONE /)
       vtk_header%s_interp = (/ -ONE, -ONE, ONE, ONE, -ONE, -ONE, ONE, ONE /)
       vtk_header%t_interp = (/ -ONE, -ONE, -ONE, -ONE, ONE, ONE, ONE, ONE /)

    else if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       if (vtk_resolution .eq. 2) then
          vtk_header%PlotConnectivity  = "0 1 2"
          vtk_header%PlotEtype         = "5"
          vtk_header%PlotOffsets       = "3"
          
          vtk_header%PlotNumberOfCells  = 1
          vtk_header%PlotNumberOfPoints = 3

          allocate(vtk_header%r_interp(3))
          allocate(vtk_header%s_interp(3))
          allocate(vtk_header%t_interp(3))
       
          ! the location of the plot points in reference space
          vtk_header%r_interp = (/ -ONE, ONE, -ONE /)
          vtk_header%s_interp = (/ -ONE, -ONE, ONE /)
          vtk_header%t_interp = ZERO

       else
          vtk_header%PlotConnectivity  = "0 1 3 1 2 4 3 4 5 1 4 3"
          vtk_header%PlotEtype         = "5 5 5 5"
          vtk_header%PlotOffsets       = "3 6 9 12"
          
          vtk_header%PlotNumberOfCells  = 4
          vtk_header%PlotNumberOfPoints = 6

          allocate(vtk_header%r_interp(6))
          allocate(vtk_header%s_interp(6))
          allocate(vtk_header%t_interp(6))
       
          ! the location of the plot points in reference space
          vtk_header%r_interp = (/ -ONE, ZERO, ONE, -ONE, ZERO, -ONE /)
          vtk_header%s_interp = (/ -ONE, -ONE, -ONE, ZERO, ZERO, ONE /)
          vtk_header%t_interp = ZERO
          
       end if

    else if (MESH_ELEM_TYPE .eq. QUADS) then
       if (vtk_resolution .eq. 2) then
          vtk_header%PlotConnectivity  = "0 1 2 3"
          vtk_header%PlotEtype         = "9"
          vtk_header%PlotOffsets       = "4"

          vtk_header%PlotNumberOfCells  = 1
          vtk_header%PlotNumberOfPoints = 4
          
          allocate(vtk_header%r_interp(4))
          allocate(vtk_header%s_interp(4))
          allocate(vtk_header%t_interp(4))
       
          ! the location of the plot points in reference space
          vtk_header%r_interp = (/ -ONE, ONE, ONE, -ONE /)
          vtk_header%s_interp = (/ -ONE, -ONE, ONE, ONE /)
          vtk_header%t_interp = ZERO

       else if (vtk_resolution .eq. 3) then
          vtk_header%PlotConnectivity  = "0 1 4 3 1 2 5 4 3 4 7 6 4 5 8 7"
          vtk_header%PlotEtype         = "9 9 9 9"
          vtk_header%PlotOffsets       = "4 8 12 16"

          vtk_header%PlotNumberOfCells  = 4
          vtk_header%PlotNumberOfPoints = 9
          
          allocate(vtk_header%r_interp(9))
          allocate(vtk_header%s_interp(9))
          allocate(vtk_header%t_interp(9))
       
          ! the location of the plot points in reference space
          vtk_header%r_interp = (/ -ONE, ZERO, ONE, -ONE, ZERO, ONE, -ONE, ZERO, ONE /)
          vtk_header%s_interp = (/ -ONE, -ONE, -ONE, ZERO, ZERO, ZERO, ONE, ONE, ONE /)
          vtk_header%t_interp = ZERO

       else if (vtk_resolution .eq. 4) then
          vtk_header%PlotConnectivity  = "0 1 5 4 1 2 6 5 2 3 7 6 4 5 9 8 5 6 10 9 6 7 11 10 8 9 13 12 9 10 14 13 10 11 15 14"
          vtk_header%PlotEtype         = "9 9 9 9 9 9 9 9 9"
          vtk_header%PlotOffsets       = "4 8 12 16 20 24 28 32 36"

          vtk_header%PlotNumberOfCells  = 9
          vtk_header%PlotNumberOfPoints = 16
          
          allocate(vtk_header%r_interp(16))
          allocate(vtk_header%s_interp(16))
          allocate(vtk_header%t_interp(16))
       
          ! the location of the plot points in reference space
          vtk_header%r_interp = (/ -1., -0.5, 0.5, 1., -1., -0.5, 0.5, 1., -1., -0.5, 0.5, 1., -1., -0.5, 0.5, 1./)
          vtk_header%s_interp = (/ -1., -1., -1., -1., -0.5, -0.5, -0.5, -0.5, 0.5, 0.5, 0.5, 0.5, 1., 1., 1., 1./)
          vtk_header%t_interp = ZERO

       end if

    end if
  end subroutine allocate_vtk_header

  subroutine deallocate_vtk_header(vtk_header)
    type(vtk_mesh_header_t), intent(inout) :: vtk_header

    deallocate(vtk_header%r_interp)
    deallocate(vtk_header%s_interp)
    deallocate(vtk_header%t_interp)
    
  end subroutine deallocate_vtk_header

end module vtk_headers_module
