f90sources += deepfry_constants.f90
f90sources += polynomials.f90
f90sources += mesh.f90
f90sources += make_mesh.f90
f90sources += df_parallel.f90
f90sources += hdf5_writer.f90
f90sources += timings.f90
f90sources += restart.f90
f90sources += linkedlist.f90
f90sources += input.f90
f90sources += sort.f90
f90sources += libfr.f90
f90sources += userbc.f90
f90sources += vtk.f90
f90sources += logo.f90

ifdef CUDA
f90sources += gpu.f90
endif