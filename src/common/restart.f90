module restart_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module

  use input_module, only: MESH_ELEM_TYPE
  use mesh_module
  use fieldvar_module

  use hdf5

  implicit none

contains
  
  subroutine write_restart_file(filename, mesh, data, step, time)
    character(len=256), intent(in)  :: filename
    type(mesh2d),      intent(in) :: mesh
    type(FieldData_t), intent(in) :: data
    integer(c_int), intent(in) :: step
    real(c_double), intent(in) :: time
    
    ! locals
    integer :: error, rank

    integer(HSIZE_T) :: dims(1)
    integer(HID_T)   :: file_id
    integer(HID_T)   :: dset_id
    integer(HID_T)   :: dspace_id    

    character(len=3), parameter :: dset_rho = "rho"
    character(len=2), parameter :: dset_Mx  = "Mx"
    character(len=2), parameter :: dset_My  = "My"
    character(len=2), parameter :: dset_Mz  = "Mz"
    character(len=1), parameter :: dset_E   = "E"

    integer(SIZE_T) :: attrlen
    integer(HID_T)  :: atype_id, attr_id, aspace_id

    character(len=5),  parameter :: dset_attributes = "attrs"
    character(len=10), parameter :: dset_attr_names = "attr_names"

    character(len=256), dimension(5) :: attr_names
    real(c_double),     dimension(5) :: attr_data

    ! set the dimensionality for the data
    dims(1) = mesh%Np*mesh%Nelements

    ! Initialize the fortran interface
    call h5open_f(error)

    ! Create a new file
    call h5fcreate_f(trim(filename), H5F_ACC_TRUNC_F, file_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! create the memory (data) space for the data
    rank = 1
    call h5screate_simple_f(rank, dims, dspace_id, error)
    
    ! Create the individual datasets
    call h5dcreate_f(file_id, dset_rho, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 1, :), dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_Mx, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 2, :), dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_My, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 3, :), dims, error)
    call h5dclose_f(dset_id, error)

#ifdef DIM3
    call h5dcreate_f(file_id, dset_Mz, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 4, :), dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_E, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 5, :), dims, error)
    call h5dclose_f(dset_id, error)
#else
    call h5dcreate_f(file_id, dset_E, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 4, :), dims, error)
    call h5dclose_f(dset_id, error)
#endif

    ! Terminate access to the data space
    call h5sclose_f(dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write out attributes

    rank    = 1
    dims(1) = 5

    attr_data(1) = dble(step)
    attr_data(2) = time
    attr_data(3) = dble(mesh%P)
    attr_data(4) = dble(mesh%Nelements)
    attr_data(5) = dble(MESH_ELEM_TYPE)
    call h5screate_simple_f(rank, dims, dspace_id, error)
    call h5dcreate_f(file_id, dset_attributes, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, attr_data, dims, error)

    attr_names(1) = "step"
    attr_names(2) = "time"
    attr_names(3) = "order"
    attr_names(4) = "nelements"
    attr_names(5) = "elem_type"
    attrlen = 256

    call h5screate_simple_f(rank, dims, aspace_id, error)
    call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
    call h5tset_size_f(atype_id, attrlen, error)
    call h5acreate_f(dset_id, dset_attr_names, atype_id, aspace_id, attr_id, error)
    call h5awrite_f(attr_id, atype_id, attr_names, dims, error)
    call h5aclose_f(attr_id, error)
    call h5sclose_f(aspace_id, error)

    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! close the file
    call h5fclose_f(file_id, error)

    ! close the fortran interface
    call h5close_f(error)

  end subroutine write_restart_file

  subroutine read_restart_file(filename, mesh, data, step, time)
    character(len=256),  intent(in   ) :: filename
    type(mesh2d), intent(inout)        :: mesh
    type(FieldData_t),        intent(inout) :: data 
    integer(c_int), intent(out)        :: step
    real(c_double), intent(out)        :: time

    ! locals
    integer :: error, rank
    integer(HSIZE_T) :: dims(1)
    integer(HID_T) :: file_id, dset_id
    integer(HID_T) :: dspace_id

    character(len=3), parameter :: dset_rho = "rho"
    character(len=2), parameter :: dset_Mx  = "Mx"
    character(len=2), parameter :: dset_My  = "My"
    character(len=2), parameter :: dset_Mz  = "Mz"
    character(len=1), parameter :: dset_E   = "E"

    ! attributes
    character(len=5), parameter :: dset_attrs = "attrs"
    real(c_double)  :: attr_data(5)

    real(c_double), allocatable :: rho(:), Mx(:), My(:), Mz(:), E(:)
    integer(c_int) :: elem_index

    ! allocate the read buffer
    allocate(rho(mesh%Np*mesh%Nelements))
    allocate(Mx( mesh%Np*mesh%Nelements))
    allocate(My( mesh%Np*mesh%Nelements))
    allocate(E(  mesh%Np*mesh%Nelements))

#ifdef DIM3
    allocate(Mz( mesh%Np*mesh%Nelements))
#endif
    
    ! Initialize the FORTRAN interfce
    call h5open_f(error)

    ! Opent the file in readonly mode
    call h5fopen_f(trim(filename), H5F_ACC_RDONLY_F, file_id, error)
    
    ! Create the dataspace id
    rank    = 1
    dims(1) = mesh%Np*mesh%Nelements
    call h5screate_simple_f(rank, dims, dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Read the datasets
    call h5dopen_f(file_id, dset_rho, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, rho(:), dims, error, dspace_id)
    call h5dclose_f(dset_id, error)

    call h5dopen_f(file_id, dset_Mx, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, Mx(:), dims, error, dspace_id)
    call h5dclose_f(dset_id, error)

    call h5dopen_f(file_id, dset_My, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, My(:), dims, error, dspace_id)
    call h5dclose_f(dset_id, error)

    call h5dopen_f(file_id, dset_E, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, E(:), dims, error, dspace_id)
    call h5dclose_f(dset_id, error)

#ifdef DIM3
    call h5dopen_f(file_id, dset_Mz, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, Mz(:), dims, error, dspace_id)
    call h5dclose_f(dset_id, error)
#endif

    ! Terminate access to the data-space    
    call h5sclose_f(dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read attributes
    rank    = 1
    dims(1) = 5
    call h5screate_simple_f(rank, dims, dspace_id, error)
    call h5dopen_f(file_id, dset_attrs, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, attr_data(:), dims, error, dspace_id)
    call h5sclose_f(dspace_id, error)

    ! close the file
    call h5fclose_f(file_id, error)

    ! close the FORTRAN interface
    call h5close_f(error)

    ! copy the buffer contents to the mesh data structure
    do elem_index = 1, mesh%Nelements
       data%u(:, 1, elem_index) = rho( 1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )
       data%u(:, 2, elem_index) = Mx(  1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )
       data%u(:, 3, elem_index) = My(  1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )

#ifdef DIM3
       data%u(:, 4, elem_index) = Mz(  1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )
       data%u(:, 5, elem_index) = E(   1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )
#else
       data%u(:, 4, elem_index) = E(   1+(elem_index-1)*mesh%Np: elem_index*mesh%Np )
#endif

    end do

    ! store the restart time and step
    step = int( attr_data(1))
    time = dble(attr_data(2))

    ! deallocate the read buffers
    deallocate(rho, Mx, My, E)

#ifdef DIM3
    deallocate(Mz)
#endif

  end subroutine read_restart_file

end module restart_module
