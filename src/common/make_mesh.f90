module make_mesh_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use mesh_module

  use hdf5

  implicit none

contains

  subroutine read_mesh(mesh, numPartitions, myRank)
    use mesh_module
    use input_module, only: mesh_filename, mesh_file_extn, solution_order, verbose, &
                            EqnSystem

    integer(c_int), intent(in)  :: numPartitions, myRank
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: Nvar
    integer(c_int) :: counter, elem_index, face_index, face, i1,i2, i, j, nbr_face_index
    integer(c_int) :: elem2face(mesh%elem_num_faces)
    integer(c_int) :: elem2vertices(mesh%elem_num_verts), vert_index

    integer(c_int) :: count_import, count_export, tmp_index
    integer(c_int) :: num_to_import, num_to_export

    character(len=9 ), parameter :: dset_nelements     = "nelements"
    character(len=5 ), parameter :: dset_nodes         = "nodes"
    character(len=1 ), parameter :: dset_x             = "x"
    character(len=1 ), parameter :: dset_y             = "y"
    character(len=1 ), parameter :: dset_z             = "z"
    character(len=6 ), parameter :: dset_xfaces        = "xfaces"
    character(len=16), parameter :: dset_elem2vertices = "element2vertices"
    character(len=9 ), parameter :: dset_face2elem     = "face2elem"
    character(len=9 ), parameter :: dset_elem2face     = "elem2face"
    character(len=9 ), parameter :: dset_neihbors      = "neighbors"
    character(len=9 ), parameter :: dset_elemtype      = "elem_type"
    character(len=12), parameter :: dset_fmarkers      = "face_markers"
    character(len=10), parameter :: dset_eattrs        = "elem_attrs"    
    character(len=16), parameter :: dset_nbr_face_indices = "nbr_face_indices"

    character(len=7),  parameter :: dset_nexport = "nexport"
    character(len=7),  parameter :: dset_nimport = "nimport"
    character(len=13), parameter :: dset_exportIndices = "exportIndices"

    character(len=13), parameter :: dset_faceindex2nbr          = "faceindex2nbr"
    character(len=22), parameter :: dset_faceindex2nbrfaceindex = "faceindex2nbrfaceindex"
    character(len=6 ), parameter :: dset_bc_map = "bc_map"

    character(len=25), parameter :: dset_nexport_per_part_periodic = "nexport_per_part_periodic"
    character(len=25), parameter :: dset_nimport_per_part_periodic = "nimport_per_part_periodic"
    character(len=12), parameter :: dset_periodic_nie = "periodic_nie"

    character(len=23), parameter :: dset_exportIndices_periodic = "export_indices_periodic"
    character(len=23), parameter :: dset_importIndices_periodic = "import_indices_periodic"
    
    character(len=21), parameter :: dset_exportFaces_periodic = "export_faces_periodic"
    character(len=21), parameter :: dset_importFaces_periodic = "import_faces_periodic"    

    integer(HID_T) :: file_id
    integer(HID_T) :: dset_id

    integer :: error

    integer(c_int)   :: dset_nelements_data(5)

    integer(c_int)   :: dset_nimport_data(numPartitions)
    integer(c_int)   :: dset_nexport_data(numPartitions)

    integer(c_int), allocatable   :: dset_exportIndices_data(:)

    integer(c_int), allocatable :: exportIndices_periodic_data(:), exportFaces_periodic_data(:)
    integer(c_int), allocatable :: importIndices_periodic_data(:), importFaces_periodic_data(:)
    integer(c_int) :: nexportIndices_periodic
    integer(c_int) :: nimportIndices_periodic

    integer(HSIZE_T) :: dset_dims(1)

    ! temporary read buffer
    real(c_double), allocatable :: dbuf(:)
    integer(c_int), allocatable :: ibuf(:)

    ! filename to read
    character(len=3)   :: proc_index
    character(len=256) :: fname

    ! Initialize the fortran interface
    call h5open_f(error)

    ! Open the meshfile
    fname = trim(mesh_filename) // trim(mesh_file_extn)
    if (numPartitions .gt. 1) then
       write(unit=proc_index, fmt='(i3.3)') myRank
       fname = trim(mesh_filename) // ".msh_part" // proc_index // trim(mesh_file_extn)
    else
       write(unit=proc_index, fmt='(i3.3)') 0
    end if

    if (verbose) write(*, *) 'Processor_'//proc_index, ' opening MeshFile ', trim(fname)
    call h5fopen_f(trim(fname), H5F_ACC_RDWR_F, file_id, error)

    ! Open the nelements data set and read
    dset_dims(1) = 5
    call h5dopen_f(file_id, dset_nelements, dset_id, error)
    call h5dread_f(&
    dset_id, H5T_NATIVE_INTEGER, dset_nelements_data, dset_dims, error)

    mesh%nnodes    = dset_nelements_data(1)
    mesh%nfaces    = dset_nelements_data(2)
    mesh%nelements = dset_nelements_data(3)
    mesh%nghost    = dset_nelements_data(4)
    mesh%ninterior_faces = dset_nelements_data(5)

    mesh%numPartitions = numPartitions
    mesh%partition     = myRank

    ! allocate the data for the mesh
    if (EqnSystem .eq. OnePhase) then
       if (mesh%DIM .eq. 2) then
          Nvar = 4
       else
          Nvar = 5
       end if
    else if (EqnSystem .eq. TwoPhase) then
       if (mesh%DIM .eq. 2) then
          Nvar = 8
       else
          Nvar = 10
       end if
    end if    

    !print*, 'Allocate mesh', myRank, mesh%Nelements, mesh%Nfaces
    call allocate_mesh(mesh, Nvar, solution_order, numPartitions, myRank)
    !print*, myRank, 'done'

    ! Now that we have the mesh allocated, we can continue reading the
    ! mesh-file and load in the juicy bytes

    dset_dims(1) = numPartitions
    call h5dopen_f(file_id, dset_nimport, dset_id, error)
    call h5dread_f(&
         dset_id, H5T_NATIVE_INTEGER, dset_nimport_data, dset_dims, error)
    call h5dclose_f(dset_id, error)

    mesh%Nimport_per_part = dset_nimport_data(:)

    dset_dims(1) = numPartitions
    call h5dopen_f(file_id, dset_nexport, dset_id, error)
    call h5dread_f(&
         dset_id, H5T_NATIVE_INTEGER, dset_nexport_data, dset_dims, error)
    call h5dclose_f(dset_id, error)

    mesh%Nexport_per_part = dset_nexport_data(:)

    mesh%Nremote_per_part(:) = dset_nimport_data(:)

    mesh%max_numExport = max(0, maxval(mesh%Nexport_per_part))
    mesh%max_numImport = max(0, maxval(mesh%Nimport_per_part))

    !print*, 'Nexport', myRank, mesh%Nexport_per_part
    !print*, 'Nimport', myRank, mesh%Nimport_per_part

    if (numPartitions .gt. 1) then
       allocate(dset_exportIndices_data(mesh%max_numExport*numPartitions))
       
       dset_dims(1) = mesh%max_numExport*numPartitions
       call h5dopen_f(file_id, dset_exportIndices, dset_id, error)
       call h5dread_f(&
            dset_id, H5T_NATIVE_INTEGER, dset_exportIndices_data, dset_dims, error)
       call h5dclose_f(dset_id, error)

       allocate(mesh%exportIndices(numPartitions, mesh%max_numExport))

       call put2d_integer(numPartitions, mesh%max_numExport, &
            dset_exportIndices_data, mesh%exportIndices)

       deallocate(dset_exportIndices_data)

    end if
    
    ! if (numPartitions .gt. 1) then
    !    allocate(mesh%exportIndices(max_numExport))
    !    allocate(mesh%exportProcs  (max_numExport))

    !    allocate(mesh%importIndices(max_numImport))
    !    allocate(mesh%importProcs(  max_numImport))

    !    mesh%exportIndices = -1
    !    mesh%exportProcs   = -1       
    ! end if
    
    ! nodes
    !allocate( dbuf(mesh%nnodes*2) )
    !dset_dims(1) = mesh%nnodes*2

    allocate( dbuf(mesh%nnodes) )
    dset_dims(1) = mesh%nnodes

    !call h5dopen_f(file_id, dset_nodes, dset_id, error)
    !call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dbuf, dset_dims, error)
    !call h5dclose_f(dset_id, error)

    call h5dopen_f(file_id, dset_x, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dbuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    mesh%points(:, 1) = dbuf(:)

    call h5dopen_f(file_id, dset_y, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dbuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    mesh%points(:, 2) = dbuf(:)

    if (mesh%DIM .eq. 3) then
       call h5dopen_f(file_id, dset_z, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dbuf, dset_dims, error)
       call h5dclose_f(dset_id, error)
       mesh%points(:, 3) = dbuf(:)
    end if

    deallocate(dbuf)

    !call put2d_double(mesh%nnodes, 2, dbuf, mesh%points)
    !deallocate(dbuf)

    ! xfaces
    allocate( ibuf(mesh%nfaces*mesh%face_nnodes) )
    dset_dims(1) = mesh%nfaces*mesh%face_nnodes

    call h5dopen_f(file_id, dset_xfaces, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nfaces, mesh%face_nnodes, ibuf, mesh%xfaces)
    deallocate(ibuf)

    ! element2vertices
    allocate( ibuf(mesh%Nelements*mesh%elem_num_verts) )
    dset_dims(1) = mesh%Nelements*mesh%elem_num_verts
    
    call h5dopen_f(file_id, dset_elem2vertices, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_verts, ibuf, mesh%element2vertices)
    deallocate(ibuf)

    ! face2elem
    allocate( ibuf(mesh%nfaces*2) )
    dset_dims(1) = mesh%nfaces*2
    
    call h5dopen_f(file_id, dset_face2elem, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nfaces, 2, ibuf, mesh%face2elem)
    deallocate(ibuf)

    ! elem2face
    allocate( ibuf(mesh%nelements*mesh%elem_num_faces) )
    dset_dims(1) = mesh%nelements*mesh%elem_num_faces

    call h5dopen_f(file_id, dset_elem2face, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_faces, ibuf, mesh%elem2face)
    deallocate(ibuf)

    ! neighbors
    allocate( ibuf(mesh%nelements*mesh%elem_num_faces) )
    dset_dims(1) = mesh%nelements*mesh%elem_num_faces
    
    call h5dopen_f(file_id, dset_neihbors, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_faces, ibuf, mesh%neighbors)
    deallocate(ibuf)

    ! element types
    dset_dims(1) = mesh%nelements
    call h5dopen_f(file_id, dset_elemtype, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%elem_type, dset_dims, error)
    call h5dclose_f(dset_id, error)

    ! face markers
    dset_dims(1) = mesh%Nfaces
    call h5dopen_f(file_id, dset_fmarkers, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%face_markers, dset_dims, error)
    call h5dclose_f(dset_id, error)

    ! element attributes
    dset_dims(1) = mesh%nelements
    call h5dopen_f(file_id, dset_eattrs, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%elem_attrs, dset_dims, error)
    call h5dclose_f(dset_id, error)

    ! neibhor_face_indices
    allocate( ibuf(mesh%nelements*mesh%elem_num_faces) )
    dset_dims(1) = mesh%nelements*mesh%elem_num_faces
    
    call h5dopen_f(file_id, dset_nbr_face_indices, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_faces, ibuf, &
                       mesh%nbr_face_indices)
    deallocate(ibuf)

    ! faceindex2nbr
    allocate( ibuf(mesh%nelements*mesh%elem_num_faces) )
    dset_dims(1) = mesh%nelements*mesh%elem_num_faces
    
    call h5dopen_f(file_id, dset_faceindex2nbr, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_faces, ibuf, &
                       mesh%faceindex2nbr)
    deallocate(ibuf)

    ! faceindex2nbrfaceindex
    allocate( ibuf(mesh%nelements*mesh%elem_num_faces) )
    dset_dims(1) = mesh%nelements*mesh%elem_num_faces
    
    call h5dopen_f(file_id, dset_faceindex2nbrfaceindex, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
    call h5dclose_f(dset_id, error)

    call put2d_integer(mesh%nelements, mesh%elem_num_faces, ibuf, &
                       mesh%faceindex2nbrfaceindex)
    deallocate(ibuf)

    ! bc_map
    dset_dims(1) = mesh%Nelements
    call h5dopen_f(file_id, dset_bc_map, dset_id, error)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%bc_map, dset_dims, error)
    call h5dclose_f(dset_id, error)

    ! do face_index = 1, mesh%Nelements
    !    if (mesh%elem_type(face_index) .eq. -1) then
    !       write(*, *) face_index, mesh%bc_map(face_index), mesh%elem_type(face_index)
    !    end if
    ! end do

    ! generate the face normals and tangents
    do face_index = 1, mesh%Nfaces
       call set_normals(mesh, face_index)
    end do

    ! generate the external index for each element face
    do elem_index = 1, mesh%Nelements
       if (mesh%elem_type(elem_index) .gt. 0) then
          do face = 1, mesh%elem_num_faces

             nbr_face_index = mesh%faceindex2nbrfaceindex(elem_index, face)

             i1 = mesh%P1*(nbr_face_index-1) + 1
             i2 = mesh%P1*(nbr_face_index)

             do j = 1, mesh%P1
                mesh%index_extern(elem_index, face, j) = i2 - (j-1)
             end do
             
          end do
       end if
    end do

    ! set the local number of elements
    mesh%Nelements_local = mesh%Nelements
    do j = 1, mesh%numPartitions
       if (mesh%Nremote_per_part(j) .ne. -1) then
          mesh%Nelements_local = mesh%Nelements_local - mesh%Nremote_per_part(j)
       end if
    end do

    !print*, 'Mesh ', myRank, mesh%Nelements, mesh%Nelements_local

    ! set the shifts in the local indices for elements to be imported
    ! from remote processors
    if (mesh%numPartitions .gt. 1) then
       allocate(mesh%importShifts(mesh%numPartitions))
       mesh%importShifts = -1
       
       do i = 1, mesh%numPartitions
          counter = 0
          if (mesh%Nimport_per_part(i) .gt. 0) then
             do j = 1, i-1
                if (mesh%Nimport_per_part(j) .gt. 0) then
                   counter = counter + mesh%Nimport_per_part(j)
                end if
             end do
             mesh%importShifts(i) = mesh%Nelements_local + counter
          end if          
          
       end do
    end if

    !print*, 'ImportShifts', myRank, mesh%Nelements, mesh%importShifts

    ! set the indices for the interior faces
    mesh%interior_faces = -1
    counter = 0
    do elem_index = 1, mesh%Nelements_local
       elem2face = mesh%elem2face(elem_index, :)
       do face = 1, mesh%elem_num_faces
          face_index = elem2face(face)
          if ( (.not. any(mesh%interior_faces .eq. face_index)) .and. &
               mesh%face_markers(face_index) .eq. 0 ) then
             counter = counter + 1
             mesh%interior_faces(counter) = face_index
          end if
       end do
    end do

    !print*, 'Ninterior faces = ', counter
    mesh%Ninterior_faces = counter

    ! set the indices for the boundary faces
    mesh%bfaces = -1
    counter = 0
    do elem_index = 1, mesh%Nelements_local
       elem2face = mesh%elem2face(elem_index, :)
       do face = 1, mesh%elem_num_faces
          face_index = elem2face(face)
          if (mesh%face_markers(face_index) .ne. 0) then
             counter = counter + 1
             mesh%bfaces(counter) = face_index
          end if
       end do
    end do

    mesh%Nbfaces = counter

    ! get the unique indices for the local nodes
    mesh%local_nodes = -1
    counter = 0
    do elem_index = 1, mesh%Nelements !change this to nelements_local for local node numbers
       elem2vertices = mesh%element2vertices(elem_index, :)
       do vert_index = 1,  mesh%elem_num_verts
          if (.not. any(mesh%local_nodes .eq. elem2vertices(vert_index))) then
             counter = counter + 1
             mesh%local_nodes(counter) = elem2vertices(vert_index)
          end if
       end do
    end do

    mesh%Nlocal_nodes = counter

    ! periodic exchange data
    parallel_periodic: if (mesh%numPartitions .gt. 1 .and. df_is_periodic) then
       allocate( ibuf(6*numPartitions) )
       dset_dims(1) = 6*numPartitions

       ! number of periodic exports per periodic direction
       call h5dopen_f(file_id, dset_nexport_per_part_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
       call h5dclose_f(dset_id, error)
       
       allocate(mesh%Nexport_per_part_periodic(numPartitions, 6))
       call put2d_integer(numPartitions, 6, ibuf, mesh%Nexport_per_part_periodic)

       ! number of periodic imports per periodic direction
       call h5dopen_f(file_id, dset_nimport_per_part_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
       call h5dclose_f(dset_id, error)

       allocate(mesh%Nimport_per_part_periodic(numPartitions, 6))
       call put2d_integer(numPartitions, 6, ibuf, mesh%Nimport_per_part_periodic)

       deallocate(ibuf)

       ! number of export and import indices
       allocate(ibuf(2))
       dset_dims(1) = 2
       
       call h5dopen_f(file_id, dset_periodic_nie, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ibuf, dset_dims, error)
       call h5dclose_f(dset_id, error)

       nexportIndices_periodic = ibuf(1)
       nimportIndices_periodic = ibuf(2)

       mesh%num_export_indices_periodic = nexportIndices_periodic
       mesh%num_import_indices_periodic = nimportIndices_periodic
       
       ! indices for export elements
       allocate(mesh%exportIndices_periodic(numPartitions, 6, ibuf(1)))
       mesh%exportIndices_periodic = -1

       ! indices for exported faces
       allocate(mesh%exportFaces_periodic(  numPartitions, 6, ibuf(1)))
       mesh%exportFaces_periodic = -1

       ! indices for imported elements
       allocate(mesh%importIndices_periodic(numPartitions, 6, ibuf(2)))
       mesh%importIndices_periodic = -1

       ! indices for imported faces
       allocate(mesh%importFaces_periodic(  numPartitions, 6, ibuf(2)))
       mesh%importFaces_periodic = -1
       
       deallocate(ibuf)

       ! export & import indices
       allocate(ibuf(nexportIndices_periodic))
       dset_dims(1) = nexportIndices_periodic

       allocate(mesh%exportIndices_periodic_tmp(dset_dims(1)))
       call h5dopen_f(file_id, dset_exportIndices_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%exportIndices_periodic_tmp, dset_dims, error)
       call h5dclose_f(dset_id, error)

       allocate(mesh%exportFaces_periodic_tmp(dset_dims(1)))
       call h5dopen_f(file_id, dset_exportFaces_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%exportFaces_periodic_tmp, dset_dims, error)
       call h5dclose_f(dset_id, error)
       
       deallocate(ibuf)

       allocate(ibuf(nimportIndices_periodic))
       dset_dims(1) = nimportIndices_periodic

       allocate(mesh%importIndices_periodic_tmp(dset_dims(1)))
       call h5dopen_f(file_id, dset_importIndices_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%importIndices_periodic_tmp, dset_dims, error)
       call h5dclose_f(dset_id, error)

       allocate(mesh%importFaces_periodic_tmp(dset_dims(1)))
       call h5dopen_f(file_id, dset_importFaces_periodic, dset_id, error)
       call h5dread_f(dset_id, H5T_NATIVE_INTEGER, mesh%importFaces_periodic_tmp, dset_dims, error)
       call h5dclose_f(dset_id, error)
       
       deallocate(ibuf)

       ! proceed to set up the parallel periodic maps for each periodic direction
       count_export = 0
       count_import = 0

       partitions: do i = 1, numPartitions
          if (i-1 .ne. myRank) then
             tags: do j = 1, 6

                num_to_export = mesh%Nexport_per_part_periodic(i, j)
                if (num_to_export .gt. 0) then

                   mesh%exportIndices_periodic(i, j, 1:num_to_export) = &
                        mesh%exportIndices_periodic_tmp(count_export+1:count_export+num_to_export)

                   mesh%exportFaces_periodic(  i, j, 1:num_to_export) = &
                        mesh%exportFaces_periodic_tmp(  count_export+1:count_export+num_to_export)

                   count_export = count_export + num_to_export

                end if
                
                num_to_import = mesh%Nimport_per_part_periodic(i, j)

                if (num_to_import .gt. 0) then
                   
                   mesh%importIndices_periodic(i, j, 1:num_to_import) = &
                        mesh%importIndices_periodic_tmp(count_import+1:count_import+num_to_import)

                   mesh%importFaces_periodic(  i, j, 1:num_to_import) = &
                        mesh%importFaces_periodic_tmp(  count_import+1:count_import+num_to_import)

                   count_import = count_import + num_to_import
                      
                end if

             end do tags
          end if
       end do partitions

       ! ! lox
       ! if (myRank .eq. 0) then
       !    print*, 'Export lox 1-2', mesh%nexport_per_part_periodic(2, 1)
       !    print*, mesh%exportIndices_periodic(2, 1, 1:mesh%nexport_per_part_periodic(2, 1))-1
       !    print*
       !    print*, 'Export lox 1-3', mesh%nexport_per_part_periodic(3, 1)
       !    print*, mesh%exportIndices_periodic(3, 1, 1:mesh%nexport_per_part_periodic(3, 1))-1
       ! end if

       ! if (myRank .eq. 1) then
       !    print*, 'Import lox 2-1', mesh%nimport_per_part_periodic(1,1)
       !    print*, mesh%importIndices_periodic(1, 1, 1:mesh%nimport_per_part_periodic(1,1))-1
       !    print*
       ! end if

       ! if (myRank .eq. 2) then
       !    print*, 'Import lox 3-1', mesh%nimport_per_part_periodic(1,1)
       !    print*, mesh%importIndices_periodic(1, 1, 1:mesh%nimport_per_part_periodic(1,1))-1
       !    print*
       ! end if

       ! ! hix
       ! if (myRank .eq. 0) then
       !    print*, 'Import hix 1-2', mesh%nimport_per_part_periodic(2, 2)
       !    print*, mesh%importIndices_periodic(2,2, 1:mesh%nimport_per_part_periodic(2, 2))-1
       !    print*
       !    print*, 'Import hix 1-3', mesh%nimport_per_part_periodic(3,2)
       !    print*, mesh%importIndices_periodic(3,2, 1:mesh%nimport_per_part_periodic(3,2))-1
       ! end if

       ! if (myRank .eq. 1) then
       !    print*, 'Export hix 2-1', mesh%nexport_per_part_periodic(1, 2)
       !    print*, mesh%exportIndices_periodic(1, 2, 1:mesh%nexport_per_part_periodic(1, 2))-1
       ! end if

       ! if (myRank .eq. 2) then
       !    print*, 'Export hix 3-1', mesh%nexport_per_part_periodic(1, 2)
       !    print*, mesh%exportIndices_periodic(1, 2, 1:mesh%nexport_per_part_periodic(1, 2))-1
       ! end if

       ! ! loy
       ! if (myRank .eq. 0) then
       !    print*, 'Import loy 1-3', mesh%nimport_per_part_periodic(3,3)
       !    print*, mesh%importIndices_periodic(3,3, 1:mesh%nimport_per_part_periodic(3,3))-1
       ! end if

       ! if (myRank .eq. 1) then
       !    print*, 'Import loy 2-3', mesh%nimport_per_part_periodic(3,3)
       !    print*, mesh%importIndices_periodic(3,3, 1:mesh%nimport_per_part_periodic(3,3))-1
       ! end if

       ! if (myRank .eq. 2) then
       !    print*, 'Export loy 3-1', mesh%nexport_per_part_periodic(1,3)
       !    print*, mesh%exportIndices_periodic(1,3,1:mesh%nexport_per_part_periodic(1,3))-1
       !    print*,
          
       !    print*, 'Export loy 3-2', mesh%nexport_per_part_periodic(2,3)
       !    print*, mesh%exportIndices_periodic(2,3,1:mesh%nexport_per_part_periodic(2,3))-1
       ! end if       
       
    end if parallel_periodic

    ! close the file
    call h5fclose_f(file_id, error)

    ! close the fortran interface
    call h5close_f(error)

    ! if (myRank .eq. 1 .or. numPartitions .eq. 1) then
    !    print*, 'Ninterior faces: ', mesh%Ninterior_faces
    !    print*, mesh%interior_faces(1:mesh%Ninterior_faces)
    !    print*,
    !    print*, 'Bfaces', mesh%Nbfaces
    !    print*, mesh%bfaces(1:mesh%Nbfaces)
    !    print*,
    !    print*,
    !    print*, mesh%Nfaces, mesh%face_markers
    ! end if
    
    ! do face_index = 1, mesh%Nfaces
    !    if (mesh%face_markers(face_index) .eq. 0) then
    !       counter = counter + 1
    !       mesh%interior_faces(counter) = face_index
    !    end if
    ! end do

    ! if (counter .ne. mesh%Ninterior_faces) then 
    !    write(*, *) 'ERROR in mesh!'
    !    stop
    ! end if

    ! ! set the indices for the boundary faces
    ! counter = 0
    ! do face_index = 1, mesh%Nfaces
    !    if (mesh%face_markers(face_index) .ne. 0) then
    !       counter = counter + 1
    !       mesh%bfaces(counter) = face_index
    !    end if
    ! end do

    ! if (counter .ne. mesh%Nfaces - mesh%Ninterior_faces) then
    !    write(*, *) 'ERROR in mesh'
    !    stop
    ! end if

  end subroutine read_mesh

  subroutine put2d_double(M, stride, inarray, outarray)
    integer(c_int), intent(in   ) :: M, stride
    real(c_double), intent(in   ) :: inarray(M*stride)
    real(c_double), intent(inout) :: outarray(M, stride)

    ! locals
    integer(c_int) :: i
    do i  = 1, M
       outarray(i, :) = inarray( (i-1)*stride+1:i*stride )
    end do
    
  end subroutine put2d_double

  subroutine put2d_integer(M, stride, inarray, outarray)
    integer(c_int), intent(in   ) :: M, stride
    integer(c_int), intent(in   ) :: inarray(M*stride)
    integer(c_int), intent(inout) :: outarray(M, stride)

    ! locals
    integer(c_int) :: i
    do i  = 1, M
       outarray(i, :) = inarray( (i-1)*stride+1:i*stride )
    end do
    
  end subroutine put2d_integer

  subroutine set_normals(mesh, face_index)
    type(mesh2d), intent(inout) :: mesh
    integer(c_int),                 intent(in   ) :: face_index

    ! locals
    real(c_double) :: xa(mesh%DIM), xb(mesh%DIM), xc(mesh%DIM)
    real(c_double) :: delx, dely, mag

    real(c_double) :: A(mesh%DIM), B(mesh%DIM)
    real(c_double) :: nx, ny, nz
    
    if (mesh%DIM .eq. 2) then
    
       xa = mesh%points( mesh%xfaces(face_index, 1), : )
       xb = mesh%points( mesh%xfaces(face_index, 2), : )
       
       delx = xb(1) - xa(1)
       dely = xb(2) - xa(2)
       mag = ONE/sqrt( delx*delx + dely*dely )
       
       ! set the physical normals
       mesh%pnormals(face_index, 1) =  dely
       mesh%pnormals(face_index, 2) = -delx

       ! set the magnitude of the normals
       mesh%magnormals(face_index) = mag

       delx = delx*mag
       dely = dely*mag
       
       ! set the tangents for this face
       mesh%tangents(face_index, 1) = delx
       mesh%tangents(face_index, 2) = dely
       
       ! set the unit normals
       mesh%normals(face_index, 1) =  dely
       mesh%normals(face_index, 2) = -delx
    else
       xa = mesh%points(mesh%xfaces(face_index, 1), :)
       xb = mesh%points(mesh%xfaces(face_index, 2), :)
       xc = mesh%points(mesh%xfaces(face_index, 3), :)

       A = xb-xa
       B = xc-xa
       
       nx = +(A(2)*B(3) - A(3)*B(2))
       ny = -(A(1)*B(3) - A(3)*B(1))
       nz = +(A(1)*B(2) - A(2)*B(1))

       mag = sqrt( nx**2 + ny**2 + nz**2 )
       
       mesh%normals(face_index,1) = nx/mag
       mesh%normals(face_index,2) = ny/mag
       mesh%normals(face_index,3) = nz/mag

       mesh%pnormals(face_index,1) = nx
       mesh%pnormals(face_index,2) = ny
       mesh%pnormals(face_index,3) = nz

       mesh%tangents(face_index,:) = ZERO
    end if

  end subroutine set_normals
  
end module make_mesh_module
