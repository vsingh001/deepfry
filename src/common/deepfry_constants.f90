module deepfry_constants_module
  use iso_c_binding, only: c_double, c_int

  implicit none

  ! Equation system to solve
  integer(kind = c_int), parameter :: OnePhase = 1
  integer(kind = c_int), parameter :: TwoPhase = 2

  ! Node placement
  integer(kind = c_int), parameter :: GAUSS_NODES   = 1
  integer(kind = c_int), parameter :: LOBATTO_NODES = 2

  ! correction function type
  integer(kind = c_int), parameter :: NDG = 1
  integer(kind = c_int), parameter :: SD  = 2
  integer(kind = c_int), parameter :: G2  = 3

  integer(kind = c_int), parameter :: QUADS     = 9
  integer(kind = c_int), parameter :: TRIANGLES = 3
  integer(kind = c_int), parameter :: TETS      = 10
  integer(kind = c_int), parameter :: HEXS      = 12

  real(kind = c_double), parameter :: EPSILON =  0.000000000000001_c_double !1D-15
  real(kind = c_double), parameter :: SMALL   =  0.000000000001_c_double
  real(kind = c_double), parameter :: LARGE   =  100000000.0_c_double

  real(kind = c_double), parameter :: ZERO    =  0.0_c_double
  real(kind = c_double), parameter :: ONE     =  1.0_c_double
  real(kind = c_double), parameter :: TWO     =  2.0_c_double
  real(kind = c_double), parameter :: THREE   =  3.0_c_double
  real(kind = c_double), parameter :: FOUR    =  4.0_c_double
  real(kind = c_double), parameter :: FIVE    =  5.0_c_double
  real(kind = c_double), parameter :: SIX     =  6.0_c_double
  real(kind = c_double), parameter :: SEVEN   =  7.0_c_double
  real(kind = c_double), parameter :: EIGHT   =  8.0_c_double
  real(kind = c_double), parameter :: NINE    =  9.0_c_double
  real(kind = c_double), parameter :: TEN     = 10.0_c_double

  real(kind = c_double), parameter :: ELEVEN  = 11.0_c_double
  real(kind = c_double), parameter :: TWELVE  = 12.0_c_double
  real(kind = c_double), parameter :: FIFTEEN = 15.0_c_double
  real(kind = c_double), parameter :: SIXTEEN = 16.0_c_double

  real(kind = c_double), parameter :: HALF    = 0.5_c_double
  real(kind = c_double), parameter :: THIRD   = ONE/THREE
  real(kind = c_double), parameter :: FOURTH  = 0.25_c_double
  real(kind = c_double), parameter :: FIFTH   = ONE/FIVE
  real(kind = c_double), parameter :: SIXTH   = ONE/SIX
  real(kind = c_double), parameter :: SEVENTH = ONE/SEVEN
  real(kind = c_double), parameter :: EIGHTH  = 0.125_c_double
  real(kind = c_double), parameter :: NINETH  = ONE/NINE
  real(kind = c_double), parameter :: TENTH   = 0.10_c_double
  real(kind = c_double), parameter :: TWELFTH = ONE/TWELVE

  real(kind = c_double), parameter :: TWO3RD    = TWO/THREE
  real(kind = c_double), parameter :: FOUR3RD   = FOUR/THREE
  real(kind = c_double), parameter :: FIVE3RD   = FIVE/THREE

  real(kind = c_double), parameter :: THREE4TH  = 0.75_c_double

  real(kind = c_double), parameter :: SEVEN12TH = SEVEN/TWELVE

  !! Pi
  real(kind = c_double), parameter :: M_PI    = &
       3.141592653589793238462643383279502884197_c_double
  real(kind = c_double), parameter :: M_SQRT_PI  = &
       1.772453850905516027298167483341145182798_c_double

  !! Roots
  real(kind = c_double), parameter :: M_SQRT_2  = &
       1.414213562373095048801688724209698078570_c_double

  real(kind = c_double), parameter :: M_SQRT_3 = &
       1.7320508075688772_c_double


  ! Limiter constants
  integer(kind = c_int), parameter :: MLP_LIMITER = 1
  integer(kind = c_int), parameter :: KRIVODONOVA_LIMITER = 2
  integer(kind = c_int), parameter :: ZHANG_XIA_SHU_LIMITER = 3

  ! Shock sensor constants
  integer(kind = c_int), parameter :: MORO_NGUYEN_PERAIRE_SENSOR = 1

end module deepfry_constants_module
