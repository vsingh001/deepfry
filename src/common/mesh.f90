module mesh_module
  use deepfry_constants_module
  use iso_c_binding, only: c_double, c_int
  use input_module,  only: MESH_ELEM_TYPE, cuBLAS, df_is_periodic
  use vtk_headers_module

#ifdef CUDA
  use gpu_module
#endif

#ifdef BOXLIB
  use BoxLib
  use multifab_module
  use ml_layout_module
  use define_bc_module
#endif

  implicit none

  type bounding_box
     real(c_double) :: xmin, ymin, zmin
     real(c_double) :: xmax, ymax, zmax
  end type bounding_box

#ifdef BOXLIB
  type fr_interface_data_t
     integer(c_int), pointer :: face2box(:)     => Null()
     integer(c_int), pointer :: face2cell(:, :) => Null()
  end type fr_interface_data_t

  type bl_interface_data_t
     integer(c_int), pointer :: patch_index(:)  => Null()
     integer(c_int), pointer :: cell_index(:,:) => Null()
     integer(c_int), pointer :: cell2elem(:)    => Null()
     integer(c_int), pointer :: face_markers(:) => Null()
  end type bl_interface_data_t
#endif

  type BoxLib_data_t
#ifdef BOXLIB
     type(ml_layout)             :: bl_mla
     type(bc_tower)              :: bl_the_bc_tower
     
     real(c_double), pointer     :: bl_dx(:, :)
     type(multifab), allocatable :: bl_Unew(:)
     type(multifab), pointer     :: bl_Uold(:)
     type(multifab), allocatable :: bl_mask(:)

     ! interface data
     type(fr_interface_data_t) :: fr_interface_data
     type(bl_interface_data_t) :: bl_interface_data

     logical, allocatable        :: pmask(:)
#endif
  end type BoxLib_data_t

  type mesh2d
     ! mesh and domain stats
     type(bounding_box) :: bbox

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Mesh data structure parameters
     integer(c_int) :: DIM                      ! Problem dimensionality
     integer(c_int) :: Nnodes                   ! Number of nodes in the mesh
     integer(c_int) :: Nelements                ! Number of elements in the mesh
     integer(c_int) :: Nfaces                   ! Number of faces in the mesh
     integer(c_int) :: Nghost                   ! Number of ghost cells if any
    
     ! Mesh element type related properties
     integer(c_int) :: elem_num_faces           ! number of faces for the mesh element type
     integer(c_int) :: elem_num_verts           ! number of vertices for the mesh element type
     integer(c_int) :: face_nnodes              ! number of nodes for the element faces
     real(c_double) :: reference_area           ! element reference area
     real(c_double) :: hmin, hmax, maxvel       ! min/max element size and maxvel for the mesh


     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Parallel mesh properties
     integer(c_int) :: Numpartitions              ! Number of mesh partitions 
     integer(c_int) :: partition                  ! partition number
     integer(c_int) :: Nelements_local            ! Number of local elements in the partition

     integer(c_int) :: Nlocal_nodes                ! number of local nodes in the partition
     integer(c_int), allocatable :: local_nodes(:)

     ! number of ghosts from non-local partitions (remote elements)
     integer(c_int), allocatable :: Nremote_per_part(:)

     ! number of cells to import from and export to remote processors
     integer(c_int) :: max_numExport

     ! number of cells to import from remote processors
     integer(c_int) :: max_numImport                      
     integer(c_int), allocatable :: Nimport_per_part(:)
     integer(c_int), allocatable :: numImport(:)
     integer(c_int), allocatable :: importProcs(:)
     integer(c_int), allocatable :: importIndices(:)

     ! number of cells to export to remote processors
     integer(c_int), allocatable :: Nexport_per_part(:)
     integer(c_int), allocatable :: numExport(:)
     integer(c_int), allocatable :: exportIndices(:, :)
     integer(c_int), allocatable :: exportProcs(:)

     ! Import offsets for parallel data exchange. The local elements
     ! of a partition are stored first and then copies of remote
     ! ghosts in order of rank. This array gives the offsets required
     ! when importing ghost cell data 
     integer(c_int), allocatable :: importShifts(:)

     ! Periodic exchange data for parallel
     integer(c_int), allocatable :: Nimport_per_part_periodic(:,:)
     integer(c_int), allocatable :: Nexport_per_part_periodic(:,:)

     integer(c_int), allocatable :: importIndices_periodic(:, :, :)
     integer(c_int), allocatable :: importFaces_periodic(  :, :, :)

     integer(c_int), allocatable :: importIndices_periodic_tmp(:)
     integer(c_int), allocatable :: importFaces_periodic_tmp(:)

     integer(c_int), allocatable :: exportIndices_periodic(:, :, :)
     integer(c_int), allocatable :: exportFaces_periodic(  :, :, :)

     integer(c_int) :: num_export_indices_periodic, num_import_indices_periodic
     integer(c_int), allocatable :: exportIndices_periodic_tmp(:)
     integer(c_int), allocatable :: exportFaces_periodic_tmp(:)

     ! Element processor number (not really needed except for
     ! plotting)
     integer(c_int), allocatable :: Proc(:)

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Mesh data structure
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     real(c_double), allocatable :: points(:,:)
     real(c_double), allocatable :: xc(:)
     real(c_double), allocatable :: yc(:)
     real(c_double), allocatable :: zc(:)

     ! Connectivity matrices
     integer(c_int), allocatable :: xfaces(:,:)           ! integer indices for the face nodes 
     integer(c_int), allocatable :: element2vertices(:,:) ! element to vertex mapping (Nelements_local, num_verts)
     integer(c_int), allocatable :: elem2face(:,:)        ! element to face mapping (Nelements, num_faces)
     integer(c_int), allocatable :: face2elem(:,:)        ! face 2 element mapping (Nfaces, 2)
     integer(c_int), allocatable :: neighbors(:,:)        ! element neighbors (Nelements, num_faces)
     
     ! Face normals and tangents
     real(c_double), allocatable :: normals(:,:)
     real(c_double), allocatable :: pnormals(:,:)
     real(c_double), allocatable :: magnormals(:)
     real(c_double), allocatable :: tangents(:,:)

     ! FIXME: Provide a description of this array
     integer(c_int), allocatable :: index_extern(:,:,:)

     ! Sorted face indices (P1, num_faces, nelements). This array is
     ! used to sort the nodes along the element face according to the
     ! physical coordinates of the nodes. The nodes are unique for two
     ! elements sharing the face and hence the sorted index gives the
     ! correct order to match up the data while working with a pair.
     integer(c_int), allocatable :: sfi(:, :, :)

     ! Neighboring face indices (Nelements, num_faces). This array
     ! gives, for each element's neighbor, the face index of the
     ! neighboring element.
     integer(c_int), allocatable :: nbr_face_indices(:,:)

     ! Face index to neibhor (Nelements, num_faces). This array gives,
     ! for each face of an element, the neighboring element.
     integer(c_int), allocatable :: faceindex2nbr(:, :)

     ! Face index to neighbor face index (Nelements, num_faces). This
     ! array gives, for each face of an element, the face index of the
     ! neighboring element
     integer(c_int), allocatable :: faceindex2nbrfaceindex(:, :)

     ! faceindex2elemfaceindex. This array (Ninterior_faces, 2)
     ! gives, for each face, the face inde of the elements abutting
     ! the face
     integer(c_int), allocatable :: faceindex2elemfaceindex(:, :)

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Data to handle regions and boundary conditions

     ! Number of interior and boundary faces
     integer(c_int) :: Ninterior_faces, Nbfaces       

     ! Interior faces (Nfaces). The indices for the interior faces
     ! (i.e. for which, face_markers = 0)
     integer(c_int), allocatable :: interior_faces(:)

     ! Boundary faces (Nbfaces). The indices for the boundary faces
     ! (i.e. for which, face_markers != 0 as defined in the mesh file)
     integer(c_int), allocatable :: bfaces(:)

     ! Element type (Nelements). Marking array for every element
     integer(c_int), allocatable :: elem_type(:)
     integer(c_int), allocatable :: elem_attrs(:) ! FIXME: Same as etype?

     ! Face markers (Nfaces). Face tags for boundary conditions etc
     integer(c_int), allocatable :: face_markers(:)
     integer(c_int), allocatable :: face_types(:,:) ! FIXME: Same as face marker?

     ! boundary condition map for periodic elements
     integer(c_int), allocatable :: bc_map(:)

     ! Vertex neighbors. The SVj set for an element vertex is defined
     ! as all elements sharing that vertex. We don't know this
     ! a-priori so we set aside a maximum number MAX_SVJ (default 32
     ! in the input file)
     integer(c_int), allocatable :: Nsvj(:, :)
     integer(c_int), allocatable :: SVj(:,:,:)

     ! Vertex maximum and minimum values. Given the SVj set, we store
     ! the minimum and maximum value of a variable at the vertex. This
     ! is like a node-centering of the data.
     real(c_double), allocatable :: vertex_var_min(:, :)
     real(c_double), allocatable :: vertex_var_max(:, :)

     ! Cell maximum and minimum values. Like the SVj set, the STj set
     ! is defined as the set of all elements sharing either a face or
     ! vertex with the given element. Naturally, the STj set is made
     ! up of the SVj set. The following variables thus define the
     ! minimum and maximum values over all cells in the vicinity of an
     ! element. FIXME: For the BoxLib coupling, we need to be more
     ! careful about this.
     real(c_double), allocatable :: STj_var_min(:, :, :)
     real(c_double), allocatable :: STj_var_max(:, :, :)

     ! element characteristic lengths. This can be used to 
     real(c_double), allocatable :: characteristic_length(:)

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Field variable defined on the mesh. FIXME: This should be
     ! abstracted out as the mesh definition is quite general and not
     ! limited to a FR field represented on it. It could be another DG
     ! scheme or a simple FVM field on the mesh.
     !type(FieldData_t), allocatable :: FieldData(:)

     ! FR order variables
     integer(c_int) :: P     ! Solution order
     integer(c_int) :: Np    ! Number of basis functions
     integer(c_int) :: P1    ! Number of face flux points
     integer(c_int) :: Nflux ! Total number of flux points
     integer(c_int) :: Nvar  ! Number of variables represented on the mesh

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Operator arrays defined on the Mesh. This is perhaps an
     ! interface layer between the basic mesh data structure and the
     ! kind of field represented on the mesh. The interface exists
     ! because the kind of operators (FR/FVM/SD) depend on the field
     ! used on the mesh

     ! solution points in local (r,s,t) element and quadrature weights
     real(c_double), allocatable :: r(:)
     real(c_double), allocatable :: s(:)
     real(c_double), allocatable :: t(:)
     real(c_double), allocatable :: w(:)

     ! vertex points in local (r,s,t) coordinate systems
     real(c_double), allocatable :: rv(:)
     real(c_double), allocatable :: sv(:)
     real(c_double), allocatable :: tv(:)

     ! flux points in local (r,s,t) coordinate system
     real(c_double), allocatable :: rf(:)
     real(c_double), allocatable :: sf(:)
     real(c_double), allocatable :: tf(:)

     ! element Jacobians
     real(c_double), allocatable :: Jn   (:,:,:,:)
     real(c_double), allocatable :: JnTinv(:,:,:,:)
     real(c_double), allocatable :: detJn(:,:)

     ! Vandermode matrices with respect to the basis and the inverse
     real(c_double), allocatable :: V(:,:)
     real(c_double), allocatable :: Vinv(:, :)

     ! Gradient of the modal and nodal basis at solution points
     real(c_double), allocatable :: Vr(:,:)
     real(c_double), allocatable :: Vs(:,:)
     real(c_double), allocatable :: Vt(:,:)

     real(c_double), allocatable :: Lr(:,:)
     real(c_double), allocatable :: Ls(:,:)
     real(c_double), allocatable :: Lt(:,:)
     real(c_double), allocatable :: Lgrad(:,:)

     ! Nodal basis matrix (Lagrange matrix: Identity :/)
     real(c_double), allocatable :: L(:,:)

     ! Nodal & modal bases evaluated at the vertex points
     real(c_double), allocatable :: Vv(:, :)
     real(c_double), allocatable :: Lv(:, :)

     ! Modal and Nodal basis evaluated at the flux points
     real(c_double), allocatable :: Vf(:, :)
     real(c_double), allocatable :: Lf(:, :)

     ! Plot point interpolation matrix and associated data
     type(vtk_mesh_header_t)     :: vtk_header
     real(c_double), allocatable :: Vplot(:, :)
     real(c_double), allocatable :: Lplot(:, :)

     ! Gradient of modal and nodal basis at flux points
     real(c_double), allocatable :: Vf_r(:, :)
     real(c_double), allocatable :: Vf_s(:, :)
     real(c_double), allocatable :: Vf_t(:, :)

     real(c_double), allocatable :: Lf_r(:, :)
     real(c_double), allocatable :: Lf_s(:, :)
     real(c_double), allocatable :: Lf_t(:, :)
     real(c_double), allocatable :: Lf_grad(:, :)

     ! Filter cutoff & matrix
     integer(c_int) :: filter_cutoff
     integer(c_int), allocatable :: basis_degrees(:)
     real(c_double), allocatable :: Filter(:, :)

     !  Element transformation factors
     real(c_double), allocatable :: xr(:, :)
     real(c_double), allocatable :: xs(:, :)
     real(c_double), allocatable :: xt(:, :)

     real(c_double), allocatable :: yr(:, :)
     real(c_double), allocatable :: ys(:, :)
     real(c_double), allocatable :: yt(:, :)

     real(c_double), allocatable :: zr(:, :)
     real(c_double), allocatable :: zs(:, :)
     real(c_double), allocatable :: zt(:, :)

     real(c_double), allocatable :: xfr(:, :)
     real(c_double), allocatable :: xfs(:, :)
     real(c_double), allocatable :: xft(:, :)

     real(c_double), allocatable :: yfr(:, :)
     real(c_double), allocatable :: yfs(:, :)
     real(c_double), allocatable :: yft(:, :)

     real(c_double), allocatable :: zfr(:, :)
     real(c_double), allocatable :: zfs(:, :)
     real(c_double), allocatable :: zft(:, :)

     real(c_double), allocatable :: rx(:, :)
     real(c_double), allocatable :: ry(:, :)
     real(c_double), allocatable :: rz(:, :)

     real(c_double), allocatable :: sx(:, :)
     real(c_double), allocatable :: sy(:, :)
     real(c_double), allocatable :: sz(:, :)

     real(c_double), allocatable :: tx(:, :)
     real(c_double), allocatable :: ty(:, :)
     real(c_double), allocatable :: tz(:, :)

     ! correction function coefficients. These are FR-specific
     real(c_double), allocatable :: phifj_solution_r(:, :)
     real(c_double), allocatable :: phifj_solution_s(:, :)
     real(c_double), allocatable :: phifj_solution_t(:, :)
     real(c_double), allocatable :: phifj_solution_grad(:, :)

     ! L2 Projection operators. These are used to define the "reduced"
     ! solution representation at the solution points.
     real(c_double), allocatable :: Pnm(:, :, :)

     ! cuBLAS data pointer
#ifdef CUDA
     type(device_data_t), pointer :: device_data
#endif

     ! BoxLib data
     type(BoxLib_data_t) :: BoxLib_data
     
  end type mesh2d
  
contains
  
  subroutine allocate_mesh(mesh, Nvar, P, numProcs, myRank)
    use input_module
    use polynomials_module
    
    integer(c_int),      intent(in   ) :: Nvar, P, numProcs, myRank
    type(mesh2d),        intent(inout) :: mesh

    integer(c_int) :: P1

    ! Set the number of soltion and flux points 
    mesh%Nvar = Nvar
    mesh%P    = P

    P1 = P + 1
       
    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       mesh%P1    = P1
       mesh%Np    = int( HALF*(P+1)*(P+2) )
       mesh%Nflux = mesh%elem_num_faces*mesh%P1

    else if (MESH_ELEM_TYPE .eq. QUADS) then
       mesh%P1   = P1
       mesh%Np   = P1**2
       mesh%Nflux = mesh%elem_num_faces*mesh%P1

    else if (MESH_ELEM_TYPE .eq. HEXS) then
       mesh%P1    = P1**2
       mesh%Np    = P1**3
       mesh%Nflux = mesh%elem_num_faces*P1**2

    else if (MESH_ELEM_TYPE .eq. TETS) then
       mesh%P1    = int( HALF*(P+1)*(P+2) )
       mesh%Np    = int(ONE/SIX * (P+1)*(P+2)*(P+3))
       mesh%Nflux = int( HALF*(P+1)*(P+2) )

    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Allocate Mesh data

    ! number of remote elements stored on this partition
    allocate(mesh%Nremote_per_part(mesh%numPartitions))
    allocate(mesh%Nimport_per_part(mesh%numPartitions))
    allocate(mesh%Nexport_per_part(mesh%numPartitions))

    allocate(mesh%numExport(mesh%numPartitions))

    allocate(mesh%Proc(mesh%Nelements))
    mesh%Proc(:) = myRank

    ! physical coordinates of the element nodes
    allocate(mesh%points(mesh%Nnodes, mesh%DIM))

    ! local or unique nodes for the mesh
    allocate(mesh%local_nodes(mesh%Nnodes))

    ! element centroids
    allocate(mesh%xc(mesh%Nelements))
    allocate(mesh%yc(mesh%Nelements))
    allocate(mesh%zc(mesh%Nelements))
    
    ! mesh connectivity information
    allocate(mesh%xfaces(mesh%Nfaces, mesh%face_nnodes))
    allocate(mesh%element2vertices(mesh%Nelements, mesh%elem_num_verts))
    allocate(mesh%face2elem(mesh%Nfaces,2))
    allocate(mesh%elem2face(mesh%Nelements, mesh%elem_num_faces))
    allocate(mesh%neighbors(mesh%Nelements, mesh%elem_num_faces))

    ! external indices for neighbors
    allocate(mesh%index_extern(mesh%Nelements, mesh%elem_num_faces, mesh%Nflux))
    allocate(mesh%sfi(mesh%P1, mesh%elem_num_faces, mesh%Nelements))

    allocate(mesh%nbr_face_indices(         mesh%Nelements, mesh%elem_num_faces))
    allocate(mesh%faceindex2nbr(            mesh%Nelements, mesh%elem_num_faces))
    allocate(mesh%faceindex2nbrfaceindex(   mesh%Nelements, mesh%elem_num_faces))

    allocate(mesh%faceindex2elemfaceindex(mesh%Nfaces, 2))

    ! Element types and attributes
    allocate(mesh%elem_type( mesh%nelements))
    allocate(mesh%elem_attrs(mesh%nelements))

    ! Face markers
    allocate(mesh%face_markers(mesh%Nfaces))
    allocate(mesh%interior_faces(mesh%Ninterior_faces))
    allocate(mesh%bfaces(mesh%Nfaces-mesh%Ninterior_faces))

    ! Face normals and tangents
    allocate(mesh%normals( mesh%nfaces, mesh%DIM))
    allocate(mesh%pnormals( mesh%nfaces, mesh%DIM))
    allocate(mesh%magnormals( mesh%nfaces ))
    allocate(mesh%tangents(mesh%nfaces, mesh%DIM))

    ! Face types (possibly redundant :/)
    allocate(mesh%face_types(mesh%nelements, mesh%elem_num_faces))

    ! Boundary maps
    allocate(mesh%bc_map(mesh%Nelements))

    ! projection operators
    allocate(mesh%Pnm(0:mesh%P, mesh%Np, mesh%Np))

    ! basis degrees
    allocate(mesh%basis_degrees(mesh%Np))

    ! element characteristic length
    allocate(mesh%characteristic_length(mesh%Nelements))

    allocate(mesh%Nsvj(mesh%elem_num_verts, mesh%Nelements))
    allocate(mesh%SVj(mesh%elem_num_verts, MAX_SVJ, mesh%Nelements))

    if (EqnSystem .eq. 1) then

       allocate(mesh%vertex_var_min(1, mesh%Nnodes))
       allocate(mesh%vertex_var_max(1, mesh%Nnodes))

       allocate(mesh%STj_var_max(1, 1, mesh%Nelements))
       allocate(mesh%STj_var_min(1, 1, mesh%Nelements))

    else if (EqnSystem .eq. 2) then
       allocate(mesh%vertex_var_min(2, mesh%Nnodes))
       allocate(mesh%vertex_var_max(2, mesh%Nnodes))

       allocate(mesh%STj_var_max(1, 2, mesh%Nelements))
       allocate(mesh%STj_var_min(1, 2, mesh%Nelements))

    end if



    ! from the order and the number of points, we can set the location
    ! of the solution points in the local coordinate system
    allocate(mesh%r(mesh%Np))
    allocate(mesh%s(mesh%Np))
    allocate(mesh%t(mesh%Np))
    allocate(mesh%w(mesh%Np))
    
    call get_solution_points(mesh%p, mesh%r, mesh%s, mesh%t, mesh%w)

    allocate(mesh%rf(mesh%Nflux))
    allocate(mesh%sf(mesh%Nflux))
    allocate(mesh%tf(mesh%Nflux))

    allocate(mesh%rv(mesh%elem_num_verts))
    allocate(mesh%sv(mesh%elem_num_verts))
    allocate(mesh%tv(mesh%elem_num_verts))

    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       mesh%rv = (/ -ONE, ONE, -ONE /)
       mesh%sv = (/ -ONE, -ONE, ONE /)

    else if (MESH_ELEM_TYPE .eq. QUADS) then
       mesh%rv = (/ -ONE, ONE, ONE, -ONE /)
       mesh%sv = (/ -ONE, -ONE, ONE, ONE /)

    else if (MESH_ELEM_TYPE .eq. HEXS) then
       mesh%rv = (/ -ONE, ONE, ONE, -ONE, -ONE, ONE, ONE, -ONE /)
       mesh%sv = (/ -ONE, -ONE, ONE, ONE, -ONE, -ONE, ONE, ONE /)
       mesh%tv = (/ -ONE, -ONE, -ONE, -ONE, ONE, ONE, ONE, ONE /)
       
    end if

    ! allocate(mesh%Jn   (mesh%Nelements, mesh%Np, 2, 2))
    ! allocate(mesh%JnT  (mesh%Nelements, mesh%Np, 2, 2))

    allocate(mesh%Jn(    mesh%DIM, mesh%DIM, mesh%Np, mesh%Nelements))
    allocate(mesh%JnTinv(mesh%DIM, mesh%DIM, mesh%Np, mesh%Nelements))
    allocate(mesh%detJn( mesh%Np,  mesh%Nelements      ))

    ! allocate the Vandermode and associated matrices
    allocate(mesh%V(   mesh%Np, mesh%Np))
    allocate(mesh%L(   mesh%Np, mesh%Np))
    allocate(mesh%Vinv(mesh%Np, mesh%Np))

    allocate(mesh%Vv(mesh%elem_num_verts, mesh%Np))
    allocate(mesh%Lv(mesh%elem_num_verts, mesh%Np))

    allocate(mesh%Vf(  mesh%Nflux, mesh%Np))
    allocate(mesh%Lf(  mesh%Nflux, mesh%Np))

    allocate(mesh%Vplot(mesh%vtk_header%PlotNumberOfPoints, mesh%Np))
    allocate(mesh%Lplot(mesh%vtk_header%PlotNumberOfPoints, mesh%Np))

    allocate(mesh%Vf_r(mesh%Nflux, mesh%Np))
    allocate(mesh%Vf_s(mesh%Nflux, mesh%Np))
    allocate(mesh%Vf_t(mesh%Nflux, mesh%Np))

    allocate(mesh%Lf_r(mesh%Nflux, mesh%Np))
    allocate(mesh%Lf_s(mesh%Nflux, mesh%Np))
    allocate(mesh%Lf_t(mesh%Nflux, mesh%Np))

    allocate(mesh%Lf_grad(mesh%DIM*mesh%Nflux, mesh%Np))
    
    ! Filter matrix
    allocate(mesh%Filter(mesh%NP, mesh%Np))

    ! allocate the element transformation arrays
    allocate(mesh%xr(mesh%Np, mesh%Nelements))
    allocate(mesh%xs(mesh%Np, mesh%Nelements))
    allocate(mesh%xt(mesh%Np, mesh%Nelements))

    allocate(mesh%yr(mesh%Np, mesh%Nelements))
    allocate(mesh%ys(mesh%Np, mesh%Nelements))
    allocate(mesh%yt(mesh%Np, mesh%Nelements))

    allocate(mesh%zr(mesh%Np, mesh%Nelements))
    allocate(mesh%zs(mesh%Np, mesh%Nelements))
    allocate(mesh%zt(mesh%Np, mesh%Nelements))

    allocate(mesh%xfr(mesh%Nflux, mesh%Nelements))
    allocate(mesh%xfs(mesh%Nflux, mesh%Nelements))
    allocate(mesh%xft(mesh%Nflux, mesh%Nelements))

    allocate(mesh%yfr(mesh%Nflux, mesh%Nelements))
    allocate(mesh%yfs(mesh%Nflux, mesh%Nelements))
    allocate(mesh%yft(mesh%Nflux, mesh%Nelements))

    allocate(mesh%zfr(mesh%Nflux, mesh%Nelements))
    allocate(mesh%zfs(mesh%Nflux, mesh%Nelements))
    allocate(mesh%zft(mesh%Nflux, mesh%Nelements))

    allocate(mesh%rx(mesh%Np, mesh%Nelements))
    allocate(mesh%ry(mesh%Np, mesh%Nelements))
    allocate(mesh%rz(mesh%Np, mesh%Nelements))

    allocate(mesh%sx(mesh%Np, mesh%Nelements))
    allocate(mesh%sy(mesh%Np, mesh%Nelements))
    allocate(mesh%sz(mesh%Np, mesh%Nelements))

    allocate(mesh%tx(mesh%Np, mesh%Nelements))
    allocate(mesh%ty(mesh%Np, mesh%Nelements))
    allocate(mesh%tz(mesh%Np, mesh%Nelements))

    ! Gradient matrices
    allocate(mesh%Vr(mesh%Np, mesh%Np))
    allocate(mesh%Vs(mesh%Np, mesh%Np))
    allocate(mesh%Vt(mesh%Np, mesh%Np))

    allocate(mesh%Lr(mesh%Np, mesh%Np))
    allocate(mesh%Ls(mesh%Np, mesh%Np))
    allocate(mesh%Lt(mesh%Np, mesh%Np))

    allocate(mesh%Lgrad(mesh%DIM*mesh%Np, mesh%Np))

    ! allocate the matrix for the correction function coefficients
    allocate(mesh%phifj_solution_r(mesh%Np, mesh%Nflux))
    allocate(mesh%phifj_solution_s(mesh%Np, mesh%Nflux))
    allocate(mesh%phifj_solution_t(mesh%Np, mesh%Nflux))

    allocate(mesh%phifj_solution_grad(mesh%DIM*mesh%Np, mesh%Nflux))

  end subroutine allocate_mesh
  
  subroutine get_element_vertices(mesh, index, xv, yv, zv)
    integer(c_int),                 intent(in ) :: index
    type(mesh2d),                   intent(in ) :: mesh
    real(c_double),                 intent(inout) :: xv(mesh%elem_num_verts)
    real(c_double),                 intent(inout) :: yv(mesh%elem_num_verts)
    real(c_double),                 intent(inout) :: zv(mesh%elem_num_verts)

    !locals
    integer(c_int) :: vertices(mesh%elem_num_verts)
    integer(c_int) :: i

    vertices = mesh%element2vertices(index, :)
    
    do i = 1, mesh%elem_num_verts
       xv(i) = mesh%points(vertices(i), 1)
       yv(i) = mesh%points(vertices(i), 2)
       zv(i) = ZERO

       if (mesh%DIM .eq. 3) then
          zv(i) = mesh%points(vertices(i), 3)
       end if

    end do
    
  end subroutine get_element_vertices

  subroutine get_element_solution_points(mesh, index, x, y, z)
    use polynomials_module
    integer,                        intent(in ) :: index
    type(mesh2d),                   intent(in ) :: mesh
    real(c_double),                 intent(out) :: x(:), y(:), z(:)

    ! locals
    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)
    
    ! get the vertices for the requested element
    call get_element_vertices(mesh, index, xv, yv, zv)

    ! get the solution points in physical space
    call rs2xy(mesh%r, mesh%s, mesh%t, mesh%rv, mesh%sv, mesh%tv, xv, yv, zv, x, y, z)

  end subroutine get_element_solution_points

  subroutine get_face_points(mesh, elem_index, face_index, x, y, z)
    use polynomials_module
    integer(c_int), intent(in ) :: elem_index, face_index
    type(mesh2d),   intent(in ) :: mesh
    real(c_double), intent(out) :: x(:), y(:), z(:)

    ! locals
    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: xf(mesh%Nflux), yf(mesh%Nflux), zf(mesh%Nflux)
    integer(c_int) :: indices(2)
    
    ! get the vertices for the requested element
    call get_element_vertices(mesh, elem_index, xv, yv, zv)

    ! get the flux points in physical space
    call rs2xy(mesh%rf, mesh%sf, mesh%tf, mesh%rv, mesh%sv, mesh%tv, &
               xv, yv, zv, xf, yf, zf)

    ! get the coordinates for the particular face
    indices(1) = mesh%P1 * (face_index-1) + 1
    indices(2) = mesh%P1 * face_index

    x(:) = xf(indices(1):indices(2))
    y(:) = yf(indices(1):indices(2))
    z(:) = zf(indices(1):indices(2))
    
  end subroutine get_face_points

  function get_element_area(mesh, index) result(avgarea)
    integer(c_int),          intent(in) :: index
    type(mesh2d),            intent(in) :: mesh
    real(c_double)                      :: avgarea

    ! locals
    real(c_double) :: area(mesh%Np)

    ! ! locals
    ! real(c_double) :: xv(mesh%elem_num_faces)
    ! real(c_double) :: yv(mesh%elem_num_faces)
    ! real(c_double) :: zv(mesh%elem_num_faces)

    ! real(c_double) :: p2, q2, a2, b2, c2, d2

    ! call get_element_vertices(mesh, index, xv, yv, zv)

    area = mesh%reference_area * mesh%detJn(:, index)
    avgarea = ONE/mesh%Np * sum(area(:))

    ! if (MESH_ELEM_TYPE .eq. 3) then
    !    area = HALF*abs( xv(1)*(yv(3) - yv(2)) + &
    !                     xv(2)*(yv(1) - yv(3)) + &
    !                     xv(3)*(yv(2) - yv(1)) )
    ! else
    !    a2 = (xv(2)-xv(1))**2 + (yv(2)-yv(1))**2
    !    b2 = (xv(3)-xv(2))**2 + (yv(3)-yv(2))**2
    !    c2 = (xv(4)-xv(3))**2 + (yv(4)-yv(3))**2
    !    d2 = (xv(1)-xv(4))**2 + (yv(1)-yv(4))**2
       
    !    p2 = (xv(2)-xv(4))**2 + (yv(2)-yv(4))**2
    !    q2 = (xv(1)-xv(3))**2 + (yv(1)-yv(3))**2

    !    area = FOURTH * sqrt(FOUR*p2*q2 - (b2 + d2 - a2 - c2)**2)
    ! end if

  end function get_element_area

  ! subroutine get_jacobians(mesh)
  !   use deepfry_constants_module
  !   type(mesh2d), intent(inout) :: mesh

  !   !locals
  !   integer(c_int) :: index, i
  !   real(c_double) :: xv(3), yv(3)

  !   if ( MESH_ELEM_TYPE .eq. TRIANGLES ) then
    
  !      do index = 1, mesh%nelements
  !         call get_element_vertices(mesh, index, xv, yv)
          
  !         mesh%Jn(index, :, 1, 1) = HALF*( xv(2) - xv(1) )
  !         mesh%Jn(index, :, 1, 2) = HALF*( xv(3) - xv(1) )
          
  !         mesh%Jn(index, :, 2, 1) = HALF*( yv(2) - yv(1) )
  !         mesh%Jn(index, :, 2, 2) = HALF*( yv(3) - yv(1) )
          
  !         ! store the transpose
  !         mesh%JnT(index, :, 1, 1) = mesh%Jn(index, :, 1, 1)
  !         mesh%JnT(index, :, 1, 2) = mesh%Jn(index, :, 2, 1)
          
  !         mesh%JnT(index, :, 2, 1) = mesh%Jn(index, :, 1, 2)
  !         mesh%JnT(index, :, 2, 2) = mesh%Jn(index, :, 2, 2)
          
  !         mesh%detJn(index, :) = mesh%Jn(index, :, 1,1)*mesh%Jn(index, :, 2,2) - &
  !                                mesh%Jn(index, :, 1,2)*mesh%Jn(index, :, 2,1)
       
  !         mesh%Jninv(index, :, 1, 1) =  ONE/mesh%detJn(index,:)*mesh%Jn(index, :, 2,2)
  !         mesh%Jninv(index, :, 1, 2) = -ONE/mesh%detJn(index,:)*mesh%Jn(index, :, 1,2)
  !         mesh%Jninv(index, :, 2, 1) = -ONE/mesh%detJn(index,:)*mesh%Jn(index, :, 2,1)
  !         mesh%Jninv(index, :, 2, 2) =  ONE/mesh%detJn(index,:)*mesh%Jn(index, :, 1,1)
          
  !      end do
       
  !   else if (MESH_ELEM_TYPE .eq. QUADS) then
  !      do index = 1, mesh%Nelements

  !         do i = 1, mesh%Np
  !            ! Jacobian matrices
  !            mesh%Jn(index, i, 1,1) = mesh%xr(i, index)
  !            mesh%Jn(index, i, 1,2) = mesh%xs(i, index)
  !            mesh%Jn(index, i, 2,1) = mesh%yr(i, index)
  !            mesh%Jn(index, i, 2,2) = mesh%ys(i, index)

  !            ! Jacobian transpose
  !            mesh%JnT(index, i, 1,1) = mesh%xr(i, index)
  !            mesh%JnT(index, i, 1,2) = mesh%yr(i, index)
  !            mesh%JnT(index, i, 2,1) = mesh%xs(i, index)
  !            mesh%JnT(index, i, 2,2) = mesh%ys(i, index)

  !            ! Determinant
  !            mesh%detJn(index, i) = mesh%Jn(index, i, 1, 1)*mesh%Jn(index, i, 2, 2) - &
  !                                   mesh%Jn(index, i, 1, 2)*mesh%Jn(index, i, 2, 1)

  !            ! Inverse
  !            mesh%Jninv(index, i, 1, 1) =  ONE/mesh%detJn(index, i)*mesh%Jn(index, i, 2, 2)
  !            mesh%Jninv(index, i, 1, 2) = -ONE/mesh%detJn(index, i)*mesh%Jn(index, i, 1, 2)
  !            mesh%Jninv(index, i, 2, 1) = -ONE/mesh%detJn(index, i)*mesh%Jn(index, i, 2, 1)
  !            mesh%Jninv(index, i, 2, 2) =  ONE/mesh%detJn(index, i)*mesh%Jn(index, i, 1, 1)
  !         end do

  !      end do
  !   end if
       
  ! end subroutine get_jacobians

  subroutine set_element_centroids(mesh)
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: index
    real(c_double), allocatable :: xv(:), yv(:), zv(:)
    real(c_double)              :: factor

    !real(c_double) :: xmin, xmax
    !real(c_double) :: ymin, ymax

    allocate(xv(mesh%elem_num_verts))
    allocate(yv(mesh%elem_num_verts))
    allocate(zv(mesh%elem_num_verts))

    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       factor = THIRD
    else if (MESH_ELEM_TYPE .eq. QUADS) then
       factor = FOURTH
    else if (MESH_ELEM_TYPE .eq. HEXS) then
       factor = EIGHTH
    end if

    mesh%zc = ZERO
    do index = 1, mesh%nelements
       call get_element_vertices(mesh, index, xv, yv, zv)

       mesh%xc(index) = factor*sum(xv)
       mesh%yc(index) = factor*sum(yv)

#ifdef DIM3
       mesh%zc(index) = factor*sum(zv)
#endif

    end do

    deallocate(xv, yv, zv)

    ! setup the bounding box for the mesh
    mesh%bbox%xmin = minval( mesh%points(:,1) )! - 0.0001_c_double
    mesh%bbox%xmax = maxval( mesh%points(:,1) ) + 0.0001_c_double

    mesh%bbox%ymin = minval( mesh%points(:,2) )! - 0.0001_c_double
    mesh%bbox%ymax = maxval( mesh%points(:,2) ) + 0.0001_c_double

    mesh%bbox%zmin = ZERO
    mesh%bbox%zmax = ZERO

    if (mesh%DIM .eq. 3) then
       mesh%bbox%zmin = minval( mesh%points(:,3) )! - 0.0001_c_double
       mesh%bbox%zmax = maxval( mesh%points(:,3) ) + 0.0001_c_double
    end if

  end subroutine set_element_centroids

  ! subroutine set_face_centroids(mesh)
  !   type(mesh2d), intent(inout) :: mesh

  !   ! locals
  !   integer(c_int) :: index
  !   real(c_double) :: xa(2), xb(2)

  !   do index = 1, mesh%Nfaces
  !      xa = mesh%points( mesh%xfaces(index, 1), : )
  !      xb = mesh%points( mesh%xfaces(index, 2), : )

  !      mesh%xfc(index) = HALF * (xa(1) + xb(1))
  !      mesh%yfc(index) = HALF * (xa(2) + xb(2))
  !   end do

  ! end subroutine set_face_centroids

  subroutine set_flux_points(mesh)
    use polynomials_module
    use input_module, only: solution_nodes

    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: index, face
    integer(c_int) :: xf_id(2)
    
    integer(c_int) :: i, j, k, m

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    !real(c_double) :: zv(mesh%elem_num_verts)

    real(c_double) :: xf(mesh%p1), yf(mesh%p1)

    real(c_double) :: rgauss(mesh%P+1), rgauss1(mesh%P+1), wgauss(mesh%P+1)
    real(c_double) :: rface(mesh%P1), sface(mesh%P1), tface(mesh%P1)

    ! Gauss nodes along each face
    call get_gauss_nodes(mesh%p, rgauss, wgauss)
    do index = 1, size(rgauss)
       rgauss1(size(rgauss1) - (index-1)) = rgauss(index)
    end do

    if (MESH_ELEM_TYPE .eq. TRIANGLES) then 
       ! set the flux points in local coordinates
       xv = (/ -ONE, ONE, -ONE /)
       yv = (/ -ONE, -ONE, ONE /)

       do face = 1, 3
          xf_id = get_face_vertex_ids(face)
          xf(1) = xv(xf_id(1)); xf(2) = xv(xf_id(2))
          yf(1) = yv(xf_id(1)); yf(2) = yv(xf_id(2))
          
          rface = xf(1) + HALF*(1 + rgauss)*(xf(2) - xf(1))
          sface = yf(1) + HALF*(1 + rgauss)*(yf(2) - yf(1))
          
          mesh%rf( (face-1)*mesh%p1+1:face*mesh%p1 ) = rface
          mesh%sf( (face-1)*mesh%p1+1:face*mesh%p1 ) = sface
       end do

    else if (MESH_ELEM_TYPE .eq. QUADS) then

       if (solution_nodes .eq. GAUSS_NODES) then
          call get_gauss_nodes(mesh%P, rgauss, wgauss)
       else if (solution_nodes .eq. LOBATTO_NODES) then
          call get_gauss_lobatto_nodes(mesh%P, rgauss, wgauss)
       end if

       do index = 1, size(rgauss)
          rgauss1(size(rgauss1) - (index-1)) = rgauss(index)
       end do

       ! bottom face
       mesh%rf(1:mesh%P1) = rgauss
       mesh%sf(1:mesh%P1) = -1

       ! right face
       mesh%rf(mesh%P1+1:2*mesh%P1) = 1
       mesh%sf(mesh%P1+1:2*mesh%P1) = rgauss

       ! top face
       mesh%rf(2*mesh%P1+1:3*mesh%P1) = rgauss1
       mesh%sf(2*mesh%P1+1:3*mesh%P1) = 1
       
       ! left face
       mesh%rf(3*mesh%P1+1:) = -1
       mesh%sf(3*mesh%P1+1:) = rgauss1

    else if (MESH_ELEM_TYPE .eq. HEXS) then

       if (solution_nodes .eq. GAUSS_NODES) then
          call get_gauss_nodes(mesh%P, rgauss, wgauss)
       else if (solution_nodes .eq. LOBATTO_NODES) then
          call get_gauss_lobatto_nodes(mesh%P, rgauss, wgauss)
       end if

       ! bottom face (face 1)
       m = 0
       do k = 1, mesh%P+1
          do i = 1, mesh%P+1
             m = m + 1
             rface(m) = rgauss(i)
             sface(m) = -ONE
             tface(m) = rgauss(k)
          end do
       end do

       mesh%rf(1:mesh%P1) = rface
       mesh%sf(1:mesh%P1) = sface
       mesh%tf(1:mesh%P1) = tface

       ! right face (face 2)
       m = 0
       do k = 1, mesh%P+1
          do j = 1, mesh%P+1
             m = m + 1
             rface(m) = ONE
             sface(m) = rgauss(j)
             tface(m) = rgauss(k)
          end do
       end do

       mesh%rf(mesh%P1+1:2*mesh%P1) = rface
       mesh%sf(mesh%P1+1:2*mesh%P1) = sface
       mesh%tf(mesh%P1+1:2*mesh%P1) = tface

       ! top face (face 3)
       m = 0
       do k = 1, mesh%P+1
          do i = 1, mesh%P+1
             m = m + 1
             rface(m) = rgauss(i)
             sface(m) = ONE
             tface(m) = rgauss(k)
          end do
       end do

       mesh%rf(2*mesh%P1+1:3*mesh%P1) = rface
       mesh%sf(2*mesh%P1+1:3*mesh%P1) = sface
       mesh%tf(2*mesh%P1+1:3*mesh%P1) = tface

       ! left face (4)
       m = 0
       do k = 1, mesh%P+1
          do j = 1, mesh%P+1
             m = m + 1
             rface(m) = -ONE
             sface(m) = rgauss(j)
             tface(m) = rgauss(k)
          end do
       end do

       mesh%rf(3*mesh%P1+1:4*mesh%P1) = rface
       mesh%sf(3*mesh%P1+1:4*mesh%P1) = sface
       mesh%tf(3*mesh%P1+1:4*mesh%P1) = tface

       ! back face (face 5)
       m = 0
       do j = 1, mesh%P+1
          do i = 1, mesh%P+1
             m = m + 1
             rface(m) = rgauss(i)
             sface(m) = rgauss(j)
             tface(m) = -ONE
          end do
       end do

       mesh%rf(4*mesh%P1+1:5*mesh%P1) = rface
       mesh%sf(4*mesh%P1+1:5*mesh%P1) = sface
       mesh%tf(4*mesh%P1+1:5*mesh%P1) = tface

       ! front face (face 6)
       m = 0
       do j = 1, mesh%P+1
          do i = 1, mesh%P+1
             m = m + 1
             rface(m) = rgauss(i)
             sface(m) = rgauss(j)
             tface(m) = ONE
          end do
       end do

       mesh%rf(5*mesh%P1+1:6*mesh%P1) = rface
       mesh%sf(5*mesh%P1+1:6*mesh%P1) = sface
       mesh%tf(5*mesh%P1+1:6*mesh%P1) = tface

    end if

  end subroutine set_flux_points

  subroutine sort_face_indices(mesh)
    use m_mrgref
    use polynomials_module

    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: elem_index, face_index, j
    integer(c_int) :: sorted_indices(mesh%P1)

    real(c_double) :: xf(mesh%P1), yf(mesh%P1), zf(mesh%P1)

    real(c_double) :: xv(mesh%elem_num_verts)
    real(c_double) :: yv(mesh%elem_num_verts)
    real(c_double) :: zv(mesh%elem_num_verts)
    
    real(c_double) :: rf(mesh%P1), sf(mesh%P1), tf(mesh%P1)

    zf = ZERO

    do elem_index = 1, mesh%Nelements
       call get_element_vertices(mesh, elem_index, xv, yv, zv)

       do face_index = 1, mesh%elem_num_faces
          
          rf = mesh%rf( (face_index-1)*mesh%P1+1:face_index*mesh%P1 )
          sf = mesh%sf( (face_index-1)*mesh%P1+1:face_index*mesh%P1 )
          tf = mesh%tf( (face_index-1)*mesh%P1+1:face_index*mesh%P1 )

          ! get the physical coordinates for the face points
          call rs2xy(rf, sf, tf, &
                     mesh%rv, mesh%sv, mesh%tv, &
                     xv, yv, zv, xf, yf, zf)

          ! sort the face indices and store it in the mesh data
          ! structure
          call mrgref(xf, yf, zf, sorted_indices)

          mesh%sfi(:, face_index, elem_index) = sorted_indices(:)

       end do
    end do

  end subroutine sort_face_indices

  subroutine get_mesh_characteristic_length(mesh)
    type(mesh2d),   intent(inout) :: mesh

    ! locals
    integer(c_int) :: elem_index, elem_faces(mesh%elem_num_faces)
    real(c_double) :: s, area, face_lengths(mesh%elem_num_faces)
    real(c_double) :: hmin, hmax
    
    hmin =  LARGE
    hmax = -LARGE

    do elem_index = 1, mesh%Nelements
       elem_faces = mesh%elem2face(elem_index, :)
       s = ZERO
       area = get_element_area(mesh, elem_index)

       if (MESH_ELEM_TYPE .eq. QUADS .or. MESH_ELEM_TYPE .eq. TRIANGLES) then
       call get_element_face_lengths(mesh, elem_index, face_lengths)
          s    = HALF * sum(face_lengths)
          hmin = min(hmin, area/s)
          hmax = max(hmax, area/s)
       else
          hmax = max(hmax, area**(THIRD))
          hmin = min(hmin, area**(THIRD))
          s = ONE
       end if

       mesh%characteristic_length(elem_index) = hmax

    end do

    mesh%hmin = hmin
    mesh%hmax = hmax

  end subroutine get_mesh_characteristic_length

  subroutine get_element_face_lengths(mesh, elem_index, face_lengths)
    type(mesh2d),   intent(in ) :: mesh
    integer(c_int), intent(in ) :: elem_index
    real(c_double), intent(out) :: face_lengths(mesh%elem_num_faces)

    ! locals
    integer(c_int) :: face

    do face = 1, mesh%elem_num_faces
       face_lengths(face) = get_face_length(mesh, mesh%elem2face(elem_index, face))
    end do

  end subroutine get_element_face_lengths

  pure function estimate_dt(P, h, ivmax) result(dt)
    use input_module, only: cfl, vmax
    integer(c_int), intent(in) :: P
    real(c_double), intent(in) :: h
    real(c_double), optional, intent(in) :: ivmax
    real(c_double) :: dt

    ! locals
    real(c_double) :: maxvel
    maxvel = vmax; if (present(ivmax)) maxvel = ivmax

    dt = cfl*h*ONE/(2*P + ONE) * ONE/maxvel * TWO3RD

  end function estimate_dt
  
  pure function face2nbr(face) result(nbr)
    integer(c_int), intent(in ) :: face
    integer(c_int)              :: nbr

    nbr = -1
    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       if (face .eq. 1) then
          nbr  = 3
       else if (face .eq. 2) then
          nbr = 1
       else
          nbr = 2
       end if
    end if

  end function face2nbr

  pure function nbr2face(nbr) result(face)
    integer(c_int), intent(in ) :: nbr
    integer(c_int)              :: face

    face = -1
    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       if (nbr .eq. 1) then
          face  = 2
       else if (nbr .eq. 2) then
          face = 3
       else
          face = 1
       end if
    end if

  end function nbr2face

  pure function get_face_length(mesh, face_index) result(face_length)
    type(mesh2d), intent(in) :: mesh
    integer(c_int),                 intent(in) :: face_index
    real(c_double)                             :: face_length

    ! locals
    real(c_double) :: xa(2), xb(2)

    xa = mesh%points( mesh%xfaces(face_index, 1), : )
    xb = mesh%points( mesh%xfaces(face_index, 2), : )

    face_length = sqrt( (xa(1)-xb(1))**2 + (xa(2)-xb(2))**2 )

  end function get_face_length

  pure function is_valid_neighbor(mesh, index) result(is_valid)
    type(mesh2d), intent(in) :: mesh
    integer(c_int),                 intent(in) :: index
    logical                                    :: is_valid

    if ( (index .ge. 1) .and. (index .le. mesh%nelements) ) then
       is_valid = .true.
    else
       is_valid = .false.
    end if

  end function is_valid_neighbor

  subroutine get_directed_normals(mesh, elem, normals, inward)
    type(mesh2d), intent(in )   :: mesh
    integer(c_int),                 intent(in )   :: elem
    real(c_double),                 intent(inout) :: normals(:,:)
    logical, optional,              intent(in )   :: inward

    ! locals
    logical        :: iinward
    integer(c_int) :: i, face
    integer(c_int) :: face_indices(mesh%elem_num_faces)

    iinward = .false. ; if (present(inward)) iinward = inward

    face_indices = mesh%elem2face(elem, :)
    do i = 1, mesh%elem_num_faces
       normals(i, :) = mesh%normals(face_indices(i), :)
    end do
    ! normals(1,:) = mesh%normals(face_indices(1), :)
    ! normals(2,:) = mesh%normals(face_indices(2), :)
    ! normals(3,:) = mesh%normals(face_indices(3), :)

    ! correct the normals so that they point out of the element
    do face = 1, mesh%elem_num_faces
       if ( mesh%face2elem(face_indices(face), 1) .ne. elem ) then
          normals(face,1) = -normals(face, 1)
          normals(face,2) = -normals(face, 2)
       end if
    end do

    ! flip the normals to point inward
    if (iinward) then
       normals(:,:) = -normals(:,:)
    end if
    
  end subroutine get_directed_normals

  subroutine setup_faceindex2elemfaceindex(mesh)
    use input_module, only: debug
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: j, tmp_index, face_index, left, rght
    integer(c_int) :: face2elem(2)
    integer(c_int) :: elem2face_left(mesh%elem_num_faces)
    integer(c_int) :: elem2face_rght(mesh%elem_num_faces)
    integer(c_int) :: left_face_index, rght_face_index

    do tmp_index = 1, mesh%Ninterior_faces
       face_index = mesh%interior_faces(tmp_index)
       
       ! get the elements abutting this face
       face2elem = mesh%face2elem(face_index, :)
       left = face2elem(1)
       rght = face2elem(2)

       ! get the global face indices for the elements
       elem2face_left(:) = mesh%elem2face(left, :)
       elem2face_rght(:) = mesh%elem2face(rght, :)

       ! find the face indices for each element abutting this face
       do j = 1, mesh%elem_num_faces
          if( elem2face_left(j) .eq. face_index) left_face_index = j 
          if( elem2face_rght(j) .eq. face_index) rght_face_index = j
       end do

       ! Sanity check for the mesh
       if (debug) then
          if ( (left_face_index .eq. -1) .or. (rght_face_index .eq. -1) ) then
             write(*, *) 'Incorrect face index', face_index
             stop
          end if
       end if

       mesh%faceindex2elemfaceindex(face_index, 1) = left_face_index
       mesh%faceindex2elemfaceindex(face_index, 2) = rght_face_index

    end do

    ! do the boundary faces. Only the left element is valid for these
    ! faces as the normal points outwards and hence into nothing
    do tmp_index = 1, mesh%Nbfaces
       face_index = mesh%bfaces(tmp_index)
       
       ! get the element abutting this face and the elem2face mapping
       ! for that element
       left = mesh%face2elem(face_index, 1)
       elem2face_left(:) = mesh%elem2face(left, :)

       ! find the face indices for each element abutting this face
       do j = 1, mesh%elem_num_faces
          if( elem2face_left(j) .eq. face_index) left_face_index = j 
       end do

       ! Sanity check. A boundary face should have a valid interior
       ! cell
       if (debug) then
          if ( (left_face_index .eq. -1) ) then
             write(*, *) 'Incorrect face index', face_index
             stop
          end if
       end if

       mesh%faceindex2elemfaceindex(face_index, 1) = left_face_index
    end do

  end subroutine setup_faceindex2elemfaceindex

  pure function get_face_vertex_ids(face) result(xf)
    integer(c_int), intent(in) :: face
    integer(c_int)             :: xf(2)

    if (face .eq. 1) then
       xf(1) = 1; xf(2) = 2

    else if (face .eq. 2) then
       xf(1) = 2; xf(2) = 3

    else 
       xf(1) = 3; xf(2) = 1
    end if

  end function get_face_vertex_ids

  subroutine delete_mesh(mesh)
    type(mesh2d), intent(inout) :: mesh

    deallocate(mesh%Nremote_per_part)
    deallocate(mesh%Nimport_per_part)
    deallocate(mesh%Nexport_per_part)

    deallocate(mesh%numExport)

    if (mesh%numPartitions .gt. 1) then
       deallocate(mesh%exportIndices)
       ! deallocate(mesh%exportProcs)

       ! deallocate(mesh%importIndices)
       ! deallocate(mesh%importProcs)
       deallocate(mesh%importShifts)

       if (df_is_periodic) then
          deallocate(mesh%nimport_per_part_periodic)
          deallocate(mesh%nexport_per_part_periodic)
          
          deallocate(mesh%exportIndices_periodic_tmp)
          deallocate(mesh%exportIndices_periodic)
          
          deallocate(mesh%exportFaces_periodic_tmp)
          deallocate(mesh%exportFaces_periodic)

          deallocate(mesh%importIndices_periodic_tmp)
          deallocate(mesh%importIndices_periodic)
          
          deallocate(mesh%importFaces_periodic_tmp)
          deallocate(mesh%importFaces_periodic)
       end if

    end if

    deallocate(mesh%Proc)
    
    deallocate(mesh%points)
    deallocate(mesh%xfaces)
    deallocate(mesh%element2vertices)
    deallocate(mesh%elem2face)
    deallocate(mesh%face2elem)
    deallocate(mesh%neighbors)

    deallocate(mesh%local_nodes)

    deallocate(mesh%index_extern)
    deallocate(mesh%sfi)

    deallocate(mesh%nbr_face_indices)
    deallocate(mesh%faceindex2nbr)
    deallocate(mesh%faceindex2nbrfaceindex)

    deallocate(mesh%faceindex2elemfaceindex)
    
    ! deallocate centroids
    deallocate(mesh%xc)
    deallocate(mesh%yc)
    deallocate(mesh%zc)

    deallocate(mesh%elem_type)
    deallocate(mesh%elem_attrs)

    deallocate(mesh%face_markers)
    deallocate(mesh%interior_faces)
    deallocate(mesh%bfaces)

    deallocate(mesh%normals)
    deallocate(mesh%pnormals)
    deallocate(mesh%magnormals)
    deallocate(mesh%tangents)
    deallocate(mesh%face_types)

    deallocate(mesh%r)
    deallocate(mesh%s)
    deallocate(mesh%t)
    deallocate(mesh%w)

    deallocate(mesh%rf)
    deallocate(mesh%sf)
    deallocate(mesh%tf)

    deallocate(mesh%rv)
    deallocate(mesh%sv)
    deallocate(mesh%tv)

    deallocate(mesh%Jn)
    deallocate(mesh%JnTinv)
    deallocate(mesh%detJn)
    !deallocate(mesh%JnT)

    deallocate(mesh%V)
    deallocate(mesh%L)
    deallocate(mesh%Vinv)

    deallocate(mesh%Vv)
    deallocate(mesh%Lv)

    deallocate(mesh%Vf)
    deallocate(mesh%Lf)

    deallocate(mesh%Vplot)
    deallocate(mesh%Lplot)
    
    deallocate(mesh%Vf_r)
    deallocate(mesh%Vf_s)

    deallocate(mesh%Lf_r)
    deallocate(mesh%Lf_s)
    deallocate(mesh%Lf_t)

    deallocate(mesh%Lf_grad)

    deallocate(mesh%Filter)

    deallocate(mesh%xr)
    deallocate(mesh%xs)
    deallocate(mesh%xt)

    deallocate(mesh%yr)
    deallocate(mesh%ys)
    deallocate(mesh%yt)

    deallocate(mesh%zr)
    deallocate(mesh%zs)
    deallocate(mesh%zt)    

    deallocate(mesh%xfr)
    deallocate(mesh%xfs)
    deallocate(mesh%xft)

    deallocate(mesh%yfr)
    deallocate(mesh%yfs)
    deallocate(mesh%yft)

    deallocate(mesh%zfr)
    deallocate(mesh%zfs)
    deallocate(mesh%zft)

    deallocate(mesh%rx)
    deallocate(mesh%ry)
    deallocate(mesh%rz)

    deallocate(mesh%sx)
    deallocate(mesh%sy)
    deallocate(mesh%sz)

    deallocate(mesh%tx)
    deallocate(mesh%ty)
    deallocate(mesh%tz)

    deallocate(mesh%Vr)
    deallocate(mesh%Vs)
    deallocate(mesh%Vt)
    
    deallocate(mesh%Lr)
    deallocate(mesh%Ls)
    deallocate(mesh%Lt)

    deallocate(mesh%Lgrad)

    deallocate(mesh%phifj_solution_r)
    deallocate(mesh%phifj_solution_s)
    deallocate(mesh%phifj_solution_t)

    deallocate(mesh%phifj_solution_grad)

    deallocate(mesh%bc_map)

    deallocate(mesh%Pnm)

    deallocate(mesh%basis_degrees)

    deallocate(mesh%Nsvj)
    deallocate(mesh%SVj)

    deallocate(mesh%vertex_var_min)
    deallocate(mesh%vertex_var_max)

    deallocate(mesh%characteristic_length)

    deallocate(mesh%STj_var_max)
    deallocate(mesh%STj_var_min)
    
  end subroutine delete_mesh
  
end module mesh_module
   
