module hdf5_writer_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use mesh_module
  use fieldvar_module

  use hdf5

  implicit none

contains

  subroutine write_hdf5_nodes(mesh)
    use polynomials_module
    use input_module, only: MESH_ELEM_TYPE

    type(mesh2d), intent(in) :: mesh
    
    character(len=256) :: filename

    integer(HSIZE_T) :: dims(1)
    integer(HID_T)   :: file_id
    integer(HID_T)   :: dset_id
    integer(HID_T)   :: dspace_id

    integer            :: error, rank
    character(len=3)   :: partition_index

    character(len=8), parameter :: dset_meshinfo = "meshinfo"
    character(len=1), parameter :: dset_x     = "x"
    character(len=1), parameter :: dset_y     = "y"
    character(len=1), parameter :: dset_z     = "z"

    integer(c_int) :: elem_index
    real(c_double) :: xdata(mesh%Nelements*mesh%Np)
    real(c_double) :: ydata(mesh%Nelements*mesh%Np)
    real(c_double) :: zdata(mesh%Nelements*mesh%Np)
    real(c_double) :: xv(mesh%elem_num_verts), yv(mesh%elem_num_verts), zv(mesh%elem_num_verts)
    real(c_double) :: xs(mesh%Np), ys(mesh%Np), zs(mesh%NP)

    real(c_double) :: meshinfo(5)

    write(unit=partition_index, fmt='(i3.3)') mesh%partition
    filename = "part_" // partition_index // ".nodes"

    ! Initialize the fortran interface
    call h5open_f(error)

    ! create the HDF5 file for the partition
    call h5fcreate_f(trim(filename), H5F_ACC_TRUNC_F, file_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! create the memory (data) space for the data

    dims(1) = 5
    rank    = 1
    call h5screate_simple_f(rank, dims, dspace_id, error)

    ! Create the individual datasets
    meshinfo(1) = mesh%P
    meshinfo(2) = mesh%Np
    meshinfo(3) = MESH_ELEM_TYPE
    meshinfo(4) = mesh%Nelements
    meshinfo(5) = mesh%Nelements_local

    call h5dcreate_f(file_id, dset_meshinfo, H5T_NATIVE_INTEGER, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, meshinfo, dims, error)
    call h5dclose_f(dset_id, error)

    ! Terminate access to the data space
    call h5sclose_f(dspace_id, error)

    dims(1) = mesh%Nelements*mesh%Np
    rank    = 1

    zdata = ZERO
    do elem_index = 1, mesh%Nelements
       zv = ZERO; zs = ZERO
       call get_element_vertices(mesh, elem_index, xv, yv, zv)
       call rs2xy(mesh%r, mesh%s, mesh%t, mesh%rv, mesh%sv, mesh%tv, &
                  xv, yv, zv, xs, ys, zs)
       
       xdata( (elem_index-1)*mesh%Np+1:elem_index*mesh%Np) = xs
       ydata( (elem_index-1)*mesh%Np+1:elem_index*mesh%Np) = ys
       zdata( (elem_index-1)*mesh%Np+1:elem_index*mesh%Np) = zs
    end do

    call h5screate_simple_f(rank, dims, dspace_id, error)

    call h5dcreate_f(file_id, dset_x, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f( dset_id, H5T_NATIVE_DOUBLE, xdata, dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_y, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f( dset_id, H5T_NATIVE_DOUBLE, ydata, dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_z, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f( dset_id, H5T_NATIVE_DOUBLE, zdata, dims, error)
    call h5dclose_f(dset_id, error)

    ! Terminate access to the data space
    call h5sclose_f(dspace_id, error)

    ! close the file
    call h5fclose_f(file_id, error)

    ! close the fortran interface
    call h5close_f(error)

  end subroutine write_hdf5_nodes
  
  subroutine write_hdf5_file(filename, mesh, data, step, time)
    use input_module, only: gamma, plot_conservative, MESH_ELEM_TYPE

    character(len=256), intent(in)  :: filename
    type(mesh2d),      intent(in) :: mesh
    type(FieldData_t), intent(in) :: data
    integer(c_int), intent(in) :: step
    real(c_double), intent(in) :: time
    
    ! locals
    character(len=256) :: plotfile_name
    integer :: error, rank

    integer(HSIZE_T) :: dims(1)
    integer(HID_T)   :: file_id
    integer(HID_T)   :: dset_id
    integer(HID_T)   :: dspace_id    

    character(len=3), parameter :: dset_rho = "rho"
    character(len=2), parameter :: dset_Mx  = "Mx"
    character(len=2), parameter :: dset_My  = "My"
    character(len=1), parameter :: dset_E   = "E"

    character(len=1), parameter :: dset_P = "P"
    character(len=4), parameter :: dset_velx = "velx"
    character(len=4), parameter :: dset_vely = "vely"

    character(len=5), parameter :: dset_rho_x = "rho_x"
    character(len=5), parameter :: dset_rho_y = "rho_y"
    character(len=6), parameter :: dset_velx_x = "velx_x"
    character(len=6), parameter :: dset_velx_y = "velx_y"
    character(len=6), parameter :: dset_vely_x = "vely_x"
    character(len=6), parameter :: dset_vely_y = "vely_y"

    integer(SIZE_T) :: attrlen
    integer(HID_T)  :: atype_id, attr_id, aspace_id

    character(len=5),  parameter :: dset_attributes = "attrs"
    character(len=10), parameter :: dset_attr_names = "attr_names"

    character(len=256), dimension(5) :: attr_names
    real(c_double),     dimension(5) :: attr_data

    real(c_double), allocatable :: velx(:,:), vely(:,:), P(:,:), tmp(:,:)

    allocate(velx(mesh%Np,mesh%Nelements))
    allocate(vely(mesh%Np,mesh%Nelements))
    allocate( tmp(mesh%Np,mesh%Nelements))
    allocate( P(mesh%Np,mesh%Nelements))

    ! Initialize the fortran interface
    call h5open_f(error)

    ! Create a new file
    plotfile_name = trim(filename) // ".hdf5"
    call h5fcreate_f(trim(plotfile_name), H5F_ACC_TRUNC_F, file_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! create the memory (data) space for the data

    dims(1) = mesh%Np*mesh%Nelements
    rank    = 1
    call h5screate_simple_f(rank, dims, dspace_id, error)
    
    ! Create the individual datasets
    call h5dcreate_f(file_id, dset_rho, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 1, :), dims, error)
    call h5dclose_f(dset_id, error)

    velx = data%u(:, 2, :)/data%u(:, 1, :)
    vely = data%u(:, 3, :)/data%u(:, 1, :)
    P    = (gamma-ONE)*(data%u(:, 4, :) - HALF*(velx**2 + vely**2))

    if (plot_conservative) then
       call h5dcreate_f(file_id, dset_Mx, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 2, :), dims, error)
       call h5dclose_f(dset_id, error)

       call h5dcreate_f(file_id, dset_My, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 3, :), dims, error)
       call h5dclose_f(dset_id, error)

       call h5dcreate_f(file_id, dset_E, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%u(:, 4, :), dims, error)
       call h5dclose_f(dset_id, error)
    end if

    ! write out primitive variables
    call h5dcreate_f(file_id, dset_velx, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, velx, dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_vely, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, vely, dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_P, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, P, dims, error)
    call h5dclose_f(dset_id, error)

    ! write out gradients
    call h5dcreate_f(file_id, dset_rho_x, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%ux(:, 1, :), dims, error)
    call h5dclose_f(dset_id, error)

    call h5dcreate_f(file_id, dset_rho_y, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data%uy(:, 1, :), dims, error)
    call h5dclose_f(dset_id, error)

    tmp = ONE/data%u(:, 1, :)*(data%ux(:, 2, :) - velx*data%ux(:, 1, :))
    call h5dcreate_f(file_id, dset_velx_x, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, tmp, dims, error)
    call h5dclose_f(dset_id, error)

    tmp = ONE/data%u(:, 1, :)*(data%uy(:, 2, :) - velx*data%uy(:, 1, :))
    call h5dcreate_f(file_id, dset_velx_y, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, tmp, dims, error)
    call h5dclose_f(dset_id, error)

    tmp = ONE/data%u(:, 1, :)*(data%ux(:, 3, :) - vely*data%ux(:, 1, :))
    call h5dcreate_f(file_id, dset_vely_x, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, tmp, dims, error)
    call h5dclose_f(dset_id, error)

    tmp = ONE/data%u(:, 1, :)*(data%uy(:, 3, :) - vely*data%uy(:, 1, :))
    call h5dcreate_f(file_id, dset_vely_y, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, tmp, dims, error)
    call h5dclose_f(dset_id, error)

    ! Terminate access to the data space
    call h5sclose_f(dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write out attributes

    dims(1) = 5

    attr_data(1) = dble(step)
    attr_data(2) = time
    attr_data(3) = dble(mesh%P)
    attr_data(4) = dble(mesh%Nelements)
    attr_data(5) = dble(MESH_ELEM_TYPE)
    call h5screate_simple_f(rank, dims, dspace_id, error)
    call h5dcreate_f(file_id, dset_attributes, H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, attr_data, dims, error)

    attr_names(1) = "step"
    attr_names(2) = "time"
    attr_names(3) = "order"
    attr_names(4) = "nelements"
    attr_names(5) = "elem_type"
    attrlen = 256

    call h5screate_simple_f(rank, dims, aspace_id, error)
    call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
    call h5tset_size_f(atype_id, attrlen, error)
    call h5acreate_f(dset_id, dset_attr_names, atype_id, aspace_id, attr_id, error)
    call h5awrite_f(attr_id, atype_id, attr_names, dims, error)
    call h5aclose_f(attr_id, error)
    call h5sclose_f(aspace_id, error)

    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   

    ! close the file
    call h5fclose_f(file_id, error)

    ! close the fortran interface
    call h5close_f(error)

    ! deallocate tmp buffers
    deallocate(velx, vely, P, tmp)

  end subroutine write_hdf5_file
  
end module hdf5_writer_module
