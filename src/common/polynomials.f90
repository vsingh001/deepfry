module polynomials_module
  use iso_c_binding, only: c_int, c_double
  use input_module,  only: MESH_ELEM_TYPE
  use deepfry_constants_module
  implicit none
  
  contains

    RECURSIVE FUNCTION Factorial(n)  RESULT(Fact)

      IMPLICIT NONE
      INTEGER :: Fact
      INTEGER, INTENT(IN) :: n

      IF (n == 0) THEN
         Fact = 1
      ELSE
         Fact = n * Factorial(n-1)
      END IF

    END FUNCTION Factorial

    function inv(A) result(Ainv)
      real(c_double), dimension(:,:), intent(in) :: A
      real(c_double), dimension(size(A,1),size(A,2)) :: Ainv
      
      real(c_double), dimension(size(A,1)) :: work  ! work array for LAPACK
      integer, dimension(size(A,1)) :: ipiv   ! pivot indices
      integer :: n, info
      
      ! External procedures defined in LAPACK
      external DGETRF
      external DGETRI
      
      ! Store A in Ainv to prevent it from being overwritten by LAPACK
      Ainv = A
      n = size(A,1)
      
      ! DGETRF computes an LU factorization of a general M-by-N matrix A
      ! using partial pivoting with row interchanges.
      call DGETRF(n, n, Ainv, n, ipiv, info)
      
      if (info /= 0) then
         stop 'Matrix is numerically singular!'
      end if
      
      ! DGETRI computes the inverse of a matrix using the LU factorization
      ! computed by DGETRF.
      call DGETRI(n, Ainv, n, ipiv, work, n, info)
      
      if (info /= 0) then
         stop 'Matrix inversion failed!'
      end if
    end function inv
    
    subroutine j_polynomial ( m, n, alpha, beta, x, cx )

      !*****************************************************************************80
      !
      !! J_POLYNOMIAL evaluates the Jacobi polynomials J(n,a,b,x).
      !
      !  Differential equation:
      !
      !    (1-X*X) Y'' + (BETA-ALPHA-(ALPHA+BETA+2) X) Y' + N (N+ALPHA+BETA+1) Y = 0
      !
      !  Recursion:
      !
      !    P(0,ALPHA,BETA,X) = 1,
      !
      !    P(1,ALPHA,BETA,X) = ( (2+ALPHA+BETA)*X + (ALPHA-BETA) ) / 2
      !
      !    P(N,ALPHA,BETA,X)  = 
      !      ( 
      !        (2*N+ALPHA+BETA-1) 
      !        * ((ALPHA^2-BETA^2)+(2*N+ALPHA+BETA)*(2*N+ALPHA+BETA-2)*X) 
      !        * P(N-1,ALPHA,BETA,X)
      !        -2*(N-1+ALPHA)*(N-1+BETA)*(2*N+ALPHA+BETA) * P(N-2,ALPHA,BETA,X)
      !      ) / 2*N*(N+ALPHA+BETA)*(2*N-2+ALPHA+BETA)
      !
      !  Restrictions:
      !
      !    -1 < ALPHA
      !    -1 < BETA
      !
      !  Norm:
      !
      !    Integral ( -1 <= X <= 1 ) ( 1 - X )^ALPHA * ( 1 + X )^BETA 
      !      * P(N,ALPHA,BETA,X)^2 dX 
      !    = 2^(ALPHA+BETA+1) * Gamma ( N + ALPHA + 1 ) * Gamma ( N + BETA + 1 ) /
      !      ( 2 * N + ALPHA + BETA ) * N! * Gamma ( N + ALPHA + BETA + 1 )
      !
      !  Special values:
      !
      !    P(N,ALPHA,BETA,1) = (N+ALPHA)!/(N!*ALPHA!) for integer ALPHA.
      !
      !  Licensing:
      !
      !    This code is distributed under the GNU LGPL license. 
      !
      !  Modified:
      !
      !    19 April 2012
      !
      !  Author:
      !
      !    John Burkardt
      !
      !  Reference:
      !
      !    Milton Abramowitz, Irene Stegun,
      !    Handbook of Mathematical Functions,
      !    National Bureau of Standards, 1964,
      !    ISBN: 0-486-61272-4,
      !    LC: QA47.A34.
      !
      !  Parameters:
      !
      !    Input, integer ( kind = 4 ) M, the number of evaluation points.
      !
      !    Input, integer ( kind = 4 ) N, the highest order polynomial to compute.  
      !    Note that polynomials 0 through N will be computed.
      !
      !    Input, real ( kind = 8 ) ALPHA, one of the parameters defining the Jacobi
      !    polynomials, ALPHA must be greater than -1.
      !
      !    Input, real ( kind = 8 ) BETA, the second parameter defining the Jacobi
      !    polynomials, BETA must be greater than -1.
      !
      !    Input, real ( kind = 8 ) X(M), the point at which the polynomials are 
      !    to be evaluated.
      !
      !    Output, real ( kind = 8 ) CX(M,0:N), the values of the first N+1 Jacobi
      !    polynomials at the point X.
      !

      integer ( c_int) m
      integer ( c_int) n

      real ( kind = c_double ) alpha
      real ( kind = c_double ) beta
      real ( kind = c_double ) cx(1:m,0:n)
      real ( kind = c_double ) c1
      real ( kind = c_double ) c2
      real ( kind = c_double ) c3
      real ( kind = c_double ) c4
      integer ( kind = 4 ) i
      real ( kind = c_double ) r_i
      real ( kind = c_double ) x(m)
      
      if ( alpha <= -ONE ) then
         write ( *, '(a)' ) ' '
         write ( *, '(a)' ) 'J_POLYNOMIAL - Fatal error!'
         write ( *, '(a,g14.6)' ) '  Illegal input value of ALPHA = ', alpha
         write ( *, '(a)' ) '  But ALPHA must be greater than -1.'
         stop
      end if
      
      if ( beta <= -ONE ) then
         write ( *, '(a)' ) ' '
         write ( *, '(a)' ) 'J_POLYNOMIAL - Fatal error!'
         write ( *, '(a,g14.6)' ) '  Illegal input value of BETA = ', beta
         write ( *, '(a)' ) '  But BETA must be greater than -1.'
         stop
      end if
      
      if ( n < 0_c_int ) then
         return
      end if
      
      cx(1:m,0) = ONE !1.0_c_double
      
      if ( n == 0_c_int ) then
         return
      end if
      
      cx(1:m,1) = ( 1.0_c_double + 0.5_c_double * ( alpha + beta ) ) * x(1:m) &
           + 0.5_c_double * ( alpha - beta )
      
      do i = 2, n
         
         r_i = real ( i, kind = c_double ) 
         
         c1 = 2.0_c_double * r_i * ( r_i + alpha + beta ) &
              * ( 2.0_c_double * r_i - 2.0_c_double + alpha + beta )
         
         c2 = ( 2.0_c_double * r_i - 1.0_c_double + alpha + beta ) &
              * ( 2.0_c_double * r_i  + alpha + beta ) &
              * ( 2.0_c_double * r_i - 2.0_c_double + alpha + beta )
         
         c3 = ( 2.0_c_double * r_i - 1.0_c_double + alpha + beta ) &
              * ( alpha + beta ) * ( alpha - beta )
         
         c4 = - 2.0_c_double * ( r_i - 1.0_c_double + alpha ) &
              * ( r_i - 1.0_c_double + beta )  * ( 2.0_c_double * r_i + alpha + beta )
         
         cx(1:m,i) = ( ( c3 + c2 * x(1:m) ) * cx(1:m,i-1) + c4 * cx(1:m,i-2) ) / c1
         
      end do
      
      return
    end subroutine j_polynomial

    function j_double_product_integral ( i, j, a, b )

      !*****************************************************************************80
      !
      !! J_DOUBLE_PRODUCT_INTEGRAL: integral of J(i,x)*J(j,x)*(1-x)^a*(1+x)^b.
      !
      !  Discussion:
      !
      !    VALUE = integral ( -1 <= x <= +1 ) J(i,x)*J(j,x)*(1-x)^a*(1+x)^b dx
      !
      !  Licensing:
      !
      !    This code is distributed under the GNU LGPL license.
      !
      !  Modified:
      !
      !    19 April 2012
      !
      !  Author:
      !
      !    John Burkardt
      !
      !  Parameters:
      !
      !    Input, integer ( kind = 4 ) I, J, the polynomial indices.
      !
      !    Input, real ( kind = 8 ) A, B, the parameters.
      !    -1 < A, B.
      !
      !    Output, real ( kind = 8 ) VALUE, the value of the integral.
      !
      integer ( kind = c_int ) i
      integer ( kind = c_int ) j
      integer ( kind = c_int ) ii
      real ( kind = c_double ) a
      real ( kind = c_double ) b
      real ( kind = c_double ) i_r8
      real ( kind = c_double ) j_double_product_integral
      real ( kind = c_double ) factorial
      real ( kind = c_double ) value
      
      if ( i /= j ) then

         value = 0.0_c_double

      else

         i_r8 = real ( i, kind = c_double )

         factorial = 1.0_c_double

         do ii = 1, i
            factorial = factorial * real ( ii, kind = c_double )
         end do

         value = 2.0_c_double ** ( a + b + 1.0_c_double ) &
              / ( 2.0_c_double * i_r8 + a + b + 1.0_c_double ) &
              * gamma ( i_r8 + a + 1.0_c_double ) &
              * gamma ( i_r8 + b + 1.0_c_double ) &
              / factorial &
              / gamma ( i_r8 + a + b + 1.0_c_double )
         
      end if
      
      j_double_product_integral = value

      return
    end function j_double_product_integral

    subroutine get_gauss_lobatto_nodes(p, r, w)
      integer(c_int), intent(in ) :: p
      real(c_double), intent(out) :: r(:)
      real(c_double), intent(out) :: w(:)

      ! locals
      if ( p .eq. 1 ) then
         r(1) = -ONE
         r(2) =  ONE

         w(1) = ONE
         w(2) = ONE
      else if ( p .eq. 2 ) then
         r(1) = -ONE
         r(2) = ZERO
         r(3) = ONE
         
         w(1) = THIRD
         w(2) = FOUR3RD
         w(3) = THIRD
      else if ( p .eq. 3 ) then
         r(1) = -ONE
         r(2) = -ONE/sqrt(FIVE)
         r(3) =  ONE/sqrt(FIVE)
         r(4) =  ONE

         w(1) = ONE/SIX
         w(2) = FIVE/SIX
         w(3) = FIVE/SIX
         w(4) = ONE/SIX
      else
         print*, 'Incorrect value specified'
         stop
      end if

    end subroutine get_gauss_lobatto_nodes

    subroutine get_gauss_nodes(p, r, w)
      integer(c_int), intent(in ) :: p
      real(c_double), intent(out) :: r(:)
      real(c_double), intent(out) :: w(:)
      
      ! locals
      real(c_double) :: d1, d2, w1, w2

      if (p .eq. 0) then
         r(1) = ZERO
         w(1) = TWO

      else if ( p .eq. 1 ) then
         r(1) = -ONE/M_SQRT_3
         r(2) = +ONE/M_SQRT_3
         
         w(1) = ONE
         w(2) = ONE
         
      else if ( p .eq. 2 ) then
         r(1) = -sqrt(ONE/FIVE3RD)
         r(2) = ZERO
         r(3) = -r(1)

         w(1) = FIVE/NINE
         w(2) = EIGHT/NINE
         w(3) = w(1)

      else if (p .eq. 3) then
         d1 = sqrt( THREE/SEVEN - TWO/SEVEN*sqrt(SIX/FIVE) )
         w1 = (THREE*SIX + sqrt(THREE*TEN))/(SIX*SIX)

         d2 = sqrt( THREE/SEVEN + TWO/SEVEN*sqrt(SIX/FIVE) )
         w2 = (THREE*SIX - sqrt(THREE*TEN))/(SIX*SIX)
         
         r(1) = -d2
         r(2) = -d1
         r(3) = +d1
         r(4) = +d2

         w(1) = w2
         w(2) = w1
         w(3) = w1
         w(4) = w2

      else if (p .eq. 4) then
         d1 = THIRD * sqrt(FIVE - TWO*sqrt(TEN/SEVEN))
         d2 = THIRD * sqrt(FIVE + TWO*sqrt(TEN/SEVEN))

         w1 = (322.0_c_double + 13.0_c_double*sqrt(70.0_c_double))/(900.0_c_double)
         w2 = (322.0_c_double - 13.0_c_double*sqrt(70.0_c_double))/(900.0_c_double)

         r(1) = -d2 ; w(1) = w2
         r(2) = -d1 ; w(2) = w1
         r(3) = ZERO; w(3) = 128.0_c_double/225.0_c_double
         r(4) = +d1 ; w(4) = w1
         r(5) = +d2 ; w(5) = w2

      else if (p .eq. 5) then
         r(1) = -0.9324695142031521_c_double; w(1) = 0.1713244923791704_c_double
         r(2) = -0.6612093864662645_c_double; w(2) = 0.3607615730481386_c_double
         r(3) = -0.2386191860831969_c_double; w(3) = 0.4679139345726910_c_double
         r(4) = +0.2386191860831969_c_double; w(4) = 0.4679139345726910_c_double
         r(5) = +0.6612093864662645_c_double; w(5) = 0.3607615730481386_c_double
         r(6) = +0.9324695142031521_c_double; w(6) = 0.1713244923791704_c_double
         
      else if (p .eq. 6) then
         r(1) = -0.9491079123427585_c_double; w(1) = 0.1294849661688697_c_double
         r(2) = -0.7415311855993945_c_double; w(2) = 0.2797053914892766_c_double
         r(3) = -0.4058451513773972_c_double; w(3) = 0.3818300505051189_c_double
         r(4) = ZERO;                         w(4) = 0.4179591836734694_c_double
         r(5) = +0.4058451513773972_c_double; w(5) = 0.3818300505051189_c_double
         r(6) = +0.7415311855993945_c_double; w(6) = 0.2797053914892766_c_double
         r(7) = +0.9491079123427585_c_double; w(7) = 0.1294849661688697_c_double

      else if (p .eq. 7) then
         r(1) = -0.9602898564975363_c_double; w(1) = 0.1012285362903763_c_double
         r(2) = -0.7966664774136267_c_double; w(2) = 0.2223810344533745_c_double
         r(3) = -0.5255324099163290_c_double; w(3) = 0.3137066458778873_c_double
         r(4) = -0.1834346424956498_c_double; w(4) = 0.3626837833783620_c_double
         r(5) =  0.1834346424956498_c_double; w(5) = 0.3626837833783620_c_double
         r(6) =  0.5255324099163290_c_double; w(6) = 0.3626837833783620_c_double
         r(7) =  0.7966664774136267_c_double; w(7) = 0.2223810344533745_c_double
         r(8) =  0.9602898564975363_c_double; w(8) = 0.1012285362903763_c_double
      end if

    end subroutine get_gauss_nodes

    subroutine get_solution_points(p, r, s, t, w)
      use input_module, only: solution_nodes
      integer,        intent(in ) :: p
      real(c_double), intent(out) :: r(:), s(:), t(:), w(:)

      ! locals
      integer(c_int) :: Np, k, j, i, m
      real(c_double) :: d1, d2
      real(c_double) :: rtmp(P+1), wtmp(P+1)

      if (solution_nodes .eq. GAUSS_NODES) then
         call get_gauss_nodes(P, rtmp, wtmp)
      else if (solution_nodes .eq. LOBATTO_NODES) then
         call get_gauss_lobatto_nodes(P, rtmp, wtmp)
      end if

      if (MESH_ELEM_TYPE .eq. QUADS) then

         m = 0
         do j = 1, P+1
            do i = 1, P+1
               m = m + 1
               r(m) = rtmp(i)
               s(m) = rtmp(j)
               w(m) = wtmp(i)*wtmp(j)
            end do
         end do

      else if (MESH_ELEM_TYPE .eq. HEXS) then
         m = 0
         do  k = 1, P+1
            do j = 1, P+1
               do i = 1, P+1
                  m = m+1
                  r(m) = rtmp(i)
                  s(m) = rtmp(j)
                  t(m) = rtmp(k)
                  w(m) = wtmp(i)*wtmp(j)*wtmp(k)
               end do
            end do
         end do

         ! print*, 'Weight sum:', sum(w)
         ! print*, r
         ! print*, s
         ! print*, t

      else if (MESH_ELEM_TYPE .eq. TRIANGLES) then
         Np = (p+1)*(p+2)/2

         if (p .eq. 0) then
            r(1) = THIRD
            s(1) = THIRD
            
            w(1) = TWO
            
         else if ( p .eq. 1 ) then
            d1 = ONE/SIX
            d2 = TWO/THREE
            r(1) = -ONE + TWO*d1
            r(2) = -ONE + TWO*d1
            r(3) = -ONE + TWO*d2
            
            s(1) = -ONE + TWO*d2
            s(2) = -ONE + TWO*d1
            s(3) = -ONE + TWO*d1
            
            ! integration weights associated with these points
            w(:) = FOUR/SIX

         else if (p .eq. 2) then
            r(1) = -0.81684757298044_c_double;  s(1) =  0.63369514596088_c_double;  w(1) = 0.219903487310666_c_double;
            r(2) = 0.63369514596088_c_double;   s(2) = -0.81684757298044_c_double;  w(2) = 0.219903487310666_c_double;
            r(3) = -0.81684757298044_c_double;  s(3) = -0.81684757298044_c_double;  w(3) = 0.219903487310666_c_double;
            r(4) = -0.108103018168072_c_double; s(4) = -0.783793963663856_c_double; w(4) = 0.446763179356_c_double;
            r(5) = -0.783793963663856_c_double; s(5) = -0.108103018168072_c_double; w(5) = 0.446763179356_c_double;
            r(6) = -0.108103018168072_c_double; s(6) = -0.108103018168072_c_double; w(6) = 0.446763179356_c_double;
            
         else if (p .eq. 3) then
            ! if ( p .eq. 2 ) then
            !    open(unit=100, file='../../../quadrature/williams-shunn-n6-d4-spu.txt', status='old', action='read')
            ! else if (p .eq. 3) then
            !    open(unit=100, file='../../../quadrature/williams-shunn-n10-d5-spu.txt', status='old', action='read')
            ! else if (p .eq. 4) then
            !    open(unit=100, file='../../../quadrature/williams-shunn-n15-d7-spu.txt', status='old', action='read')
            ! else if (p .eq. 5) then
            !    open(unit=100, file='../../../quadrature/williams-shunn-n21-d8-spu.txt', status='old', action='read')            
            ! end if
            
            r(1) = -0.333333333333333_c_double; s(1) = -0.333333333333333_c_double; w(1) = 0.40308597716946_c_double
            r(2) = -0.888871894660414_c_double; s(2) =  0.777743789320828_c_double; w(2) = 0.083911025993298_c_double
            r(3) =  0.777743789320828_c_double; s(3) = -0.888871894660414_c_double; w(3) = 0.083911025993298_c_double
            r(4) = -0.888871894660414_c_double; s(4) = -0.888871894660414_c_double; w(4) = 0.083911025993298_c_double
            r(5) =  0.268421495491446_c_double; s(5) = -0.408932576528214_c_double; w(5) = 0.224196824141774_c_double
            r(6) = -0.859488918963232_c_double; s(6) = -0.408932576528214_c_double; w(6) = 0.224196824141774_c_double
            r(7) = -0.408932576528214_c_double; s(7) = -0.859488918963232_c_double; w(7) = 0.224196824141774_c_double
            r(8) =  0.268421495491446_c_double; s(8) = -0.859488918963232_c_double; w(8) = 0.224196824141774_c_double
            r(9) = -0.859488918963232_c_double; s(9) =  0.268421495491446_c_double; w(9) = 0.224196824141774_c_double
            r(10)= -0.408932576528214_c_double; s(10)=  0.268421495491446_c_double; w(10)= 0.224196824141774_c_double

         else if (p .eq. 4) then
            r(1 ) = -0.928258244608532_c_double;  s(1 ) = 0.856516489217064_c_double;   w(1 ) = 0.035830910024606_c_double;
            r(2 ) = 0.856516489217064_c_double;   s(2 ) = -0.928258244608532_c_double;  w(2 ) = 0.035830910024606_c_double;
            r(3 ) = -0.928258244608532_c_double;  s(3 ) = -0.928258244608532_c_double;  w(3 ) = 0.035830910024606_c_double;
            r(4 ) = -0.516541208464066_c_double;  s(4 ) = 0.033082416928132_c_double;   w(4 ) = 0.25542439176253_c_double;
            r(5 ) = 0.033082416928132_c_double;   s(5 ) = -0.516541208464066_c_double;  w(5 ) = 0.25542439176253_c_double;
            r(6 ) = -0.516541208464066_c_double;  s(6 ) = -0.516541208464066_c_double;  w(6 ) = 0.25542439176253_c_double;
            r(7 ) = -0.051382424445842_c_double;  s(7 ) = -0.897235151108316_c_double;  w(7 ) = 0.15241212477107_c_double;
            r(8 ) = -0.897235151108316_c_double;  s(8 ) = -0.051382424445842_c_double;  w(8 ) = 0.15241212477107_c_double;
            r(9 ) = -0.051382424445842_c_double;  s(9 ) = -0.051382424445842_c_double;  w(9 ) = 0.15241212477107_c_double;
            r(10) = 0.502367262212968_c_double;   s(10) = -0.5969922362364_c_double;    w(10) = 0.11149962005423_c_double;
            r(11) = -0.905375025976568_c_double;  s(11) = -0.5969922362364_c_double;    w(11) = 0.11149962005423_c_double;
            r(12) = -0.5969922362364_c_double;    s(12) = -0.905375025976568_c_double;  w(12) = 0.11149962005423_c_double;
            r(13) = 0.502367262212968_c_double;   s(13) = -0.905375025976568_c_double;  w(13) = 0.11149962005423_c_double;
            r(14) = -0.905375025976568_c_double;  s(14) = 0.502367262212968_c_double;   w(14) = 0.11149962005423_c_double;
            r(15) = -0.5969922362364_c_double;    s(15) = 0.502367262212968_c_double;   w(15) = 0.11149962005423_c_double;

         else if (p .eq. 5) then
            r(1 ) = -0.943774095634672_c_double; s(1 ) = 0.887548191269344_c_double;   w(1 ) = 0.020718749393076_c_double;
            r(2 ) = 0.887548191269344_c_double;  s(2 ) = -0.943774095634672_c_double;  w(2 ) = 0.020718749393076_c_double;
            r(3 ) = -0.943774095634672_c_double; s(3 ) = -0.943774095634672_c_double;  w(3 ) = 0.020718749393076_c_double;
            r(4 ) = -0.645721803061366_c_double; s(4 ) = 0.291443606122732_c_double;   w(4 ) = 0.150789768653476_c_double;
            r(5 ) = 0.291443606122732_c_double;  s(5 ) = -0.645721803061366_c_double;  w(5 ) = 0.150789768653476_c_double;
            r(6 ) = -0.645721803061366_c_double; s(6 ) = -0.645721803061366_c_double;  w(6 ) = 0.150789768653476_c_double;
            r(7 ) = -0.188982808265134_c_double; s(7 ) = -0.622034383469732_c_double;  w(7 ) = 0.195095604746484_c_double;
            r(8 ) = -0.622034383469732_c_double; s(8 ) = -0.188982808265134_c_double;  w(8 ) = 0.195095604746484_c_double;
            r(9 ) = -0.188982808265134_c_double; s(9 ) = -0.188982808265134_c_double;  w(9 ) = 0.195095604746484_c_double;
            r(10) = 0.635801960056998_c_double;  s(10) = -0.702868375458226_c_double;  w(10) = 0.057938538744946_c_double;
            r(11) = -0.932933584598772_c_double; s(11) = -0.702868375458226_c_double;  w(11) = 0.057938538744946_c_double;
            r(12) = -0.702868375458226_c_double; s(12) = -0.932933584598772_c_double;  w(12) = 0.057938538744946_c_double;
            r(13) = 0.635801960056998_c_double;  s(13) = -0.932933584598772_c_double;  w(13) = 0.057938538744946_c_double;
            r(14) = -0.932933584598772_c_double; s(14) = 0.635801960056998_c_double;   w(14) = 0.057938538744946_c_double;
            r(15) = -0.702868375458226_c_double; s(15) = 0.635801960056998_c_double;   w(15) = 0.057938538744946_c_double;
            r(16) = 0.209957823550266_c_double;  s(16) = -0.285607402768638_c_double;  w(16) = 0.09209273319187_c_double;
            r(17) = -0.924350420781628_c_double; s(17) = -0.285607402768638_c_double;  w(17) = 0.09209273319187_c_double;
            r(18) = -0.285607402768638_c_double; s(18) = -0.924350420781628_c_double;  w(18) = 0.09209273319187_c_double;
            r(19) = 0.209957823550266_c_double;  s(19) = -0.924350420781628_c_double;  w(19) = 0.09209273319187_c_double;
            r(20) = -0.924350420781628_c_double; s(20) = 0.209957823550266_c_double;   w(20) = 0.09209273319187_c_double;
            r(21) = -0.285607402768638_c_double; s(21) = 0.209957823550266_c_double;   w(21) = 0.09209273319187_c_double;

         end if
      end if

    end subroutine get_solution_points

    subroutine rs2xy(r, s, t, rv, sv, tv, xv, yv, zv, x, y, z)
      real(c_double), intent(in ) :: r(:), s(:), t(:)
      real(c_double), intent(in ) :: rv(:), sv(:), tv(:)
      real(c_double), intent(in ) :: xv(:), yv(:), zv(:)
      real(c_double), intent(out) :: x(:), y(:), z(:)

      ! locals
      real(c_double), allocatable :: x1(:), y1(:), x2(:), y2(:)

      integer(c_int) :: Nverts, i
      Nverts = size(rv)

      allocate(x1(size(x)))
      allocate(x2(size(x)))

      allocate(y1(size(y)))
      allocate(y2(size(y)))

      x(:) = ZERO
      y(:) = ZERO
      z(:) = ZERO
      
      if (MESH_ELEM_TYPE .eq. TRIANGLES) then

         x = HALF * (-(r+s)*xv(1) + (r+1)*xv(2) + (s+1)*xv(3))
         y = HALF * (-(r+s)*yv(1) + (r+1)*yv(2) + (s+1)*yv(3))

      else if ( MESH_ELEM_TYPE .eq. QUADS ) then
         
         x = ZERO; y = ZERO
         do i = 1, Nverts
            x = x + xv(i) * (FOURTH*(ONE + rv(i)*r)*(ONE + sv(i)*s))
            y = y + yv(i) * (FOURTH*(ONE + rv(i)*r)*(ONE + sv(i)*s))
         end do

      else if ( MESH_ELEM_TYPE .eq. HEXS ) then
         
         x = ZERO; y = ZERO; z = ZERO
         do i = 1, Nverts
            x = x + xv(i) * EIGHTH*(ONE + rv(i)*r)*(ONE + sv(i)*s)*(ONE + tv(i)*t)
            y = y + yv(i) * EIGHTH*(ONE + rv(i)*r)*(ONE + sv(i)*s)*(ONE + tv(i)*t)
            z = z + zv(i) * EIGHTH*(ONE + rv(i)*r)*(ONE + sv(i)*s)*(ONE + tv(i)*t)
         end do
        
      end if

      deallocate(x1, x2, y1, y2)
      
    end subroutine rs2xy

    subroutine xy2rs(x,y,z,xv,yv,zv,rv,sv,tv,r,s,t)
      real(c_double), intent(in ) :: x(:), y(:), z(:)
      real(c_double), intent(in ) :: xv(:), yv(:), zv(:)
      real(c_double), intent(in ) :: rv(:), sv(:), tv(:)
      real(c_double), intent(out) :: r(:), s(:), t(:)

      real(c_double) :: ri, si, ti
      integer(c_int) :: i
      logical        :: has_converged

      if (MESH_ELEM_TYPE .eq. TRIANGLES) then
         r = (TWO*x - (xv(3) + xv(2)))*(yv(3) - yv(1)) - (TWO*y -(yv(3) + yv(2)))*(xv(3) - xv(1))
         r = r/( (xv(2) - xv(1))*(yv(3) - yv(1)) - (yv(2) - yv(1))*(xv(3) - xv(1)) )
         
         s = (TWO*x - (xv(3) + xv(2)))*(yv(2) - yv(1)) - (TWO*y - (yv(3) + yv(2)))*(xv(2) - xv(1))
         s = s/( (xv(3) - xv(1))*(yv(2) - yv(1)) - (yv(3) - yv(1))*(xv(2) - xv(1)) )

      else if (MESH_ELEM_TYPE .eq. QUADS) then
         do i = 1, size(r)
            call xy2rs_inner( x(i), y(i), z(i), xv, yv, zv, rv, sv, tv, ri, si, ti, has_converged)
            r(i) = ri
            s(i) = si
         end do

      else if (MESH_ELEM_TYPE .eq. HEXS) then
         do i = 1, size(r)
            call xy2rs_inner( x(i), y(i), z(i), xv, yv, zv, rv, sv, tv, ri, si, ti, has_converged)
            r(i) = ri
            s(i) = si
            t(i) = ti
         end do

      end if
      
    end subroutine xy2rs

    subroutine xy2rs_inner(xp, yp, zp, xv, yv, zv, rv, sv, tv, r, s, t, &
         has_converged, rguess, sguess, tguess)
      real(c_double), intent(in ) :: xp, yp, zp
      real(c_double), intent(in ) :: xv(:), yv(:), zv(:)
      real(c_double), intent(in ) :: rv(:), sv(:), tv(:)
      real(c_double), intent(out) :: r, s, t
      logical,        intent(out) :: has_converged

      real(c_double), optional :: rguess, sguess, tguess

      ! locals
      integer(c_int) :: i, iter_max, Nverts, vert
      real(c_double) :: r0, s0, t0
      real(c_double) :: N_vert, Nr, Ns, Nt

      real(c_double) :: detJac
      real(c_double), allocatable :: Jac(:, :), invJac(:, :), F(:)

      real(c_double) :: dr, ds, dt, tol

      Nverts = size(rv)

      r0 = ZERO; if (present(rguess)) r0 = rguess
      s0 = ZERO; if (present(sguess)) s0 = rguess
      t0 = ZERO; if (present(tguess)) t0 = tguess
      
      iter_max = 20
      tol      = 0.000001_c_double

      r = r0; s = s0; t = t0

      has_converged = .false.
      if ( MESH_ELEM_TYPE .eq. QUADS) then

         allocate(Jac(2,2))
         allocate(invJac(2,2))
         allocate(F(2))

         do i = 1, iter_max

            Jac = ZERO
            F   = ZERO
            do vert = 1, Nverts

               Nr = FOURTH*rv(vert)*(ONE + sv(vert)*s)
               Ns = FOURTH*(ONE + rv(vert)*r)*sv(vert)

               Jac(1,1) = Jac(1,1) + xv(vert)*Nr
               Jac(1,2) = Jac(1,2) + xv(vert)*Ns
               
               Jac(2,1) = Jac(2,1) + yv(vert)*Nr
               Jac(2,2) = Jac(2,2) + yv(vert)*Ns

               N_vert = FOURTH*(ONE + rv(vert)*r)*(ONE + sv(vert)*s)

               F(1) = F(1) + xv(vert)*N_vert
               F(2) = F(2) + yv(vert)*N_vert
            end do

            detJac = ONE/(Jac(2,2)*Jac(1,1) - Jac(1,2)*Jac(2,1))
            
            invJac(1,1) = +detJac * Jac(2,2)
            invJac(1,2) = -detJac * Jac(1,2)
            
            invJac(2,1) = -detJac * Jac(2,1)
            invJac(2,2) = +detJac * Jac(1,1)

            F(1) = F(1) - xp
            F(2) = F(2) - yp
            
            dr = invJac(1,1)*F(1) + invJac(1,2)*F(2)
            ds = invJac(2,1)*F(1) + invJac(2,2)*F(2)

            r = r - dr
            s = s - ds

            if (abs(dr) .le. tol .and. abs(ds) .le. tol) then
               has_converged = .true.
            end if
         
            if (has_converged) exit
         end do

      else if ( MESH_ELEM_TYPE .eq. HEXS) then

         allocate(Jac(3,3))
         allocate(invJac(3,3))
         allocate(F(3))

         do i = 1, iter_max

            Jac = ZERO
            F   = ZERO
            do vert = 1, Nverts

               Nr = EIGHTH*rv(vert)*(ONE + sv(vert)*s)*(ONE + tv(vert)*t)
               Ns = EIGHTH*(ONE + rv(vert)*r)*sv(vert)*(ONE + tv(vert)*t)
               Nt = EIGHTH*(ONE + rv(vert)*r)*(ONE + sv(vert)*s)*tv(vert)

               Jac(1,1) = Jac(1,1) + xv(vert)*Nr
               Jac(1,2) = Jac(1,2) + xv(vert)*Ns
               Jac(1,3) = Jac(1,3) + xv(vert)*Nt

               Jac(2,1) = Jac(2,1) + yv(vert)*Nr
               Jac(2,2) = Jac(2,2) + yv(vert)*Ns
               Jac(2,3) = Jac(2,3) + yv(vert)*Nt

               Jac(3,1) = Jac(3,1) + zv(vert)*Nr
               Jac(3,2) = Jac(3,2) + zv(vert)*Ns
               Jac(3,3) = Jac(3,3) + zv(vert)*Nt

               N_vert = EIGHTH*(ONE + rv(vert)*r)*(ONE + sv(vert)*s)*(ONE + tv(vert)*t)

               F(1) = F(1) + xv(vert)*N_vert
               F(2) = F(2) + yv(vert)*N_vert
               F(3) = F(3) + zv(vert)*N_vert

            end do

            invJac = inv(Jac)

            F(1) = F(1) - xp
            F(2) = F(2) - yp
            F(3) = F(3) - zp
            
            dr = invJac(1,1)*F(1) + invJac(1,2)*F(2) + invJac(1,3)*F(3)
            ds = invJac(2,1)*F(1) + invJac(2,2)*F(2) + invJac(2,3)*F(3)
            dt = invJac(3,1)*F(1) + invJac(3,2)*F(2) + invJac(3,3)*F(3)

            r = r - dr
            s = s - ds
            t = t - dt

            if (abs(dr) .le. tol .and. abs(ds) .le. tol .and. abs(dt) .le. tol) then
               has_converged = .true.
            end if
         
            if (has_converged) exit
         end do

      else if (MESH_ELEM_TYPE .eq. TRIANGLES) then
         r = (TWO*xp - (xv(3) + xv(2)))*(yv(3) - yv(1)) - (TWO*yp -(yv(3) + yv(2)))*(xv(3) - xv(1))
         r = r/( (xv(2) - xv(1))*(yv(3) - yv(1)) - (yv(2) - yv(1))*(xv(3) - xv(1)) )
         
         s = (TWO*xp - (xv(3) + xv(2)))*(yv(2) - yv(1)) - (TWO*yp - (yv(3) + yv(2)))*(xv(2) - xv(1))
         s = s/( (xv(3) - xv(1))*(yv(2) - yv(1)) - (yv(3) - yv(1))*(xv(2) - xv(1)) )
      end if

      deallocate(Jac)
      deallocate(invJac)
      deallocate(F)

    end subroutine xy2rs_inner

    subroutine jacobi_polynomial(px, x, m, n, alpha, beta, inormalize)
      integer(c_int), intent(in ) :: m, n
      real(c_double), intent(in ) :: alpha, beta
      real(c_double), intent(out) :: px(:)
      real(c_double), intent(in ) :: x(:)
      logical, optional, intent(in) :: inormalize
      
      !locals
      logical :: normalize
      real(c_double), allocatable :: cx(:,:)
      real(c_double)              :: factor

      normalize = .true.; if (present(inormalize)) normalize = inormalize

      allocate(cx(m, 0:n))

      call j_polynomial(m, n, alpha, beta, x, cx)
      factor = j_double_product_integral(n, n, alpha, beta)
      
      if ( .not. normalize) then
         px = cx(:, n)
      else
         px = cx(:, n)/sqrt(factor)
      end if

      deallocate(cx)

    end subroutine jacobi_polynomial

    subroutine grad_jacobi_polynomial(px, x, m, n, alpha, beta, ilegendre)
      integer(c_int), intent(in ) :: m, n
      real(c_double), intent(in ) :: alpha, beta
      real(c_double), intent(in ) :: x(:)
      real(c_double), intent(out) :: px(:)
      logical, optional, intent(in) :: ilegendre
      
      !locals
      logical :: legendre, normalize
      real(c_double) :: factor

      legendre = .false.; if (present(ilegendre)) legendre = ilegendre

      if (legendre) then
         normalize = .false.
         factor = HALF * (n + alpha + beta + ONE)
      else
         normalize = .true.
         factor = sqrt(n*(n + alpha + beta +ONE))
      end if
      
      if ( n .eq. 0 ) then
         px = ZERO
      else 
         call jacobi_polynomial(px, x, m, n-1, alpha+1, beta+1, normalize)
      end if
      px = px * factor

    end subroutine grad_jacobi_polynomial

    subroutine rs2ab(r, s, a, b)
      real(c_double), intent(in ) :: r(:), s(:)
      real(c_double), intent(out) :: a(:), b(:)
      
      ! locals
      integer(c_int) :: i

      do i = 1, size(r)
         a(i) = -ONE
         if (s(i) .ne. ONE) then
            a(i) = TWO*(1 + r(i))/(1 - s(i)) - ONE
         end if
         b(i) = s(i)
      end do
      
    end subroutine rs2ab
    
    subroutine get_vandermode_matrix(P, r, s, t, Np, V, Vinv, L, basis_degrees)
      integer(c_int), intent(in ) :: P, Np
      real(c_double), intent(in ) :: r(:), s(:), t(:)
      integer(c_int), intent(out) :: basis_degrees(:)
      real(c_double), intent(out) :: V(:,:), Vinv(:,:), L(:,:)

      integer(c_int) :: i, j, k, m
      real(c_double) :: alpha, beta
      real(c_double) :: a(size(r)), b(size(r))
      real(c_double) :: pa(size(r)), pb(size(r)), pc(size(r))

      if (MESH_ELEM_TYPE .eq. TRIANGLES) then
         call rs2ab(r, s, a, b)
         
         do i = 0, p
            do j = 0, p
               if ( (i+j) .le. p ) then
                  m = int(j + (p+1)*i + 1 - HALF*i*(i-1))
                  
                  alpha = ZERO; beta  = ZERO
                  call jacobi_polynomial(pa, a, Np, i, alpha, beta)
                  
                  alpha = dble(2_c_int*i + 1_c_int); beta  = ZERO
                  call jacobi_polynomial(pb, b, Np, j, alpha, beta)
                  
                  V(:, m) = M_SQRT_2 * pa * pb * (ONE-b)**i

                  basis_degrees(m) = max(i, j)

               end if
               
            end do
         end do

      else if (MESH_ELEM_TYPE .eq. QUADS) then
         m = 0
         do j = 0, p
            do i = 0, p
              m = m + 1 
              call jacobi_polynomial(pa, r, Np, i, ZERO, ZERO)
              call jacobi_polynomial(pb, s, Np, j, ZERO, ZERO)
              V(:, m) = pa(:)*pb(:)

              basis_degrees(m) = max(i,j)

           end do
        end do
        
     else if (MESH_ELEM_TYPE .eq. HEXS) then
        m = 0
        do k = 0, P
           do j = 0, P
              do i = 0, P
                 m = m + 1
                 call jacobi_polynomial(pa, r, Np, i, ZERO, ZERO)
                 call jacobi_polynomial(pb, s, Np, j, ZERO, ZERO)
                 call jacobi_polynomial(pc, t, Np, k, ZERO, ZERO)
                 V(:, m) = pa(:)*pb(:)*pc(:)

                 basis_degrees(m) = max(i,j,k)
                 
              end do
           end do
        end do

      end if

      ! print*, 'BASIS DEGREES'
      ! print*, basis_degrees

      ! write(*, *) 'VANDERMODE MATRIX'
      ! do i = 1, size(V,1)
      !    write(*, *) V(i,:)
      ! end do

      ! store the inverse of the vandermode matrix
      !if (MESH_ELEM_TYPE .eq. TRIANGLES) then
      Vinv = inv(V)
      !end if

      ! call DGEMM('N', 'N', Np, Np, Np, &
      !      ONE, Vinv, Np, &
      !      V, Np, &
      !      ZERO, L, Np)

      ! write(*, *) 'VANDERMODE MATRIX'
      ! do i = 1, size(V,1)
      !    write(*, *) V(i,:)
      ! end do

      ! write(*, *) 'VANDERMODE INVERSE'
      ! do i = 1, size(Vinv,1)
      !    write(*, *) Vinv(i,:)
      ! end do

    end subroutine get_vandermode_matrix

    subroutine get_grad_vandermode_matrix(P, r, s, t, Np, V, Vinv, Lr, Ls, Lt, Lgrad, dim)
      integer(c_int), intent(in ) :: p, Np, dim
      real(c_double), intent(in ) :: r(:), s(:), t(:)
      real(c_double), intent(in ) :: V(:,:), Vinv(:,:)
      real(c_double), intent(out) :: Lr(:,:), Ls(:,:), Lt(:,:)
      real(c_double), intent(out) :: Lgrad(:,:)

      real(c_double) :: pa(size(r)), dpa(size(r))
      real(c_double) :: pb(size(r)), dpb(size(r))
      real(c_double) :: pc(size(r)), dpc(size(r))

      real(c_double) :: Dr_tmp(size(r), size(r))
      real(c_double) :: Ds_tmp(size(r), size(r))
      real(c_double) :: Dt_tmp(size(r), size(r))

      real(c_double) :: a(size(r)), b(size(r))
      real(c_double) :: alpha, beta
      integer(c_int) :: i, j, k, m

      !call get_gauss_nodes(P, r1d, w1d)

      if ( MESH_ELEM_TYPE .eq. TRIANGLES ) then
         call rs2ab(r, s, a, b)

         do i = 0, p
            do j = 0, p
               if ( (i+j) .le. p ) then
                  m = int(j + (p+1)*i + 1 - HALF*i*(i-1))

                  alpha = ZERO; beta  = ZERO
                  call jacobi_polynomial(      pa, a, Np, i, alpha, beta)
                  call grad_jacobi_polynomial(dpa, a, Np, i, alpha, beta)

                  alpha = dble(2_c_int*i + 1_c_int); beta  = ZERO
                  call jacobi_polynomial(      pb, b, Np, j, alpha, beta)
                  call grad_jacobi_polynomial(dpb, b, Np, j, alpha, beta)

                  !r-derivative
                  Dr_tmp(:, m) = M_SQRT_2 *pb*dpa*TWO/(ONE-s)*(ONE-b)**i

                  !s-derivative
                  Ds_tmp(:, m) = M_SQRT_2 * (TWO*(ONE+r)/(ONE-s)**2*pb*dpa*(ONE-b)**i) + &
                                 M_SQRT_2 * (pa*(dpb*(ONE-b)**i - pb*i*(ONE-b)**(i-ONE))) 
               end if
               
            end do
         end do

      else if ( MESH_ELEM_TYPE .eq. QUADS ) then
         m = 0
         do j = 0, P
            do i = 0, P
               m = m + 1
               call jacobi_polynomial(      pa, s, Np, j, ZERO, ZERO)
               call grad_jacobi_polynomial(dpa, r, Np, i, ZERO, ZERO)
               Dr_tmp(:, m) = pa(:)*dpa(:)
               
               call jacobi_polynomial(      pa, r, Np, i, ZERO, ZERO)
               call grad_jacobi_polynomial(dpa, s, Np, j, ZERO, ZERO)
               Ds_tmp(:, m) = pa(:)*dpa(:)
               
            end do
         end do

      else if (MESH_ELEM_TYPE .eq. HEXS) then
         m = 0
         do k = 0, P
            do j = 0, P
               do i = 0, P
                  m = m + 1

                  call jacobi_polynomial(pa, r, Np, i, ZERO, ZERO)
                  call jacobi_polynomial(pb, s, Np, j, ZERO, ZERO)
                  call jacobi_polynomial(pc, t, Np, k, ZERO, ZERO)

                  call grad_jacobi_polynomial(dpa, r, Np, i, ZERO, ZERO)
                  call grad_jacobi_polynomial(dpb, s, Np, j, ZERO, ZERO)
                  call grad_jacobi_polynomial(dpc, t, Np, k, ZERO, ZERO)

                  Dr_tmp(:, m) = dpa(:)*pb(:)*pc(:)
                  Ds_tmp(:, m) = pa(:)*dpb(:)*pc(:)
                  Dt_tmp(:, m) = pa(:)*pb(:)*dpc(:)
                  
               end do
            end do
         end do

      end if

      call DGEMM('N', 'N', Np, Np, Np, ONE, Dr_tmp, Np, Vinv, Np, ZERO, Lr, Np)
      call DGEMM('N', 'N', Np, Np, Np, ONE, Ds_tmp, Np, Vinv, Np, ZERO, Ls, Np)
      
      ! stack the gradient matrices
      Lgrad(1:Np , :)     = Lr
      Lgrad(Np+1:2*Np, :) = Ls

      if (dim .eq. 3) then
         call DGEMM('N', 'N', Np, Np, Np, ONE, Dt_tmp, Np, Vinv, Np, ZERO, Lt, Np)
         Lgrad(2*Np+1:, :) = Lt
      end if

    ! write(*, *) 'GRADIENT Lr'
    ! do i = 1, Np
    !    write(*, *) Lr(i,:)
    ! end do

    ! write(*, *) 'GRADIENT Ls'
    ! do i = 1, Np
    !    write(*, *) Ls(i,:)
    ! end do

    ! write(*, *) 'GRADIENT Lt'
    ! do i = 1, Np
    !    write(*, *) Lt(i,:)
    ! end do

    ! write(*, *) 'Lgrad'
    ! do i = 1, 2*Np
    !    write(*, *) Lgrad(i, :)
    ! end do
      
    end subroutine get_grad_vandermode_matrix

  ! subroutine get_flux_point_vandermode_matrix(P, r, s, t, Np, elem_num_faces, V, Vinv, Vf, Lf, dim)
  !   integer(c_int), intent(in ) :: P, Np, elem_num_faces, dim
  !   real(c_double), intent(in ) :: r(:), s(:), t(:)
  !   real(c_double), intent(in ) :: V(:,:), Vinv(:,:)
  !   real(c_double), intent(out) :: Vf(:,:), Lf(:,:)

  !   ! locals
  !   integer(c_int) :: i, j, m, P1, num_points
  !   real(c_double) :: alpha, beta
  !   real(c_double) :: a(elem_num_faces*(P+1)), b(elem_num_faces*(P+1))
  !   real(c_double) :: pa(elem_num_faces*(P+1)), pb(elem_num_faces*(P+1))

  !   P1         = P + 1
  !   num_points = elem_num_faces*P1

  !   if (MESH_ELEM_TYPE .eq. TRIANGLES) then
  !      call rs2ab(r, s, a, b)

  !      do i = 0, P
  !         do j = 0, P
  !            if ( (i+j) .le. P ) then
  !               m = int(j + (P+1)*i + 1 - HALF*i*(i-1))
             
  !               alpha = ZERO; beta  = ZERO
  !               call jacobi_polynomial(pa, a, num_points, i, alpha, beta)
             
  !               alpha = dble(2_c_int*i + 1_c_int); beta  = ZERO
  !               call jacobi_polynomial(pb, b, num_points, j, alpha, beta)
    
  !               Vf(:, m) = M_SQRT_2 * pa * pb * (ONE-b)**i

  !            end if
  !         end do
  !      end do

  !   else if (MESH_ELEM_TYPE .eq. QUADS) then
  !      m = 0
  !      do j = 0, P
  !         do i = 0, P
  !            m = m + 1
  !            call jacobi_polynomial(pa, r, num_points, i, ZERO, ZERO)
  !            call jacobi_polynomial(pb, s, num_points, j, ZERO, ZERO)
             
  !            Vf(:, m) = pa(:)*pb(:)

  !         end do
  !      end do
  !   end if

  !   call DGEMM('N', 'N', num_points, Np, Np, ONE, Vf, num_points, &
  !              Vinv, Np, ZERO, Lf, num_points)

  !   ! write(*, *) 'Flux point matrix:: Lf'
  !   ! do i = 1, num_points
  !   !   write(*, *) Lf(i,:)
  !   ! end do
    
  ! end subroutine get_flux_point_vandermode_matrix

  subroutine evaluate_vandermode_matrix(P, Np, Npoints, r, s, t, V, Vinv, Vmat, Lmat)
    integer(c_int), intent(in ) :: P, Np, Npoints
    real(c_double), intent(in ) :: r(:), s(:), t(:)
    real(c_double), intent(in ) :: V(:,:), Vinv(:,:)
    real(c_double), intent(out) :: Vmat(:,:), Lmat(:,:)

    ! locals
    integer(c_int) :: i, j, k, m, P1
    real(c_double) :: alpha, beta
    real(c_double) :: a( size(r)), b( size(r))
    real(c_double) :: pa(size(r)), pb(size(r)), pc(size(r))

    P1         = P + 1

    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       call rs2ab(r, s, a, b)

       do i = 0, P
          do j = 0, P
             if ( (i+j) .le. P ) then
                m = int(j + (P+1)*i + 1 - HALF*i*(i-1))
             
                alpha = ZERO; beta  = ZERO
                call jacobi_polynomial(pa, a, Npoints, i, alpha, beta)
             
                alpha = dble(2_c_int*i + 1_c_int); beta  = ZERO
                call jacobi_polynomial(pb, b, Npoints, j, alpha, beta)
    
                Vmat(:, m) = M_SQRT_2 * pa * pb * (ONE-b)**i
             end if
          end do
       end do

    else if (MESH_ELEM_TYPE .eq. QUADS) then
       m = 0
       do j = 0, P
          do i = 0, P
             m = m + 1
             call jacobi_polynomial(pa, r, Npoints, i, ZERO, ZERO)
             call jacobi_polynomial(pb, s, Npoints, j, ZERO, ZERO)
             Vmat(:, m) = pa(:)*pb(:)
          end do
       end do
       
    else if (MESH_ELEM_TYPE .eq. HEXS) then
       m = 0
       do k = 0, P
          do j = 0, P
             do i = 0, P
                m = m + 1
                call jacobi_polynomial(pa, r, Npoints, i, ZERO, ZERO)
                call jacobi_polynomial(pb, s, Npoints, j, ZERO, ZERO)
                call jacobi_polynomial(pc, t, Npoints, k, ZERO, ZERO)
                Vmat(:, m) = pa(:)*pb(:)*pc(:)
             end do
          end do
       end do

    end if

    call DGEMM('N', 'N', Npoints, Np, Np, &
         ONE, Vmat, Npoints, &
         Vinv, Np, &
         ZERO, Lmat, Npoints)

    ! write(*, *) 'Vandermode matrix:: Lf', shape(Lmat)
    ! do i = 1, Npoints
    !   write(*, *) Lmat(i,:)
    ! end do
    ! print*
    ! endif
    
  end subroutine evaluate_vandermode_matrix

  subroutine get_flux_point_grad_vandermode_matrix(&
    Np, Nflux, Lf, Lr, Ls, Lt, Lf_r, Lf_s, Lf_t, Lf_grad, dim)

    integer(c_int), intent(in ) :: Np, Nflux, dim
    real(c_double), intent(in ) :: Lr(:,:), Ls(:,:), Lt(:, :), Lf(:, :)
    real(c_double), intent(out) :: Lf_r(:,:), Lf_s(:,:), Lf_t(:, :)
    real(c_double), intent(out) :: Lf_grad(:, :)

    ! set the gradient at flux point matrix as the interpolation of
    ! the gradient at the solution points at the flux points
    call DGEMM('N', 'N', Nflux, Np, Np, &
         ONE, Lf, Nflux, &
         Lr, Np, &
         ZERO, Lf_r, Nflux)

    call DGEMM('N', 'N', Nflux, Np, Np, &
         ONE, Lf, Nflux, &
         Ls, Np, &
         ZERO, Lf_s, Nflux)

    ! stack the matrices
    Lf_grad(1:Nflux,         :) = Lf_r(:, :)
    Lf_grad(Nflux+1:2*Nflux, :) = Lf_s(:, :)

    if (dim .eq. 3) then
       call DGEMM('N', 'N', Nflux, Np, Np, &
            ONE, Lf, Nflux, &
            Lt, Np, &
            ZERO, Lf_t, Nflux)

       Lf_grad(2*Nflux+1:, :) = Lf_t(:, :)

    end if

    ! write(*, *) 'Flux point gradient matrix:: Lf_r'
    ! do i = 1, Nflux
    !    write(*, *) Lf_r(i,:)
    ! end do
    ! print*

    ! write(*, *) 'Flux point gradient matrix:: Lf_s'
    ! do i = 1, Nflux
    !    write(*, *) Lf_s(i,:)
    ! end do
    ! print*

    ! write(*, *) 'Flux point gradient matrix:: Lf_t'
    ! do i = 1, Nflux
    !    write(*, *) Lf_t(i,:)
    ! end do
    ! print*
    
  end subroutine get_flux_point_grad_vandermode_matrix

  subroutine set_correction_function_coefficients_at_solution_points(&
       P, Np, Nflux, elem_num_faces, r, s, t, V, Vf, &
       phifj_solution_r, phifj_solution_s, phifj_solution_t, phifj_solution_grad, DIM)
    use input_module, only: ESFR_ctype

    integer(c_int), intent(in ) :: DIM, P, Np, Nflux, elem_num_faces
    real(c_double), intent(in ) :: r(Np), s(Np), t(Np)
    real(c_double), intent(in ) :: V(Np, Np), Vf(Nflux, Np)

    real(c_double), intent(out) :: phifj_solution_r(Np, Nflux)
    real(c_double), intent(out) :: phifj_solution_s(Np, Nflux)
    real(c_double), intent(out) :: phifj_solution_t(Np, Nflux)

    real(c_double), intent(out) :: phifj_solution_grad(DIM*Np, Nflux)
    
    ! locals
    integer(c_int) :: face, m, j, i, k, P1, jj, ii
    real(c_double) :: LgP(Np), LgP1(Np)

    ! local variables for triangular meshes
    real(c_double) :: nhat(3*(P+1), 2)
    real(c_double) :: modal_basis_along_face(P+1)
    real(c_double) :: rgauss(P+1), wgauss(P+1)
    real(c_double) :: correction_coeffs(Np, 3*(P+1))
    real(c_double) :: phifj_solution(Np, 3*(P+1))

    ! left and right Radau polynomial and their derivatives
    real(c_double) :: dgL(P+1), dgR(P+1)
    real(c_double) :: dLegendreP(P+1), dLegendreP1(P+1), dLegendrePm1(P+1)
    real(c_double) :: esfr_c, esfr_a, esfr_eta, tmp1, tmp2, tmp3

    P1 = P + 1

    phifj_solution_r = ZERO
    phifj_solution_s = ZERO
    phifj_solution_t = ZERO

    !write(*, *) r
    !write(*, *) s

    ! get Gaussian quadrature nodes and weights
    call get_gauss_nodes(P, rgauss, wgauss)

    if ( MESH_ELEM_TYPE .eq. TRIANGLES) then
       ! transformed element normals
       nhat(1:P1, 1) = ZERO
       nhat(1:P1, 2) = -ONE

       nhat(P1+1:P1*2, 1) = ONE/M_SQRT_2
       nhat(P1+1:P1*2, 2) = ONE/M_SQRT_2

       nhat(2*P1+1:3*P1, 1) = -ONE
       nhat(2*P1+1:3*P1, 2) = ZERO

       ! FIXME. THE COEFFICIENTS ARE ACTUALLY INTEGRALS ALONG THE
       ! FACES. 
       do m = 1, Np
          do face = 1, 3
             modal_basis_along_face = Vf( (face-1)*P1+1:face*P1, m )
             correction_coeffs(m, (face-1)*P1+1:face*P1) = modal_basis_along_face*wgauss
          end do
          !mesh%correction_coeffs(m, :) = mesh%Vf(:, m)
       end do

       ! correct the coefficients for face 2 with the Hypotenuse length
       correction_coeffs(:, P1+1:2*P1) = M_SQRT_2 * & 
            correction_coeffs(:, P1+1:2*P1)

       ! Multiply the correction coefficients with the Dubiner basis
       ! functions evaluated at the solution and flux points to get the
       ! correction functions
       call DGEMM('N', 'N', Np, 3*P1, Np, &
            ONE, V, Np, &
            correction_coeffs, Np, &
            ZERO, phifj_solution, Np)

       ! multiply each row of the correction function with the
       ! transformed element normals. These matrices will be used to
       ! compute the gradients and flux divergence
       do m = 1, Np
          phifj_solution_r(m, :) = phifj_solution(m, :) * nhat(:,1)
          phifj_solution_s(m, :) = phifj_solution(m, :) * nhat(:,2)       
       end do
       
       ! write(*, *) 'CORRECTION FUNCTION'
       ! do m = 1, Np
       !    write(*, *) phifj_solution(m,:)
       ! end do
       
       ! Stack the matrices
       ! phifj_solution_grad(1:Np,  :) = phifj_solution_r(:, :)
       ! phifj_solution_grad(Np+1:, :) = phifj_solution_s(:, :)
       

    else if (MESH_ELEM_TYPE .eq. QUADS) then

       !!!!!!!!!! r-derivatives !!!!!!!!!!
       ! Derivative of the Legendre polys evaluated at the r-points
       call grad_jacobi_polynomial(LgP,  r, Np, P,  ZERO, ZERO, ilegendre=.true.)
       call grad_jacobi_polynomial(LgP1, r, Np, P1, ZERO, ZERO, ilegendre=.true.)

       ! derivative of the left and right correction functions
       face = 4
       do jj = 1, P1
          j = (face-1)*P1 + jj
          do ii = 1, P1
             i = abs(jj-P1)*P1 + ii
             phifj_solution_r(i, j) = HALF * (-1)**P * (LgP(i) - LgP1(i))
          end do
       end do

       face = 2
       do jj = 1, P1
          j = (face-1)*P1 + jj
          do ii = 1, P1
             i = (jj-1) * P1 + ii
             phifj_solution_r(i, j) = HALF * (LgP(i) + LgP1(i))
          end do
       end do

       !!!!!!!!!! s-derivatives !!!!!!!!!!
       ! Derivatives of the Legendre polys evaluated at the s-points
       call grad_jacobi_polynomial(LgP,  s, Np, P,  ZERO, ZERO, ilegendre=.true.)
       call grad_jacobi_polynomial(LgP1, s, Np, P1, ZERO, ZERO, ilegendre=.true.)

       ! derivative of the left and right correction functions
       face = 1
       do jj = 1, P1
          j = (face-1)*P1 + jj
          do ii = 1, P1
             i = (ii-1)*P1 + jj
             phifj_solution_s(i, j) = HALF * (-1)**P * (LgP(i) - LgP1(i))
          end do
       end do

       face = 3
       do jj = 1, P1
          j = (face-1)*P1 + jj
          do ii = 1, P1
             i = abs(ii-P1)*P1 + P1 - (jj-1)
             phifj_solution_s( i, j ) = HALF * (LgP(i) + LgP1(i))
          end do
       end do

    else if (MESH_ELEM_TYPE .eq. HEXS) then

       call grad_jacobi_polynomial(dLegendreP,  rgauss, P1, P, ZERO, ZERO, ilegendre=.true.)
       call grad_jacobi_polynomial(dLegendreP1, rgauss, P1, P1, ZERO, ZERO, ilegendre=.true.)

       dLegendrePm1 = ZERO
       if (P .gt. 1) then
          call grad_jacobi_polynomial(dLegendrePm1, rgauss, P1, P-1, ZERO, ZERO, ilegendre=.true.)
       end if

       tmp1 = dble( factorial(P) )
       tmp2 = dble( factorial(2*P) )

       esfr_a   = tmp2/(TWO**P * tmp1**2)

       if (ESFR_ctype .eq. NDG) then     ! collocation based nodal-DG
          esfr_c = ZERO

       else if (ESFR_ctype .eq. SD) then ! Spectral difference method
          esfr_c = TWO*dble(P)/( (TWO*dble(P) + ONE)*(dble(P) + ONE) )
          esfr_c = esfr_c/( (esfr_a*tmp1)**2 )

       else if (ESFR_ctype .eq. G2) then ! Hyunh type (g2) scheme
          esfr_c = TWO*( dble(P) + ONE )/( TWO*dble(P) + ONE )
          esfr_c = esfr_c/dble(P)
          esfr_c = esfr_c/(esfr_a*tmp1)**2
       end if

       esfr_eta = HALF*esfr_c * ((TWO*dble(P) + ONE) * (esfr_a * tmp1)**2)
       tmp3     = ONE/(ONE + esfr_eta)

       !dgL = HALF * (-ONE)**P * (dLegendreP - dLegendreP1)
       !dgR = HALF * ( ONE)**P * (dLegendreP + dLegendreP1)

       dgL = HALF * (-ONE)**P * (dLegendreP - tmp3*(esfr_eta*dLegendrePm1 + dLegendreP1))
       dgR = HALF * ( ONE)**P * (dLegendreP + tmp3*(esfr_eta*dLegendrePm1 + dLegendreP1))

       m = 0
       do k = 1, P+1
          do j = 1, P+1
             do i = 1, P+1
                m = m + 1

                ! face 1
                jj = (k-1)*P1 + i
                phifj_solution_s(m, jj) = dgL(j)

                ! face 3
                jj = 2*P1**2 + (k-1)*P1 + i
                phifj_solution_s(m, jj) = dgR(j)

                ! face 2
                jj = P1**2 + (k-1)*P1 + j
                phifj_solution_r(m, jj) = dgR(i)

                ! face 4
                jj = 3*P1**2 + (k-1)*P1 + j
                phifj_solution_r(m, jj) = dgL(i)

                ! face 5
                jj = 4*P1**2 + (j-1)*P1 + i
                phifj_solution_t(m, jj) = dgL(k)
                
                ! face 6
                jj = 5*P1**2 + (j-1)*P1 + i
                phifj_solution_t(m, jj) = dgR(k)

             end do
          end do
       end do
    end if

    ! Stack the matrices
    phifj_solution_grad(1:Np,      :) = phifj_solution_r(:, :)
    phifj_solution_grad(Np+1:2*Np, :) = phifj_solution_s(:, :)

    if (DIM .eq. 3) then
       phifj_solution_grad(2*Np+1:,  :) = phifj_solution_t(:, :)
    end if

    ! print*, 'PHIFJ_SOLUTION_GRAD'
    ! do m = 1, 2*Np
    !    write(*, *) phifj_solution_grad(m, :)
    ! end do

    ! write(*, *) 'CORRECTION MATRIX phifj_solution_r'
    ! do i = 1, Np
    !    write(*,*) phifj_solution_r(i, :)
    ! end do
    ! write(*, *) 

    ! write(*, *) 'CORRECTION MATRIX phifj_solution_s'
    ! do i = 1, Np
    !    write(*,*) phifj_solution_s(i, :)
    ! end do
    ! write(*, *) 

    ! write(*, *) 'CORRECTION MATRIX phifj_solution_t'
    ! do i = 1, Np
    !    write(*,*) phifj_solution_t(i, :)
    ! end do
    ! write(*, *) 

  end subroutine set_correction_function_coefficients_at_solution_points

  subroutine set_filter_matrix(P, Np, V, Vinv, Filter)
    use input_module, only: filter_cutoff_percentage, alpha_filter, s_filter

    integer(c_int), intent(in ) :: P, Np
    real(c_double), intent(in ) :: V(Np, Np), Vinv(Np, Np)
    real(c_double), intent(out) :: Filter(Np, Np)
    

    ! locals
    integer(c_int) :: filter_cutoff, i, j, k, m, ii, iii, basis_degrees(Np)
    real(c_double) :: filter_diag(Np, Np), Vtmp(Np, Np), tmp

    if (MESH_ELEM_TYPE .eq. TRIANGLES) then
       do i = 0, P
          do j = 0, P
             if ( (i+j) .le. P ) then
                m = int(j + (P+1)*i + 1 - HALF*i*(i-1))
                basis_degrees(m) = max(i,j)
             end if
          end do
       end do

    else if (MESH_ELEM_TYPE .eq. QUADS) then
       m = 0
       do i = 0, P
          do j = 0, P
             m = m + 1
             basis_degrees(m) = max(i, j)
          end do
       end do

    else if (MESH_ELEM_TYPE .eq. HEXS) then
       m = 0
       do i = 0, P
          do j = 0, P
             do k = 0, P
                m = m + 1
                basis_degrees(m) = max(i, j, k)
             end do
          end do
       end do

    end if
    
    filter_cutoff = int(0.01_c_double*filter_cutoff_percentage*P)
    filter_diag = ZERO
    iii = maxval(basis_degrees)
    do i = 1, Np
       ii = basis_degrees(i)
       if ( ii .ge. filter_cutoff .and. filter_cutoff .lt. P) then
          tmp = ((dble(ii - filter_cutoff))/(dble(iii - filter_cutoff)))**s_filter
          filter_diag(i,i) = exp(-alpha_filter*tmp)
       else
          filter_diag(i,i) = ONE
       end if
    end do

    !print*, 'FILTER CUTOFF ', filter_cutoff

    ! print*, 'BASIS DEGREES'
    ! print*,  basis_degrees
    ! print*,
    ! print*, 'DIAGONAL MATRIX'
    ! do i = 1, Np
    !    print*, filter_diag(i, i)
    ! end do

    ! form the filter matrix
    call DGEMM('N', 'N', Np, Np, Np, &
         ONE, filter_diag, Np, &
         Vinv, Np, &
         ZERO, Vtmp, Np)

    call DGEMM('N', 'N', Np, Np, Np, &
         ONE, V, Np, &
         Vtmp, Np, &
         ZERO, Filter, Np)

    ! ! Filter matrix
    ! write(*, *) 'Filter matrix'
    ! do i = 1, Np
    !    write(*, *) Filter(i, :)
    ! end do

  end subroutine set_filter_matrix
  
end module polynomials_module
