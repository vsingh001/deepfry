module userbc_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use fieldvar_module

  implicit none

contains

subroutine get_userbc_common_solution_values(mesh, data, time, beta)
  type(mesh2d),             intent(inout) :: mesh
  type(FieldData_t),        intent(inout) :: data
  real(c_double),           intent(in   ) :: time
  real(c_double), optional, intent(in   ) :: beta

end subroutine get_userbc_common_solution_values

subroutine get_userbc_interaction_flux(mesh, data, time, mu, Cp, Cv, Pr, Rs)
  real(c_double), intent(in ) :: mu, Cp, Cv, Pr, Rs
  real(c_double), intent(in ) :: time
  type(mesh2d), intent(inout) :: mesh
  type(FieldData_t),        intent(inout) :: data

end subroutine get_userbc_interaction_flux

end module userbc_module
