module fieldvar_module
   use deepfry_constants_module
   use iso_c_binding, only: c_int, c_double
   implicit none

   type FieldData_t

   integer(c_int) :: Nvar

   ! Solution vectors. FR data on the mesh is defined as a rank-3
   ! array (Npoints, Nvar, Nelements), where, Npoints could be
   ! either the solution data within the element (Np) or flux points
   ! on the faces of the element (Nflux)
   
   ! Conserved variable vector and temp initial value copy
   real(c_double), allocatable :: u(:, :, :)
   real(c_double), allocatable :: u0(:, :, :)
   real(c_double), allocatable :: uavg(:, :)

   ! interfacial terms defined at the solution points
   real(c_double), allocatable :: ui(:, :, :)

   ! alpha-flux prefactors (solution points)
   real(c_double), allocatable :: HH(:, :, :)
   real(c_double), allocatable :: II(:, :, :)

   ! gradient of void fractions
   real(c_double), allocatable :: ax(:, :, :)
   real(c_double), allocatable :: ay(:, :, :)

   real(c_double), allocatable :: ax_avg(:, :)
   real(c_double), allocatable :: ay_avg(:, :)

   real(c_double), allocatable :: alphag_flux(:, :)

   ! P1-projected solution at the solution and flux points
   real(c_double), allocatable :: up1(:, :,  :)
   real(c_double), allocatable :: up1f(:, :, :)
   
   ! Modal solution values
   real(c_double), allocatable :: umodal(:,:,:)
   
   ! Positivity-preserving solution and average. FIXME: Need to
   ! build on this
   real(c_double), allocatable :: upp(:, :, :)
   real(c_double), allocatable :: uavgpp(:, :)
   
   ! Gradients and their averages
   real(c_double), allocatable :: ux(:, :, :)
   real(c_double), allocatable :: uxavg(:, :)
   
   real(c_double), allocatable :: uy(:, :, :)
   real(c_double), allocatable :: uyavg(:, :)
   
   real(c_double), allocatable :: uz(:, :, :)
   real(c_double), allocatable :: uzavg(:, :)
   
   ! Gradients at the flux points
   real(c_double), allocatable :: ux_flux(:, :, :)
   real(c_double), allocatable :: uy_flux(:, :, :)
   real(c_double), allocatable :: uz_flux(:, :, :)
   
   ! GPU gradients. If using the cuBLAS GPU implementation, the
   ! gradient of the discontinuous solution is computed on the
   ! GPU. This is evaluated in one-shot by stacking the element
   ! arrays column-wise. The resulting gradient array is also
   ! stacked columnwise. The same holds for the GPU computed
   ! derivative of discontinuous flux and fluxpoint-interpolated
   ! discontinuous fluxes
   real(c_double), allocatable :: host_ugrad(:,:)
   real(c_double), allocatable :: host_Fgrad(:,:)
   real(c_double), allocatable :: host_Fdflux(:, :)
   
   ! Flux-point interpolated solution values and LDG common solution
   ! values at the flux points
   real(c_double), allocatable :: ucommon(:, :, :)
   real(c_double), allocatable :: uflux(:, :, :)
   
   ! vertex interpolated solution values
   real(c_double), allocatable :: uvert(:, :, :)
   
   ! Discontinuous flux at the solution and flux points
   real(c_double), allocatable :: Fdiscont(:, :) ! stacked array of Fd, Gd & Hd

   ! Discontinuous (Inviscid) flux at the flux points
   real(c_double), allocatable :: Fd_flux(:,:,:)
   real(c_double), allocatable :: Gd_flux(:,:,:)
   real(c_double), allocatable :: Hd_flux(:,:,:)

   real(c_double), allocatable :: Fd_uavg_flux(:, :)
   real(c_double), allocatable :: Gd_uavg_flux(:, :)
   real(c_double), allocatable :: Hd_uavg_flux(:, :)
   
   ! Discontinuous (Viscous) flux at the flux points
   real(c_double), allocatable :: Fv_flux(:,:,:)
   real(c_double), allocatable :: Gv_flux(:,:,:)
   real(c_double), allocatable :: Hv_flux(:,:,:)
   
   ! Interaction flux at the flux points
   real(c_double), allocatable :: Fi(:,:,:)
   real(c_double), allocatable :: Gi(:,:,:)
   real(c_double), allocatable :: Hi(:,:,:)

   real(c_double), allocatable :: agi_x(:, :)
   real(c_double), allocatable :: agi_y(:, :)
   
   ! FLux divergence or RK-integration RHS vector
   real(c_double), allocatable :: k1(:,:,:)
   
   ! troubled cell indicators and for high-speed flow, shock sensor
   ! and artificial viscosity
   integer(c_int) :: ntci
   real(c_double), allocatable :: tci(:,:,:)
   real(c_double), allocatable :: alpha(:)
   real(c_double), allocatable :: avisc(:)
   real(c_double), allocatable :: shock_sensor(:)
   end type FieldData_t

 contains

   subroutine allocate_field(DIM, elem_num_verts, Np, Nflux, Nvar, &
        Nelements, data)

     type(FieldData_t), intent(inout) :: data
     integer(c_int), intent(in)       :: DIM, Np, Nflux, Nvar, elem_num_verts, Nelements

     ! locals
     integer(c_int) :: Nvar_interface

     data%Nvar = Nvar
     Nvar_interface = DIM+1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Allocate the solution arrays and cell averages
    allocate(data%u(Np,  Nvar, Nelements))
    allocate(data%u0(Np, Nvar, Nelements))
    allocate(data%uavg(       Nvar, Nelements))

    allocate(data%ui(Np, Nvar_interface, Nelements))
    allocate(data%HH(Np, Nvar, Nelements))
    allocate(data%II(Np, Nvar, Nelements))

    allocate(data%ax(Np, 1, Nelements))
    allocate(data%ay(Np, 1, Nelements))

    allocate(data%ax_avg(1, Nelements))
    allocate(data%ay_avg(1, Nelements))

    allocate(data%alphag_flux(Nflux, Nelements))

    ! P1-projected solution values
    allocate(data%up1( Np, Nvar, Nelements))
    allocate(data%up1f(Nflux, Nvar, Nelements))

    ! Modal solution values
    allocate(data%umodal(Np, Nvar, Nelements))

    ! Positivity-preserving solution and average. FIXME: Need to
    ! build on this
    allocate(data%upp(Np, Nvar, Nelements))
    allocate(data%uavgpp(Nvar, Nelements))

    ! Gradients and their averages
    allocate(data%ux(Np, Nvar, Nelements))
    allocate(data%uy(Np, Nvar, Nelements))
    allocate(data%uz(Np, Nvar, Nelements))

    allocate(data%uxavg(Nvar, Nelements))
    allocate(data%uyavg(Nvar, Nelements))
    allocate(data%uzavg(Nvar, Nelements))
    
    ! gradients at the flux points
    allocate(data%ux_flux(Nflux, Nvar, Nelements))
    allocate(data%uy_flux(Nflux, Nvar, Nelements))
    allocate(data%uz_flux(Nflux, Nvar, Nelements))

   ! GPU gradients
#ifdef CUDA
    allocate(data%host_ugrad(DIM*Np, Nvar*Nelements))
    allocate(data%host_Fgrad(DIM*Np, DIM*Nvar*Nelements))
    allocate(data%host_Fdflux(Nflux, DIM*Nvar*Nelements))
#endif

    ! flux point and common solution values
    allocate(data%ucommon(Nflux, Nvar, Nelements))
    allocate(data%uflux(  Nflux, Nvar, Nelements))

    ! vertex values
    allocate(data%uvert(elem_num_verts, Nvar, Nelements))

    ! discontinuous fluxes
    allocate(data%Fdiscont(Np, DIM*Nvar*Nelements))
    allocate(data%Fd_flux(Nflux, Nvar, Nelements))
    allocate(data%Gd_flux(Nflux, Nvar, Nelements))
    allocate(data%Hd_flux(Nflux, Nvar, Nelements))

    allocate(data%Fd_uavg_flux(Nvar, Nelements))
    allocate(data%Gd_uavg_flux(Nvar, Nelements))
    allocate(data%Hd_uavg_flux(Nvar, Nelements))

    allocate(data%Fv_flux(Nflux, Nvar, Nelements))
    allocate(data%Gv_flux(Nflux, Nvar, Nelements))
    allocate(data%Hv_flux(Nflux, Nvar, Nelements))
    
    allocate(data%Fi(Nflux, Nvar, Nelements))
    allocate(data%Gi(Nflux, Nvar, Nelements))
    allocate(data%Hi(Nflux, Nvar, Nelements))

    allocate(data%agi_x(Nflux, Nelements))
    allocate(data%agi_y(Nflux, Nelements))
    
    ! rhs-update
    allocate(data%k1(Np, Nvar, Nelements))

    ! shock-sensors and avisc parameters
    allocate(data%tci(1,1,Nelements))
    allocate(data%alpha(Nelements))
    allocate(data%avisc(Nelements))
    allocate(data%shock_sensor(Nelements))

    data%tci   = ZERO
    data%alpha = ZERO
    data%avisc = ZERO
    data%shock_sensor = ZERO

  end subroutine allocate_field

  subroutine deallocate_field(data)
    type(FieldData_t), intent(inout) :: data

    deallocate(data%u, data%u0, data%uavg)
    deallocate(data%up1, data%up1f)
    deallocate(data%ui)
    
    deallocate(data%HH)
    deallocate(data%II)

    deallocate(data%ax)
    deallocate(data%ay)

    deallocate(data%ax_avg)
    deallocate(data%ay_avg)

    deallocate(data%alphag_flux)
    
    deallocate(data%umodal)
    deallocate(data%upp, data%uavgpp)

    deallocate(data%ux, data%uy, data%uz)

#ifdef CUDA
    deallocate(data%host_ugrad, data%host_Fgrad, data%host_Fdflux)
#endif

    deallocate(data%uxavg, data%uyavg, data%uzavg)

    deallocate(data%uflux, data%ucommon)
    deallocate(data%uvert)

    deallocate(data%ux_flux, data%uy_flux, data%uz_flux)

    deallocate(data%Fdiscont)

    deallocate(data%Fd_flux, data%Fv_flux, data%Fi)
    deallocate(data%Gd_flux, data%Gv_flux, data%Gi)
    deallocate(data%Hd_flux, data%Hv_flux, data%Hi)

    deallocate(data%agi_x)
    deallocate(data%agi_y)

    deallocate(data%Fd_uavg_flux)
    deallocate(data%Gd_uavg_flux)
    deallocate(data%Hd_uavg_flux)

    deallocate(data%k1)

    deallocate(data%tci, data%alpha, data%avisc, data%shock_sensor)
    
  end subroutine deallocate_field
   
end module fieldvar_module
