module limiter_module
  use iso_c_binding, only: c_double, c_int
  use deepfry_constants_module
  use input_module
  use mesh_module
  use fr_module
  use linked_list_module
  use df_bc_module

  use parallel_module
  use fieldvar_module

#ifdef BOXLIB
  use parallel
#else
  use mpi
#endif
  
  implicit none

  real(c_double) :: MLP_SMALL = 0.00000001_c_double
  
contains
  
  subroutine initialize_limiter(mesh)
    type(mesh2d), intent(inout) :: mesh
    
    call set_projection_operators(mesh)

  end subroutine initialize_limiter

  subroutine limit(mesh, data, time, linked_list, mpi_data, initial_limiting)
    use input_module, only: limiter_type, avisc
    real(c_double),         intent(in   ) :: time    
    type(mesh2d),           intent(inout) :: mesh
    type(FieldData_t),      intent(inout) :: data
    type(linked_list_data), intent(inout) :: linked_list
    type(mpi_data_t),       intent(inout) :: mpi_data
    logical, optional,      intent(in   ) :: initial_limiting


    ! locals
    integer :: elem_index
    logical :: ilimit
    ilimit = .false. ; if (present(initial_limiting)) ilimit = initial_limiting

    ! reset troubled cell markers
    data%tci = ZERO

    ! ensure positivity if the RK integration caused unphysical values
    if ( avisc .gt. ZERO ) then
       call zhang_xia_shu_pp_limit(mesh, data, time, linked_list, mpi_data)
    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Get the P1 projected solution

    !$OMP PARALLEL DO PRIVATE(elem_index) &
    !$OMP SCHEDULE(static)
    do elem_index = 1, mesh%Nelements

       ! P1 projected solution at the solution points
       call DGEMM('N', 'N', mesh%Np, mesh%Nvar, mesh%Np, &
            ONE, mesh%Pnm(1, :, :), mesh%Np, &
            data%u(:, :, elem_index), mesh%Np, &
            ZERO, data%up1(:, :, elem_index), mesh%Np)

       ! P1 projected solution interpolated to the flux points
       call DGEMM('N', 'N', mesh%Nflux, mesh%Nvar, mesh%Np, &
            ONE, mesh%Lf, mesh%Nflux, &
            data%up1(:, :, elem_index), mesh%Np, &
            ZERO, data%up1f(:, :, elem_index), mesh%Nflux)

    end do
    !$OMP END PARALLEL DO

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! compute the min and max for the STj set for all elements
    if (limiter_type .ne. ZHANG_XIA_SHU_LIMITER) then
       call set_STj_min_max(mesh, data, linked_list, mpi_data)
    end if

    if (limiter_type .eq. MLP_LIMITER .or. ilimit) then
       call mlp_limit(mesh, data, time, linked_list, mpi_data)

    ! else if (limiter_type .eq. KRIVODONOVA_LIMITER) then
    !    call krivodonova_limit(mesh, data, time, linked_list, mpi_data)

    else if (limiter_type .eq. ZHANG_XIA_SHU_LIMITER) then
       call zhang_xia_shu_pp_limit(mesh, data, time, linked_list, mpi_data)
    end if

    ! flatten polynomials if the limiting caused unphysical values
    if ( limiter_type .ne. ZHANG_XIA_SHU_LIMITER) then
       call zhang_xia_shu_pp_limit(mesh, data, time, linked_list, mpi_data)
    end if

    ! handle periodicity since the solution values may have changed !FIXME
    !call copy_mapped_elements(mesh)

  end subroutine limit

  subroutine mlp_limit(mesh, data, time, linked_list, mpi_data)
    use input_module, only: limiter_scaling_factor
    real(c_double),         intent(in   ) :: time
    type(mesh2d),           intent(inout) :: mesh
    type(FieldData_t),      intent(inout) :: data
    type(linked_list_data), intent(inout) :: linked_list
    type(mpi_data_t),       intent(inout) :: mpi_data
    
    ! locals
    integer(c_int) :: i, var, m, elem_index, counter
    real(c_double) :: u1(mesh%Np, mesh%Nvar), umm1(mesh%Np, mesh%Nvar)
    real(c_double) :: u1flux(mesh%Nflux, mesh%Nvar)
    
    real(c_double) :: phi_mlp, tmp
    real(c_double) :: phi(mesh%Np + mesh%Nflux)
    logical        :: iPn_mode

    !real(c_double) :: s, savg, p1s, s_min, s_max
    real(c_double) :: arhog, arhol
    real(c_double) :: arhog_avg, arhol_avg
    real(c_double) :: p1g, p1l
    real(c_double) :: arhog_min, arhog_max
    real(c_double) :: arhol_min, arhol_max

    ! go from the highest mode to the lowest and Project the solution
    ! to a lower dimensional space
    if (mesh%P .gt. 1 .and. hierarchial_limiting) then
       do m = mesh%P, 2, -1
          
          iPn_mode = .false.
          if (m .eq. mesh%P) then
             iPn_mode = .true.
          end if

          ! compute the MLP marker from the current solution vector
          data%tci = ZERO
          call mark_troubled_cells(mesh, data, linked_list, mpi_data, iPn_mode)

          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ! Project onto M-1
          
          !$OMP PARALLEL DO PRIVATE(elem_index, umm1) &
          !$OMP SCHEDULE(dynamic,2)
          do elem_index = 1, mesh%Nelements
             if (data%tci(1,1,elem_index) .gt. ZERO) then
                call DGEMM('N', 'N', mesh%Np, mesh%Nvar, mesh%Np, &
                     ONE, mesh%Pnm(m-1, :, :), mesh%Np, &
                     data%u(:, :, elem_index), mesh%Np, &
                     ZERO, umm1(:, :), mesh%Np)
                
                data%u(:, :, elem_index) = umm1(:,:)
             end if
          end do
          !$OMP END PARALLEL DO

       end do

       ! compute the MLP marker for the final limiting stage
       data%tci = ZERO
       call mark_troubled_cells(mesh, data, linked_list, mpi_data)

    else
       ! compute the MLP marker from the solution vector
       iPn_mode = .true.
       call mark_troubled_cells(mesh, data, linked_list, mpi_data, iPn_mode)
       !data%tci = ONE
    end if

    ! all elements now have their prolematic modes truncated. What
    ! remains is the linear mode which is limited in the FVM sense

    !$omp parallel do private(elem_index, i, var, counter, u1, u1flux, &
    !$omp                     arhog_avg, arhol_avg, arhog_min, arhog_max, &
    !$omp                     arhol_min, arhol_max, phi, &
    !$omp                     arhog, arhol, p1g, p1l, tmp, phi_mlp) &
    !$omp schedule(dynamic,2)
    do elem_index = 1, mesh%Nelements
       data%alpha(elem_index) = ONE
       counter = 0

       is_troubled_cell: if (data%tci(1,1,elem_index) .gt. ZERO) then
       
          ! get the linear projection (P1) for the solution
          u1(:, :)    = data%up1( :, :, elem_index)
          u1flux(:,:) = data%up1f(:, :, elem_index)

          ! compute the phi candidates for the vertices
#ifdef DIM3
#else
          arhog_avg = data%uavg(1, elem_index)
          arhol_avg = data%uavg(5, elem_index)

          arhog_min = min(arhog_avg, mesh%STj_var_min(1,1,elem_index))
          arhol_min = min(arhol_avg, mesh%STj_var_min(1,2,elem_index))

          arhog_max = max(arhog_avg, mesh%STj_var_max(1,1,elem_index))
          arhol_max = max(arhol_avg, mesh%STj_var_max(1,2,elem_index))
#endif

          ! default limiter value (no limiting)
          phi = ONE

          ! compute the limiter variable at the flux points
          do i = 1, mesh%Nflux
             ! isolate the linear mode
#ifdef DIM3
#else
             arhog = u1flux(i, 1); p1g = arhog - arhog_avg
             arhol = u1flux(i, 5); p1l = arhol - arhol_avg
#endif
             
             counter = counter + 1
             if ( abs(p1g) .gt. SMALL)  then
                tmp = ONE/p1g
                phi(counter) = max(ZERO, mlp_u1(max(tmp*(arhog_min - arhog_avg), &
                                                    tmp*(arhog_max - arhog_avg))))        
             end if
             
             if (abs(p1l) .gt. SMALL) then
                tmp = ONE/p1l
                phi(counter) = min(phi(counter), mlp_u1(max(tmp*(arhol_min - arhol_avg), &
                                                            tmp*(arhol_max - arhol_avg))))
             end if
          end do

          ! compute the limiter variable at the solution nodes
          do i = 1, mesh%Np
             ! isolate the linear mode
#ifdef DIM3
#else
             arhog = u1(i, 1); p1g = arhog - arhog_avg
             arhol = u1(i, 5); p1l = arhol - arhol_avg
#endif
             counter = counter + 1
             if ( abs(p1g) .gt. SMALL)  then
                tmp = ONE/p1g
                phi(counter) = max(ZERO, mlp_u1(max(tmp*(arhog_min - arhog_avg), &
                                                    tmp*(arhog_max - arhog_avg))))
             end if
             
             if (abs(p1l) .gt. SMALL) then
                tmp = ONE/p1l
                phi(counter) = min(phi(counter), mlp_u1(max(tmp*(arhol_min - arhol_avg), &
                                                            tmp*(arhol_max - arhol_avg))))
             end if
          end do

          phi_mlp = minval(phi) * limiter_scaling_factor
          data%alpha(elem_index) = phi_mlp

          ! compute the P1 limited solution
          do var = 1, mesh%Nvar
             
             ! limited value at the solution points
             do i = 1, mesh%Np
                data%u(i, var, elem_index) = data%uavg(var, elem_index) + &
                     phi_mlp*(u1(i, var) - data%uavg(var, elem_index))
             end do

             ! limited value at the flux points
             do i = 1, mesh%Nflux
                data%uflux(i, var, elem_index) = data%uavg(var, elem_index) + &
                     phi_mlp*(u1flux(i, var) - data%uavg(var, elem_index))
             end do
          end do
       
       end if is_troubled_cell
    end do
    !$omp end parallel do

  end subroutine mlp_limit

  ! subroutine krivodonova_limit(mesh, data, time, linked_list, mpi_data)
  !   use input_module, only: limiter_scaling_factor
  !   real(c_double),         intent(in   ) :: time
  !   type(mesh2d),           intent(inout) :: mesh
  !   type(FieldData_t),      intent(inout) :: data
  !   type(linked_list_data), intent(inout) :: linked_list
  !   type(mpi_data_t),       intent(inout) :: mpi_data

  !   ! locals
  !   integer(c_int) :: elem_index, i, var
  !   real(c_double) :: u1(mesh%Np, mesh%Nvar)
  !   real(c_double) :: u1flux(mesh%Nflux, mesh%Nvar)
  !   real(c_double) :: alphav(mesh%Np + mesh%Nflux), alpha
  !   real(c_double) :: s, savg, s_min, s_max

  !   ! mark troubled cells
  !   call mark_troubled_cells(mesh, data, linked_list, mpi_data, .true.)

  !   !$OMP PARALLEL DO PRIVATE(elem_index, i, var, s, savg, s_min, s_max, alphav, &
  !   !$OMP                     alpha, u1, u1flux) &
  !   !$OMP SCHEDULE(dynamic,2)
  !   do elem_index = 1, mesh%Nelements
  !      data%alpha(elem_index) = ONE
  !      if (data%tci(1,1,elem_index) .gt. ZERO) then

  !         ! average entropy in this cell
  !         savg = get_entropy(data%uavg(:,elem_index))

  !         ! get the min and max entropy for neighboring cells
  !         s_min = min(savg, mesh%STj_var_min(1,1,elem_index))
  !         s_max = max(savg, mesh%STj_var_max(1,1,elem_index))

  !         ! default limiter value is 1 (no limiting)
  !         alphav = ONE
          
  !         ! limiter value from the solution points
  !         do i = 1, mesh%Np
  !            s = get_entropy(data%u(i, :, elem_index))

  !            ! do the limiting (Eq. 13c)
  !            if (s .gt. s_max) then
  !               alphav(i) = max(ZERO, (s_max - savg)/(s - savg))
  !            else if ( s .lt. s_min ) then
  !               alphav(i) = max(ZERO, (s_min - savg)/(s - savg))
  !            end if
  !         end do

  !         ! limriter value from the flux points
  !         do i = 1, mesh%Nflux
  !            s = get_entropy(data%uflux(i, :, elem_index))

  !            ! do the limiting
  !            if (s .gt. s_max) then
  !               alphav(mesh%Np + i) = max(ZERO, (s_max-savg)/(s-savg))
  !            else if (s .lt. s_min) then
  !               alphav(mesh%Np + i) = max(ZERO, (s_min-savg)/(s-savg))
  !            end if
  !         end do

  !         ! limiter value
  !         alpha = minval(alphav) * limiter_scaling_factor
  !         data%alpha(elem_index) = alpha

  !         ! get the P1 projected solution
  !         u1(:, :)     = data%up1( :, :, elem_index)
  !         u1flux(:, :) = data%up1f(:, :, elem_index)

  !         ! compute the P1 limited solution
  !         do var = 1, mesh%Nvar

  !            ! limited value at the solution points
  !            do i = 1, mesh%Np
  !               data%u(i, var, elem_index) = data%uavg(var, elem_index) + &
  !                    alpha*(u1(i, var) - data%uavg(var, elem_index))
  !            end do

  !            ! limited value at the flux points
  !            do i = 1, mesh%Nflux
  !               data%uflux(i, var, elem_index) = data%uavg(var, elem_index) + &
  !                    alpha*(u1flux(i, var) - data%uavg(var, elem_index))
  !            end do

  !         end do
  !      end if
  !   end do
  !   !$OMP END PARALLEL DO

  ! end subroutine krivodonova_limit

  subroutine zhang_xia_shu_pp_limit(mesh, data, time, linked_list, mpi_data)
    use input_module, only: gamma_gas, gamma_liq, pinf_gas, pinf_liq, q_gas, q_liq
    use flux_module, only: get_primitive

    real(c_double),         intent(in   ) :: time
    type(mesh2d),           intent(inout) :: mesh
    type(FieldData_t),      intent(inout) :: data
    type(linked_list_data), intent(in   ) :: linked_list
    type(mpi_data_t),       intent(inout) :: mpi_data
    
    ! locals
    integer(c_int) :: elem_index, i, j, var, nquad
    real(c_double) :: uavg(mesh%Nvar)
    !real(c_double) :: rho, rhoavg, rhomin, p, pavg
    real(c_double) :: theta1, tmp
    real(c_double) :: tx

    real(c_double) :: arhog, arhol, arhog_avg, arhol_avg
    real(c_double) :: arhog_min, arhol_min
    real(c_double) :: w(5), wavg(5)

    real(c_double) :: arhog_s(mesh%Np), arhol_s(mesh%Np)
    real(c_double) :: ws(mesh%Np, 5)

    real(c_double) :: arhog_flux(mesh%Nflux)
    real(c_double) :: arhol_flux(mesh%Nflux)
    real(c_double) :: wflux(mesh%Nflux, 5)

    real(c_double), allocatable :: uflux(:, :)
    real(c_double), allocatable :: uquad(:, :)

    Nquad = mesh%Np + mesh%Nflux

    allocate( uquad(nquad,      mesh%Nvar) )
    allocate( uflux(mesh%Nflux, mesh%Nvar) )

    !$omp parallel do private(elem_index, i, j, var, uavg, uflux, &
    !$omp                     arhog_avg, arhol_avg, wavg, uquad, &
    !$omp                     arhog_s, arhol_s, ws, arhog_flux, arhol_flux, wflux, &
    !$omp                     arhog_min, arhol_min, tmp, theta1, arhog, arhol, w, tx) &
    !$omp schedule(dynamic,2)
    do elem_index = 1, mesh%Nelements
       if (mesh%elem_type(elem_index) .ne. -1) then

          uavg(:)     = data%uavg( :,    elem_index)
          uflux(:, :) = data%uflux(:, :, elem_index)
             
          ! get the average density and pressure
          arhog_avg = uavg(1)
          arhol_avg = uavg(5)
#ifdef DIM3
#else
          call get_primitive(uavg, gamma_gas, gamma_liq, pinf_gas, pinf_liq, q_gas, q_liq, wavg)
#endif

          ! sanity check on the averages
          if (debug) then
             if ( min(arhog_avg, arhol_avg) .lt. SMALL .or. wavg(1) .lt. SMALL ) then
                print*, 'average values too small...', elem_index, arhog_avg, arhol_avg
                stop
             end if
          end if

          ! get the values at the quadrature points
          uquad(1:mesh%Np,  :) = data%u(:, :, elem_index)       
          uquad(mesh%Np+1:, :) = uflux( :, :)
          
          ! density and pressure at the solution points
          arhog_s = data%u(:, 1, elem_index)
          arhol_s = data%u(:, 5, elem_index)
          do j = 1, mesh%Np
             call get_primitive(data%u(j, :, elem_index), &
                  gamma_gas, gamma_liq, pinf_gas, pinf_liq, q_gas, q_liq, ws(j,:))
          end do
          
          ! desnity and pressure at the flux points
          arhog_flux = uflux(:, 1)
          arhol_flux = uflux(:, 5)
          do j = 1, mesh%Nflux
             call get_primitive(uflux(j, :), gamma_gas, gamma_liq, &
                  pinf_gas, pinf_liq, q_gas, q_liq, wflux(j,:))
          end do

          ! Now limit the solution: first treat the density Eq. (5.10, 5.11)
          if ( all(uquad(:, 1) .gt. SMALL) ) then
             arhog_min = minval(uquad(:,1))

             tmp = ONE/(arhog_avg - arhog_min)
             theta1 = max(ZERO, min(tmp*(arhog_avg - SMALL), ONE))
          else
             theta1 = ZERO
          end if
          uquad(:, 1) = arhog_avg + theta1 * (uquad(:, 1) - arhog_avg)

          if ( all(uquad(:, 5) .gt. SMALL) ) then
             arhol_min = minval(uquad(:,5))

             tmp = ONE/(arhol_avg - arhol_min)
             theta1 = max(ZERO, min(tmp*(arhol_avg - SMALL), ONE))
          else
             theta1 = ZERO
          end if
          uquad(:, 5) = arhol_avg + theta1 * (uquad(:, 5) - arhol_avg)

          ! now modify the pressure Eq. (5.12, 5.13, 5.14)
          tx = ONE
          if ( all(ws(:,1) .ge. SMALL) .and. all(wflux(:,1) .ge. SMALL) ) then
             do i = 1, Nquad
                arhog = uquad(i, 1)
                arhol = uquad(i, 5)

                arhog_avg = uavg(1)
                arhol_avg = uavg(5)
             
                call get_primitive(uquad(i, :), gamma_gas, gamma_liq, &
                     pinf_gas, pinf_liq, q_gas, q_liq, w)

                call get_primitive(uavg, gamma_gas, gamma_liq, &
                     pinf_gas, pinf_liq, q_gas, q_liq, wavg)
             
                if ( w(1) .lt. SMALL ) then
                   tx = min(tx, wavg(1)/(wavg(1) - w(1)))
                end if

             end do

             ! set the limited solution values
             do var = 1, mesh%Nvar
                uquad(:, var) = uavg(var) + tx*(uquad(:, var) - uavg(var))
             end do

          else
             tx = ZERO
             do var = 1, mesh%Nvar
                uquad(:, var) = uavg(var)
             end do
          end if
       
          ! set the limited solution
          data%u(:, :, elem_index)     = uquad(1:mesh%Np,  :)
          data%uflux(:, :, elem_index) = uquad(mesh%Np+1:, :)

       end if
    end do
    !$OMP END PARALLEL DO

    deallocate(uquad, uflux)

  end subroutine zhang_xia_shu_pp_limit
  
  subroutine mark_troubled_cells(mesh, data, linked_list, mpi_data, iPn_mode)
    use input_module, only: MAX_SVJ

    type(linked_list_data), intent(inout) :: linked_list
    type(FieldData_t),      intent(inout) :: data
    type(mesh2d),           intent(inout) :: mesh
    logical, optional,      intent(in   ) :: iPn_mode
    type(mpi_data_t),       intent(inout) :: mpi_data
    
    ! locals
    integer(c_int) :: elem_index, i

    real(c_double) :: arhog, arhol
    real(c_double) :: arhog_avg, arhol_avg
    real(c_double) :: arhog_max, arhog_min, arhol_max, arhol_min

    logical :: Pn_mode
    integer(c_int) :: myrank, numprocs, mpi_err

    Pn_mode = .false.; if (present(iPn_mode)) Pn_mode = iPn_mode

    !$omp parallel do private(elem_index, i, arhog, arhol, arhog_avg, arhol_avg, &
    !$omp                     arhog_max, arhog_min, arhol_max, arhol_min) &
    !$omp schedule(dynamic,2)
    do elem_index = 1, mesh%Nelements
       
#ifdef DIM3
#else
       arhog_avg = data%uavg(1, elem_index)
       arhol_avg = data%uavg(5, elem_index)
#endif

       ! min/max sensor variables around the neighborhood of the
       ! element
       arhog_max = mesh%STj_var_max(1, 1, elem_index)
       arhog_min = mesh%STj_var_min(1, 1, elem_index)

       arhol_max = mesh%STj_var_max(1, 2, elem_index)
       arhol_min = mesh%STj_var_min(1, 2, elem_index)

       ! check solution values
       do i = 1, mesh%Np
#ifdef DIM3
#else
          arhog = data%u(i, 1, elem_index)
          arhol = data%u(i, 5, elem_index)
#endif

          if ( (arhog .le. arhog_min + MLP_SMALL) .or. &
               (arhog .ge. arhog_max - MLP_SMALL) ) then
             data%tci(1, 1, elem_index) = ONE
          end if
          
          if ( (arhol .le. arhol_min + MLP_SMALL) .or. &
               (arhol .ge. arhol_max - MLP_SMALL) ) then
             data%tci(1, 1, elem_index) = ONE
          end if
       end do

       if (data%tci(1,1,elem_index) .gt. ZERO) cycle

       ! check flux point values
       do i = 1, mesh%Nflux 
#ifdef DIM3
#else
          arhog = data%uflux(i, 1, elem_index)
          arhol = data%uflux(i, 5, elem_index)
#endif
          if ( (arhog .le. arhog_min + MLP_SMALL) .or. &
               (arhog .ge. arhog_max - MLP_SMALL) ) then
             data%tci(1, 1, elem_index) = ONE
          end if

          if ( (arhol .le. arhol_min + MLP_SMALL) .or. &
               (arhol .ge. arhol_max - MLP_SMALL) ) then
             data%tci(1, 1, elem_index) = ONE
          end if
       end do
          
    end do
    !$omp end parallel do
    
    ! unmark smooth extrema
    call unmark_smooth_extrema(mesh, data)

    ! mark troubled cells using the artificial viscosity parameter
    if (use_shock_sensor_as_marker) then

       !$omp parallel do private(elem_index) &
       !$omp schedule(dynamic,2)
       do elem_index = 1, mesh%Nelements
          if (data%avisc(elem_index) .gt. avisc_min) then
              data%tci(1,1,elem_index) = ONE
          end if
       end do
       !$omp end parallel do
    end if

    ! exchange the troubled cell marker for ghost cells. The limiter
    ! value, phi (and hence solution and flux points) will be affected
    ! by it.
    if (mesh%numPartitions .gt. 1) then
       
       call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, mpi_err)
       call MPI_Comm_Size(MPI_COMM_WORLD, numProcs, mpi_err)

       call exchange_data(1, 1, mesh, data%tci, TCI_TAG, &
            mpi_data%avg_send, mpi_data%avg_recv, myRank, numProcs, MPI_DOUBLE)
    end if
    
  end subroutine mark_troubled_cells

  subroutine unmark_smooth_extrema(mesh, data)
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int) :: elem_index, i

    real(c_double) :: u1(mesh%Np, mesh%Nvar)

    real(c_double) :: u1f(  mesh%Nflux, mesh%Nvar)
    real(c_double) :: uflux(mesh%Nflux, mesh%Nvar)

    real(c_double) :: arhog, arhol, arhog_avg, arhol_avg
    real(c_double) :: arhog_max, arhog_min
    real(c_double) :: arhol_max, arhol_min
    real(c_double) :: arhog1, arhol1
    real(c_double) :: g_slope, l_slope
    real(c_double) :: g1fpn, l1fpn

    real(c_double) :: elem_area
    logical        :: unmark(2,mesh%Nflux)

    !$omp parallel do private(elem_index, i, uflux, u1, u1f, arhog_avg, arhol_avg, &
    !$omp                    arhog, arhol, arhog_max, arhog_min, arhol_max, arhol_min, &
    !$omp                    arhog1, arhol1, g_slope, l_slope, g1fpn, l1fpn, unmark) &
    !$omp schedule(dynamic,2)
    do elem_index = 1, mesh%Nelements
       if (data%tci(1,1,elem_index) .gt. ZERO) then

          ! get the flux point values
          uflux = data%uflux(:, :, elem_index)

          ! get the P1 approximation
          u1  = data%up1( :, :, elem_index)
          u1f = data%up1f(:, :, elem_index)

          elem_area = get_element_area(mesh, elem_index)

#ifdef DIM3
#else
          arhog_avg = data%uavg(1, elem_index)
          arhol_avg = data%uavg(5, elem_index)

          arhog_max = mesh%STJ_var_max(1,1,elem_index)
          arhol_max = mesh%STj_var_max(1,2,elem_index)

          arhog_min = mesh%STj_var_min(1,1,elem_index)
          arhol_min = mesh%STj_var_min(1,2,elem_index)
#endif          
          unmark = .false.
          do i = 1, mesh%Nflux
#ifdef DIM3
#else
             arhog = data%uflux(i, 1, elem_index)
             arhol = data%uflux(i, 5, elem_index)

             arhog1 = u1f(i, 1)
             arhol1 = u1f(i, 5)
#endif

             g_slope = arhog1 - arhog_avg
             l_slope = arhol1 - arhol_avg

             g1fpn = arhog - arhog1
             l1fpn = arhol - arhol1

             ! check for a smooth local maximum
             if ( (g_slope .gt. MLP_SMALL) .and. (g1fpn .lt. MLP_SMALL) .and. &
                  (arhog .gt. arhog_max) ) then
                unmark(1,i) = .true.
             end if

             if ( (l_slope .gt. MLP_SMALL) .and. (l1fpn .lt. MLP_SMALL) .and. &
                  (arhol .gt. arhol_max) ) then
                unmark(2,i) = .true.
             end if

             ! check for a smooth local minimum
             if ( (g_slope .lt. MLP_SMALL) .and. (g1fpn .gt. MLP_SMALL) .and. &
                  (arhog .lt. arhog_min) ) then
                unmark(1, i) = .true.
             end if
             
             if ( (l_slope .lt. MLP_SMALL) .and. (l1fpn .gt. MLP_SMALL) .and. &
                  (arhol .lt. arhol_min) ) then
                unmark(2, i) = .true.
             end if

             ! final deactivation threshold
             if ( abs(arhog - arhog_avg) .le. &
                  max(mlp_deactivating_threshold*arhog_avg, elem_area) ) then
                unmark(1, i) = .true.
             end if

             if ( abs(arhol - arhol_avg) .le. &
                  max(mlp_deactivating_threshold*arhol_avg, elem_area) ) then
                unmark(2, i) = .true.
             end if

          end do
          
          ! unmark this cell if all flux point values are OK
          if ( all(unmark .eqv. .true.) ) then
             data%tci(1,1,elem_index) = ZERO
          end if

       end if
    end do
    !$omp end parallel do
    
  end subroutine unmark_smooth_extrema

  subroutine Moro_Nguyen_Peraire_divergence_sensor(mesh, data)
    use input_module, only: gamma, mnp_sensor_kh, avisc_alpha, avisc_beta
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int) :: elem_index, i
    real(c_double) :: h, fac1, fac2, tmp_upper
    real(c_double) :: lower, upper
    real(c_double) :: alpha, beta

    real(c_double), allocatable :: rho(:), velx(:), vely(:), P(:), E(:)
    real(c_double), allocatable :: v2(:), c2(:), M2(:), c(:)

    real(c_double), allocatable :: rho_x(:), rho_y(:), velx_x(:), vely_y(:), div(:)
    real(c_double), allocatable :: sensor(:), avisc(:)

    allocate(rho(mesh%Np), velx(mesh%Np), vely(mesh%Np), P(mesh%Np), E(mesh%Np))
    allocate(v2(mesh%Np), c2(mesh%Np), M2(mesh%Np), c(mesh%Np))

    allocate(rho_x(mesh%Np), rho_y(mesh%Np))
    allocate(velx_x(mesh%Np), vely_y(mesh%Np))
    allocate(div(mesh%Np))
    allocate(sensor(mesh%Np), avisc(mesh%Np))    

    ! constants
    fac1 = TWO/(gamma + ONE)
    fac2 = (gamma-ONE)/TWO

    alpha = avisc_alpha
    beta  = avisc_beta

    tmp_upper = ONE/alpha * log(TWO)

    ! default values
    data%shock_sensor = ZERO
    data%avisc = ZERO

    !$OMP PARALLEL DO PRIVATE(elem_index, i, rho, velx, vely, v2, P, c2, M2, &
    !$OMP                     rho_x, rho_y, velx_x, vely_y, div, c, h, sensor, &
    !$OMP                     lower, upper, avisc) &
    !$OMP SCHEDULE(dynamic,2)
    do elem_index = 1, mesh%Nelements
       rho =  data%u( :, 1, elem_index)
       velx = data%u(:, 2, elem_index)/rho
       vely = data%u(:, 3, elem_index)/rho

       v2 = velx**2 + vely**2
       P    = (gamma-ONE)*(data%u(:, 4, elem_index) - HALF*rho*v2)
       
       c2 = gamma*P/rho
       M2 = v2/c2

       rho_x = data%ux(:, 1, elem_index)
       rho_y = data%uy(:, 1, elem_index)

       velx_x = ONE/rho * (data%ux(:, 2, elem_index) - velx*rho_x)
       vely_y = ONE/rho * (data%uy(:, 3, elem_index) - vely*rho_y)

       div = velx_x + vely_y

       c = sqrt(c2*fac1*(ONE + fac2*M2))
       h      = HALF*mesh%characteristic_length(elem_index)
       
       ! shock sensor Eq. (6)
       sensor = -mnp_sensor_kh*(h/mesh%P)*div/c
       data%shock_sensor(elem_index) = maxval(sensor)

       ! artificial viscosity Eq. (13,14)
       avisc = ZERO
       do i = 1, mesh%Np
          lower = max(ZERO, sensor(i) - beta)
          upper = lower + tmp_upper
          avisc(i) = ONE/alpha * log( ONE + exp(alpha*(sensor(i) - beta)) )

          if ( avisc(i) .le. lower ) then
             avisc(i) = lower
          end if

          if ( avisc(i) .ge. upper ) then
             avisc(i) = upper
          end if
       end do
       
       ! Eq. (12)
       avisc = mnp_sensor_kh*(h/mesh%P)*sqrt(velx**2 + vely**2 + c2)*avisc
       data%avisc(elem_index) = maxval(avisc)

    end do
    !$OMP END PARALLEL DO

    deallocate(rho, velx, vely, P, E)
    deallocate(v2, c2, M2, c)

    deallocate(rho_x, rho_y)
    deallocate(velx_x, vely_y)
    deallocate(div)
    deallocate(sensor, avisc)

  end subroutine Moro_Nguyen_Peraire_divergence_sensor

  subroutine set_projection_operators(mesh)
    type(mesh2d), intent(inout) :: mesh
    
    ! locals
    integer(c_int) :: i,j,m
    real(c_double) :: Pnm(mesh%Np, mesh%Np), tmp(mesh%Np, mesh%Np)
    
    do m = 0, mesh%P
       Pnm = ZERO

       ! construct the projection operator
       do i = 1, mesh%Np
          do j = 1, mesh%Np
             if ((i .eq. j) .and. (mesh%basis_degrees(i) .le. m)) then
                Pnm(i, j) = ONE
             end if
          end do
       end do
          
       call DGEMM('N', 'N', mesh%Np, mesh%Np, mesh%Np, &
            ONE, mesh%V, mesh%Np,  &
            Pnm, mesh%Np, &
            ZERO, tmp, mesh%Np)
       
       call DGEMM('N', 'N', mesh%Np, mesh%Np, mesh%Np, &
            ONE, tmp, mesh%Np, &
            mesh%Vinv, mesh%Np, &
            ZERO, mesh%Pnm(m, :, :), mesh%Np)

    end do

  end subroutine set_projection_operators

  subroutine set_nodal_cell_averages(mesh, data, linked_list)
    use input_module, only: MAX_SVJ

    type(mesh2d),           intent(inout) :: mesh
    type(FieldData_t),      intent(inout) :: data
    type(linked_list_data), intent(inout) :: linked_list

    ! locals
    integer(c_int) :: node, i
    real(c_double) :: xp, yp, zp

    integer(c_int) :: nsvj, elem_index
    
    real(c_double), allocatable :: Varavg(:,:)
    integer(c_int), allocatable :: SVj(:)

    allocate(Varavg(2,MAX_SVJ))
    allocate(SVj(MAX_SVJ))
    
    do node = 1, mesh%Nnodes
       xp = mesh%points(node, 1); yp = mesh%points(node, 2)
       call get_elements_sharing_vertex(xp, yp, zp, mesh, linked_list, SVj, nsvj)

       ! store the average for all cells sharing the vertex
       do i = 1, nsvj
          elem_index = SVj(i)
#ifdef DIM3
          Varavg(1,i) = data%uavg(1, elem_index)
          Varavg(2,i) = data%uavg(6, elem_index)
#else          
          Varavg(1,i) = data%uavg(1, elem_index)
          Varavg(2,i) = data%uavg(5, elem_index)
#endif
       end do

       ! compute the min and max values for the indicator variable and store it 
       mesh%vertex_var_min(1,node) = minval(Varavg(1,1:nsvj))
       mesh%vertex_var_min(2,node) = minval(Varavg(2,1:nsvj))

       mesh%vertex_var_max(1,node) = maxval(Varavg(1,1:nsvj))
       mesh%vertex_var_max(2,node) = maxval(Varavg(2,1:nsvj))
    end do

    deallocate(Varavg)
    deallocate(SVj)

  end subroutine set_nodal_cell_averages

  subroutine set_STj_min_max(mesh, data, linked_list, mpi_data)
    use input_module, only: MAX_SVJ
    type(mesh2d),           intent(inout) :: mesh
    type(FieldData_t),      intent(inout) :: data
    type(linked_list_data), intent(inout) :: linked_list
    type(mpi_data_t),       intent(inout) :: mpi_data

    ! locals
    integer(c_int) :: i, elem_index, nbr_index, counter
    integer(c_int) :: vertex_id, Nsvj

    real(c_double), allocatable :: Varavg(:,:)

    integer(c_int) :: myRank, numProcs, mpi_err

    allocate(Varavg(2,MAX_SVJ*mesh%elem_num_faces))

    !$OMP PARALLEL DO PRIVATE(elem_index, counter, vertex_id, i, NSVj, nbr_index, Varavg) &
    !$OMP SCHEDULE(dynamic,2)
    do elem_index = 1, mesh%Nelements_local

       counter = 0
       do vertex_id = 1, mesh%elem_num_verts
          Nsvj = mesh%Nsvj(vertex_id, elem_index)

          do i = 1, Nsvj
             nbr_index = mesh%SVj(vertex_id, i, elem_index)

             ! exclude self
             if (nbr_index .ne. elem_index) then
                counter = counter + 1

#ifdef DIM3
#else
                Varavg(1, counter) = data%uavg(1, elem_index)
                Varavg(2, counter) = data%uavg(5, elem_index)
#endif

             end if
          end do

       end do

       mesh%STj_var_min(1,1,elem_index) = minval(Varavg(1,1:counter))
       mesh%STj_var_min(1,2,elem_index) = minval(Varavg(2,1:counter))
       
       mesh%STj_var_max(1,1,elem_index) = maxval(Varavg(1,1:counter))
       mesh%STj_var_max(1,2,elem_index) = maxval(Varavg(2,1:counter))
       
    end do
    !$OMP END PARALLEL DO

    ! ! exchange the local min/max for the STj set for each
    ! ! processor. After this call, the ghost-remotes will have valid
    ! ! min & max values but from their local processors. This will be
    ! ! updated with the current assignment.
    ! in_parallel: if (mesh%numPartitions .gt. 1) then
       
    !    call MPI_Comm_Rank(MPI_COMM_WORLD, myrank, mpi_err)
    !    call MPI_Comm_Size(MPI_COMM_WORLD, numProcs, mpi_err)

    !    call exchange_data(1, 1, mesh, mesh%STJ_var_min, STj_min_TAG, &
    !         mpi_data%avg_send, mpi_data%avg_recv, myRank, numProcs, MPI_DOUBLE)
       
    !    call exchange_data(1, 1, mesh, mesh%STj_var_max, STj_max_TAG, &
    !         mpi_data%avg_send, mpi_data%avg_recv, myRank, numProcs, MPI_DOUBLE)

    !    ! recompute the min/max for ghost-remote elements, taking into
    !    ! consideration the partial min/max just exhanged.
    
    !    !$OMP PARALLEL DO PRIVATE(elem_index, counter, i, vertex_id, Nsvj, nbr_index, Varavg)
    !    do elem_index = mesh%Nelements_local+1, mesh%Nelements
    !       counter = 0
       
    !       do vertex_id = 1, mesh%elem_num_verts
    !          Nsvj = mesh%NSvj(vertex_id, elem_index)
             
    !          do i = 1, Nsvj
    !             nbr_index = mesh%SVj(vertex_id, i, elem_index)
                
    !             ! exclude self
    !             if (nbr_index .ne. elem_index) then
    !                counter = counter + 1
    !                Varavg(counter) = get_entropy(data%uavg(:, nbr_index))
    !             end if
    !          end do
    !       end do
          
    !       mesh%STj_var_min(1,1,elem_index) = &
    !            min(mesh%STj_var_min(1,1,elem_index), minval(Varavg(1:counter)))
          
    !       mesh%STj_var_max(1,1,elem_index) = &
    !            max(mesh%STJ_var_max(1,1,elem_index), maxval(Varavg(1:counter)))
    !    end do
    !    !$OMP END PARALLEL DO

    ! end if in_parallel
       
    deallocate(Varavg)
    
    ! FIXME: small errors in serial and parallel results
    !mesh%STj_var_min = ZERO
    !mesh%STj_var_max = ONE
    
  end subroutine set_STj_min_max

  pure function mlp_u1(r) result(phi)
    real(c_double), intent(in) :: r
    real(c_double)             :: phi
    phi = min(ONE, r)
  end function mlp_u1

  pure function get_entropy(u) result(S)
    use input_module, only: gamma
    real(c_double), intent(in) :: u(:)
    real(c_double)             :: S

    ! locals
    real(c_double) :: rho, Mx, My, E, P
    
    rho = u(1)
    Mx  = u(2)
    My  = u(3)
    E   = u(4)

    P = (gamma-ONE)*(E - HALF*(Mx**2 + My**2)/rho)
    S = P/rho**gamma
    
  end function get_entropy
      
end module limiter_module
