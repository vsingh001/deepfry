module frutils_module
  use iso_c_binding, only: c_double, c_int
  use mesh_module
  use fr_module
  use df_bc_module
  use userbc_module
  use fieldvar_module
  use system_module

#ifdef CUDA  
  use gpu_module
#endif

#ifdef BOXLIB
  use parallel
  use omp_module
  use df_boxlib_interface_module
#else
  use omp_lib
  use parallel_module
#endif
  
  implicit none

contains

    subroutine get_gradients(mesh, data, time, mpi_data, myRank, numProcs)
      integer(c_int),    intent(in   ) :: myRank, numProcs
      type(FieldData_t), intent(inout) :: data
      type(mesh2d),      intent(inout) :: mesh
      type(mpi_data_t),  intent(inout) :: mpi_data
      real(c_double),    intent(in   ) :: time

      !call copy_mapped_elements(mesh, data) ! FIXME: handle periodicity

      ! Exchange solution values
      if (mesh%numPartitions .gt. 1) then
         call exchange_data(mesh%Np, mesh%Nvar, mesh, data%u, USOL_TAG, &
              mpi_data%sol_send, mpi_data%sol_recv, myRank, numProcs, MPI_DOUBLE)
      end if

#ifdef CUDA
      call gpu_ugrad(mesh%device_data, data%u)
#endif

      call interpolate_to_flux_points(mesh%Np, mesh%Nflux, mesh%Nvar, mesh%Nelements, &
           mesh%Lf, data%u, data%uflux)

      call get_common_solution_values(mesh, data%uflux, data%ucommon)
      call get_boundary_common_solution_values(mesh, data)
      call get_userbc_common_solution_values(mesh, data, time)

#ifdef BOXLIB
      call get_BoxLib_interface_common_solution_values(mesh, data)
#endif

      ! exchange ucommon 
      if (mesh%numPartitions .gt. 1) then
         call exchange_data(mesh%Nflux, mesh%Nvar, mesh, data%ucommon, UCOMMON_TAG, &
              mpi_data%flux_send, mpi_data%flux_recv, myRank, numProcs, MPI_DOUBLE)
      end if

      !call copy_mapped_elements(mesh, data) ! FIXME: handle periodicity

#ifdef CUDA
      call read_host_ugrad(mesh%device_data, mesh%host_ugrad)
#endif

      ! call the libfr gradients
      call compute_gradients(mesh, data%u, data%uflux, data%ucommon, &
           data%ux, data%uy, data%uz, data%ux_flux, data%uy_flux, data%uz_flux)

      ! get the gradients of the void fraction from the solution gradients
      call get_void_fraction_gradients(mesh, data%u, data%ux, data%uy, data%uz, &
           data%ax, data%ay)

      ! call get_corrected_void_fraction_gradients(mesh, data, &
      !      data%alphag_flux, data%agi_x, data%agi_y, data%ax, data%ay)

    end subroutine get_gradients

    subroutine compute_averages(mesh, data)
      type(mesh2d),      intent(inout) :: mesh
      type(FieldData_t), intent(inout) :: data

      call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%u, data%uavg)

      call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%ux, data%uxavg)

      call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%uy, data%uyavg)

      call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%uz, data%uzavg)

      ! average gradients of the void-fractions
      call compute_cell_averages(mesh%Np, 1, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%ax, data%ax_avg)

      call compute_cell_averages(mesh%Np, 1, mesh%Nelements, mesh%w, &
           mesh%reference_area, data%ay, data%ay_avg)

      !call copy_mapped_elements(mesh, data)

    end subroutine compute_averages

  end module frutils_module
