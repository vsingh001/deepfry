module visit_writer_module
  use iso_c_binding, only: c_double
  use deepfry_constants_module
  use input_module
  use mesh_module, only: mesh2d
  use fieldvar_module

  implicit none

  include "silo_f9x.inc"

  contains

    subroutine write_master(filename, lfname, mesh, step, time)
      type(mesh2d),       intent(inout) :: mesh
      character(len=256), intent(in   ) :: filename
      integer(c_int),     intent(in   ) :: lfname
      integer(c_int),     intent(in   ) :: step
      real(c_double),     intent(in   ) :: time

      ! locals
      integer :: dbfile, err, ierr, nmesh, oldlen
      integer :: lmeshnames(mesh%numPartitions)
      integer :: meshtypes(mesh%numPartitions)

      integer(c_int) :: i
      character(len=256) :: fname, plotfile_name

      character(len=9)   :: plot_index
      character(len=3)   :: partition_index

      character(len=lfname) :: meshnames(mesh%numPartitions)

      character(len=lfname-1) :: rhonames(mesh%numPartitions)
      integer                 :: lrhoames(mesh%numPartitions)
      integer                 :: vartypes(mesh%numPartitions)

      character(len=lfname)  :: rhox(     mesh%numPartitions)
      integer                :: lrhox(mesh%numPartitions)

      character(len=lfname)  :: rhoy(     mesh%numPartitions)
      integer                :: lrhoy(mesh%numPartitions)

      character(len=lfname)  :: rhoz(     mesh%numPartitions)
      integer                :: lrhoz(mesh%numPartitions)

      character(len=lfname-2) :: Mx( mesh%numPartitions)
      integer                 :: lMx(mesh%numPartitions)      

      character(len=lfname-2) :: My( mesh%numPartitions)
      integer                 :: lMy(mesh%numPartitions)

      character(len=lfname-2) :: Mz(mesh%numPartitions)
      integer                 :: lMz(mesh%numPartitions)

      character(len=lfname-3) :: E( mesh%numPartitions)
      integer                 :: lE(mesh%numPartitions)

      character(len=lfname) :: Mx_x(mesh%numPartitions)
      character(len=lfname) :: Mx_y(mesh%numPartitions)
      character(len=lfname) :: Mx_z(mesh%numPartitions)

      character(len=lfname) :: My_x(mesh%numPartitions)
      character(len=lfname) :: My_y(mesh%numPartitions)
      character(len=lfname) :: My_z(mesh%numPartitions)

      character(len=lfname) :: Mz_x(mesh%numPartitions)
      character(len=lfname) :: Mz_y(mesh%numPartitions)
      character(len=lfname) :: Mz_z(mesh%numPartitions)
      
      character(len=lfname-1) :: E_x(mesh%numPartitions)
      character(len=lfname-1) :: E_y(mesh%numPartitions)
      character(len=lfname-1) :: E_z(mesh%numPartitions)

      integer :: lMx_x(mesh%numPartitions), lMx_y(mesh%numPartitions), lMx_z(mesh%numPartitions)
      integer :: lMy_x(mesh%numPartitions), lMy_y(mesh%numPartitions), lMy_z(mesh%numPartitions)
      integer :: lMz_x(mesh%numPartitions), lMz_y(mesh%numPartitions), lMz_z(mesh%numPartitions)

      integer :: lE_x(mesh%numPartitions), lE_y(mesh%numPartitions), lE_z(mesh%numPartitions)

      character(len=lfname) ::  Proc(mesh%numPartitions)
      integer               :: lProc(mesh%numPartitions)

      ! create a new silo file
      write(unit=plot_index, fmt='(i9.9)') step
      fname = trim(plot_filename) // plot_index //".root"
      ierr = dbcreate(trim(fname), len_trim(fname), DB_CLOBBER, DB_LOCAL,&
                      "multimesh root", 14, DB_HDF5, dbfile)

      nmesh = mesh%numPartitions
      
      oldlen = dbget2dstrlen()
      err    = dbset2dstrlen(lfname)

      ! get the individual file names for the partitions
      do i = 1, mesh%numPartitions
         write(unit=partition_index, fmt='(i3.3)') i-1

         plotfile_name = trim(plot_filename) // ".part" // partition_index
         plotfile_name = trim(plotfile_name) // "_" // plot_index // ":mesh"
         meshnames(i)  = trim(plotfile_name)
         lmeshnames(i) = lfname

         meshtypes(i) = DB_UCDMESH

         ! ! variable information
         vartypes(i) = DB_UCDVAR
         rhonames(i) = trim(plot_filename) // ".part" // partition_index // "_" // plot_index // ":rho"
         lrhoames(i) = len_trim(rhonames(i))

      end do

      ! write the multi-mesh object
      err = dbputmmesh(dbfile, "mesh", 4, nmesh, meshnames, lmeshnames, meshtypes, &
           DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! write out the  information
      oldlen = dbget2dstrlen()
      err = dbset2dstrlen(lfname-1)

      ! put the multi-mesh variable information
      do i = 1, mesh%numPartitions
         write(unit=partition_index, fmt='(i3.3)') i-1
         plotfile_name = trim(plot_filename) // ".part" // partition_index
         plotfile_name = trim(plotfile_name) // "_" // plot_index

         vartypes(i) = DB_UCDVAR

         rhonames(i) = trim(plotfile_name) // ":rho"
         lrhoames(i) = len_trim(rhonames(i))
         
         rhox(i)  = trim(plotfile_name) // ":rhox"
         lrhox(i) = len_trim(rhox(i))

         rhoy(i)  = trim(plotfile_name) // ":rhoy"
         lrhoy(i) = len_trim(rhoy(i))

         rhoz(i)  = trim(plotfile_name) // ":rhoz"
         lrhoz(i) = len_trim(rhoz(i))

         Mx(i)  = trim(plotfile_name) // ":Mx"
         lMx(i) = len_trim(Mx(i))

         My(i)  = trim(plotfile_name) // ":My"
         lMy(i) = len_trim(My(i))

#ifdef DIM3
         Mz(i)  = trim(plotfile_name) // ":Mz"
         lMz(i) = len_trim(Mz(i))
#endif

         E(i)  = trim(plotfile_name) // ":E"
         lE(i) = len_trim(E(i))

         ! gradients
         Mx_x(i)  = trim(plotfile_name) // ":Mx_x"; lMx_x(i) = len_trim(Mx_x(i))
         Mx_y(i)  = trim(plotfile_name) // ":Mx_y"; lMx_y(i) = len_trim(Mx_y(i))

         My_x(i)  = trim(plotfile_name) // ":My_x"; lMy_x(i) = len_trim(My_x(i))
         My_y(i)  = trim(plotfile_name) // ":My_y"; lMy_y(i) = len_trim(My_y(i))

         E_x(i) = trim(plotfile_name) // ":E_x"; lE_x = len_trim(E_x(i))
         E_y(i) = trim(plotfile_name) // ":E_y"; lE_y = len_trim(E_y(i))

#ifdef DIM3
         Mz_x(i)  = trim(plotfile_name) // ":Mz_x"; lMz_x(i) = len_trim(Mz_x(i))
         Mz_y(i)  = trim(plotfile_name) // ":Mz_y"; lMz_y(i) = len_trim(Mz_y(i))
         Mz_z(i)  = trim(plotfile_name) // ":Mz_z"; lMz_z(i) = len_trim(Mz_z(i))

         Mx_z(i)  = trim(plotfile_name) // ":Mx_z"; lMx_z(i) = len_trim(Mx_z(i))
         My_z(i)  = trim(plotfile_name) // ":My_z"; lMy_z(i) = len_trim(My_z(i))

         E_z(i) = trim(plotfile_name) // ":E_z"; lE_z = len_trim(E_z(i))
#endif

         ! processor id
         Proc(i) = trim(plotfile_name) // ":Proc"; lProc(i) = len_trim(Proc(i))

      end do
      oldlen = dbget2dstrlen()

      ! multivar-rho
      err = dbset2dstrlen(lrhoames(1))
      err = dbputmvar(dbfile, "rho", 3, mesh%numPartitions, rhonames, lrhoames, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar-rhox
      err = dbset2dstrlen(lrhox(1))
      err = dbputmvar(dbfile, "rhox", 4, mesh%numPartitions, rhox, lrhox, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar-rhoy
      err = dbset2dstrlen(lrhoy(1))
      err = dbputmvar(dbfile, "rhoy", 4, mesh%numPartitions, rhoy, lrhoy, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

#ifdef DIM3
      ! multivar-rhoz
      err = dbset2dstrlen(lrhoz(1))
      err = dbputmvar(dbfile, "rhoz", 4, mesh%numPartitions, rhoz, lrhoz, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)
#endif

      ! multivar-Mx
      err = dbset2dstrlen(lMx(1))
      err = dbputmvar(dbfile, "Mx", 2, mesh%numPartitions, Mx, lMx, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar-My
      err = dbset2dstrlen(lMy(1))
      err = dbputmvar(dbfile, "My", 2, mesh%numPartitions, My, lMy, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

#ifdef DIM3
      ! multivar-Mz
      err = dbset2dstrlen(lMz(1))
      err = dbputmvar(dbfile, "Mz", 2, mesh%numPartitions, Mz, lMz, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)
#endif

      ! multivar-E
      err = dbset2dstrlen(lE(1))
      err = dbputmvar(dbfile, "E", 1, mesh%numPartitions, E, lE, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! multivar Mx_x
      err = dbset2dstrlen(lMx_x(1))
      err = dbputmvar(dbfile, "Mx_x", 4, mesh%numPartitions, Mx_x, lMx_x, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar Mx_y
      err = dbset2dstrlen(lMx_y(1))
      err = dbputmvar(dbfile, "Mx_y", 4, mesh%numPartitions, Mx_y, lMx_y, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar My_x
      err = dbset2dstrlen(lMy_x(1))
      err = dbputmvar(dbfile, "My_x", 4, mesh%numPartitions, My_x, lMy_x, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar My_y
      err = dbset2dstrlen(lMy_y(1))
      err = dbputmvar(dbfile, "My_y", 4, mesh%numPartitions, My_y, lMy_y, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar E_x
      err = dbset2dstrlen(lE_x(1))
      err = dbputmvar(dbfile, "E_x", 3, mesh%numPartitions, E_x, lE_x, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar E_y
      err = dbset2dstrlen(lE_y(1))
      err = dbputmvar(dbfile, "E_y", 3, mesh%numPartitions, E_y, lE_y, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

#ifdef DIM3
      ! multivar Mx_z
      err = dbset2dstrlen(lMx_z(1))
      err = dbputmvar(dbfile, "Mx_z", 4, mesh%numPartitions, Mx_z, lMx_z, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar My_z
      err = dbset2dstrlen(lMy_z(1))
      err = dbputmvar(dbfile, "My_z", 4, mesh%numPartitions, My_z, lMy_z, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar Mz_x
      err = dbset2dstrlen(lMz_x(1))
      err = dbputmvar(dbfile, "Mz_x", 4, mesh%numPartitions, Mz_x, lMz_x, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar Mz_y
      err = dbset2dstrlen(lMz_y(1))
      err = dbputmvar(dbfile, "Mz_y", 4, mesh%numPartitions, Mz_y, lMz_y, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar Mz_z
      err = dbset2dstrlen(lMz_z(1))
      err = dbputmvar(dbfile, "Mz_z", 4, mesh%numPartitions, Mz_z, lMz_z, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      ! multivar E_z
      err = dbset2dstrlen(lE_z(1))
      err = dbputmvar(dbfile, "E_z", 3, mesh%numPartitions, E_z, lE_z, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

#endif

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! AUxillary data
      ! multivar Proc
      err = dbset2dstrlen(lProc(1))
      err = dbputmvar(dbfile, "Proc", 4, mesh%numPartitions, Proc, lProc, &
                      vartypes, DB_F77NULL, ierr)
      err = dbset2dstrlen(oldlen)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! close the silo file
      err = dbclose(dbfile)      

    end subroutine write_master

    subroutine write_visit_file(filename, mesh, data, step, time)

      type(mesh2d), intent(inout)                :: mesh
      type(FieldData_t), intent(inout)           :: data
      character(len=256),  intent(in)            :: filename
      real(c_double),      intent(in)            :: time
      integer(c_int),      intent(in)            :: step

      ! locals
      integer, allocatable :: shapecounts(:), shapesize(:), nodelist(:), shapetypes(:)
      integer              :: nshapetypes, nshapecounts
      integer              :: nnodes, nzones, ndims
      integer              :: dbfile, ierr, err, i, j

      integer(c_int) :: hi_offset(1), lo_offset(1)
      integer(c_int) :: vertex

      real(c_double), allocatable :: x(:), y(:), z(:)

      real(c_double), allocatable :: del(:), del1(:), del2(:), del21(:), del22(:), del3(:)
      real(c_double), allocatable :: P(:)
      real(c_double), allocatable :: alphag(:), alphal(:)
      real(c_double), allocatable :: arhog(:), arhol(:)
      real(c_double), allocatable :: rhog(:), rhol(:)
      real(c_double), allocatable :: eg(:), el(:)
      real(c_double), allocatable :: ug(:), vg(:)
      real(c_double), allocatable :: ul(:), vl(:)

      real(c_double), allocatable :: ug_x(:), ul_x(:), vg_x(:), vl_x(:)
      real(c_double), allocatable :: egx(:), elx(:)
      real(c_double), allocatable :: del_x(:), del1_x(:), del2_x(:), del3_x(:)
      real(c_double), allocatable :: del21_x(:), del22_x(:)
      real(c_double), allocatable :: Px(:), rhog_x(:), rhol_x(:)
      real(c_double), allocatable :: arhog_x(:), arhol_x(:)

      integer :: counter, optlistid, noptions
      integer :: Nelements
      integer :: E_comp

      E_comp = 4
#ifdef DIM3
      E_comp = 5
#endif

      ! plot only local elements till i figure out how to make the
      ! ghost zones thingie work in visit
      Nelements = mesh%Nelements_local
      Nnodes    = mesh%Nlocal_nodes

      ! allocate variables
      allocate(del(Nelements), del1(Nelements), del2(Nelements), del3(Nelements))
      allocate(del21(Nelements), del22(Nelements))
      allocate(P(Nelements))
      allocate(alphag(Nelements), alphal(Nelements))
      allocate(arhog(Nelements), arhol(Nelements))
      allocate(rhog(Nelements), rhol(Nelements))
      allocate(eg(Nelements), el(Nelements))
      allocate(ug(Nelements), vg(Nelements))
      allocate(ul(Nelements), vl(Nelements))

      allocate(ug_x(Nelements), vg_x(Nelements))
      allocate(ul_x(Nelements), vl_x(Nelements))
      allocate(egx(Nelements), elx(Nelements))
      allocate(del_x(Nelements), del1_x(Nelements), del2_x(Nelements), del3_x(Nelements))
      allocate(del21_x(Nelements), del22_x(Nelements))
      allocate(Px(Nelements))
      allocate(rhog_x(Nelements), rhol_x(Nelements))
      allocate(arhog_x(Nelements), arhol_x(Nelements))

      ! create the visit file
      ierr = dbcreate(trim(filename), len_trim(filename), DB_CLOBBER, DB_LOCAL,&
                      "None", 4, DB_HDF5, dbfile)
      
      if(dbfile.eq.-1) then
         write (*,*) "Could not create Silo file!\n"
      endif

      ! create the option list and add the cycle and time
      noptions = 2
      if (mesh%numPartitions .gt. 1) then
         noptions = noptions + 2
      end if
      ierr = dbmkoptlist(noptions, optlistid)
      ierr = dbaddiopt(optlistid, DBOPT_CYCLE, step)
      ierr = dbadddopt(optlistid, DBOPT_DTIME, time)

      if (mesh%numPartitions .gt. 1) then
         lo_offset(1) = 1
         hi_offset(1) = mesh%Nelements_local
         ierr = dbadddopt(optlistid, DBOPT_LO_OFFSET, lo_offset)
         ierr = dbadddopt(optlistid, DBOPT_HI_OFFSET, hi_offset)
      end if

      ! get the node coordinates
      allocate( x(Nnodes), y(Nnodes) )
      if (mesh%DIM .eq. 3) then
         allocate( z(Nnodes) )
      end if

      do i = 1, Nnodes
         vertex = mesh%local_nodes(i)

         x(i) = mesh%points(vertex,1)
         y(i) = mesh%points(vertex,2)
         if ( mesh%DIM .eq. 3 ) then
            z(i) = mesh%points(vertex, 3)
         end if
      end do

      ! get the mesh connectivity
      allocate ( nodelist(Nelements*mesh%elem_num_verts) )
      
      counter = 1
      do i = 1, Nelements
         do j = 1, mesh%elem_num_verts
            nodelist(counter) = mesh%element2vertices(i, j); counter = counter + 1
         end do
      end do

      ! we have only single type meshes for now
      allocate( shapesize(1), shapecounts(1) )

      shapesize(1) =   mesh%elem_num_verts
      shapecounts(1) = Nelements

      nshapetypes = 1
      allocate(shapetypes(nshapetypes))
      
      nnodes = Nnodes
      nzones = Nelements
      ndims  = mesh%DIM

      ! set the visit element shape types
      if ( MESH_ELEM_TYPE .eq. TRIANGLES ) then
         shapetypes(1) = DB_ZONETYPE_TRIANGLE

      else if (MESH_ELEM_TYPE .eq. QUADS) then
         shapetypes(1) = DB_ZONETYPE_QUAD

      else if (MESH_ELEM_TYPE .eq. HEXS) then
         shapetypes(1) = DB_ZONETYPE_HEX

      else if ( MESH_ELEM_TYPE .eq. TETS ) then
         shapetypes(1) = DB_ZONETYPE_TET
      end if

      ! suppress annoying deprecation messages
      !err = DBSetDepWarn(0)

      ! write out connectivity information
      !err = dbputzl(dbfile, "zonelist", 8, Nelements, ndims, nodelist,&
      !             Nelements*mesh%elem_num_faces, 1, shapesize, shapecounts, nshapetypes, ierr)

      err = dbputzl2(dbfile, "zonelist", 8, Nelements, ndims, nodelist, Nelements*mesh%elem_num_verts, &
                     1, 0, 0, shapetypes, shapesize, shapecounts, nshapetypes, optlistid, ierr)
           
      ug = data%uavg(2, 1:Nelements)/data%uavg(1, 1:Nelements)
      vg = data%uavg(3, 1:Nelements)/data%uavg(1, 1:Nelements)
      eg = data%uavg(4, 1:Nelements)/data%uavg(1, 1:Nelements) - HALF*(ug**2 + vg**2)
      
      ul = data%uavg(6, 1:Nelements)/data%uavg(5, 1:Nelements)
      vl = data%uavg(7, 1:Nelements)/data%uavg(5, 1:Nelements)
      el = data%uavg(8, 1:Nelements)/data%uavg(5, 1:Nelements) - HALF*(ul**2 + vl**2)

      arhog = data%uavg(1, 1:Nelements)
      arhol = data%uavg(5, 1:Nelements)

      del1 = (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)**2
      del21 = arhol*el*(gamma_liq-ONE)
      del22 = arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq
      del2  = TWO * del21*del22
      del3  = (arhol*el*(gamma_liq-ONE))**2

      del = del1 + del2 + del3
     
      ! P = HALF*( arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
      !            arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
      !            sqrt( (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)**2 + &
      !            TWO*   arhol*el*(gamma_liq-ONE) * &
      !            (  arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq ) +&
      !            ( (arhol*el*(gamma_liq-ONE))**2 ) ) )

      P = HALF*( arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                 arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + sqrt(del) )

      rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*(eg - q_gas) )
      rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*(el - q_liq) )
      
      alphag = arhog/rhog
      alphal = ONE - alphag

      arhog_x = data%uxavg(1,1:Nelements)
      arhol_x = data%uxavg(5,1:Nelements)

      ug_x = ONE/arhog*(data%uxavg(2, 1:Nelements) - ug*arhog_x)
      vg_x = ONE/arhog*(data%uxavg(3, 1:Nelements) - vg*arhog_x)

      ul_x = ONE/arhol*(data%uxavg(6, 1:Nelements) - ul*arhol_x)
      vl_x = ONE/arhol*(data%uxavg(7, 1:Nelements) - vl*arhol_x)

      egx  = ONE/arhog*(data%uxavg(4,1:Nelements) - data%uavg(4, 1:Nelements)/arhog*arhog_x)
      elx  = ONE/arhol*(data%uxavg(8,1:Nelements) - data%uavg(8, 1:nelements)/arhol*arhol_x)

      egx = egx - (ug*ug_x + vg*vg_x)
      elx = elx - (ul*ul_x + vl*vl_x)

      del1_x = TWO*sqrt(del1)*( (gamma_gas-ONE)*(eg*arhog_x + arhog*egx) )
      del3_x = TWO*sqrt(del3)*( (gamma_liq-ONE)*(el*arhol_x + arhol*elx) )

      del22_x = (gamma_gas-ONE)*(arhog_x*eg + egx*arhog)
      del21_x = (gamma_liq-ONE)*(arhol_x*el + elx*arhol)
      del2_x  = TWO*(del21*del22_x + del21_x*del22)

      del_x = del1_x + del2_x + del3_x

      Px = HALF*( (gamma_gas-ONE)*(arhog_x*eg + egx*arhog) + &
                  (gamma_liq-ONE)*(arhol_x*el + elx*arhol) + HALF*del_x/sqrt(del) )

      rhog_x = ONE/(gamma_gas-ONE)*(Px/eg - (P + gamma_gas*pinf_gas)/(eg**2) * egx)
      rhol_x = ONE/(gamma_liq-ONE)*(Px/el - (P + gamma_liq*pinf_liq)/(el**2) * elx)

      ! write out the unstructured mesh
      if (mesh%DIM .eq. 2) then
         err = dbputum(dbfile, "mesh", 4, ndims, x, y, DB_F77NULL, &
              "x", 1, "y", 1, DB_F77NULL, 0, DB_DOUBLE, nnodes, &
              nzones, "zonelist", 8, DB_F77NULL, 0, DB_F77NULL, ierr)
      else
         err = dbputum(dbfile, "mesh", 4, ndims, x, y, z, &
              "x", 1, "y", 1, "z", 1, DB_DOUBLE, nnodes, &
              nzones, "zonelist", 8, DB_F77NULL, 0, DB_F77NULL, ierr)
      end if

      ! write out state variables
      err = dbputuv1(dbfile, "rhog", 4, "mesh", 4, rhog, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "aMxg", 4, "mesh", 4, data%uavg(2,1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "aMxl", 4, "mesh", 4, data%uavg(6,1:Nelements), &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "arhog", 5, "mesh", 4, data%uavg(1,1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "arhol", 5, "mesh", 4, data%uavg(5,1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "rhol", 4, "mesh", 4, rhol, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "P", 1, "mesh", 4, P, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "velx_gas", 8, "mesh", 4, ug, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "vely_gas", 8, "mesh", 4, vg, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "velx_liq", 8, "mesh", 4, ul, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "vely_liq", 8, "mesh", 4, vl, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "mixture", 7, "mesh", 4, alphag*rhog+alphal*rhol, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "alphag", 6, "mesh", 4, alphag, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "ax", 2, "mesh", 4, data%ax_avg(1, 1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "ay", 2, "mesh", 4, data%ay_avg(1, 1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "arhog_x", 7, "mesh", 4, arhog_x, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "arhol_x", 7, "mesh", 4, arhol_x, &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "aMxg_x", 6, "mesh", 4, data%uxavg(2, 1:Nelements), &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "aMxl_x", 6, "mesh", 4, data%uxavg(6, 1:Nelements), &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "Px", 2, "mesh", 4, Px, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "ug_x", 4, "mesh", 4, ug_x, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "ul_x", 4, "mesh", 4, ul_x, &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "vg_x", 4, "mesh", 4, vg_x, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "vl_x", 4, "mesh", 4, vl_x, &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "rhog_x", 6, "mesh", 4, rhog_x, &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      ! err = dbputuv1(dbfile, "rhol_x", 6, "mesh", 4, rhol_x, &
      !      Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!       if (plot_gradients) then
!          err = dbputuv1(dbfile, "rhox", 4, "mesh", 4, data%uxavg(1,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "rhoy", 4, "mesh", 4, data%uyavg(1,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "rhoz", 4, "mesh", 4, data%uzavg(1,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "Mx_x", 4, "mesh", 4, data%uxavg(2,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "Mx_y", 4, "mesh", 4, data%uyavg(2,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "Mx_z", 4, "mesh", 4, data%uzavg(2,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "My_x", 4, "mesh", 4, data%uxavg(3,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "My_y", 4, "mesh", 4, data%uyavg(3,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "My_z", 4, "mesh", 4, data%uzavg(3,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "E_x", 3, "mesh", 4, data%uxavg(E_comp,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "E_y", 3, "mesh", 4, data%uyavg(E_comp,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "E_z", 3, "mesh", 4, data%uzavg(E_comp,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

! #ifdef DIM3
!          err = dbputuv1(dbfile, "Mz_x", 4, "mesh", 4, data%uxavg(4,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
         
!          err = dbputuv1(dbfile, "Mz_y", 4, "mesh", 4, data%uyavg(4,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

!          err = dbputuv1(dbfile, "Mz_z", 4, "mesh", 4, data%uzavg(4,1:Nelements), &
!               Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)
! #endif

!       end if

      err = dbputuv1(dbfile, "type", 4, "mesh", 4, mesh%elem_type(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_INT, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "bcmap", 5, "mesh", 4, mesh%bc_map(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_INT, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "tci", 3, "mesh", 4, data%tci(1,1,1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "Shock_sensor", 12, "mesh", 4, data%shock_sensor(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "avisc", 5, "mesh", 4, data%avisc(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "Phi", 3, "mesh", 4, data%alpha(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_DOUBLE, DB_ZONECENT, DB_F77NULL, ierr)

      err = dbputuv1(dbfile, "Proc", 4, "mesh", 4, mesh%Proc(1:Nelements), &
           Nelements, DB_F77NULL, 0, DB_INT, DB_ZONECENT, DB_F77NULL, ierr)
      
      ! free the option list
      err = dbfreeoptlist(optlistid)
      
      ierr = dbclose(dbfile)

      ! free memory
      deallocate(x)
      deallocate(y)

      if (mesh%DIM .eq. 3) then
         deallocate(z)
      end if
      
      deallocate(shapesize)
      deallocate(shapecounts)
      deallocate(nodelist)
      deallocate(shapetypes)

      deallocate(P)
      deallocate(del, del1, del2, del3, del21, del22)

      deallocate(alphag)
      deallocate(alphal)
      deallocate(arhog)
      deallocate(arhol)
      deallocate(rhog)
      deallocate(rhol)
      deallocate(eg)
      deallocate(el)
      deallocate(ug)
      deallocate(ul)
      deallocate(vg)
      deallocate(vl)

      deallocate(ug_x, vg_x)
      deallocate(ul_x, vl_x)
      deallocate(egx, elx)
      deallocate(del_x, del1_x, del2_x, del3_x)
      deallocate(del21_x, del22_x)
      deallocate(Px)
      deallocate(rhog_x, rhol_x)
      deallocate(arhog_x, arhol_x)
      
    end subroutine write_visit_file

  end module visit_writer_module
