module advance_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use system_module
  use mesh_module
  use linked_list_module
  use fieldvar_module
  use input_module
  
  use parallel_module

  implicit none
  
contains

  subroutine advance_timestep(system, mesh, data, dt, time, istep, linked_list, mpi_data, &
       myRank, numProcs)
    type(NavierStokesSystem), intent(inout) :: system
    type(mesh2d),             intent(inout) :: mesh
    type(FieldData_t),        intent(inout) :: data
    type(mpi_data_t),         intent(inout) :: mpi_data
    type(linked_list_data),   intent(inout) :: linked_list
    real(c_double),           intent(in   ) :: dt, time
    integer(c_int),           intent(in   ) :: istep, myRank, numProcs

    ! locals
    integer(c_int)      :: index

    !mesh = system%mesh
    
    !$OMP PARALLEL DO PRIVATE(index)
    do index = 1, mesh%Nelements
       data%u0(:,:,index) = data%u(:,:,index)
    end do
    !$OMP END PARALLEL DO

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call evaluate_rhs(system, mesh, data, time, istep, linked_list, mpi_data, myRank, numProcs)
    !$omp parallel do private(index)
    do index = 1, mesh%Nelements_local
       data%u(:, :, index) = data%u0(:, :, index) + &
            dt*(data%k1(:, :, index) - data%HH(:, :, index) - data%II(:, :, index))
    end do
    !$omp end parallel do
    if (relaxation) then
       call relax(mesh, data, data%u, data%ui)
    end if

    call evaluate_rhs(system, mesh, data, time, istep, linked_list, mpi_data, myRank, numProcs)
    !$omp parallel do private(index)
    do index = 1, mesh%Nelements_local
       data%u(:, :, index) = THREE4TH*data%u0(:, :, index) + FOURTH*(data%u(:, :, index) + &
            dt*(data%k1(:, :, index) - data%HH(:, :, index) - data%II(:, :, index)))
    end do
    !$omp end parallel do
    if (relaxation) then
       call relax(mesh, data, data%u, data%ui)
    end if

    call evaluate_rhs(system, mesh, data, time, istep, linked_list, mpi_data, myRank, numProcs)
    !$omp parallel do private(index)
    do index = 1, mesh%Nelements_local
       data%u(:, :, index) = THIRD*data%u0(:, :, index) + TWO3RD*(data%u(:, :, index) + &
            dt*(data%k1(:, :, index) - data%HH(:, :, index) - data%II(:, :, index)))
    end do
    !$omp end parallel do
    if (relaxation) then
       call relax(mesh, data, data%u, data%ui)
    end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  end subroutine advance_timestep

end module advance_module
