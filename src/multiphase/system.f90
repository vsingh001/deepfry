! Module for various systems (Euler, Navier Stokes)
module system_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use fr_module
  use linked_list_module
  use limiter_module
  use flux_module

  use df_bc_module
  use userbc_module

  use parallel_module
  use timings_module

  use fieldvar_module

#ifdef CUDA  
  use gpu_module
#endif

#ifdef BOXLIB
  use parallel
  use omp_module
  use df_boxlib_interface_module
#else
  use omp_lib
  use parallel_module
#endif

  implicit none

  type BaseSystem
  end type BaseSystem

  type, EXTENDS(BaseSystem) :: NavierStokesSystem
     ! constants defining the system
     real(c_double) :: gamma, mu, Cp, Cv, Pr, Rs

     ! mesh
     !type(mesh2d), pointer :: mesh
  end type NavierStokesSystem

  contains
    subroutine initialize_system(system, gamma, mu, Cp, Pr)
      type(NavierStokesSystem), intent(inout) :: system
      real(c_double), optional, intent(in)    :: gamma, mu, Cp, Pr

      ! locals
      real(c_double) :: igamma, imu, iCp, iPr

      ! initialize constants
      igamma = 1.4_c_double  ; if (present(gamma)) igamma = gamma
      imu    = ZERO          ; if (present(mu))    imu    = mu
      iCp    = 1005_c_double ; if (present(Cp))    iCp    = Cp
      iPr    = 0.72_c_double ; if (present(Pr))    iPr    = Pr

      system%gamma = igamma
      system%mu    = imu
      system%Cp    = iCp
      system%Pr    = iPr
      system%Cv    = system%Cp/system%gamma
      system%Rs    = system%Cp-system%Cv

    end subroutine initialize_system

    subroutine evaluate_rhs(system, mesh, data, time, istep, &
         linked_list, mpi_data, myRank, numProcs)

      use input_module, only: limiter_type, shock_sensor_type, initial_limit_nsteps, &
                              use_shock_sensor_as_marker

      integer(c_int),           intent(in   ) :: istep, myRank, numProcs
      real(c_double),           intent(in   ) :: time
      type(NavierStokesSystem), intent(inout) :: system
      type(mesh2d),             intent(inout) :: mesh
      type(FieldData_t),        intent(inout) :: data
      type(linked_list_data),   intent(inout) :: linked_list
      type(mpi_data_t),         intent(inout) :: mpi_data

      real(c_double) :: t_start, t_end
      real(c_double) :: t1, t2, t3
      logical :: initial_limiting, ilimit, ishock_sensor

      initial_limiting = .false.

      !$ t_start = omp_get_wtime()

      ! copy mapped elements !FIXME: To figure out the periodic
      ! mapping for 3D & parallel
      !call copy_mapped_elements(system%mesh)

      ! Exchange updated solution values for the ghost-remote cells.
      if (mesh%numPartitions .gt. 1) then
         call exchange_data(mesh%Np, mesh%Nvar, mesh, data%u, USOL_TAG, &
              mpi_data%sol_send, mpi_data%sol_recv, myRank, numProcs, MPI_DOUBLE)
      end if

      ! Interpolate the solution to the flux points. This is a local
      ! operation and is done for all elements (local + ghost-remote).
      !$ t1 = omp_get_wtime()
      call interpolate_to_flux_points(mesh%Np, mesh%Nflux, mesh%Nvar, mesh%Nelements, &
           mesh%Lf, data%u, data%uflux)
      !$ t2 = omp_get_wtime()
      interpolate_at_face_time = t2-t1

      ! check if limiting needs to be done
      ilimit = (limiter_type .gt. 0) .or. (istep .le. initial_limit_nsteps)

      if (ilimit) then
         
         ! Compute the cell averages. The cell averages play an
         ! important role in ensuring positivity of the schemes.
         call compute_cell_averages(mesh%Np, mesh%Nvar, mesh%Nelements, &
              mesh%w, mesh%reference_area, data%u, data%uavg)

         ! limit the solution and flux point values. Note that the
         ! limiting process is typically determined by checking the
         ! values in a neighboring region of the element
         ! (STj/SVj). This is computed within the limiter and mpi
         ! exchange is called to share the value.
         call limit(mesh, data, time, linked_list, mpi_data, initial_limiting)

         ! Exchange ghost solution and flux point values which may
         ! have been limited
         if (mesh%numPartitions .gt. 1) then
            call exchange_data(mesh%Np, mesh%Nvar, mesh, data%u, USOL_TAG, &
                 mpi_data%sol_send, mpi_data%sol_recv, myRank, numProcs, MPI_DOUBLE)

            call exchange_data(mesh%Nflux, mesh%Nvar, mesh, data%uflux, UFLX_TAG, &
                 mpi_data%flux_send, mpi_data%flux_recv, myRank, numProcs, MPI_DOUBLE)

         end if

         ! ensure we're working with physically meaningful values
         if (debug) then
            call checkbounds(mesh, data)
         end if
      end if

      ! compute the void fraction at the flux points
      call set_void_fraction_at_flux_points(mesh, data%uflux, data%alphag_flux)

#ifdef CUDA
      ! write solution vector to the GPU and take the gradient
      !$ t1 = omp_get_wtime()
      call gpu_ugrad(mesh%device_data, data%u)
      !$ t2 = omp_get_wtime()
      gpu_ugrad_write_time = t2-t1
#endif

      ! Ucommon
      !$ t1 = omp_get_wtime()
      call get_common_solution_values(mesh, data%uflux, data%ucommon)
      !$ t3 = omp_get_wtime()
      call get_boundary_common_solution_values(mesh, data)
      call get_userbc_common_solution_values(  mesh, data, time)

#ifdef BOXLIB
      call get_BoxLib_interface_common_solution_values(mesh, data)
#endif

      !$ t2 = omp_get_wtime()
      get_ucommon_time      = t2 - t1
      boundary_ucommon_time = t2 - t3

      ! Exchange ghost common solution values
      if (mesh%numPartitions .gt. 1) then
         call exchange_data(mesh%Nflux, mesh%Nvar, mesh, data%ucommon, &
              UCOMMON_TAG, &
              mpi_data%flux_send, mpi_data%flux_recv, myRank, numProcs, MPI_DOUBLE)
      end if

#ifdef CUDA
      !$ t1 = omp_get_wtime()
      call read_host_ugrad(mesh%device_data, data%host_ugrad)
      !$ t2 = omp_get_wtime()
      gpu_ugrad_read_time =  t2-t1
#endif

      ! Auxillary variables (gradients)
      !$ t1 = omp_get_wtime()
      call compute_gradients(mesh, data%u, data%uflux, data%ucommon, &
           data%ux, data%uy, data%uz, data%ux_flux, data%uy_flux, data%uz_flux)
      !$ t2 = omp_get_wtime()
      get_gradients_time = t2 - t1

      ! gradients of the void fraction
      call get_void_fraction_gradients(mesh, data%u, data%ux, data%uy, data%uz, &
           data%ax, data%ay)

      ! Shock sensor and artificial viscosity
      ishock_sensor = (shock_sensor_type .gt. 0) .or. (use_shock_sensor_as_marker)
      if (ishock_sensor) then
         if (shock_sensor_type .eq. MORO_NGUYEN_PERAIRE_SENSOR) then
            call Moro_Nguyen_Peraire_divergence_sensor(mesh, data)
         end if
      end if

      ! Discontinuous flux at solution points
      !$ t1 = omp_get_wtime()
      call get_discontinuous_flux_at_solution_points(system, mesh, data)
      !$ t2 = omp_get_wtime()
      discontinuous_flux_sol_time = t2 - t1

#ifdef CUDA
      ! Write the discontinuous flux and take the gradient
      !$ t1 = omp_get_wtime()
      call gpu_Fgrad(mesh%device_data, data%Fdiscont)
      !$ t2 = omp_get_wtime()
      gpu_Fgrad_write_time = t2 - t1
#endif

      ! Discontinuous flux at the flux points
      !$ t1 = omp_get_wtime()
      call get_discontinuous_flux_at_flux_points(system, mesh, data)
      !$ t2 = omp_get_wtime()
      discontinuous_flux_flux_time = t2 - t1

      ! Interaction flux
      !$ t1 = omp_get_wtime()
      call get_interaction_flux(system, mesh, data)
      !$ t3 = omp_get_wtime()
      call get_boundary_interaction_flux(mesh, data, &
           system%mu, system%Cp, system%Cv, system%Pr, system%Rs)

      call get_userbc_interaction_flux(  mesh, data, time, &
           system%mu, system%Cp, system%Cv, system%Pr, system%Rs)
      !$ t2 = omp_get_wtime()
      get_interaction_flux_time = t2 - t1
      boundary_interaction_time = t2 - t3

      ! Dont know if we need this right now
      ! call get_corrected_void_fraction_gradients(mesh, data, &
      !      data%alphag_flux, data%agi_x, data%agi_y, data%ax, data%ay)

      ! interfacial source terms for the two fluid model
      call get_interface_source_terms(mesh, data%u, data%ax, data%ay, data%ui, data%HH, data%II)

#ifdef CUDA
      !$ t1 = omp_get_wtime()
      call read_host_Fgrad(mesh%device_data, &
                           data%host_Fgrad, data%host_Fdflux)
      !$ t2 = omp_get_wtime()
      gpu_Fgrad_read_time = t2 - t1
#endif      
      
      ! Flux divergence
      !$ t1 = omp_get_wtime()
      call compute_flux_divergence(mesh, data, &
           data%Fdiscont, data%Fi, data%Gi, data%Hi, mesh%detJn, data%k1)
      !$ t2 = omp_get_wtime()
      flux_divergence_time = t2 - t1

      !$ t_end = omp_get_wtime()
      rhs_evaluation_time = t_end - t_start
    
    end subroutine evaluate_rhs

  subroutine get_discontinuous_flux_at_solution_points(system, mesh, data)
    type(NavierStokesSystem), intent(inout) :: system
    type(mesh2d),             intent(inout) :: mesh
    type(FieldData_t),        intent(inout) :: data

    ! locals
    integer(c_int) :: elem_index, var

    real(c_double) :: Fd(mesh%Np, mesh%Nvar), Fv(mesh%Np, mesh%Nvar)
    real(c_double) :: Gd(mesh%Np, mesh%Nvar), Gv(mesh%Np, mesh%Nvar)
    real(c_double) :: Hd(mesh%Np, mesh%Nvar), Hv(mesh%Np, mesh%Nvar)

    real(c_double) :: xr(mesh%Np), xs(mesh%Np), xt(mesh%Np)
    real(c_double) :: yr(mesh%Np), ys(mesh%Np), yt(mesh%Np)
    real(c_double) :: zr(mesh%Np), zs(mesh%Np), zt(mesh%Np)
    
    real(c_double) :: rhog(mesh%Np), Mxg(mesh%Np), Myg(mesh%Np), Mzg(mesh%Np), Eg(mesh%Np)
    real(c_double) :: rhol(mesh%Np), Mxl(mesh%Np), Myl(mesh%Np), Mzl(mesh%Np), El(mesh%Np)

    Hd = ZERO
    Hv = ZERO
    
    !$omp parallel do private(var, elem_index, &
    !$omp                     rhog, Mxg, Myg, Mzg, Eg, &
    !$omp                     rhol, Mxl, Myl, Mzl, El, &
    !$omp                     xr, xs, xt, yr, ys, yt, zr, zs, zt, &
    !$omp                     Fd, Gd, Hd, Fv, Gv, Hv) & 
    !$omp schedule(static)
    do elem_index = 1, mesh%Nelements
       rhog = data%u(:, 1, elem_index); rhol = data%u(:, 5, elem_index)
       Mxg  = data%u(:, 2, elem_index); Mxl  = data%u(:, 6, elem_index)
       Myg  = data%u(:, 3, elem_index); Myl  = data%u(:, 7, elem_index)
       Eg   = data%u(:, 4, elem_index); El   = data%u(:, 8, elem_index)

       Mzg  = ZERO
       Mzl  = ZERO

#ifdef DIM3
       Mzg = data%u(:, 4, elem_index); Mzl = data%u(:, 9,  elem_index)
       Eg  = data%u(:, 5, elem_index); El  = data%u(:, 10, elem_index)
#endif

       call evaluate_flux(mesh%Np, mesh%Nvar, &
            rhog, Mxg, Myg, Mzg, Eg, &
            rhol, Mxl, Myl, Mzl, El, &
            Fd, Gd, Hd, &
            Fv, Gv, Hv)

       ! element transformation factors
       xr = mesh%xr(:, elem_index); xs = mesh%xs(:, elem_index); xt = mesh%xt(:, elem_index)
       yr = mesh%yr(:, elem_index); ys = mesh%ys(:, elem_index); yt = mesh%yt(:, elem_index)
       zr = mesh%zr(:, elem_index); zs = mesh%zs(:, elem_index); zt = mesh%zt(:, elem_index)

       ! store the TRANSFORMED discontinuous flux at the solution points
       do var = 1, mesh%Nvar
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar+var) = &
               (ys*zt - yt*zs)*(Fd(:, var) + Fv(:, var)) + &
               (xt*zs - xs*zt)*(Gd(:, var) + Gv(:, var)) + &
               (xs*yt - xt*ys)*(Hd(:, var) + Hv(:, var))
          
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar + mesh%Nvar+var) = &
               (yt*zr - yr*zt)*(Fd(:, var) + Fv(:, var)) + &
               (xr*zt - xt*zr)*(Gd(:, var) + Gv(:, var)) + &
               (xt*yr - xr*yt)*(Hd(:, var) + Hv(:, var))
          
#ifdef DIM3
          data%Fdiscont(:, (elem_index-1)*mesh%DIM*mesh%Nvar + 2*mesh%Nvar+var) = &
               (yr*zs - ys*zr)*(Fd(:, var) + Fv(:, var)) + &
               (xs*zr - xr*zs)*(Gd(:, var) + Gv(:, var)) + &
               (xr*ys - xs*yr)*(Hd(:, var) + Hv(:, var))
#endif
          
       end do
       
    end do
    !$omp end parallel do
         
  end subroutine get_discontinuous_flux_at_solution_points

  subroutine get_discontinuous_flux_at_flux_points(system, mesh, data)
      type(NavierStokesSystem), intent(inout) :: system
      type(mesh2d),             intent(inout) :: mesh
      type(FieldData_t),        intent(inout) :: data

      ! locals
      integer(c_int) :: Nflux, Nvar, elem_index

      real(c_double), allocatable :: rhog(:), Mxg(:), Myg(:), Mzg(:), Eg(:)
      real(c_double), allocatable :: rhol(:), Mxl(:), Myl(:), Mzl(:), El(:)

      real(c_double) :: Fd(mesh%Nflux, mesh%Nvar)
      real(c_double) :: Fv(mesh%Nflux, mesh%Nvar)

      real(c_double) :: Gd(mesh%Nflux, mesh%Nvar)
      real(c_double) :: Gv(mesh%Nflux, mesh%Nvar)

      real(c_double) :: Hd(mesh%Nflux, mesh%Nvar)
      real(c_double) :: Hv(mesh%Nflux, mesh%Nvar)

      Nflux = mesh%Nflux
      Nvar  = mesh%Nvar

      allocate( rhog(Nflux), Mxg(Nflux), Myg(Nflux), Mzg(Nflux), Eg(Nflux) )
      allocate( rhol(Nflux), Mxl(Nflux), Myl(Nflux), Mzl(Nflux), El(Nflux) )

      Hd = ZERO
      Hv = ZERO

      !$omp parallel do private(elem_index, &
      !$omp                     rhog, Mxg, Myg, Mzg, Eg, &
      !$omp                     rhol, Mxl, Myl, Mzl, El, &
      !$omp                     Fd, Gd, Hd, Fv, Gv, Hv) &
      !$omp schedule(dynamic,2)
      do elem_index = 1, mesh%nelements
         rhog = data%uflux(:, 1, elem_index); rhol = data%uflux(:, 5, elem_index)
         Mxg  = data%uflux(:, 2, elem_index); Mxl  = data%uflux(:, 6, elem_index)
         Myg  = data%uflux(:, 3, elem_index); Myl  = data%uflux(:, 7, elem_index)
         Eg   = data%uflux(:, 4, elem_index); El   = data%uflux(:, 8, elem_index)

         Mzg  = ZERO
         Mzl  = ZERO

#ifdef DIM3
         Mzg = data%uflux(:, 4, elem_index); Mzl = data%uflux(:, 9, elem_index)
         Eg  = data%uflux(:, 5, elem_index); Egl = data%uflux(:, 10, elem_index)
#endif

         call evaluate_flux(Nflux, Nvar, &
              rhog, Mxg, Myg, Mzg, Eg, &
              rhol, Mxl, Myl, Mzl, El, &
              Fd, Gd, Hd, &
              Fv, Gv, Hv)

         ! store the PHYSICAL flux at the flux points
         data%Fd_flux(:, :, elem_index) = Fd
         data%Gd_flux(:, :, elem_index) = Gd
         data%Hd_flux(:, :, elem_index) = Hd

         data%Fv_flux(:, :, elem_index) = Fv
         data%Gv_flux(:, :, elem_index) = Gv
         data%Hv_flux(:, :, elem_index) = Hv

      end do
      !$omp end parallel do

      deallocate( rhog, Mxg, Myg, Mzg, Eg )
      deallocate( rhol, Mxl, Myl, Mzl, El )

    end subroutine get_discontinuous_flux_at_flux_points

    subroutine get_interaction_flux(system, mesh, data)
      use input_module

      type(NavierStokesSystem), intent(inout) :: system
      type(mesh2d),             intent(inout) :: mesh
      type(FieldData_t),        intent(inout) :: data
      
      ! locals
      integer(c_int) :: P1, Nflux, Nvar
      integer(c_int) :: j, elem_num_faces
      integer(c_int) :: face2elem(2), left, rght
      integer(c_int) :: tmp_index, glb_face_index, left_face_index, rght_face_index
      integer(c_int) :: left_indices(2), rght_indices(2)
      
      real(c_double) :: uflux_left(mesh%P1, mesh%Nvar)
      real(c_double) :: uflux_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: tmpi_left(mesh%P1, mesh%Nvar)
      real(c_double) :: tmpi_rght(mesh%P1, mesh%Nvar)

      real(c_double), allocatable :: tmp_Fi_left(:, :), tmp_Fv_left(:, :)
      real(c_double), allocatable :: tmp_Gi_left(:, :), tmp_Gv_left(:, :)
      real(c_double), allocatable :: tmp_Hi_left(:, :), tmp_Hv_left(:, :)

      real(c_double), allocatable :: tmp_Fi_rght(:, :), tmp_Fv_rght(:, :)
      real(c_double), allocatable :: tmp_Gi_rght(:, :), tmp_Gv_rght(:, :)
      real(c_double), allocatable :: tmp_Hi_rght(:, :), tmp_Hv_rght(:, :)

      integer(c_int) :: sfi_left(mesh%P1)
      integer(c_int) :: sfi_rght(mesh%P1)

      real(c_double) :: Fd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Fd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Fv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Fv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Gd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Gd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Hd_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Hd_rght(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv_left(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv_rght(mesh%P1, mesh%Nvar)

      real(c_double) :: Fi(mesh%P1, mesh%Nvar)
      real(c_double) :: Gi(mesh%P1, mesh%Nvar)
      real(c_double) :: Hi(mesh%P1, mesh%Nvar)

      real(c_double) :: Fv(mesh%P1, mesh%Nvar)
      real(c_double) :: Gv(mesh%P1, mesh%Nvar)
      real(c_double) :: Hv(mesh%P1, mesh%Nvar)

      real(c_double) :: agi_x(mesh%P1)
      real(c_double) :: agi_y(mesh%P1)

      real(c_double) :: wp_left(mesh%P1, 5), wp_rght(mesh%P1, 5)

      real(c_double) :: ilambda, ibeta_viscous, itau

      !real(c_double) :: mu, Cp, Cv, Pr, Rs
      real(c_double) :: Rs
      
      elem_num_faces = mesh%elem_num_faces

      ! mu = system%mu
      ! Cp = system%Cp
      ! Cv = system%Cv
      ! Pr = system%Pr
      Rs = system%Rs

      ilambda       = HALF
      ibeta_viscous = ldg_beta
      itau          = ldg_tau

      left_face_index = -1
      rght_face_index = -1

      P1 = mesh%P1
      Nvar = mesh%Nvar
      Nflux = mesh%Nflux

      allocate(tmp_Fi_left(mesh%P1, mesh%Nvar))
      allocate(tmp_Fv_left(mesh%P1, mesh%Nvar))

      allocate(tmp_Gi_left(mesh%P1, mesh%Nvar))
      allocate(tmp_Gv_left(mesh%P1, mesh%Nvar))

      allocate(tmp_Hi_left(mesh%P1, mesh%Nvar))
      allocate(tmp_Hv_left(mesh%P1, mesh%Nvar))

      allocate(tmp_Fi_rght(mesh%P1, mesh%Nvar))
      allocate(tmp_Fv_rght(mesh%P1, mesh%Nvar))

      allocate(tmp_Gi_rght(mesh%P1, mesh%Nvar))
      allocate(tmp_Gv_rght(mesh%P1, mesh%Nvar))

      allocate(tmp_Hi_rght(mesh%P1, mesh%Nvar))
      allocate(tmp_Hv_rght(mesh%P1, mesh%Nvar))

      !$omp parallel do private(j, tmp_index, glb_face_index, face2elem, left, rght, &
      !$omp                     left_face_index, rght_face_index, left_indices, rght_indices, &
      !$omp                     uflux_left, uflux_rght, &
      !$omp                     wp_left, wp_rght, &
      !$omp                     agi_x, agi_y, &
      !$omp                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
      !$omp                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
      !$omp                     Hd_left, Hd_rght, Hv_left, Hv_rght, &
      !$omp                     tmpi_left, tmpi_rght, &
      !$omp                     tmp_Fi_left, tmp_Fi_rght, tmp_Fv_left, tmp_Fv_rght, &
      !$omp                     tmp_Gi_left, tmp_Gi_rght, tmp_Gv_left, tmp_Gv_rght, &
      !$omp                     tmp_Hi_left, tmp_Hi_rght, tmp_Hv_left, tmp_Hv_rght, &
      !$omp                     sfi_left, sfi_rght, &
      !$omp                     Fi, Gi, Hi, Fv, Gv, Hv) &
      !$omp SCHEDULE(dynamic,2)
      do tmp_index = 1, mesh%Ninterior_faces
         glb_face_index = mesh%interior_faces(tmp_index)
         face2elem = mesh%face2elem(glb_face_index, :); left = face2elem(1); rght = face2elem(2)

         ! get the face indices for each element abutting this face
         left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
         rght_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 2)

         left_indices(1) = mesh%P1*(left_face_index-1)+1
         left_indices(2) = mesh%P1*left_face_index
            
         rght_indices(1) = mesh%P1*(rght_face_index-1)+1
         rght_indices(2) = mesh%P1*rght_face_index
          
         !!!!!!!!!!!!!!!!!!!!! Abutting solution values !!!!!!!!!!!!!!!!!!!!
         tmpi_left = data%uflux(left_indices(1):left_indices(2), :, left)
         tmpi_rght = data%uflux(rght_indices(1):rght_indices(2), :, rght)

         !!!!!!!!!!!!!!!!!!!!! Abutting F-fluxes !!!!!!!!!!!!!!!!!!!!
         tmp_Fi_left = data%Fd_flux(left_indices(1):left_indices(2), :, left)
         tmp_Fv_left = data%Fv_flux(left_indices(1):left_indices(2), :, left)

         tmp_Fi_rght = data%Fd_flux(rght_indices(1):rght_indices(2), :, rght)
         tmp_Fv_rght(1:mesh%P1,1:Nvar) = data%Fv_flux(rght_indices(1):rght_indices(2), :, rght)

         ! !!!!!!!!!!!!!!!!!!!! Abutting G-fluxes !!!!!!!!!!!!!!!!!!!!
         tmp_Gi_left = data%Gd_flux(left_indices(1):left_indices(2), :, left)
         tmp_Gv_left = data%Gv_flux(left_indices(1):left_indices(2), :, left)

         tmp_Gi_rght = data%Gd_flux(rght_indices(1):rght_indices(2), :, rght)
         tmp_Gv_rght = data%Gv_flux(rght_indices(1):rght_indices(2), :, rght)

#ifdef DIM3
         ! !!!!!!!!!!!!!!!!!!!! Abutting H-fluxes !!!!!!!!!!!!!!!!!!!!
         tmp_Hi_left = data%Hd_flux(left_indices(1):left_indices(2), :, left)
         tmp_Hv_left = data%Hv_flux(left_indices(1):left_indices(2), :, left)

         tmp_Hi_rght = data%Hd_flux(rght_indices(1):rght_indices(2), :, rght)
         tmp_Hv_rght = data%Hv_flux(rght_indices(1):rght_indices(2), :, rght)
#endif

         ! get the sorted face indices
         sfi_left = mesh%sfi(:, left_face_index, left)
         sfi_rght = mesh%sfi(:, rght_face_index, rght)

         ! line up the data
         do j = 1, mesh%P1
            uflux_left(j, :) = tmpi_left(sfi_left(j), :)
            uflux_rght(j, :) = tmpi_rght(sfi_rght(j), :)

            Fd_left(j, :) = tmp_Fi_left(sfi_left(j), :)
            Fd_rght(j, :) = tmp_Fi_rght(sfi_rght(j), :)

            Fv_left(j, :) = tmp_Fv_left(sfi_left(j), :)
            Fv_rght(j, :) = tmp_Fv_rght(sfi_rght(j), :)

            Gd_left(j, :) = tmp_Gi_left(sfi_left(j), :)
            Gd_rght(j, :) = tmp_Gi_rght(sfi_rght(j), :)

            Gv_left(j, :) = tmp_Gv_left(sfi_left(j), :)
            Gv_rght(j, :) = tmp_Gv_rght(sfi_rght(j), :)

#ifdef DIM3 
            Hd_left(j, :) = tmp_Hi_left(sfi_left(j), :)
            Hd_rght(j, :) = tmp_Hi_rght(sfi_rght(j), :)

            Hv_left(j, :) = tmp_Hv_left(sfi_left(j), :)
            Hv_rght(j, :) = tmp_Hv_rght(sfi_rght(j), :)
#else
            Hd_left = ZERO; Hd_rght = ZERO
            Hv_left = ZERO; Hv_rght = ZERO
#endif

         end do

         ! Evaluate the interaction flux for this face
         call evaluate_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
              mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
              uflux_left, uflux_rght, &
              Fd_left, Fd_rght, Fv_left, Fv_rght, &
              Gd_left, Gd_rght, Gv_left, Gv_rght, &
              Hd_left, Hd_rght, Hv_left, Hv_rght, &
              Fi, Gi, Hi, &
              Fv, Gv, Hv, &
              agi_x, agi_y, &
              system%gamma, mesh%face_markers(glb_face_index), &
              ilambda, ibeta_viscous, itau, &
              glb_face_index=glb_face_index)

         ! store the PHYSICAL interaction flux for each element abutting this face
         do j = 1, mesh%P1
            ! mesh%Fi(left_indices(1) + (j-1), :, left) = Fi(j, :) + Fv(j, :)
            ! mesh%Gi(left_indices(1) + (j-1), :, left) = Gi(j, :) + Gv(j, :)
            ! mesh%Hi(left_indices(1) + (j-1), :, left) = Hi(j, :) + Hv(j, :)
            
            ! mesh%Fi(rght_indices(2) - (j-1), :, rght) = Fi(j, :) + Fv(j, :)
            ! mesh%Gi(rght_indices(2) - (j-1), :, rght) = Gi(j, :) + Gv(j, :)
            ! mesh%Hi(rght_indices(2) - (j-1), :, rght) = Hi(j, :) + Hv(j, :)

            data%Fi(left_indices(1) + (sfi_left(j)-1), :, left) = Fi(j, :) + Fv(j, :)
            data%Gi(left_indices(1) + (sfi_left(j)-1), :, left) = Gi(j, :) + Gv(j, :)
            data%Hi(left_indices(1) + (sfi_left(j)-1), :, left) = Hi(j, :) + Hv(j, :)
            
            data%Fi(rght_indices(1) + (sfi_rght(j)-1), :, rght) = Fi(j, :) + Fv(j, :)
            data%Gi(rght_indices(1) + (sfi_rght(j)-1), :, rght) = Gi(j, :) + Gv(j, :)
            data%Hi(rght_indices(1) + (sfi_rght(j)-1), :, rght) = Hi(j, :) + Hv(j, :)

            data%agi_x( left_indices(1) + (sfi_left(j)-1), left) = agi_x(j)
            data%agi_y( left_indices(1) + (sfi_left(j)-1), left) = agi_y(j)

         end do

      end do
      !$omp end parallel do

      deallocate(tmp_Fi_left, tmp_Fi_rght)
      deallocate(tmp_Gi_left, tmp_Gi_rght)
      deallocate(tmp_Hi_left, tmp_Hi_rght)

      deallocate(tmp_Fv_left, tmp_Fv_rght)
      deallocate(tmp_Gv_left, tmp_Gv_rght)
      deallocate(tmp_Hv_left, tmp_Hv_rght)

    end subroutine get_interaction_flux   

    subroutine set_void_fraction_at_flux_points(mesh, uflux, alphag_flux)
      use input_module
      type(mesh2d),   intent(in ) :: mesh
      real(c_double), intent(in ) :: uflux(mesh%Nflux, mesh%Nvar, mesh%Nelements)
      real(c_double), intent(out) :: alphag_flux(mesh%Nflux, mesh%Nelements)

      ! locals
      integer(c_int) :: elem_index, j
      real(c_double) :: wp(5)

      !$omp parallel do private(elem_index, j, wp)
      do elem_index = 1, mesh%Nelements
         do j = 1, mesh%Nflux
            call get_primitive(uflux(j, :, elem_index), gamma_gas, gamma_liq, &
                 pinf_gas, pinf_liq, q_gas, q_liq, wp)

            alphag_flux(j, elem_index) = wp(4)
         end do
      end do
      !$omp end parallel do

    end subroutine set_void_fraction_at_flux_points

    subroutine get_void_fraction_gradients(mesh, u, ux, uy, uz, ax, ay)
      use input_module
      type(mesh2d),   intent(in) :: mesh
      real(c_double), intent(in) :: u(mesh%Np,  mesh%Nvar, mesh%Nelements)
      real(c_double), intent(in) :: ux(mesh%Np, mesh%Nvar, mesh%Nelements)
      real(c_double), intent(in) :: uy(mesh%Np, mesh%Nvar, mesh%Nelements)
      real(c_double), intent(in) :: uz(mesh%Np, mesh%Nvar, mesh%Nelements)
      real(c_double), intent(inout) :: ax(mesh%Np, 1, mesh%Nelements)
      real(c_double), intent(inout) :: ay(mesh%Np, 1, mesh%Nelements)

      ! locals
      integer(c_int) :: elem_index, j
      real(c_double) :: arhog, arhol, ug, ul, vg, vl, eg, el, rhog, rhol, P, alphag, alphal
      real(c_double) :: del, del1, del21, del22, del2, del3

      real(c_double) :: u1x, u2x, u3x, u4x, u5x, u6x, u7x, u8x
      real(c_double) :: ug_x, ul_x, vg_x, vl_x
      real(c_double) :: egx, elx
      real(c_double) :: Px, rhog_x, rhol_x

      real(c_double) :: tmpg, tmpl
      real(c_double) :: del_x, del1_x, del21_x, del22_x, del2_x, del3_x
      
      !$omp parallel do private(elem_index, j, arhog, arhol, ug, ul, vg, vl, eg, el, rhog, &
      !$omp                     rhol, P, alphag, alphal, del, del1, del21, del22, del2, del3, &
      !$omp                     u1x, u2x, u3x, u4x, u5x, u6x, u7x, u8x, &
      !$omp                     ug_x, ul_x, vg_x, vl_x, egx, elx, Px, rhog_x, rhol_x, &
      !$omp                     tmpg, tmpl, del_x, del1_x, del21_x, del22_x, del2_x, del3_x)
      do elem_index = 1, mesh%Nelements
         do j = 1, mesh%Np           

            ! state variables
            arhog = u(j, 1, elem_index); arhol = u(j, 5, elem_index)
            
            ug = u(j, 2, elem_index)/arhog; ul = u(j, 6, elem_index)/arhol
            vg = u(j, 3, elem_index)/arhog; vl = u(j, 7, elem_index)/arhol

            eg = u(j, 4, elem_index)/arhog - HALF*(ug*ug + vg*vg)
            el = u(j, 8, elem_index)/arhol - HALF*(ul*ul + vl*vl)

            del1  = (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)

            del21 = arhol*el*(gamma_liq-ONE)
            del22 = arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq
            del2  = TWO * del21*del22

            del3  = (arhol*el*(gamma_liq-ONE))

            del = del1*del1 + del2 + del3*del3

            P = HALF*(arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                      arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                      sqrt(del))

            tmpg = eg - q_gas
            tmpl = el - q_liq

            rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*tmpg )
            rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*tmpl )
            
            alphag = arhog/rhog; alphal = ONE-alphag

            ! X-gradients
            u1x = ux(j, 1, elem_index); u5x = ux(j, 5, elem_index)
            u2x = ux(j, 2, elem_index); u6x = ux(j, 6, elem_index)
            u3x = ux(j, 3, elem_index); u7x = ux(j, 7, elem_index)
            u4x = ux(j, 4, elem_index); u8x = ux(j, 8, elem_index)

            ug_x = ONE/arhog*(u2x - ug*u1x)
            ul_x = ONE/arhol*(u6x - ul*u5x)

            vg_x = ONE/arhog*(u3x - vg*u1x)
            vl_x = ONE/arhol*(u7x - vg*u5x)

            egx  = ONE/arhog*(u4x - u(j, 4, elem_index)/arhog*u1x) - (ug*ug_x + vg*vg_x)
            elx  = ONE/arhol*(u8x - u(j, 8, elem_index)/arhol*u5x) - (ul*ul_x + vl*vl_x)

            del1_x = TWO*del1*( (gamma_gas-ONE)*(eg*u1x + arhog*egx) )
            del3_x = TWO*del3*( (gamma_liq-ONE)*(el*u5x + arhol*elx) )

            del21_x = (gamma_liq-ONE)*(u5x*el + elx*arhol)
            del22_x = (gamma_gas-ONE)*(u1x*eg + egx*arhog)
            del2_x  = TWO*(del21*del22_x + del21_x*del22)

            del_x = del1_x + del2_x + del3_x

            Px = HALF*( (gamma_gas-ONE)*(u1x*eg + egx*arhog) + &
                        (gamma_liq-ONE)*(u5x*el + elx*arhol) + HALF*del_x/sqrt(del) )

            rhog_x = ONE/(gamma_gas-ONE)*(Px/tmpg - (P + gamma_gas*pinf_gas)/(tmpg**2) * egx)
            rhol_x = ONE/(gamma_liq-ONE)*(Px/tmpl - (P + gamma_liq*pinf_liq)/(tmpl**2) * elx)

            ax(j, 1, elem_index) = ONE/rhog*(u1x - arhog/rhog*rhog_x)

            ! Y-gradients
            u1x = uy(j, 1, elem_index); u5x = uy(j, 5, elem_index)
            u2x = uy(j, 2, elem_index); u6x = uy(j, 6, elem_index)
            u3x = uy(j, 3, elem_index); u7x = uy(j, 7, elem_index)
            u4x = uy(j, 4, elem_index); u8x = uy(j, 8, elem_index)

            ug_x = ONE/arhog*(u2x - ug*u1x)
            ul_x = ONE/arhol*(u6x - ul*u5x)

            vg_x = ONE/arhog*(u3x - vg*u1x)
            vl_x = ONE/arhol*(u7x - vg*u5x)

            egx  = ONE/arhog*(u4x - u(j, 4, elem_index)/arhog*u1x) - (ug*ug_x + vg*vg_x)
            elx  = ONE/arhol*(u8x - u(j, 8, elem_index)/arhol*u5x) - (ul*ul_x + vl*vl_x)

            del1_x = TWO*del1*( (gamma_gas-ONE)*(eg*u1x + arhog*egx) )
            del3_x = TWO*del3*( (gamma_liq-ONE)*(el*u5x + arhol*elx) )

            del21_x = (gamma_liq-ONE)*(u5x*el + elx*arhol)
            del22_x = (gamma_gas-ONE)*(u1x*eg + egx*arhog)
            del2_x  = TWO*(del21*del22_x + del21_x*del22)

            del_x = del1_x + del2_x + del3_x

            Px = HALF*( (gamma_gas-ONE)*(u1x*eg + egx*arhog) + &
                        (gamma_liq-ONE)*(u5x*el + elx*arhol) + HALF*del_x/sqrt(del) )

            rhog_x = ONE/(gamma_gas-ONE)*(Px/tmpg - (P + gamma_gas*pinf_gas)/(tmpg**2) * egx)
            rhol_x = ONE/(gamma_liq-ONE)*(Px/tmpl - (P + gamma_liq*pinf_liq)/(tmpl**2) * elx)

            ay(j, 1, elem_index) = ONE/rhog*(u1x - arhog/rhog*rhog_x)

         end do
      end do
      !$omp end parallel do

    end subroutine get_void_fraction_gradients

    subroutine get_corrected_void_fraction_gradients(mesh, data, alphag_flux, agi_x, agi_y, ax, ay)
      type(mesh2d),      intent(in   ) :: mesh
      type(FieldData_t), intent(inout) :: data
      real(c_double),    intent(in   ) :: agi_x(mesh%Nflux, mesh%Nelements)
      real(c_double),    intent(in   ) :: agi_y(mesh%Nflux, mesh%Nelements)
      real(c_double),    intent(in   ) :: alphag_flux(mesh%Nflux, mesh%Nelements)
      real(c_double),    intent(inout) :: ax(mesh%Np, 1, mesh%Nelements)
      real(c_double),    intent(inout) :: ay(mesh%Np, 1, mesh%Nelements)

      ! locals
      integer(c_int) :: elem_index, j
      real(c_double) :: alphag_grad(mesh%DIM*mesh%Np, mesh%DIM)
      real(c_double) :: alphag_corr(mesh%Nflux, mesh%DIM)

      real(c_double) :: xfr(mesh%Nflux), xfs(mesh%Nflux), xft(mesh%Nflux)
      real(c_double) :: yfr(mesh%Nflux), yfs(mesh%Nflux), yft(mesh%Nflux)
      real(c_double) :: zfr(mesh%Nflux), zfs(mesh%Nflux), zft(mesh%Nflux)

      real(c_double) :: rx(mesh%Np), ry(mesh%Np), rz(mesh%Np)
      real(c_double) :: sx(mesh%Np), sy(mesh%Np), sz(mesh%Np)
      real(c_double) :: tx(mesh%Np), ty(mesh%Np), tz(mesh%Np)

      real(c_double) :: ag_r(mesh%Np)
      real(c_double) :: ag_s(mesh%Np)
      
      do elem_index = 1, mesh%Nelements

         ! element transformation factors
         xfr = mesh%xfr(:, elem_index); xfs = mesh%xfs(:, elem_index); xft = mesh%xft(:, elem_index)
         yfr = mesh%yfr(:, elem_index); yfs = mesh%yfs(:, elem_index); yft = mesh%yft(:, elem_index)
         zfr = mesh%zfr(:, elem_index); zfs = mesh%zfs(:, elem_index); zft = mesh%zft(:, elem_index)

         alphag_corr(:, 1) = (yfs*zft - yft*zfs) * (agi_x(:, elem_index) - alphag_flux(:, elem_index)) + &
                             (xft*zfs - xfs*zft) * (agi_y(:, elem_index) - alphag_flux(:, elem_index))

         alphag_corr(:, 2) = (yft*zfr - yfr*zft) * (agi_x(:, elem_index) - alphag_flux(:, elem_index)) + &
                             (xfr*zft - zft*zfr) * (agi_y(:, elem_index) - alphag_flux(:, elem_index))
         
       call DGEMM('N', 'N', mesh%DIM*mesh%Np, mesh%DIM, mesh%Nflux, &
            ONE, mesh%phifj_solution_grad, mesh%DIM*mesh%Np, &
            alphag_corr, mesh%Nflux, &
            ZERO, alphag_grad, mesh%DIM*mesh%Np)

       ag_r = alphag_grad(1:mesh%Np, 1)
       ag_s = alphag_grad(mesh%Np+1:2*mesh%Np, 2)

       ! inverse jacobian for the local element
       rx =  mesh%rx(:, elem_index); ry = mesh%ry(:, elem_index); rz = mesh%rz(:, elem_index)
       sx =  mesh%sx(:, elem_index); sy = mesh%sy(:, elem_index); sz = mesh%sz(:, elem_index)
       tx =  mesh%tx(:, elem_index); ty = mesh%ty(:, elem_index); tz = mesh%tz(:, elem_index)

       do j = 1, mesh%Np
          ax(j, 1, elem_index) = ax(j, 1, elem_index) + rx(j)*ag_r(j) + sx(j)*ag_s(j)
          ay(j, 1, elem_index) = ay(j, 1, elem_index) + ry(j)*ag_r(j) + sy(j)*ag_s(j)
       end do

      end do
      
    end subroutine get_corrected_void_fraction_gradients

    subroutine get_interface_source_terms(mesh, u, ax, ay, ui, HH, II)
      use input_module
      type(mesh2d),      intent(in)    :: mesh
      real(c_double),    intent(in)    :: u(mesh%Np,  mesh%Nvar, mesh%Nelements)
      real(c_double),    intent(in)    :: ax(mesh%Np, 1, mesh%Nelements)
      real(c_double),    intent(in)    :: ay(mesh%Np, 1, mesh%Nelements)
      real(c_double),    intent(inout) :: ui(mesh%Np, mesh%DIM+1, mesh%Nelements)
      real(c_double),    intent(inout) :: HH(mesh%Np, mesh%Nvar, mesh%Nelements)
      real(c_double),    intent(inout) :: II(mesh%Np, mesh%Nvar, mesh%Nelements)

      ! locals
      integer(c_int) :: elem_index, j, var
      real(c_double) :: arhog, arhol, ug, ul, vg, vl
      real(c_double) :: eg, el, del, del1, del2, del3, P
      real(c_double) :: alphag, alphal, rhog, rhol

      real(c_double) :: u_interface, v_interface, p_interface, delta

      delta = 1.2_c_double

      !$omp parallel do private(elem_index, j, var, arhog, arhol, ug, ul, vg, vl, &
      !$omp                     eg, el, del, del1, del2, del3, P, &
      !$omp                     alphag, alphal, rhog, rhol, u_interface, v_interface, &
      !$omp                     p_interface)
      do elem_index = 1, mesh%Nelements
         do j = 1, mesh%Np

            arhog = u(j, 1, elem_index); arhol = u(j, 5, elem_index)
            ug = u(j, 2, elem_index)/arhog
            vg = u(j, 3, elem_index)/arhog

            ul = u(j, 6, elem_index)/arhol
            vl = u(j, 7, elem_index)/arhol

            eg = u(j, 4, elem_index)/arhog - HALF*(ug*ug + vg*vg)
            el = u(j, 8, elem_index)/arhol - HALF*(ul*ul + vl*vl)

            del1  = (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)

            del2  = TWO * (arhol*el*(gamma_liq-ONE)) * &
                          (arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq)

            del3  = (arhol*el*(gamma_liq-ONE))

            del = del1*del1 + del2 + del3*del3

            P = HALF*(arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                      arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                      sqrt(del))

            rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*(eg-q_gas) )
            rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*(el-q_liq) )
            
            alphag = arhog/rhog; alphal = ONE-alphag

            u_interface = (alphag*rhog*ug + alphal*rhol*ul) / (alphag*rhog + alphal*rhol)
            v_interface = (alphag*rhog*vg + alphal*rhol*vl) / (alphag*rhog + alphal*rhol)

            p_interface = P - &
                 delta*(alphag*alphal*rhog*rhol)/(alphag*rhol + alphal*rhog) * &
                 ((ug-ul)**2 + (vg-vl)**2)

            ui(j, 1, elem_index) = u_interface
            ui(j, 2, elem_index) = v_interface
            ui(j, 3, elem_index) = p_interface

            ! compute the source terms (HH, II)
            HH(j, 1, elem_index) = ZERO
            HH(j, 2, elem_index) = -p_interface;
            HH(j, 3, elem_index) = ZERO;
            HH(j, 4, elem_index) = -p_interface*u_interface;

            HH(j, 5, elem_index) = ZERO
            HH(j, 6, elem_index) = p_interface
            HH(j, 7, elem_index) = ZERO
            HH(j, 8, elem_index) = p_interface*u_interface
            
            II(j, 1, elem_index) = ZERO;
            II(j, 2, elem_index) = ZERO;
            II(j, 3, elem_index) = -p_interface; 
            II(j, 4, elem_index) = -p_interface*v_interface;

            II(j, 5, elem_index) = ZERO
            II(j, 6, elem_index) = ZERO
            II(j, 7, elem_index) = p_interface
            II(j, 8, elem_index) = p_interface*v_interface
         end do

         ! multiply the interface operators by the gradient of the
         ! void fraction
         do var = 1, mesh%Nvar
            HH(:, var, elem_index) = HH(:, var, elem_index)*ax(:, 1, elem_index)
            II(:, var, elem_index) = II(:, var, elem_index)*ay(:, 1, elem_index)
         end do

      end do
      !$omp end parallel do

    end subroutine get_interface_source_terms

    subroutine relax(mesh, data, u, ui)
      type(mesh2d), intent(in) :: mesh
      type(FieldData_t), intent(inout) :: data
      real(c_double), intent(inout) :: u(mesh%Np, mesh%Nvar,   mesh%Nelements)
      real(c_double), intent(inout) :: ui(mesh%Np, mesh%DIM+1, mesh%Nelements)

      ! locals
      integer(c_int) :: elem_index, j

      real(c_double) :: alphag, alphal, rhog, rhol, arhog, arhol, ug, ul, vg, vl
      real(c_double) :: eg0, el0, del, del1, del2, del3, P
      real(c_double) :: ug0, vg0, ul0, vl0, eg, el

      call get_interface_source_terms(mesh, data%u, data%ax, data%ay, &
           data%ui, data%HH, data%II)

      !$omp parallel do private(elem_index, j, alphag, alphal, rhog, rhol, arhog, arhol, &
      !$omp                     ug, ul, vg, vl, eg0, el0, del, del1, del2, del3, P, &
      !$omp                     ug0, vg0, ul0, vl0, eg, el)
      do elem_index = 1, mesh%Nelements
         do j = 1, mesh%Np
            arhog = u(j, 1, elem_index); arhol = u(j, 5, elem_index)

            ug0 = u(j, 2, elem_index)/arhog
            vg0 = u(j, 3, elem_index)/arhog

            ul0 = u(j, 6, elem_index)/arhol
            vl0 = u(j, 7, elem_index)/arhol

            eg0 = u(j, 4, elem_index)/arhog - HALF*(ug0*ug0 + vg0*vg0)
            el0 = u(j, 8, elem_index)/arhol - HALF*(ul0*ul0 + vl0*vl0)

            del1  = (arhog*eg0*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)
            del3  = (arhol*el0*(gamma_liq-ONE))

            del2  = TWO * (arhol*el0*(gamma_liq-ONE)) * &
                          (arhog*eg0*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq)

            del = del1*del1 + del2 + del3*del3

            P = HALF*(arhog*eg0*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                      arhol*el0*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                      sqrt(del))

            rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*(eg0-q_gas) )
            rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*(el0-q_liq) )
            
            alphag = arhog/rhog; alphal = ONE-alphag

            ug = (alphag*rhog*ug0 + alphal*rhol*ul0)/(alphag*rhog + alphal*rhol)
            ul = ug

            vg = (alphag*rhog*vg0 + alphal*rhol*vl0)/(alphag*rhog + alphal*rhol)
            vl = vg

            eg = eg0 + HALF*( (ug - ug0)*(ui(j, 1, elem_index)-ug0) + &
                              (vg - vg0)*(ui(j, 2, elem_index)-vg0) )

            el = el0 - HALF*( (ul - ul0)*(ui(j, 1, elem_index)-ul0) + &
                              (vl - vl0)*(ui(j, 2, elem_index)-vl0) )

            ! update the conserved variables
            u(j, 1, elem_index) = alphag*rhog
            u(j, 2, elem_index) = alphag*rhog*ug
            u(j, 3, elem_index) = alphag*rhog*vg
            u(j, 4, elem_index) = alphag*rhog*(eg + HALF*(ug*ug + vg*vg))

            u(j, 5, elem_index) = alphal*rhol
            u(j, 6, elem_index) = alphal*rhol*ul
            u(j, 7, elem_index) = alphal*rhol*vl
            u(j, 8, elem_index) = alphal*rhol*(el + HALF*(ul*ul + vl*vl))
            
            ! now recompute the pressure, density and void fractions
            del1  = (alphag*rhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)
            del3  = (alphal*rhol*el*(gamma_liq-ONE))

            del2  = TWO * (alphal*rhol*el*(gamma_liq-ONE)) * &
                          (alphag*rhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq)
            del = del1*del1 + del2 + del3*del3

            P = HALF*(alphag*rhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                      alphal*rhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                      sqrt(del))

            rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*(eg-q_gas) )
            rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*(el-q_liq) )
            
            alphag = u(j, 1, elem_index)/rhog; alphal = ONE-alphag

            ! update the conserved variables
            u(j, 1, elem_index) = alphag*rhog
            u(j, 2, elem_index) = alphag*rhog*ug
            u(j, 3, elem_index) = alphag*rhog*vg
            u(j, 4, elem_index) = alphag*rhog*(eg + HALF*(ug*ug + vg*vg))

            u(j, 5, elem_index) = alphal*rhol
            u(j, 6, elem_index) = alphal*rhol*ul
            u(j, 7, elem_index) = alphal*rhol*vl
            u(j, 8, elem_index) = alphal*rhol*(el + HALF*(ul*ul + vl*vl))
            
         end do
      end do
      !$omp end parallel do

    end subroutine relax

    subroutine get_maxvel(mesh, data, gamma_gas, gamma_liq, pinf_gas, pinf_liq)
      type(mesh2d),      intent(inout) :: mesh
      real(c_double),    intent(in   ) :: gamma_gas, gamma_liq, pinf_gas, pinf_liq
      type(FieldData_t), intent(in   ) :: data

      ! locals
      integer(c_int) :: elem_index, j
      real(c_double) :: elem_smax, smax

      real(c_double) :: wp(mesh%NP, mesh%Nvar)
      real(c_double) :: ug(mesh%Np), vg(mesh%Np)
      real(c_double) :: ul(mesh%Np), vl(mesh%Np)
      real(c_double) :: cg(mesh%Np), cl(mesh%Np)
      real(c_double) :: ugmax, ulmax
      
      ! initialize smax
      smax = -ONE

      !$omp PARALLEL DO PRIVATE(elem_index, j, wp, ug, vg, ul, vl, &
      !$omp                     ugmax, ulmax, cg, cl, elem_smax) reduction(max:smax)
      do elem_index = 1, mesh%Nelements
         ug = data%u(:, 2, elem_index)/data%u(:, 1, elem_index)
         vg = data%u(:, 3, elem_index)/data%u(:, 1, elem_index)

         ul = data%u(:, 6, elem_index)/data%u(:, 5, elem_index)
         vl = data%u(:, 7, elem_index)/data%u(:, 5, elem_index)

         do j = 1, mesh%Np
            call get_primitive(data%u(j, :, elem_index), gamma_gas, gamma_liq, &
                 pinf_gas, pinf_liq, q_gas, q_liq, wp(j,:))
         end do

         cg = sqrt(gamma_gas*(wp(:, 1) + pinf_gas)/wp(:, 2))
         cl = sqrt(gamma_liq*(wp(:, 1) + pinf_liq)/wp(:, 3))

         ugmax = maxval( sqrt(ug*ug + vg*vg) + cg )
         ulmax = maxval( sqrt(ul*ul + vl*vl) + cl )

         elem_smax = max( ugmax, ulmax )
         smax      = max(smax, elem_smax)
      end do
      !$omp END PARALLEL DO

      ! set the maximum velocity for the mesh
      mesh%maxvel = smax
      
    end subroutine get_maxvel

    subroutine primitive_variable_min_max(mesh, data, Wmin, Wmax)
      type(mesh2d),      intent(in ) :: mesh
      type(FieldData_t), intent(in)  :: data
      real(c_double), intent(out) :: Wmin(mesh%Nvar)
      real(c_double), intent(out) :: Wmax(mesh%Nvar)

      ! locals
      integer(c_int) :: index

      Wmin = +LARGE
      Wmax = -LARGE

      !$omp parallel do private(index) reduction(max:Wmax) reduction(min:Wmin)
      do index = 1, mesh%Nelements_local
         if (mesh%elem_type(index) .ne. -1) then

            Wmin(1) = min(Wmin(1), minval(data%u(:, 1, index)))
            Wmax(1) = max(Wmax(1), maxval(data%u(:, 1, index)))

            Wmin(2) = min(Wmin(2), minval(data%u(:, 2, index)/data%u(:, 1, index)))
            Wmax(2) = max(Wmax(2), maxval(data%u(:, 2, index)/data%u(:, 1, index)))

            Wmin(3) = min(Wmin(3), minval(data%u(:, 3, index)/data%u(:, 1, index)))
            Wmax(3) = max(Wmax(3), maxval(data%u(:, 3, index)/data%u(:, 1, index)))

#ifdef DIM3 
            Wmin(4) = min(Wmin(4), minval(data%u(:, 4, index)/data%u(:, 1, index)))
            Wmax(4) = max(Wmax(4), maxval(data%u(:, 4, index)/data%u(:, 1, index)))
#endif
         end if
      end do
      !$omp end parallel do
            
    end subroutine primitive_variable_min_max

  subroutine checkbounds(mesh, data)
    type(mesh2d),      intent(in)  :: mesh
    type(FieldData_t), intent(in) :: data

    ! locals
    integer(c_int) :: elem_index
    integer(c_int) :: rho_comp, E_comp

    rho_comp = 1; E_comp   = 4
    if (mesh%DIM .eq. 3) then
       E_comp = 5
    end if
    
    !$omp parallel do private(elem_index)
    do elem_index = 1, mesh%Nelements
       if(mesh%elem_type(elem_index) .eq. 1) then
          if ( any(data%u(:, rho_comp, elem_index) .lt. EPSILON) .or. &
               any(data%u(:,   E_comp, elem_index) .lt. EPSILON) ) then
             print*, 'Non physical values encountered!', elem_index
             stop
          end if
       end if
    end do
    !$omp end parallel do
  end subroutine checkbounds

end module system_module
