module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    type(FieldData_t), intent(inout) :: data
    type(mesh2d),      intent(inout) :: mesh
    
    write(*, *) 'Write your own initialization routine!'
    stop

  end subroutine set_initial_values

end module initialize_module
