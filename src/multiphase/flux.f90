module flux_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module

implicit none

contains
    subroutine evaluate_flux(N, Nvar, &
         arhog, aMxg, aMyg, aMzg, aEg, &
         arhol, aMxl, aMyl, aMzl, aEl, &
         F, G, H, Fv, Gv, Hv)
      use input_module

      integer(c_int), intent(in   ) :: n, nvar
      real(c_double), intent(in   ) :: arhog(N), arhol(N)
      real(c_double), intent(in   ) :: aMxg(N),   aMxl(N)
      real(c_double), intent(in   ) :: aMyg(N),   aMyl(N)
      real(c_double), intent(in   ) :: aMzg(N),   aMzl(N)
      real(c_double), intent(in   ) ::  aEg(N),    aEl(N)

      real(c_double), intent(inout) :: F(N, Nvar), G(N, Nvar), H(N, Nvar)
      real(c_double), intent(inout) :: Fv(N, Nvar), Gv(N, Nvar), Hv(N, Nvar)

      ! locals
      real(c_double) :: ug(N), vg(N), ul(N), vl(N)
      real(c_double) :: Etg(N), Etl(N), eg(N), el(N), P(N)
      real(c_double) :: rhog(N), rhol(N), alphag(N), alphal(N)

      real(c_double) :: del(N), del1(N), del2(N), del21(N), del22(N), del3(N)

      ! velocities
      ug = aMxg/arhog; vg = aMyg/arhog
      ul = aMxl/arhol; vl = aMyl/arhol

      ! specific internal energies
      Etg = aEg/arhog
      Etl = aEl/arhol

      eg = Etg - HALF*(ug*ug + vg*vg)
      el = Etl - HALF*(ul*ul + vl*vl)

      ! Pressure
      del1 = (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)**2
      del21 = arhol*el*(gamma_liq-ONE)
      del22 = arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq
      del2  = TWO * del21*del22
      del3  = (arhol*el*(gamma_liq-ONE))**2

      del = del1 + del2 + del3

      P = HALF*( arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                 arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + sqrt(del) )

      rhog = (P + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*(eg - q_gas) )
      rhol = (P + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*(el - q_liq) )
      
      alphag = arhog/rhog
      alphal = ONE-alphag

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! X-inviscid fluxes
      F(:, 1) = alphag*(rhog*ug)
      F(:, 2) = alphag*(rhog*ug*ug + P)
      F(:, 3) = alphag*(rhog*ug*vg)
      F(:, 4) = alphag*ug*(rhog*Etg + P)

      F(:, 5) = alphal*(rhol*ul)
      F(:, 6) = alphal*(rhol*ul*ul + P)
      F(:, 7) = alphal*(rhol*ul*vl)
      F(:, 8) = alphal*ul*(rhol*Etl + P)

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Y-inviscid fluxes
      G(:, 1) = alphag*(rhog*vg)
      G(:, 2) = alphag*(rhog*vg*ug)
      G(:, 3) = alphag*(rhog*vg*vg + P)
      G(:, 4) = alphag*vg*(rhog*Etg + P)

      G(:, 5) = alphal*(rhol*vl)
      G(:, 6) = alphal*(rhol*vl*ul)
      G(:, 7) = alphal*(rhol*vl*vl + P)
      G(:, 8) = alphal*vl*(rhol*Etl + P)

      H  = ZERO; Hv = ZERO
      Fv = ZERO; Gv = ZERO

    end subroutine evaluate_flux

    subroutine evaluate_interaction_flux(DIM, p1, nvar, &
      nminus, pnminus, &
      uminus, uplus,  &
      Fdminus, Fdplus, Fvminus, Fvplus, &
      Gdminus, Gdplus, Gvminus, Gvplus, &
      Hdminus, Hdplus, Hvminus, Hvplus, &
      Fi, Gi, Hi, &
      Fv, Gv, Hv, &
      agi_x, agi_y, &
      igamma, face_type, &
      ilambda, ibeta_viscous, itau, glb_face_index)

      use input_module

      integer(c_int), intent(in ) :: DIM, p1, nvar
      real(c_double), intent(in ) :: nminus(DIM), pnminus(DIM)
      real(c_double), intent(in ) :: uminus(p1, nvar), uplus( p1, nvar)

      real(c_double), intent(in ) :: Fdminus(p1, nvar), Fdplus(p1, nvar)
      real(c_double), intent(in ) :: Fvminus(p1, nvar), Fvplus(p1, nvar)

      real(c_double), intent(in ) :: Gdminus(p1, nvar), Gdplus(p1, nvar)
      real(c_double), intent(in ) :: Gvminus(p1, nvar), Gvplus(p1, nvar)

      real(c_double), intent(in ) :: Hdminus(p1, nvar), Hdplus(p1, nvar)
      real(c_double), intent(in ) :: Hvminus(p1, nvar), Hvplus(p1, nvar)

      real(c_double), intent(out) :: Fi(p1, nvar), Gi(p1, nvar), Hi(p1, nvar)
      real(c_double), intent(out) :: Fv(p1, nvar), Gv(p1, nvar), Hv(p1, nvar)

      real(c_double), intent(out) :: agi_x(P1), agi_y(P1)

      real(c_double), intent(in) :: igamma
      integer(c_int), intent(in) :: face_type

      integer(c_int), intent(in) :: glb_face_index
      real(c_double), intent(in) :: ilambda
      real(c_double), intent(in) :: ibeta_viscous
      real(c_double), intent(in) :: itau

      ! locals
      integer(c_int) :: j, var
      real(c_double) :: lambda, beta_viscous, tau_penalty

      real(c_double) :: ujumpx(p1, nvar), ujumpy(p1, nvar), ujumpz(P1, Nvar)

      real(c_double) :: alphag_minus(P1), alphag_plus(P1)
      real(c_double) :: arhog_minus(P1), arhog_plus(P1)
      real(c_double) :: rhog_minus(P1), rhog_plus(P1)
      real(c_double) :: ug_minus(P1), ug_plus(P1)
      real(c_double) :: vg_minus(P1), vg_plus(P1)
      real(c_double) :: velg_dotn_minus(P1), velg_dotn_plus(P1)
      real(c_double) :: Etg_minus(P1), Etg_plus(P1)
      real(c_double) :: eg_minus(P1), eg_plus(P1)
      real(c_double) :: cg_minus(P1), cg_plus(P1)

      real(c_double) :: alphal_minus(P1), alphal_plus(P1)
      real(c_double) :: arhol_minus(P1), arhol_plus(P1)
      real(c_double) :: rhol_minus(P1), rhol_plus(P1)
      real(c_double) :: ul_minus(P1), ul_plus(P1)
      real(c_double) :: vl_minus(P1), vl_plus(P1)
      real(c_double) :: vell_dotn_minus(P1), vell_dotn_plus(P1)
      real(c_double) :: Etl_minus(P1), Etl_plus(P1)
      real(c_double) :: el_minus(P1), el_plus(P1)
      real(c_double) :: cl_minus(P1), cl_plus(P1)

      real(c_double) :: cgmax(P1), clmax(P1)
      real(c_double) :: cmax_gas, cmax_liq

      real(c_double) :: Ustr_minus(P1, Nvar), Ustr_plus(P1, Nvar)

      real(c_double) :: del(P1), del1(P1), del2(P1), del3(P1)
      real(c_double) :: P_left(P1), P_rght(P1)
      
      !real(c_double) :: Sg_L, Sg_R, Sl_L, Sl_R
      !real(c_double) :: Sg_L(P1), Sg_R(P1), Sl_L(P1), Sl_R(P1)
      real(c_double) :: Smin, Smax

      real(c_double) :: p_minus(P1), p_plus(P1), uL(P1), uR(P1), vL(P1)

      ! real(c_double) :: vR(P1), wL(P1), wR(P1), rhoL(P1), rhoR(P1)
      ! real(c_double) :: Sgstr(P1), Slstr(P1), Sstr(P1)
      ! real(c_double) :: tmp(P1), tmp1(P1)

      real(c_double) :: nx, ny, nz

      lambda = ilambda
      beta_viscous = ibeta_viscous
      tau_penalty = itau

      ! unit normals
      nx = nminus(1)
      ny = nminus(2)
      nz = ZERO
#ifdef DIM3
      nz  = nminus(3)
#endif

      ! jump terms
      ujumpx = nx*(uminus - uplus)
      ujumpy = ny*(uminus - uplus)
      ujumpz = nz*(uminus - uplus)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Inviscid interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      arhog_minus = uminus(:, 1); arhog_plus = uplus(:, 1)
      arhol_minus = uminus(:, 5); arhol_plus = uplus(:, 5)

      ug_minus = uminus(:, 2)/arhog_minus; ug_plus = uplus(:, 2)/arhog_plus
      vg_minus = uminus(:, 3)/arhog_minus; vg_plus = uplus(:, 3)/arhog_plus

      velg_dotn_minus = nx*ug_minus + ny*vg_minus
      velg_dotn_plus =  nx*ug_plus +  ny*vg_plus

      ul_minus = uminus(:, 6)/arhol_minus; ul_plus = uplus(:, 6)/arhol_plus
      vl_minus = uminus(:, 7)/arhol_minus; vl_plus = uplus(:, 7)/arhol_plus
      
      vell_dotn_minus = nx*ul_minus + ny*vl_minus
      vell_dotn_plus  = nx*ul_plus  + ny*vl_plus

      Etg_minus = uminus(:, 4)/arhog_minus
      Etg_plus  = uplus(:, 4)/arhog_plus

      eg_minus = Etg_minus - HALF*(ug_minus*ug_minus + vg_minus*vg_minus)
      eg_plus  = Etg_plus  - HALF*(  ug_plus*ug_plus +   vg_plus*vg_plus)

      Etl_minus = uminus(:, 8)/arhol_minus
      Etl_plus  = uplus(:, 8)/arhol_plus

      el_minus = Etl_minus - HALF*(ul_minus*ul_minus + vl_minus*vl_minus)
      el_plus  = Etl_plus  - HALF*(  ul_plus*ul_plus +   vl_plus*vl_plus)

      ! left-state pressure
      del1 = (arhog_minus*eg_minus*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)
      del3 = (arhol_minus*el_minus*(gamma_liq-ONE))
      
      del2 = TWO * (arhol_minus*el_minus*(gamma_liq-ONE)) * &
           (arhog_minus*eg_minus*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq)

      del = del1*del1 + del2 + del3*del3

      P_minus = HALF*(arhog_minus*eg_minus*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                      arhol_minus*el_minus*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                      sqrt(del))

      ! right-state pressure
      del1 = (arhog_plus*eg_plus*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)
      del3 = (arhol_plus*el_plus*(gamma_liq-ONE))
      
      del2 = TWO * (arhol_plus*el_plus*(gamma_liq-ONE)) * &
                   (arhog_plus*eg_plus*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq)

      del = del1*del1 + del2 + del3*del3

      P_plus = HALF*(arhog_plus*eg_plus*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                     arhol_plus*el_plus*(gamma_liq-ONE) - gamma_liq*pinf_liq + &
                     sqrt(del))

      ! left and right densities
      rhog_minus = (P_minus + gamma_gas*pinf_gas)/((gamma_gas-ONE)*(eg_minus-q_gas))
      rhog_plus  = (P_plus  + gamma_gas*pinf_gas)/((gamma_gas-ONE)*(eg_plus -q_gas))

      rhol_minus = (P_minus + gamma_liq*pinf_liq)/((gamma_liq-ONE)*(el_minus-q_liq))
      rhol_plus  = (P_plus  + gamma_liq*pinf_liq)/((gamma_liq-ONE)*(el_plus -q_liq))

      ! left and right sound speeds for the two phases
      cg_minus = sqrt(gamma_gas*(P_minus + pinf_gas)/rhog_minus)
      cg_plus  = sqrt(gamma_gas*(P_plus  + pinf_gas)/rhog_plus )

      cl_minus = sqrt(gamma_liq*(P_minus + pinf_liq)/rhol_minus)
      cl_plus  = sqrt(gamma_liq*(P_plus  + pinf_liq)/rhol_plus )

      alphag_minus = arhog_minus/rhog_minus
      alphag_plus  = arhog_plus/rhog_plus

      alphal_minus = ONE - alphag_minus
      alphal_plus  = ONE - alphag_plus

      cgmax = HALF * ( cg_minus + cg_plus + abs(velg_dotn_minus + velg_dotn_plus) )
      cmax_gas = maxval(cgmax)

      clmax = HALF * ( cl_minus + cl_plus + abs(vell_dotn_minus + vell_dotn_plus) )
      cmax_liq = maxval(clmax)

      Smax = max(cmax_gas, cmax_liq)
      Smin = -Smax

      ! set the left-most and right-most wave speeds in the system
      cmax_gas = Smax; cmax_liq = Smax

      ! store the inviscid interaction flux
      Fi = HALF * (Fdminus + Fdplus)
      Gi = HALF * (Gdminus + Gdplus)
      Hi = HALF * (Hdminus + Hdplus)

      do var = 1, Nvar/2
         do j = 1, P1
            Fi(j, 1+(var-1)) = Fi(j, 1+(var-1)) + lambda*cmax_gas*ujumpx(j, 1+(var-1))
            Fi(j, 5+(var-1)) = Fi(j, 5+(var-1)) + lambda*cmax_liq*ujumpx(j, 5+(var-1))

            Gi(j, 1+(var-1)) = Gi(j, 1+(var-1)) + lambda*cmax_gas*ujumpy(j, 1+(var-1))
            Gi(j, 5+(var-1)) = Gi(j, 5+(var-1)) + lambda*cmax_liq*ujumpy(j, 5+(var-1))

            agi_x = HALF * (alphag_minus + alphag_plus) + &
                 lambda*cmax_gas*nx*(alphag_minus - alphag_plus)

            agi_y = HALF * (alphag_minus + alphag_plus) + &
                 lambda*cmax_gas*ny*(alphag_minus - alphag_plus)

         end do
      end do

      Hi(:, :) = ZERO

      ! ! compute the minimum eigenvalue for each phase
      ! !Sg_L = min( minval(ug_minus - cg_minus), minval(ug_plus - cg_plus) )
      ! !Sg_R = max( maxval(ug_minus + cg_minus), maxval(ug_plus + cg_plus) )

      ! !Sl_L = min( minval(ul_minus - cl_minus), minval(ul_plus - cl_plus) )
      ! !Sl_R = max( maxval(ul_minus + cl_minus), maxval(ul_plus + cl_plus) )

      ! do j = 1, P1
      !    Sg_L(j) = min( velg_dotn_minus(j) - cg_minus(j), velg_dotn_plus(j)  - cg_plus(j) )
      !    Sg_R(j) = max( velg_dotn_minus(j) + cg_minus(j), velg_dotn_plus(j)  + cg_plus(j) )

      !    Sl_L(j) = min( vell_dotn_minus(j) - cl_minus(j), vell_dotn_plus(j) - cl_plus(j) )
      !    Sl_R(j) = max( vell_dotn_minus(j) + cl_plus(j),  vell_dotn_plus(j) + cl_plus(j) )
      ! end do

      ! !Sg_L = min( minval(velg_dotn_minus - cg_minus), minval(velg_dotn_plus - cg_plus) )
      ! !Sg_R = max( maxval(velg_dotn_minus + cg_minus), maxval(velg_dotn_plus + cg_plus)

      ! !Sl_L = min( minval(vell_dotn_minus - cl_minus), minval(velg_dotn_plus - cl_plus) )
      ! !Sl_R = max( maxval(vell_dotn_minus + cl_minus), maxval(velg_dotn_plus + cl_plus) )

      ! !Smax = max(ZERO, max(Sg_R, Sl_R))
      ! !Smin = min(ZERO, min(Sg_L, Sl_L))

      ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! p_left  = P_minus*(alphag_minus + alphal_minus)
      ! p_rght  = P_plus*(alphag_plus + alphal_plus)

      ! rhoL = alphag_minus*rhog_minus + alphal_minus*rhol_minus
      ! rhoR = alphag_plus*rhog_plus + alphal_plus*rhol_plus

      ! ! uL = ONE/rhoL * (alphag_minus*rhog_minus*ug_minus + &
      ! !                  alphal_minus*rhol_minus*ul_minus)

      ! ! uR = ONE/rhoR * (alphag_plus*rhog_plus*ug_plus + &
      ! !                  alphal_plus*rhol_plus*ul_plus)
      ! uL = ONE/rhoL * (alphag_minus*rhog_minus*velg_dotn_minus + &
      !                  alphal_minus*rhol_minus*vell_dotn_minus)

      ! uR = ONE/rhoR * (alphag_plus*rhog_plus*velg_dotn_plus + &
      !                  alphal_plus*rhol_plus*vell_dotn_plus)

      ! Sgstr = p_rght - p_left + rhoL*uL*(Sg_L - uL) - rhoR*uR*(Sg_R - uR)
      ! Sgstr = Sgstr/(rhoL*(Sg_L - uL) - rhoR*(Sg_R - uR))

      ! Slstr = p_rght - p_left + rhoL*uL*(Sl_L - uL) - rhoR*uR*(Sl_R - uR)
      ! Slstr = Slstr/(rhoL*(Sl_L - uL) - rhoR*(Sl_R - uR))

      ! Sstr = HALF * (Sgstr + Slstr)

      ! tmp  = alphag_minus*rhog_minus*(Sg_L - velg_dotn_minus)/(Sg_L - Sstr)
      ! tmp1 = Sstr + P_minus/( rhog_minus*(Sg_L - velg_dotn_minus) )

      ! Ustr_minus(:,1) = tmp
      ! Ustr_minus(:,2) = tmp * (ug_minus - (velg_dotn_minus - Sstr)*nx)
      ! Ustr_minus(:,3) = tmp * (vg_minus - (velg_dotn_minus - Sstr)*ny)
      ! Ustr_minus(:,4) = tmp * (Etg_minus + (Sstr - velg_dotn_minus) * tmp1 )

      ! tmp  = alphal_minus*rhol_minus*(Sl_L - vell_dotn_minus)/(Sl_L - Sstr)
      ! tmp1 = Sstr + P_minus/( rhol_minus*(Sl_L - vell_dotn_minus) )
      
      ! Ustr_minus(:,5) = tmp
      ! Ustr_minus(:,6) = tmp * (ul_minus - (vell_dotn_minus - Sstr)*nx)
      ! Ustr_minus(:,7) = tmp * (vl_minus - (vell_dotn_minus - Sstr)*ny)
      ! Ustr_minus(:,8) = tmp * (Etl_minus + (Sstr - vell_dotn_minus) * tmp1)

      ! tmp  = alphag_plus*rhog_plus*(Sg_R - ug_plus)/(Sg_R - Sstr)
      ! tmp1 = Sstr + P_plus/( rhog_plus*(Sg_R - velg_dotn_plus) )

      ! Ustr_plus(:,1) = tmp
      ! Ustr_plus(:,2) = tmp * (ug_plus - (velg_dotn_plus - Sstr)*nx)
      ! Ustr_plus(:,3) = tmp * (vg_plus - (velg_dotn_plus - Sstr)*ny)
      ! Ustr_plus(:,4) = tmp * (Etg_plus + (Sstr - velg_dotn_plus) * tmp1 )

      ! tmp  = alphal_plus*rhol_plus*(Sl_R - ul_plus)/(Sl_R - Sstr)
      ! tmp1 = Sstr + P_plus/( rhol_plus*(Sl_R - vell_dotn_plus) )

      ! Ustr_plus(:,5) = tmp
      ! Ustr_plus(:,6) = tmp * (ul_plus - (vell_dotn_plus - Sstr)*nx)
      ! Ustr_plus(:,7) = tmp * (vl_plus - (vell_dotn_plus - Sstr)*ny)
      ! Ustr_plus(:,8) = tmp * (Etl_plus + (Sstr - vell_dotn_plus) * tmp1 )

      ! ! ! HLLC solver
      ! ! do j = 1, P1
      ! !    if (Sl_L(j) .ge. ZERO) then
      ! !       Fi(j, :) = Fdminus(j, :)
      ! !       Gi(j, :) = Gdminus(j, :)
            
      ! !    else if (Sl_L(j) .lt. ZERO .and. Sg_L(j) .ge. ZERO) then
      ! !       Fi(j, 1:4) = Fdminus(j, 1:4)
      ! !       Gi(j, 1:4) = Gdminus(j, 1:4)

      ! !       Fi(j, 5:8) = Fdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))
      ! !       Gi(j, 5:8) = Gdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))

      ! !    else if (Sg_L(j) .lt. ZERO .and. Sstr(j) .ge. ZERO) then
      ! !       Fi(j, 1:4) = Fdminus(j, 1:4) + Sg_L(j)*(Ustr_minus(j, 1:4) - uminus(j, 1:4))
      ! !       Gi(j, 1:4) = Gdminus(j, 1:4) + Sg_L(j)*(Ustr_minus(j, 1:4) - uminus(j, 1:4))

      ! !       Fi(j, 5:8) = Fdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))
      ! !       Gi(j, 5:8) = Gdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))

      ! !    else if (Sstr(j) .lt. ZERO .and. Sg_R(j) .ge. ZERO) then
      ! !       Fi(j, 1:4) = Fdplus(j, 1:4) + Sg_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))
      ! !       Gi(j, 1:4) = Gdplus(j, 1:4) + Sg_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))

      ! !       Fi(j, 5:8) = Fdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))
      ! !       Gi(j, 5:8) = Gdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))

      ! !    else if (Sg_R(j) .lt. ZERO .and. Sstr(j) .ge. ZERO) then
      ! !       Fi(j, 1:4) = Fdplus(j, 1:4)
      ! !       Gi(j, 1:4) = Gdplus(j, 1:4)

      ! !       Fi(j, 5:8) = Fdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))
      ! !       Gi(j, 5:8) = Gdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))

      ! !    else if (Sl_L(j) .le. ZERO) then
      ! !       Fi(j, :) = Fdplus(j, :)
      ! !       Gi(j, :) = Gdplus(j, :)

      ! !    else
      ! !       print*, 'Incorrect wave speeds...'
      ! !       stop
      ! !    end if
      ! ! end do

      ! ! OLD HLLC solver
      ! do j = 1, P1
      !    if (Sg_L(j) .ge. ZERO) then
      !       Fi(j, 1:4) = Fdminus(j, 1:4)
      !       Gi(j, 1:4) = Gdminus(j, 1:4)

      !    else if (Sg_L(j) .le. ZERO .and. Sstr(j) .ge. ZERO) then
      !       Fi(j, 1:4) = Fdminus(j, 1:4) + Sg_L(j)*(Ustr_minus(j, 1:4) - uminus(j, 1:4))
      !       Gi(j, 1:4) = Gdminus(j, 1:4) + Sg_L(j)*(Ustr_minus(j, 1:4) - uminus(j, 1:4))

      !    else if (Sstr(j) .lt. ZERO .and. Sg_R(j) .ge. ZERO) then
      !       Fi(j, 1:4) = Fdplus(j, 1:4) + Sg_R(j)*(Ustr_plus(j, 1:4) - uplus(j, 1:4))
      !       Gi(j, 1:4) = Gdplus(j, 1:4) + Sg_R(j)*(Ustr_plus(j, 1:4) - uplus(j, 1:4))

      !    else if (Sg_R(j) .lt. ZERO) then
      !       Fi(j, 1:4) = Fdplus(j, 1:4)
      !       Gi(j, 1:4) = Gdplus(j, 1:4)
      !    end if

      !    if (Sl_L(j) .ge. ZERO) then
      !       Fi(j, 5:8) = Fdminus(j, 5:8)
      !       Gi(j, 5:8) = Gdminus(j, 5:8)

      !    else if (Sl_L(j) .le. ZERO .and. Sstr(j) .ge. ZERO) then
      !       Fi(j, 5:8) = Fdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))
      !       Gi(j, 5:8) = Gdminus(j, 5:8) + Sl_L(j)*(Ustr_minus(j, 5:8) - uminus(j, 5:8))

      !    else if (Sstr(j) .lt. ZERO .and. Sl_R(j) .ge. ZERO) then
      !       Fi(j, 5:8) = Fdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))
      !       Gi(j, 5:8) = Gdplus(j, 5:8) + Sl_R(j)*(Ustr_plus(j, 5:8) - uplus(j, 5:8))

      !    else if (Sl_R(j) .lt. ZERO) then
      !       Fi(j, 5:8) = Fdplus(j, 5:8)
      !       Gi(j, 5:8) = Gdplus(j, 5:8)
      !    end if

      ! end do

      ! ! ! HLL solver for low void fractions
      ! ! if (any(alphag_minus .lt. 0.1_c_double) .or. any(alphag_plus .lt. 0.1_c_double) .or. &
      ! !     any(alphag_minus .gt. 0.9_c_double) .or. any(alphag_plus .gt. 0.9_c_double)) then 

      !    do j = 1, P1
      !       ! Gas phase
      !       if(Sg_L(j) .ge. ZERO) then
      !          Fi(j, 1:4) = Fdminus(j, 1:4)
      !          Gi(j, 1:4) = Gdminus(j, 1:4)

      !       else if (Sg_R(j) .le. ZERO) then
      !          Fi(j, 1:4) = Fdplus(j, 1:4)
      !          Gi(j, 1:4) = Gdplus(j, 1:4)

      !       else
      !          Fi(j, 1:4) = Sg_R(j)*Fdminus(j, 1:4) - Sg_L(j)*Fdplus(j,1:4)
      !          Fi(j, 1:4) = Fi(j, 1:4) - Sg_L(j)*Sg_R(j)*ujumpx(j, 1:4)
      !          Fi(j, 1:4) = Fi(j, 1:4)/(Sg_R(j) - Sg_L(j))

      !          Gi(j, 1:4) = Sg_R(j)*Gdminus(j, 1:4) - Sg_L(j)*Gdplus(j, 1:4)
      !          Gi(j, 1:4) = Gi(j, 1:4) - Sg_L(j)*Sg_R(j)*ujumpy(j, 1:4)
      !          Gi(j, 1:4) = Gi(j, 1:4)/(Sg_R(j) - Sg_L(j))

      !       end if

      !       ! liquid phase
      !       if (Sl_L(j) .ge. ZERO) then
      !          Fi(j, 5:8) = Fdminus(j, 5:8)
      !          Gi(j, 5:8) = Gdminus(j, 5:8)

      !       else if (Sl_R(j) .le. ZERO) then
      !          Fi(j, 5:8) = Fdplus(j, 5:8)
      !          Gi(j, 5:8) = Gdplus(j, 5:8)
         
      !       else
      !          Fi(j, 5:8) = Sl_R(j)*Fdminus(j, 5:8) - Sl_L(j)*Fdplus(j, 5:8)
      !          Fi(j, 5:8) = Fi(j, 5:8) - Sl_L(j)*Sl_R(j)*ujumpx(j, 5:8)
      !          Fi(j, 5:8) = Fi(j, 5:8)/(Sl_R(j) - Sl_L(j))

      !          Gi(j, 5:8) = Sl_R(j)*Gdminus(j, 5:8) - Sl_L(j)*Gdplus(j, 5:8)
      !          Gi(j, 5:8) = Gi(j, 5:8) - Sl_R(j)*Sl_L(j)*ujumpy(j, 5:8)
      !          Gi(j, 5:8) = Gi(j, 5:8)/( Sl_R(j) - Sl_L(j) )

      !       end if
      !    end do
      ! !end if
         
      ! ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! ! ! compute the minimum eigenvalue for each phase (Y-component)
      ! ! Sg_L = min( minval(vg_minus - cg_minus), minval(vg_plus - cg_plus) )
      ! ! Sg_R = max( maxval(vg_minus + cg_minus), maxval(vg_plus + cg_plus) )

      ! ! Sl_L = min( minval(vl_minus - cl_minus), minval(vl_plus - cl_plus) )
      ! ! Sl_R = max( maxval(vl_minus + cl_minus), maxval(vl_plus + cl_plus) )

      ! ! Smax = max(ZERO, max(Sg_R, Sl_R))
      ! ! Smin = min(ZERO, min(Sg_L, Sl_L))

      ! ! p_left = p_minus*(alphag_minus + alphal_minus)
      ! ! p_rght = p_plus*(alphag_plus + alphal_plus)

      ! ! rhoL = alphag_minus*rhog_minus + alphal_minus*rhol_minus
      ! ! rhoR = alphag_plus*rhog_plus + alphal_plus*rhol_plus

      ! ! vL = ONE/rhoL * (alphag_minus*rhog_minus*vg_minus + &
      ! !                  alphal_minus*rhol_minus*vl_minus)

      ! ! vR = ONE/rhoR * (alphag_plus*rhog_plus*vg_plus + &
      ! !                  alphal_plus*rhol_plus*vl_plus)

      ! ! Sgstr = p_rght - p_left + rhoL*vL*(Sg_L - vL) - rhoR*vR*(Sg_R - vR)
      ! ! Sgstr = Sgstr/(rhoL*(Sg_L - vL) - rhoR*(Sg_R - vR))

      ! ! Slstr = p_rght - p_left + rhoL*vL*(Sl_L - vL) - rhoR*vR*(Sl_R - vR)
      ! ! Slstr = Slstr/(rhoL*(Sl_L - vL) - rhoR*(Sl_R - vR))

      ! ! Sstr = HALF * (Sgstr + Slstr)

      ! ! !Sg_L = Smin; Sl_L = Smin
      ! ! !Sg_R = Smax; Sl_R = Smax

      ! ! tmp = alphag_minus*rhog_minus*(Sg_L - vg_minus)/(Sg_L - Sstr)
      ! ! Ustr_minus(:,1) = tmp
      ! ! Ustr_minus(:,2) = tmp * (ug_minus - (vg_minus - Sstr)*nx)
      ! ! Ustr_minus(:,3) = tmp * (vg_minus - (vg_minus - Sstr)*ny)
      ! ! Ustr_minus(:,4) = tmp * (uminus(:,4)/arhog_minus + &
      ! !                         (Sstr - vg_minus)*(Sstr + p_minus/(rhog_minus*(Sg_L - vg_minus))))

      ! ! tmp = alphal_minus*rhol_minus*(Sl_L - vl_minus)/(Sl_L - Sstr)
      ! ! Ustr_minus(:,5) = tmp
      ! ! Ustr_minus(:,6) = tmp * (ul_minus - (vl_minus - Sstr)*nx)
      ! ! Ustr_minus(:,7) = tmp * (vl_minus - (vl_minus - Sstr)*ny)
      ! ! Ustr_minus(:,8) = tmp * (uminus(:,8)/arhol_minus + &
      ! !                         (Sstr-vl_minus)*(Sstr + p_minus/(rhol_minus*(Sl_L - vl_minus))))

      ! ! tmp = alphag_plus*rhog_plus*(Sg_R - vg_plus)/(Sg_R - Sstr)
      ! ! Ustr_plus(:,1) = tmp
      ! ! Ustr_plus(:,2) = tmp * (ug_plus - (vg_plus - Sstr)*nx)
      ! ! Ustr_plus(:,3) = tmp * (vg_plus - (vg_plus - Sstr)*ny)
      ! ! Ustr_plus(:,4) = tmp * (uplus(:,4)/arhog_plus + &
      ! !                        (Sstr-vg_plus)*(Sstr + p_plus/(rhog_plus*(Sg_R - vg_plus))))

      ! ! tmp = alphal_plus*rhol_plus*(Sl_R - vl_plus)/(Sl_R - Sstr)
      ! ! Ustr_plus(:,5) = tmp
      ! ! Ustr_plus(:,6) = tmp * (ul_plus - (vl_plus - Sstr)*nx)
      ! ! Ustr_plus(:,7) = tmp * (vl_plus - (vl_plus - Sstr)*ny)
      ! ! Ustr_plus(:,8) = tmp * (uplus(:,8)/arhol_plus + &
      ! !                        (Sstr-vl_plus)*(Sstr + p_plus/(rhol_plus*(Sl_R - vl_plus))))

      ! ! ! HLLC SOLVER
      ! ! do j = 1, P1
      ! !    if (Sg_L .ge. ZERO) then
      ! !       Gi(j, 1:4) = Gdminus(j, 1:4)
      ! !    else if (Sg_L .lt. ZERO .and. Sstr(j) .ge. ZERO) then
      ! !       Gi(j, 1:4) = Gdminus(j, 1:4) + Sg_L*(Ustr_minus(j, 1:4) - uminus(j, 1:4))
      ! !    else if (Sstr(j) .lt. ZERO .and. Sg_R .ge. ZERO) then
      ! !       Gi(j, 1:4) = Gdplus(j, 1:4) + Sg_R*(Ustr_plus(j, 1:4) - uplus(j, 1:4))
      ! !    else if (Sg_R .lt. ZERO) then
      ! !       Gi(j, 1:4) = Gdplus(j, 1:4)
      ! !    end if
         
      ! !    if (Sl_L .ge. ZERO) then
      ! !       Gi(j, 5:8) = Gdminus(j, 5:8)
      ! !    else if (Sl_L .lt. ZERO .and. Sstr(j) .ge. ZERO) then
      ! !       Gi(j, 5:8) = Gdminus(j, 5:8) + Sl_L*(Ustr_minus(j, 5:8) - uminus(j, 5:8))
      ! !    else if (Sstr(j) .lt. ZERO .and. Sl_R .ge. ZERO) then
      ! !       Gi(j, 5:8) = Gdplus(j, 5:8) + Sl_R*(Ustr_plus(j, 5:8) - uplus(j, 5:8))
      ! !    else if (Sl_R .lt. ZERO) then
      ! !       Gi(j, 5:8) = Gdplus(j, 5:8)
      ! !    end if
      ! ! end do

      ! ! ! HLL solver
      ! ! if (any(alphag_minus .lt. 0.01_c_double) .or. any(alphag_plus .lt. 0.01_c_double) .or. &
      ! !     any(alphag_minus .gt. 0.99_c_double) .or. any(alphag_plus .gt. 0.99_c_double)) then

      ! !    ! G-flux for the gas phase
      ! !    if (Sg_L .ge. ZERO) then
      ! !       Gi(:, 1:4) = Gdminus(:, 1:4)

      ! !    else if (Sg_R .le. ZERO) then
      ! !       Gi(:, 1:4) = Gdplus(:, 1:4)

      ! !    else
      ! !       Gi(:, 1:4) = (Sg_R*Gdminus(:,1:4) - Sg_L*Gdplus(:,1:4) - Sg_L*Sg_R*ujumpy(:,1:4))/(Sg_R - Sg_L)

      ! !    end if

      ! !    ! G-flux for the liquid phase
      ! !    if (Sl_L .ge. ZERO) then
      ! !       Gi(:, 5:8) = Gdminus(:, 5:8)
            
      ! !    else if (Sl_R .le. ZERO) then
      ! !       Gi(:, 5:8) = Gdplus(:, 5:8)

      ! !    else
      ! !       Gi(:, 5:8) = (Sl_R*Gdminus(:,5:8) - Sl_L*Gdplus(:,5:8) - Sl_R*Sl_L*ujumpy(:,5:8))/(Sl_R - Sl_L) 

      ! !    end if
      ! ! end if

      ! ! set the H-fluxes to be 0 for now
      ! Hi(:, :) = ZERO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Viscous interaction fluxes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! get the centered average flux
      Fv = HALF * (Fvminus + Fvplus)
      Gv = HALF * (Gvminus + Gvplus)
      Hv = HALF * (Hvminus + Hvplus)

      ! get the penalty term
      Fv = Fv + tau_penalty*nx*( uminus - uplus )
      Gv = Gv + tau_penalty*ny*( uminus - uplus )
      Hv = Hv + tau_penalty*nz*( uminus - uplus )


      ! get the directional term
      Fv = Fv + beta_viscous*(nx*(Fvminus - Fvplus) + &
                              ny*(Gvminus - Gvplus) + &
                              nz*(Hvminus - Hvplus))

      Gv = Gv + beta_viscous*(nx*(Fvminus - Fvplus) + &
                              ny*(Gvminus - Gvplus) + &
                              nz*(Hvminus - Hvplus))

      Hv = Hv + beta_viscous*(nx*(Fvminus - Fvplus) + &
                              ny*(Gvminus - Gvplus) + &
                              nz*(Hvminus - Hvplus))

      ! the input parameter 'vdiff' & 'avisc' control whether the
      ! viscous contribution is used (default vdiff=0, avisc=0)
      Fv = max(vdiff, avisc)*Fv
      Gv = max(vdiff, avisc)*Gv

#ifdef DIM3
      Hv = max(vdiff, avisc)*Hv
#else
      Hv = ZERO
#endif
      
    end subroutine evaluate_interaction_flux

    subroutine get_primitive(u, gamma_gas, gamma_liq, pinf_gas, pinf_liq, q_gas, q_liq, w)
      real(c_double), intent(in)  :: u(8)
      real(c_double), intent(in)  :: gamma_gas, gamma_liq, pinf_gas, pinf_liq, q_gas, q_liq
      real(c_double), intent(out) :: w(5)

      ! locals
      real(c_double) :: arhog, arhol, aMxg, aMxl
      real(c_double) :: aMyg, aMyl, aEg, aEl
      real(c_double) :: ug, vg, ul, vl, Etg, Etl, eg, el
      real(c_double) :: del1, del21, del22, del2, del3, del
      real(c_double) :: tmpg, tmpl

      ! conserved variables
      arhog = u(1); arhol = u(5)
      aMxg  = u(2); aMxl  = u(6)
      aMyg  = u(3); aMyl  = u(7)
      aEg   = u(4); aEl   = u(8)

      ! velocities
      ug = aMxg/arhog; vg = aMyg/arhog
      ul = aMxl/arhol; vl = aMyl/arhol

      ! specific internal energies
      Etg = aEg/arhog
      Etl = aEl/arhol

      eg = Etg - HALF*(ug*ug + vg*vg)
      el = Etl - HALF*(ul*ul + vl*vl)

      ! Pressure
      del1 = (arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + gamma_liq*pinf_liq)**2
      del21 = arhol*el*(gamma_liq-ONE)
      del22 = arhog*eg*(gamma_gas-ONE) + gamma_gas*pinf_gas - gamma_liq*pinf_liq
      del2  = TWO * del21*del22
      del3  = (arhol*el*(gamma_liq-ONE))**2

      del = del1 + del2 + del3

      tmpg = eg - q_gas
      tmpl = el - q_liq

      w(1) = HALF*( arhog*eg*(gamma_gas-ONE) - gamma_gas*pinf_gas + &
                    arhol*el*(gamma_liq-ONE) - gamma_liq*pinf_liq + sqrt(del) ) ! Pressure
      
      w(2) = (w(1) + gamma_gas*pinf_gas)/( (gamma_gas - ONE)*tmpg ) ! rho_gas
      w(3) = (w(1) + gamma_liq*pinf_liq)/( (gamma_liq - ONE)*tmpl ) ! rho_liq
 
      w(4) = arhog/w(2) ! alpha_gas
      w(5) = ONE-w(4)   ! alpha_liq

    end subroutine get_primitive

end module flux_module
