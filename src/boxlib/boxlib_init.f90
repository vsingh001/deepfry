module df_boxlib_module
  
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  
  use Boxlib
  use layout_module
  use multifab_module
  use parallel

  implicit none

  integer, save :: bl_DIM
  integer, save :: rho_comp, E_comp
  integer, save :: icomp_rho, icomp_vel, icomp_E
  integer, save :: icomp_proc
  integer, save :: icomp_mask, icomp_cell2elem

  integer, save :: n_plot_comps = 0
  integer, save :: n_comp = 0

  logical, allocatable, save, public :: pmask(:)

  character(len=20), allocatable :: bl_plot_names(:)

contains

  subroutine initialize_df_boxlib
    use input_module, only: dm_in

    bl_DIM = dm_in
    allocate(pmask(dm_in))
    pmask = .false.
    
    call boxlib_initialize()

    ! 2D/3D indexing distinguishing
    call bl_init_variables()
    
    ! get the number of plot variables
    call bl_init_plot_variables()
    
    ! Allocate the
    allocate(bl_plot_names(n_plot_comps))
    call bl_get_bl_plot_names(bl_plot_names)
    
  end subroutine initialize_df_boxlib
  
  function get_next_plot_index(num) result(next)
    integer :: num, next
    
    next = n_plot_comps + 1
    n_plot_comps = n_plot_comps + num
    return

  end function get_next_plot_index

  subroutine bl_init_variables

    rho_comp = 1
    if (bl_DIM .eq. 3) then
       n_comp = 5
       E_comp = 5
    else
       n_comp = 4
       E_comp = 4
    end if

  end subroutine bl_init_variables

  subroutine bl_init_plot_variables

    icomp_rho = get_next_plot_index(1)
    icomp_vel = get_next_plot_index(bl_DIM)
    icomp_E   = get_next_plot_index(1)

    icomp_proc = get_next_plot_index(1)
    icomp_mask = get_next_plot_index(1)
    icomp_cell2elem = get_next_plot_index(1)

  end subroutine bl_init_plot_variables

  subroutine bl_get_bl_plot_names(bl_plot_names)
    character(len=20), intent(inout) :: bl_plot_names(:)
    
    bl_plot_names(icomp_rho) = "rho"
    bl_plot_names(icomp_vel) = "xvel"
    bl_plot_names(icomp_vel+1) = "yvel"
    if (bl_DIM .eq. 3) then
       bl_plot_names(icomp_vel+2) = "zvel"
    end if
    
    bl_plot_names(icomp_E) = "E"

    ! processor number
    bl_plot_names(icomp_proc) = "Proc"

    ! mask & cell2elem
    bl_plot_names(icomp_mask) = "mask"
    bl_plot_names(icomp_cell2elem) = "cell2elem"

    print*, bl_plot_names, icomp_mask, n_plot_comps
    
  end subroutine bl_get_bl_plot_names

  subroutine shutdown_df_boxlib
    
    call layout_flush_copyassoc_cache()

    if ( parallel_IOProcessor() ) then    
       print*, 'BoxLib Memory At End of Program'
       print*,
    end if

    call print(multifab_mem_stats(),    "    multifab")
    call print(fab_mem_stats(),         "         fab")
    call print(boxarray_mem_stats(),    "    boxarray")
    call print(layout_mem_stats(),      "      layout")
    call print(boxassoc_mem_stats(),    "    boxassoc")
    call print(fgassoc_mem_stats(),     "     fgassoc")
    call print(syncassoc_mem_stats(),   "   syncassoc")
    call print(copyassoc_mem_stats(),   "   copyassoc")
    call print(fluxassoc_mem_stats(),   "   fluxassoc")

    ! deallocate stuff
    deallocate(pmask)
    deallocate(bl_plot_names)

    ! This calls MPI finalize
    call boxlib_finalize()

  end subroutine shutdown_df_boxlib
  
end module df_boxlib_module

