module boxlib_initialize_grid_module
  use deepfry_constants_module
  use df_boxlib_module
  use define_bc_module

  use ml_boxarray_module
  use multifab_module
  use ml_layout_module
  use ml_restrict_fill_module
  
  use iso_c_binding, only: c_int, c_double
  
  implicit none

contains

  subroutine initialize_with_adaptive_grids(mla, dx, Uold, the_bc_tower)
    use input_module

    use make_new_grids_module
    use initialize_module

    type(ml_layout), intent(out  ) :: mla
    real(c_double),  pointer       :: dx(:,:)
    type(multifab),  pointer       :: Uold(:)
    type(bc_tower),  intent(out  ) :: the_bc_tower

    ! locals
    type(ml_boxarray) :: mba

    type(layout)      :: la_array(max_levs)
    type(box)         :: bxs

    integer(c_int) :: n, ng_s, nl
    integer(c_int) :: lo(bl_DIM), hi(bl_DIM), nlevs
    logical        :: new_grid

    integer :: d

    type(multifab), pointer :: tag_mf(:)

    ! set up hi & lo for indexing
    lo = 0
    hi(1) = n_cellx - 1
    hi(2) = n_celly - 1
    if (bl_DIM .gt. 2) then
       hi(3) = n_cellz -1
    end if
    
    call ml_boxarray_build_n(mba, max_levs, bl_DIM)
    do n = 1, max_levs-1
       mba%rr(n,:) = ref_ratio
    end do

    ! Allocate the solution vector on the grid
    allocate(Uold(max_levs))

    ! build the level 1 boxarray
    call box_build_2(bxs, lo, hi)
    call boxarray_build_bx(mba%bas(1), bxs)
    call boxarray_maxsize(mba%bas(1), max_grid_size_1)

    ! problem domain
    mba%pd(1) = bxs
    do n = 2, max_levs
       mba%pd(n) = refine(mba%pd(n-1), mba%rr((n-1), :))
    end do

    ! initialize dx
    allocate(dx(max_levs, bl_DIM))
    do d = 1, bl_DIM
       dx(1, d) = (prob_hi(d) - prob_lo(d))/real(extent(mba%pd(1),d), kind=c_double)
    end do
    do n = 2, max_levs
       dx(n,:) = dx(n-1,:)/mba%rr(n-1,:)
    end do

    ! set the numbbe of ghost cells required by hydrodynamics stencil
    ng_s = 2

    ! initialize boundary conditions
    call initialize_bc(the_bc_tower, max_levs, pmask)

    ! Build the level 1 layout
    call layout_build_ba(la_array(1), mba%bas(1), mba%pd(1), pmask)

    ! define the bc tower at level 1
    call bc_tower_level_build(the_bc_tower, 1, la_array(1))

    nlevs = 1
    multilevel_initialization: if (max_levs .gt. 1) then
       ! build and initialize data on level 1
       call multifab_build(Uold(1), la_array(1), n_comp, ng_s)

       call init_data_on_level(1, Uold(1), dx(1,:), the_bc_tower%bc_tower_array(1), &
                               prob_lo)

       ! Fill level 1's ghost cells and apply boundary conditions
       call ml_restrict_and_fill(1, uold, mba%rr, the_bc_tower%bc_tower_array, &
            icomp=1, &
            bcomp=1, &
            nc=multifab_ncomp(Uold(1)), &
            ng=multifab_nghost(Uold(1)))


       new_grid = .true.
       nl = 1

       do while( (nl .lt. max_levs) .and. (new_grid) )
          level_gt_than1: if (nl .gt. 1) then
             ! build the multifab at level nl
             call multifab_build(Uold(nl), la_array(nl), n_comp, ng_s)

             ! define the bc tower at this level
             call bc_tower_level_build(the_bc_tower, nl, la_array(nl))

             ! initialize data at this level
             call init_data_on_level(nl, Uold(nl), dx(nl,:), &
                  the_bc_tower%bc_tower_array(nl), prob_lo)

             ! restrict and fill ghost cell data at this level
             call ml_restrict_and_fill(nl, Uold, mba%rr, the_bc_tower%bc_tower_array, &
                  icomp=1, &
                  bcomp=1, &
                  nc=multifab_ncomp(Uold(1)), &
                  ng=multifab_nghost(Uold(1)))
          end if level_gt_than1

          ! ALlocate tagging arrays
          allocate(tag_mf(max_levs))
          do n = 1, nl
             call build( tag_mf(n), la_array(n), 1, 0)
             call setval(tag_mf(n), ZERO, all=.true.)
             
             ! FIXME: Add tagging definition call here
          end do

          ! Do we need finer grids?
          if (nl .eq. 1) then
             call make_new_grids(new_grid, la_array(nl), la_array(nl+1), Uold(nl), dx(nl, 1), &
                  amr_buf_width, &
                  ref_ratio, &
                  lev=nl, &
                  max_grid_size=max_grid_size_2, &
                  aux_tag_mf=tag_mf(nl))
          else
             call make_new_grids(new_grid, la_array(nl), la_array(nl+1), Uold(nl), dx(nl, 1), &
                  amr_buf_width, &
                  ref_ratio, &
                  lev=nl, &
                  max_grid_size=max_grid_size_3, &
                  aux_tag_mf=tag_mf(nl))
          end if

          do n = 1, nl
             call destroy(tag_mf(nl))
          end do
          deallocate(tag_mf)

          ! build new grids if cells were tagged for refinement
          if (new_grid) then
             ! add the new level's boxes to the mba's boxarray list
             call copy(mba%bas(nl+1), get_boxarray(la_array(nl+1)))
             
             ! enforce proper nesting
             proper_nesting_enforcement: if(nl .ge. 2) then
                if (.not. ml_boxarray_properly_nested(mba, ng_s, pmask, 2, nl+1)) then
                   ! delete old multifabs to rebuild them
                   do n = 2, nl
                      call destroy(Uold(n))
                   end do

                   call enforce_proper_nesting(mba, la_array, max_grid_size_2, max_grid_size_3)

                   ! fix all lower level cells
                   do n = 2, nl
                      ! make sure bcs are properly defined everywhere
                      call bc_tower_level_build(the_bc_tower, n, la_array(n))
                      
                      ! rebuild and fill lower level data
                      call multifab_build(Uold(n), la_array(n), n_comp, ng_s)

                      ! fill physical region with valid data
                      call init_data_on_level(n, Uold(n), dx(n, :), &
                           the_bc_tower%bc_tower_array(n), prob_lo)
                   end do

                   call ml_restrict_and_fill(nl, Uold, mba%rr, the_bc_tower%bc_tower_array, &
                        icomp=1, bcomp=1, &
                        nc=multifab_ncomp(Uold(1)), &
                        ng=multifab_nghost(Uold(1)))

                end if
             end if proper_nesting_enforcement

             nlevs = nl+1
             nl = nl+1

          end if ! (if new_grid)
       end do

       ! destroy old multifabs
       do n = 1, nlevs-1
          call destroy(Uold(n))
       end do

       nlevs = nl

       ! final enforcement of proper nesting
       if (nlevs .ge. 3) then
          call enforce_proper_nesting(mba, la_array, max_grid_size_2, max_grid_size_3)
       end if

    end if multilevel_initialization ! if (maxlev > 1)

    ! rebuild the layout array
    do n = 1, nlevs
       call destroy(la_array(n))
    end do

    ! call layout restricted build
    call ml_layout_restricted_build(mla, mba, nlevs, pmask)

    nlevs = mla%nlevel
    ! if (nlevs .ne. max_levs) then
    !    call bl_error('initialize_with_adaptive_grids: nlevs .ne. max_levs not supported yet')
    ! end if

    ! this makes sure the boundary conditions are properly defined everywhere
    do n = 1,nlevs
       call bc_tower_level_build(the_bc_tower,n,mla%la(n))
    end do

    ! build states
    do n = 1,nlevs
       call multifab_build(uold(n), mla%la(n), n_comp, ng_s)
       call setval(uold(n), ZERO, all=.true.)
    end do

    ! init data on the multifabs
    call init_data(Uold,dx,the_bc_tower%bc_tower_array,mla,prob_lo)
    call destroy(mba)

  end subroutine initialize_with_adaptive_grids

  subroutine fill_mask(mesh, mla, mask)
    use mesh_module
    
    type(mesh2d),    intent(in) :: mesh
    type(ml_layout), intent(in) :: mla
    type(multifab),  pointer    :: mask(:)

    

  end subroutine fill_mask

end module boxlib_initialize_grid_module
