module df_boxlib_interface_module
  use iso_c_binding, only: c_double, c_int

  use deepfry_constants_module
  use df_boxlib_module

  use multifab_module
  use ml_layout_module
  use layout_module
  use box_module
  use fieldvar_module

  use mesh_module

  implicit none

contains

  subroutine allocate_interface_data(mesh, mla, dx, &
       fr_interface_data, bl_interface_data)
    type(mesh2d),    intent(in   ) :: mesh
    type(ml_layout), intent(inout) :: mla
    real(c_double),  intent(in   ) :: dx(:, :)

    type(fr_interface_data_t), intent(inout) :: fr_interface_data
    type(bl_interface_data_t), intent(inout) :: bl_interface_data

    allocate(fr_interface_data%face2box(mesh%Nbfaces))
    allocate(fr_interface_data%face2cell(mesh%Nbfaces, mesh%DIM))

    allocate(bl_interface_data%patch_index(mesh%Nbfaces))
    allocate(bl_interface_data%cell_index(mesh%Nbfaces, mesh%DIM))
    allocate(bl_interface_data%cell2elem(mesh%Nbfaces))    
    allocate(bl_interface_data%face_markers(mesh%Nbfaces))

  end subroutine allocate_interface_data

  subroutine deallocate_interface_data(fr_interface_data, bl_interface_data)
    type(fr_interface_data_t), intent(inout) :: fr_interface_data
    type(bl_interface_data_t), intent(inout) :: bl_interface_data

    deallocate(fr_interface_data%face2box)
    deallocate(fr_interface_data%face2cell)

    deallocate(bl_interface_data%patch_index)
    deallocate(bl_interface_data%cell_index)
    deallocate(bl_interface_data%cell2elem)
    deallocate(bl_interface_data%face_markers)

  end subroutine deallocate_interface_data

  subroutine set_interface_data(mesh, data, mla, dx, &
       mask, fr_interface_data, bl_interface_data)
    use input_module

    type(mesh2d),      intent(in   ) :: mesh
    type(FieldData_t), intent(inout) :: data
    type(ml_layout), intent(inout) :: mla
    real(c_double),  intent(in   ) :: dx(:, :)

    type(fr_interface_data_t), intent(inout) :: fr_interface_data
    type(bl_interface_data_t), intent(inout) :: bl_interface_data

    type(multifab), intent(inout) :: mask(:)

    ! locals
    integer(c_int) :: face
    real(c_double) :: xa(2), xb(2), xc(2)

    integer(c_int) :: i, j, k, cell_index(mesh%DIM)
    integer(c_int) :: ng, lo(mesh%DIM), hi(mesh%DIM)
    type(box)                       :: bx
    type(box_intersector), pointer  :: bi(:)

    real(c_double), pointer :: maskp(:, :, :, :)

    ng = multifab_nghost(mask(1))

    do face = 1, mesh%Nbfaces
       xa = mesh%points(mesh%xfaces(mesh%bfaces(face), 1), :)
       xb = mesh%points(mesh%xfaces(mesh%bfaces(face), 2), :)

       xc(1) = HALF * (xa(1) + xb(1)) + 1D-6*mesh%normals(mesh%bfaces(face), 1)
       xc(2) = HALF * (xa(2) + xb(2)) + 1D-6*mesh%normals(mesh%bfaces(face), 2)

       do i = 1, mla%dim
          cell_index(i) = floor((xc(i)-prob_lo(i))/dx(1, i)) + lwb(mla%la(1)%lap%pd, i)
       end do

       call build(bx, cell_index(1:mesh%DIM))
       bi => layout_get_box_intersector(mla%la(1),bx)

       ! set the BoxLib & FR interface data
       maskp => dataptr(mask(1), bi(1)%i)
       lo = lwb(get_box(mask(1), bi(1)%i))
       hi = upb(get_box(mask(1), bi(1)%i))

       i = lo(1) + mod(cell_index(1), max_grid_size)
       j = lo(2) + mod(cell_index(2), max_grid_size)
       k = 1

       fr_interface_data%face2box(face) = bi(1)%i     ! which patch to look for data
       fr_interface_data%face2cell(face,1) = i        ! local 'i' index
       fr_interface_data%face2cell(face,2) = j        ! local 'j' index

       bl_interface_data%patch_index(face)  = bi(1)%i 
       bl_interface_data%cell_index(face,1) = i
       bl_interface_data%cell_index(face,2) = j

       ! which fr element to look for data in
       bl_interface_data%cell2elem(face)   = mesh%face2elem(mesh%bfaces(face), 1)

       ! what interface cell are we?. Defined by FR mesh boundary
       ! markers (left, right, top, bottom, front, back)
       bl_interface_data%face_markers(face) = mesh%face_markers(mesh%bfaces(face))

       ! storing the pertinent information in the mask multifab as well
       maskp(i, j, k, 1) = mesh%face_markers(mesh%bfaces(face))
       maskp(i, j, k, 2) = mesh%face2elem(mesh%bfaces(face), 1)

       deallocate(bi)
       
    end do
    
  end subroutine set_interface_data

  subroutine get_BoxLib_interface_common_solution_values(mesh, data, beta)
    use input_module, only: left_marker, right_marker, top_marker, bottom_marker, &
                            front_marker, back_marker
    use mesh_module

    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    real(c_double), optional,       intent(in   ) :: beta

    ! locals
    integer(c_int) :: j, tmp_index, glb_face_index, left_face_index, left
    integer(c_int) :: elem2face_left(mesh%elem_num_faces), face2elem(2), left_indices(2)
    real(c_double) :: rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost

    real(c_double), pointer :: Up(:, :, :, :)
    integer(c_int) :: ci, cj, ck

    left_face_index = -1

    do tmp_index = 1, mesh%Nbfaces
       glb_face_index    = mesh%bfaces(tmp_index)
       face2elem         = mesh%face2elem(glb_face_index, :)
       left              = face2elem(1)
       elem2face_left(:) = mesh%elem2face(left, :)
       
       do j = 1, mesh%elem_num_faces
          if(elem2face_left(j) .eq. glb_face_index) left_face_index = j
       end do
       left_indices(1) = mesh%P1*(left_face_index-1)+1
       left_indices(2) = mesh%P1*left_face_index

       if (mesh%face_markers(glb_face_index) .eq. left_marker .or. &
            mesh%face_markers(glb_face_index) .eq. right_marker .or. &
            mesh%face_markers(glb_face_index) .eq. top_marker .or. &
            mesh%face_markers(glb_face_index) .eq. bottom_marker .or. &
            mesh%face_markers(glb_face_index) .eq. front_marker .or. &
            mesh%face_markers(glb_face_index) .eq. back_marker) then
          
          ! get the indices for the BoxLib cell adjacent to this face
          Up => dataptr(mesh%BoxLib_data%bl_Uold(1), &
                        mesh%BoxLib_data%fr_interface_data%face2box(tmp_index))

          ci = mesh%BoxLib_data%fr_interface_data%face2cell(tmp_index, 1)
          cj = mesh%BoxLib_data%fr_interface_data%face2cell(tmp_index, 2)
          ck = 1

          do j = 1, mesh%P1
             ! ghost values read from the BoxLib cell
             rhoghost = Up(ci, cj, ck, 1)
             Vxghost  = Up(ci, cj, ck, 2)/rhoghost
             Vyghost  = Up(ci, cj, ck, 3)/rhoghost
             Vzghost  = ZERO
             Eghost   = Up(ci, cj, ck, 4)

             data%ucommon(left_indices(1) + (j-1), 1, left) = rhoghost
             data%ucommon(left_indices(1) + (j-1), 2, left) = rhoghost*Vxghost
             data%ucommon(left_indices(1) + (j-1), 3, left) = rhoghost*Vyghost
             data%ucommon(left_indices(1) + (j-1), 4, left) = Eghost

#ifdef DIM3
             Vzghost = Up(ci, cj, ck, 4)/rhoghost
             Eghost  = Up(ci, cj, ck, 5)

             data%ucommon(left_indices(1) + (j-1), 4, left) = rhoghost*Vzghost
             data%ucommon(left_indices(1) + (j-1), 5, left) = Eghost
#endif
          end do
          
       end if
    end do

  end subroutine get_BoxLib_interface_common_solution_values

  subroutine patch_print(mesh, U, mask, mla, dx)
    use input_module

    type(mesh2d),    intent(in) :: mesh
    type(multifab),  intent(in) :: U(:)
    real(c_double),  intent(in) :: dx(:,:)
    type(multifab),  intent(inout) :: mask(:)
    type(ml_layout), intent(inout) :: mla

    integer(c_int) :: n, patch
    integer(c_int) :: lo(mla%dim), hi(mla%dim)

    integer(c_int) :: face
    real(c_double) :: xa(2), xb(2), xc(2)

    do n = 1, mla%nlevel
       do patch = 1, nfabs(U(n))

          lo = lwb(get_box(U(n), patch))
          hi = upb(get_box(U(n), patch))

          !print*, 'BoxLib: ', n, patch, lo, hi
       end do
    end do

  end subroutine patch_print

end module df_boxlib_interface_module
