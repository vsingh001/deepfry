module bl_plot_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use input_module
  
  use df_boxlib_module
  
  implicit none

contains
  !---------------------------------------------------------------------------
  ! make_processor_number
  !---------------------------------------------------------------------------
  subroutine make_processor_number(plotdata,comp_proc)

    type(multifab), intent(inout) :: plotdata
    integer,        intent(in   ) :: comp_proc
    
    real(kind=dp_t), pointer :: pp(:,:,:,:)
    integer :: i

    do i = 1, nfabs(plotdata)
       pp => dataptr(plotdata, i)
       pp(:,:,:,comp_proc) = parallel_myproc()
    enddo
  end subroutine make_processor_number

  subroutine make_bl_plotfile(dirname, mla, U, mask, plot_names, mba, dx, dt, &
                              the_bc_tower, write_pf_time, time)
    use layout_module
    use multifab_module
    use parallel
    use define_bc_module
    use fabio_module
    use ml_layout_module
    use ml_boxarray_module

    character(len=*) , intent(in   ) :: dirname
    type(ml_layout)  , intent(in   ) :: mla
    type(multifab)   , intent(in   ) :: U(:)
    type(multifab)   , intent(in   ) :: mask(:)
    character(len=20), intent(in   ) :: plot_names(:)
    type(ml_boxarray), intent(in   ) :: mba
    real(c_double),    intent(in   ) :: dt,time,dx(:,:)
    type(bc_tower),    intent(in   ) :: the_bc_tower
    real(c_double),    intent(out  ) :: write_pf_time

    ! locals
    type(multifab) :: plotdata(mla%nlevel)

    integer :: n, prec, dm, nlevs

    real(c_double) :: writetime1, writetime2

    dm    = mla%dim
    nlevs = mla%nlevel

    prec = FABIO_DOUBLE

    do n = 1, nlevs
       ! build the plotting multifabs
       call multifab_build(plotdata(n), mla%la(n), n_plot_comps, 0)
       
       ! Density
       call multifab_copy_c(plotdata(n), icomp_rho, U(n), rho_comp, 1)
       
       ! Velocity
       call multifab_copy_c(plotdata(n), icomp_vel, U(n), 2, dm)

       ! Energy
       call multifab_copy_c(plotdata(n), icomp_E, U(n), E_comp, 1)

       ! Processor number
       call make_processor_number(plotdata(n), icomp_proc)

       ! Mask
       call multifab_copy_c(plotdata(n), icomp_mask, mask(n), 1, 1)
       call multifab_copy_c(plotdata(n), icomp_cell2elem, mask(n), 2, 1)       
       
    end do

    if (parallel_IOProcessor()) then
       write(6, *) 'BoxLib: Writing state to plotfile ', trim(dirname)
    end if

    writetime1 = parallel_wtime()

    call fabio_ml_multifab_write_d(plotdata, mba%rr(:, 1), dirname, plot_names, &
         mba%pd(1), prob_lo, prob_hi, time, dx(1, :), &
         nOutFiles=nOutFiles, &
         lUsingNFiles=lUsingNFiles, prec=prec)

    writetime2 = parallel_wtime() - writetime1
    call parallel_reduce(writetime1, writetime2, MPI_MAX, proc=parallel_IOProcessorNode())
    if (parallel_IOProcessor()) then
       print*,'Time to write plotfile: ',writetime1,' seconds'
       print*,''
    end if

    write_pf_time = writetime1

    do n = 1, nlevs
       call destroy(plotdata(n))
    end do

  end subroutine make_bl_plotfile

end module bl_plot_module
