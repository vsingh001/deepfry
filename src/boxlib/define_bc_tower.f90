module define_bc_module

  use iso_c_binding, only: c_int, c_double
  use box_module, only: box
  use ml_layout_module
  use bc_module

  implicit none

  type bc_level

     integer, pointer :: phys_bc_level_array(:,:,:)   => Null()
     integer, pointer :: adv_bc_level_array( :,:,:,:) => Null()
     integer, pointer :: ell_bc_level_array( :,:,:,:) => Null()

  end type bc_level

  type bc_tower

     integer :: max_level_built = 0

     ! an array of bc_levels, one for each level of refinement
     type(bc_level), pointer :: bc_tower_array(:) => Null()

     ! 1st index is the direction (1=x, 2=y, 3=z)
     ! 2nd index is the side (1=lo, 2=hi)
     integer       , pointer :: domain_bc(:,:) => Null()

  end type bc_tower

  private

  interface build
     module procedure bc_tower_init
     module procedure bc_tower_level_build
  end interface build

  interface destroy
     module procedure bc_tower_destroy
  end interface destroy

  public :: bc_level, bc_tower, bc_tower_init, bc_tower_level_build, &
       bc_tower_destroy, initialize_bc, build, destroy

contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   subroutine initialize_bc(the_bc_tower,num_levs,pmask)

    use bc_module
    use bl_error_module
    use input_module

    type(bc_tower), intent(out  ) :: the_bc_tower
    integer       , intent(in   ) :: num_levs
    logical       , intent(in   ) :: pmask(:)
    
    integer :: domain_phys_bc(size(pmask),2), dm

    dm = size(pmask)

    ! Define the physical boundary conditions on the domain
    ! Put the bc values from the inputs file into domain_phys_bc
    domain_phys_bc(1,1) = bc_x_lo
    domain_phys_bc(1,2) = bc_x_hi
    if (pmask(1)) then
       domain_phys_bc(1,:) = BC_PER
       if (bc_x_lo .ne. -1 .or. bc_x_hi .ne. -1) &
            call bl_error('MUST HAVE BCX = -1 if PMASK = T')
    end if
    if (dm > 1) then
       domain_phys_bc(2,1) = bc_y_lo
       domain_phys_bc(2,2) = bc_y_hi
       if (pmask(2)) then
          domain_phys_bc(2,:) = BC_PER
          if (bc_y_lo .ne. -1 .or. bc_y_hi .ne. -1) &
               call bl_error('MUST HAVE BCY = -1 if PMASK = T') 
       end if
    end if
    if (dm > 2) then
       domain_phys_bc(3,1) = bc_z_lo
       domain_phys_bc(3,2) = bc_z_hi
       if (pmask(3)) then
          domain_phys_bc(3,:) = BC_PER
          if (bc_z_lo .ne. -1 .or. bc_z_hi .ne. -1) &
               call bl_error('MUST HAVE BCZ = -1 if PMASK = T')
       end if
    end if
    
    ! Initialize the_bc_tower object.
    call bc_tower_init(the_bc_tower,num_levs,dm,domain_phys_bc)
    
  end subroutine initialize_bc

  subroutine bc_tower_init(bct,num_levs,dm,phys_bc_in)

    type(bc_tower), intent(out) :: bct
    integer(c_int), intent(in ) :: num_levs
    integer(c_int), intent(in ) :: dm
    integer(c_int), intent(in ) :: phys_bc_in(:,:)

    allocate(bct%bc_tower_array(num_levs))
    allocate(bct%domain_bc(dm,2))

    bct%domain_bc(:,:) = phys_bc_in(:,:)

  end subroutine bc_tower_init

  subroutine bc_tower_level_build(bct,n,la)
    use df_boxlib_module

    type(bc_tower ), intent(inout) :: bct
    integer        , intent(in   ) :: n
    type(layout)   , intent(in   ) :: la

    integer :: ngrids, dm
    integer :: default_value

    if (associated(bct%bc_tower_array(n)%phys_bc_level_array)) then
      deallocate(bct%bc_tower_array(n)%phys_bc_level_array)
      deallocate(bct%bc_tower_array(n)%adv_bc_level_array)
      deallocate(bct%bc_tower_array(n)%ell_bc_level_array)
      bct%bc_tower_array(n)%phys_bc_level_array => NULL()
      bct%bc_tower_array(n)%adv_bc_level_array => NULL()
      bct%bc_tower_array(n)%ell_bc_level_array => NULL()
    end if

    ngrids = layout_nlocal(la)
    dm = layout_dim(la)

    allocate(bct%bc_tower_array(n)%phys_bc_level_array(0:ngrids,dm,2))
    default_value = INTERIOR
    call phys_bc_level_build(bct%bc_tower_array(n)%phys_bc_level_array,la, &
                             bct%domain_bc,default_value)

    allocate(bct%bc_tower_array(n)%adv_bc_level_array(0:ngrids,dm,2,n_comp))
    default_value = INTERIOR
    call adv_bc_level_build(bct%bc_tower_array(n)%adv_bc_level_array, &
                            bct%bc_tower_array(n)%phys_bc_level_array,default_value)

    allocate(bct%bc_tower_array(n)%ell_bc_level_array(0:ngrids,dm,2,n_comp))
    default_value = BC_INT
    call ell_bc_level_build(bct%bc_tower_array(n)%ell_bc_level_array, &
                            bct%bc_tower_array(n)%phys_bc_level_array,default_value)

     bct%max_level_built = n

  end subroutine bc_tower_level_build

  subroutine phys_bc_level_build(phys_bc_level,la_level,domain_bc,default_value)

    integer     , intent(inout) :: phys_bc_level(0:,:,:)
    integer     , intent(in   ) :: domain_bc(:,:)
    type(layout), intent(in   ) :: la_level
    integer     , intent(in   ) :: default_value
    type(box) :: bx,pd
    integer :: i,d

    pd = layout_get_pd(la_level) 

    phys_bc_level = default_value

    ! phys_bc_level(0:nlocal,dim,2) contains the physical name of the
    ! boundary condition type on each edge of each box that makes up
    ! the current level.  The phys_bc_level(0,:,:) 'box' refers to the
    ! entire domain.  If an edge of a box is not on a physical
    ! boundary, then it is set to default_value (typically INTERIOR).
    !
    ! These boundary condition types are used to interpret the actual
    ! method to fill the ghostcells for each variable, as described in
    ! the adv_bc_level and ell_bc_level arrays.

    i = 0
    do d = 1,layout_dim(la_level)
       phys_bc_level(i,d,1) = domain_bc(d,1)
       phys_bc_level(i,d,2) = domain_bc(d,2)
    end do

    ! loop over individual grids
    do i = 1,layout_nlocal(la_level)    ! loop over grids
       bx = layout_get_box(la_level,global_index(la_level,i))  ! grab box associated with the grid
       do d = 1,layout_dim(la_level)    ! loop over directions
          ! if one side of a grid is a domain boundary, set the 
          ! physical boundary condition
          if (lwb(bx,d) == lwb(pd,d)) phys_bc_level(i,d,1) = domain_bc(d,1)
          if (upb(bx,d) == upb(pd,d)) phys_bc_level(i,d,2) = domain_bc(d,2)
       end do
    end do

    ! do i = 1,layout_nlocal(la_level)
    !    bx = layout_get_box(la_level,global_index(la_level,i))
    !    do d = 1,layout_dim(la_level)
    !       if (bx%lo(d) == pd%lo(d)) phys_bc_level(i,d,1) = domain_bc(d,1)
    !       if (bx%hi(d) == pd%hi(d)) phys_bc_level(i,d,2) = domain_bc(d,2)
    !    end do
    ! end do

  end subroutine phys_bc_level_build

  subroutine adv_bc_level_build(adv_bc_level,phys_bc_level,default_value)
    use df_boxlib_module

    integer  , intent(inout) :: adv_bc_level( 0:,:,:,:)
    integer  , intent(in   ) :: phys_bc_level(0:,:,:)
    integer  , intent(in   ) :: default_value

    integer :: dm
    integer :: igrid, d, lohi

    ! set the default value
    adv_bc_level = default_value

    dm = size(adv_bc_level,dim=2)

    do igrid=0,size(adv_bc_level,dim=1)-1         ! loop over grids
       do d=1,dm                                  ! loop over directions
          do lohi=1,2                             ! loop over lo/hi side

             if (phys_bc_level(igrid,d,lohi) == OUTLET) then
                
                adv_bc_level(igrid,d,lohi, rho_comp) = FOEXTRAP
                adv_bc_level(igrid,d,lohi, 2:2+dm)   = FOEXTRAP
                adv_bc_level(igrid,d,lohi, E_comp)   = FOEXTRAP
             end if
             
          end do
       end do
    end do
  end subroutine adv_bc_level_build

  subroutine ell_bc_level_build(ell_bc_level,phys_bc_level,default_value)

    integer  , intent(inout) ::  ell_bc_level(0:,:,:,:)
    integer  , intent(in   ) :: phys_bc_level(0:,:,:)
    integer,   intent(in   ) :: default_value

    integer :: dm
    integer :: igrid,d,lohi

    ell_bc_level = BC_INT
 
    dm = size(ell_bc_level,dim=2)

    ! if the physical boundary conditions is something other than
    ! INTERIOR, then overwrite the default value
    do igrid=0,size(ell_bc_level,dim=1)-1   ! loop over grids
    do d=1,dm                               ! loop over directions
    do lohi=1,2                             ! loop over lo/hi side

       if (phys_bc_level(igrid,d,lohi) == INLET) then

          call bl_error("define_bc_tower.f90: INLET not supported for this example")

       else if (phys_bc_level(igrid,d,lohi) == OUTLET) then

          ell_bc_level(igrid,d,lohi,1) = BC_NEU

       else if (phys_bc_level(igrid,d,lohi) == SYMMETRY) then

          call bl_error("define_bc_tower.f90: SYMMETRY not supported for this example")

       else if (phys_bc_level(igrid,d,lohi) == SLIP_WALL) then

          call bl_error("define_bc_tower.f90: SLIP_WALL not supported for this example")

       else if (phys_bc_level(igrid,d,lohi) == NO_SLIP_WALL) then

          ell_bc_level(igrid,d,lohi,1) = BC_DIR

       else if (phys_bc_level(igrid,d,lohi) == PERIODIC) then

          ell_bc_level(igrid,d,lohi,1) = BC_PER

       end if

    end do
    end do
    end do

  end subroutine ell_bc_level_build

  subroutine bc_tower_destroy(bct)

    type(bc_tower), intent(inout) :: bct

    integer :: i,num_levs

    num_levs = size(bct%bc_tower_array,dim=1)

    do i = 1,num_levs
       deallocate(bct%bc_tower_array(i)%phys_bc_level_array)
       deallocate(bct%bc_tower_array(i)%adv_bc_level_array)
       deallocate(bct%bc_tower_array(i)%ell_bc_level_array)
       bct%bc_tower_array(i)%phys_bc_level_array => NULL()
       bct%bc_tower_array(i)%adv_bc_level_array => NULL()
       bct%bc_tower_array(i)%ell_bc_level_array => NULL()
    end do
    deallocate(bct%bc_tower_array)
    deallocate(bct%domain_bc)

  end subroutine bc_tower_destroy

end module define_bc_module
