"""2D Grid data structures for conservation laws. The basic meshing
module is :pymodule:`meshpy` which produces triangular meshes for
arbitrary 2D domains.

I use the MeshPy's representation of the mesh, which is basically a
bunch of arrays, and construct Face2D and Element2D objects from
it. This can be viewed as a pre processing step in which the mesh
information is transformed into nice (at least I think so) Python
objects which are easier to work with.

For example, each Face2D object has two elements (left and right) that
adjoin the face. Simillarly, each element has a list of Face2D objects
for the faces making up the element. Higher up, a Grid2D object has a
list of all the elements and unique faces comprising the mesh. We can
now iterate over and compute fluxes in the same way as for a 1D
structured mesh.

I follow the cell-centered approach and physical variables are
eassigned to the element centroid of the triangulation.

"""

import numpy
from numpy import ma
from matplotlib.mlab import find
import polynomials as poly

from matvec import Vertex, Vector

class FaceType:
    Interior = 0
    Boundary = 100
    Transmissive = 101
    SolidWall = 102
    Inflow = 11
    Outflow = 12

class ElementType:
    Interior = 0
    Ghost = 1000

class Boundary:
    id = 100

class TransmissiveBoundary(Boundary):
    id = 101

class ReflectiveBoundary(Boundary):
    id = 102

class MeshType:
    MeshPy = 0

class Face(object):
    """An element face in a finite volume mesh.

    Data Attributes:
    ----------------

    vertices : list
         A list of two vertices for the face

    normal : Vector
        Normal vector for the face

    left : Element
        A reference to the element/triangle to the left of the face

    right : Element
        A reference to the element/triangle to the right of the face

    Notes:
    ------

    The normal to a face is computed by taking the following cross
    product:

    normal = vertices[1] - vertices[0] X k

    This is because we assume that as we move from vertex 1 to vertex
    2 of the face, the element to which the face belongs lies to the
    left.

    An element/cell face separates two adjoining finite volume
    cells. The left and right elements for the face is defined with
    respect to the direction of the face normal. The element that lies
    in the direction of the face normal is called the right element
    and the other one is called the left element.

    """

    def __init__(self, mesh, index, x, y, nvar, left, right, marker=0):
        """Constructor for the Face.

        **Parameters**

        mesh : meshpy.MeshInfo
            Underlying meshpy mesh object.

        index : int
            Integer index for the face. This indexes into the MeshPy face
            list.

        x, y : array (2, 1)
            Coordinates for the face.

        nvar : int
            Number of variables for the flux definition

        left, right : Element
            Adjoining elements for the face.

        marker : int
            Identifier for the face.

        """
        self._mesh = mesh
        self.index = index

        self.nnodes = nnodes = len(x)
        self.nodes = nodes = []
        for i in range( nnodes ):
            nodes.append( Vertex(x[i], y[i]) )

        self.x = x; self.y = y
        self.marker = marker

        self.left = left; self.right = right

        self.nvar = nvar
        self.flux = numpy.zeros(shape=(nvar))

        # setup face attributes like normal and tangents
        self._setup_attributes()

    def _setup_attributes(self):
        x = self.x; y = self.y

        # set the face centers
        self.xc = 0.5 * numpy.sum( x )
        self.yc = 0.5 * numpy.sum( y )

        # set the normal and tangent
        delx = x[1] - x[0]
        dely = y[1] - y[0]

        self.tangent = tangent = Vertex( delx, dely );
        tangent.normalize()

        self.normal = normal = Vertex( dely, -delx )
        normal.normalize()
    
class Element(object):
    """A general mesh element in a finite volume method.

    In 2D, a triangulation creates triangles in which conservation is
    enforced. These triangles are used as the elements.

    We use the MeshPy package to generate the triangulations. The
    triangle data structure is a list of three vertices and references
    to the three neighboring triangles.

    The naming convention for the triangles is as follows

                              0
                             /\
                            /  \
                           /    \ 
                  face 0  /      \ face 2 neighbor 1
              neighbor 2 /        \
                       _/__________\_
                      1   face 1     2
                          neighbor 0


    Data Attributes:
    ----------------

    vertices : list
         The three Vertex objects representing the vertices

    neighbors : list
         Three Element objects representing the three neighbor

    faces : list
         A list of three faces for the triangle

    """

    def __init__(self, mesh, index, x, y, q, w, marker=0,
                 id=ElementType.Interior):
        """Constructor for the Element.

        **Parameters**

        mesh : meshpy.MeshInfo
            Underlying MeshPy mesh

        index : int
            Integer index for the element. This indexes into the mesh data
            structure.

        x, y : array (n, 1)
            Coordinate arrays for the element nodes. The ordering of the
            nodes are according to the diagram above.

        q, w : array (nvar, 1)
            Vector of conserved and primitive variables.

        marker : int
            Identifier for the element.            

        """
        self._mesh = mesh
        self.index = index

        self.id = id

        # setup the list of vertices for this element
        self.vertices = vertices = []
        self.nvertices = nvertices = len(x)

        for i in range(nvertices):
            vertices.append( Vertex( x[i], y[i] ) )

        # conserved and primitive variables
        self.q = q; self.w = w

        self.x = x; self.y = y
        self.marker = marker

        # initialize the list of faces
        self.faces = []

        # the list of neighbors
        self.neighbors = []

        # setup the element attributes
        self._setup_attributes()

    def _setup_attributes(self):
        """Setup element attributes like centroid etc"""
        self.xc = numpy.sum( self.x )/3.0
        self.yc = numpy.sum( self.y )/3.0

class Grid2D(object):
    """A 2D grid data structure generated from MeshPy's triangulation.

    **Data Attributes**

    _mesh : meshpy.MeshInfo
        Underlying MeshPy mesh representation. 

    elements : list
        A list of Element objects which compise the mesh

    faces : list
        A list Face objects which comprise the mesh. Only unique faces
        are stored.

    nvar : int
        Number of variables. Values are assumed to be  averages at
        the element centroid. For example, the Euler equations in 2D
        would have 4 variables.

    nelements : int
        Number of elements (triangles) for this mesh

    q, w : numpy.ndarray (nvar, nelements)
        Linear array for the conserved and primitive variables.
        Views of this array are stored with the individual elements.    

    """
    def __init__(self, mesh, nvar, N=1, diffusiion=False):
        """Generate the grid from a MeshPy mesh"""
        self.N = N
        self.Np = Np = int(0.5 * (N+1)*(N+2))
        self.Nfp = Nfp = 3*(N+1)
        self.nvar = nvar
        self._mesh = mesh

        self.diffusiion = diffusiion

        # initialize the boundary faces list
        self.transmissive_boundaries = []
        self.reflective_boundaries = []

        # create the vector of variables and the transformation
        # matrices and vectors
        self.r, self.s, self.V = poly.get_vandermode2d(N)
        self.a, self.b = poly.rs2ab(self.r, self.s)

        self.pa,  self.pb = poly.get_jacobi_polynomials2d(N)
        self.dpa, self.dpb = poly.get_grad_jacobi_polynomials2d(N)

        self.Vinv = numpy.linalg.inv(self.V)
        self.dr, self.ds = poly.get_grad_vandermode2d(N)
       
        # Create the mesh structure. At this point the total number of
        # elements including ghost cells are set
        self._setup()

        # compute the Jacobian of the transformation now that
        # information about the triangles is available
        self._compute_geometric_factors()

        # location of the flux points along each face of the triangle
        # in local coordinates. Also evaluate the modal basis at these
        # flux points which will be used to interpolate element
        # solution values later in the FR procedure.
        self._setup_flux_points()
        self._evaluate_modal_basis_at_flux_points()
        self._evaluate_modal_basis_at_solution_points()

        self._evaluate_grad_modal_basis_at_flux_points()
        self._evaluate_grad_modal_basis_at_solution_points()

        # transformed face normals
        self._setup_transformed_element_normals()
        
        # storage space for the solution vectors and fluxes
        self.u = numpy.zeros( shape=(self.nelements, Np, nvar) )
        self.uavg = numpy.zeros( shape=(self.nelements, nvar) )

        # interpolated values, common solution values and correction
        # function at the flux points
        self.uflux = numpy.zeros(shape=(self.nelements, Nfp, nvar))
        self.ucommon = numpy.zeros( shape=(self.nelements, Nfp, nvar) )
        self.u_correction = numpy.zeros( shape=(self.nelements, Nfp, nvar) )

        # gradients at solution and flux points
        self.ux_solution = numpy.zeros( shape=(self.nelements, Np, nvar) )
        self.uy_solution = numpy.zeros( shape=(self.nelements, Np, nvar) )

        self.ux_avg = numpy.zeros( shape=(self.nelements, nvar) )
        self.uy_avg = numpy.zeros( shape=(self.nelements, nvar) )

        self.ux_flux = numpy.zeros( shape=(self.nelements, Nfp, nvar) )
        self.uy_flux = numpy.zeros( shape=(self.nelements, Nfp, nvar) )

        # discontinuous flux at the solution points
        self.Fd_solution = numpy.zeros( shape=(self.nelements, Np, nvar) )
        self.Gd_solution = numpy.zeros( shape=(self.nelements, Np, nvar) )

        # discontinuous flux at the flux points
        self.Fd_flux = numpy.zeros( shape=(self.nelements, Nfp, nvar) )
        self.Gd_flux = numpy.zeros( shape=(self.nelements, Nfp, nvar) )

        # interaction flux at the flux points
        self.Fi = numpy.zeros( shape=(self.nelements, Nfp, nvar) )
        self.Gi = numpy.zeros( shape=(self.nelements, Nfp, nvar) )

        # correction flux at the flux points
        self.Fc = numpy.zeros( shape=(self.nelements, Nfp, nvar) )
        self.Gc = numpy.zeros( shape=(self.nelements, Nfp, nvar) )

        # rhs-stage vectors
        self.k1 = numpy.zeros( shape=(self.nelements, Np, nvar) )

    def get_element_vertices(self, index):
        """Return the coordinates (x, y) for an element"""
        vertices = self.elements[ index ]
        return self.points[ vertices ]

    def get_element_solution_points(self,index):
        vertices = self.get_element_vertices(index)
        xv = vertices[:,0]; yv = vertices[:,1]

        return poly.rs2xy(self.r, self.s, xv, yv)

    def get_flux_points(self, elem, face=None):
        xe = self.get_element_vertices(elem)
        r, s = self.elem_rs[elem]

        if face is not None:
            r = r[face]
            s = s[face]
        else:
            r = r.ravel()
            s = s.ravel()

        return poly.rs2xy(r, s, xe[:,0], xe[:, 1])

    def get_element_area(self, index):
        vertices = self.get_element_vertices(index)
        xv = vertices[:,0]; yv = vertices[:,1]

        return 0.5 * abs( xv[0]*(yv[2] - yv[1]) + xv[1]*(yv[0] - yv[2]) + xv[2]*(yv[1] - yv[0]) )

    def get_ghost_cells(self):
        """Return indices of ghost cells"""
        return (find( self.neighbors == -1 ) / 3)[::2]

    def get_faces(self, marker=0):
        """Return indices of faces adjoining domain boundaries.

        Parameters:
        -----------

        marker : int
            Marker type for the face

        The marker type could be any of the FaceType identifiers.

        Example : To get all transmissive boundaries

        grid.get_faces(marker=FaceType.Transmissive)

        """
        return find( self.face_markers == marker )

    def step(self, dt):
        """Update the soltution to the next time step"""
        nelements = self.nelements

        # interpolate solution values at flux points
        self.interpolate_at_flux_points()

        # compute the common solution values
        self.get_common_solution_values()

        # compute the auxillary function at solution and flux points
        self.get_gradients_at_solution_points()
        self.get_gradients_at_flux_points()

        # compute the discontinuous flux
        self.get_discontinuous_flux()

        # compute the interaction flux
        self.get_interaction_flux()

        # compute the flux divergence
        self.flux_divergence()

        # update the solutionx
        self.u[:] = self.u[:] + dt*self.k1[:]

    def interpolate_at_flux_points(self):
        nelements = self.nelements

        V = self.V; Vinv = self.Vinv

        u = self.u
        uflux = self.uflux
        psi_flux_points = self.psi_flux_points

        for elem in range(nelements):
            # get the modal representation of the solution
            umodal = Vinv.dot(u[elem])

            # interpolate at the flux points
            uinterp = psi_flux_points[elem].dot(umodal)

            # store the interpolated values
            uflux[elem,:] = uinterp

    def get_common_solution_values(self):
        nelements = self.nelements

        uflux = self.uflux
        ucommon = self.ucommon
        u_correction = self.u_correction

        face2elem = self.face2elem
        elem2face = self.elem2face

        face_normals = self.face_normals

        Np1 = self.N+1
        for elem in range(nelements-self.nghost):
            for face in range(3):
                
                # get the index for this face
                eface = elem2face[elem][face]

                # get the left and right elements abutting this face
                abutting_elements = face2elem[eface]
                left = abutting_elements[0]; right = abutting_elements[1]

                # solution local to this element
                uminus = uflux[elem, face*Np1:(face+1)*Np1, :]
                
                # solution external to this element
                if left == elem:
                    nbr_elem = right
                else:
                    nbr_elem = left
                    
                # get the face index for the neighboring element and
                # the corresponding flux points along the face
                nbr_face_index = numpy.where(elem2face[nbr_elem] == eface)[0]
                uplus = uflux[nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]

                # store the common solution values for this face
                ucommon[elem, face*Np1:(face+1)*Np1] = 0.5 * ( uminus + uplus - (uminus - uplus) )

                # store the correction function
                u_correction[elem, face*Np1:(face+1)*Np1, :] = ucommon[elem, face*Np1:(face+1)*Np1] -\
                                                               uflux[  elem, face*Np1:(face+1)*Np1]

    def get_gradients_at_solution_points(self):
        Np1 = self.N+1
        nelements = self.nelements

        xr = self.xr; xs = self.xs
        yr = self.yr; ys = self.ys

        psi_solution = self.psi_solution_points
        psi_flux = self.psi_flux_points

        psi_r = self.psi_r_solution
        psi_s = self.psi_s_solution

        u = self.u
        ux = self.ux_solution
        uy = self.uy_solution

        u_correction = self.u_correction

        Jn = self.Jn
        Vinv = self.Vinv

        transformed_normals = self.transformed_element_normals
        sigmas = self.sigmas
        
        for var in range(self.nvar):
            for elem in range(nelements):

                # gradient of the discontinuous solution 
                umodal = Vinv.dot(u[elem, :, var])
                qr = psi_r.dot(umodal)
                qs = psi_s.dot(umodal)
                
                # compute the correction from the flux points
                #sigma = psi_flux[elem].T

                sigma = sigmas[elem].T
                psifj = psi_solution.dot(sigma)

                nr, ns = transformed_normals[elem]
                nr = u_correction[elem, :, var]*nr
                ns = u_correction[elem, :, var]*ns

                #_qr[:] = 0.0; _qs[:] = 0.0
                #for j in range(3*Np1):
                #    _qr += nr[j]*psifj[:,j]
                #    _qs += ns[j]*psifj[:,j]

                _qr = psifj.dot(nr)
                _qs = psifj.dot(ns)

                qr = qr + _qr
                qs = qs + _qs

                # get the gradient in physical space
                J = xr[elem]*ys[elem] - xs[elem]*yr[elem]
                rx = +ys[elem]/J; ry = -xs[elem]/J
                sx = -yr[elem]/J; sy = +xr[elem]/J

                ux[elem, :, var] = rx*qr + sx*qs
                uy[elem, :, var] = ry*qr + sy*qs

    def get_gradients_at_flux_points(self):
        nelements = self.nelements

        xr = self.xr; xs = self.xs
        yr = self.yr; ys = self.ys

        psi_r = self.psi_r_flux
        psi_s = self.psi_s_flux

        psi_solution = self.psi_solution_points
        psi_flux = self.psi_flux_points

        u = self.u
        ux = self.ux_flux
        uy = self.uy_flux

        u_correction = self.u_correction

        Jn = self.Jn
        Vinv = self.Vinv

        transformed_normals = self.transformed_element_normals
        sigmas = self.sigmas

        for var in range(self.nvar):
            for elem in range(nelements):

                # compute the gradient from the solution
                # representation
                umodal = Vinv.dot(u[elem, :, var])
                qr = psi_r[elem].dot(umodal)
                qs = psi_s[elem].dot(umodal)

                # compute the correction from the flux points
                #sigma = psi_flux[elem].T
                sigma = sigmas[elem].T
                psifj = psi_flux[elem].dot(sigma)

                nr, ns = transformed_normals[elem]
                nr = u_correction[elem, :, var]*nr
                ns = u_correction[elem, :, var]*ns

                #_qr = numpy.sum(nr*u_correction[elem, :, var]*psifj, axis=1)
                #_qs = numpy.sum(ns*u_correction[elem, :, var]*psifj, axis=1)

                _qr = psifj.dot(nr)
                _qs = psifj.dot(ns)

                qr = qr + _qr
                qs = qs + _qs

                # get the ggradient in the physical space
                JnTinv = numpy.linalg.inv(self.Jn[elem].T)

                ux[elem, :, var] = JnTinv[0,0]*qr + JnTinv[0,1]*qs
                uy[elem, :, var] = JnTinv[1,0]*qr + JnTinv[1,1]*qs

    def get_discontinuous_flux(self):
        nelements = self.nelements
        
        u_solution  = self.u; ux_solution = self.ux_solution; uy_solution = self.uy_solution
        u_flux  = self.uflux; ux_flux     = self.ux_flux;     uy_flux     = self.uy_flux

        Fd_solution = self.Fd_solution; Gd_solution = self.Gd_solution
        Fd_flux     = self.Fd_flux    ; Gd_flux     = self.Gd_flux

        for elem in range(nelements):

            # get the fluxes at the solution points            
            f0, f1, f2, f3, g0, g1, g2, g3 = self._get_flux(u_solution[elem], ux_solution[elem], uy_solution[elem])

            Fd_solution[elem, :, 0] = f0
            Fd_solution[elem, :, 1] = f1
            Fd_solution[elem, :, 2] = f2
            Fd_solution[elem, :, 3] = f3

            Gd_solution[elem, :, 0] = g0
            Gd_solution[elem, :, 1] = g1
            Gd_solution[elem, :, 2] = g2
            Gd_solution[elem, :, 3] = g3

            # get the fluxes at the flux points
            f0, f1, f2, f3, g0, g1, g2, g3 = self._get_flux(u_flux[elem], ux_flux[elem], uy_flux[elem])

            # store the flux at the flux points
            Fd_flux[elem,:,0] = f0
            Fd_flux[elem,:,1] = f1
            Fd_flux[elem,:,2] = f2
            Fd_flux[elem,:,3] = f3

            Gd_flux[elem,:,0] = g0
            Gd_flux[elem,:,1] = g1
            Gd_flux[elem,:,2] = g2
            Gd_flux[elem,:,3] = g3

    def get_interaction_flux(self):
        nelements = self.nelements
        nghost = self.nghost
        Np1 = self.N + 1
        
        u = self.uflux; ux = self.ux_flux; uy = self.uy_flux

        Fd = self.Fd_flux; Gd = self.Gd_flux
        Fi = self.Fi; Gi = self.Gi
        Fc = self.Fc; Gc = self.Gc

        Fd_solution = self.Fd_solution; Gd_solution = self.Gd_solution

        nminus = numpy.zeros(shape=(2,))
        nplus  = numpy.zeros(shape=(2,))

        face_normals = self.face_normals
        elem2face = self.elem2face
        face2elem = self.face2elem
        Jn = self.Jn

        transformed_normals = self.transformed_element_normals

        _Fi = numpy.zeros(shape=(2,self.nvar))
        _Gi = numpy.zeros(shape=(2,self.nvar))

        _Fc = numpy.zeros(shape=(2,self.nvar))
        _Gc = numpy.zeros(shape=(2,self.nvar))

        for elem in range(nelements-nghost):
            Jninv = numpy.linalg.inv(Jn[elem])*numpy.linalg.det(Jn[elem])

            # store the transformed discontinuous flux at the solution
            # points
            tmpF = Jninv[0,0]*Fd_solution[elem] + Jninv[0,1]*Gd_solution[elem]
            tmpG = Jninv[1,0]*Fd_solution[elem] + Jninv[1,1]*Gd_solution[elem]

            Fd_solution[elem] = tmpF
            Gd_solution[elem] = tmpG

            # iterate over each face and store the transformed
            # interaction flux and corrections
            for face in range(3):
                
                # get the index for this face
                eface = elem2face[elem][face]

                # get the left and right elements abutting this face
                abutting_elements = face2elem[eface]
                left = abutting_elements[0]; right = abutting_elements[1]

                # solution local to this element
                uminus =  u[ elem,   face*Np1:(face+1)*Np1, :]
                qxminus = ux[elem,   face*Np1:(face+1)*Np1, :]
                qyminus = uy[elem,   face*Np1:(face+1)*Np1, :]
                Fdminus = Fd[elem,   face*Np1:(face+1)*Np1, :]
                Gdminus = Gd[elem,   face*Np1:(face+1)*Np1, :]
                
                # solution external to this element
                if left == elem:
                    nbr_elem = right
                    nminus = face_normals[eface]
                    nplus  = -nminus
                else:
                    nbr_elem = left
                    nplus  = face_normals[eface]
                    nminus = -nplus

                # get the face index for the neighboring element
                nbr_face_index = numpy.where(elem2face[nbr_elem] == eface)[0]

                uplus =  u[ nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]
                qxplus = ux[nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]
                qyplus = uy[nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]
                Fdplus = Fd[nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]
                Gdplus = Gd[nbr_elem, nbr_face_index*Np1:(nbr_face_index+1)*Np1, :]

                # get and store the inviscid interaction flux
                f0, f1, f2, f3, g0, g1, g2, g3 = self._inviscid_interaction_flux(
                    uminus, qxminus, qyminus, nminus, uplus, qxplus, qyplus, nplus)

                _Fi[:, 0] = f0; _Gi[:, 0] = g0
                _Fi[:, 1] = f1; _Gi[:, 1] = g1
                _Fi[:, 2] = f2; _Gi[:, 2] = g2
                _Fi[:, 3] = f3; _Gi[:, 3] = g3

                # Fi[elem, face*Np1:(face+1)*Np1, 0] = f0
                # Fi[elem, face*Np1:(face+1)*Np1, 1] = f1
                # Fi[elem, face*Np1:(face+1)*Np1, 2] = f2
                # Fi[elem, face*Np1:(face+1)*Np1, 3] = f3

                # Gi[elem, face*Np1:(face+1)*Np1, 0] = g0
                # Gi[elem, face*Np1:(face+1)*Np1, 1] = g1
                # Gi[elem, face*Np1:(face+1)*Np1, 2] = g2
                # Gi[elem, face*Np1:(face+1)*Np1, 3] = g3

                # get and store the diffusive interaction flux
                if self.diffusiion:
                    f0, f1, f2, f3, g0, g1, g2, g3 = self._diffusive_interaction_flux(
                        uminus, Fdminus, Gdminus, nminus, uplus, Fdplus, Gdplus, nplus)

                    _Fi[:, 0] += f0; _Gi[:, 0] += g0
                    _Fi[:, 1] += f1; _Gi[:, 1] += g1
                    _Fi[:, 2] += f2; _Gi[:, 2] += g2
                    _Fi[:, 3] += f3; _Gi[:, 3] += g3

                # store the interaction fluxes
                Fi[elem, face*Np1:(face+1)*Np1, :] = _Fi[:]
                Gi[elem, face*Np1:(face+1)*Np1, :] = _Gi[:]

                # transform the flux-diffference to reference space
                tmpF = Jninv[0,0]*(_Fi-Fdminus) + Jninv[0,1]*(_Gi-Gdminus)
                tmpG = Jninv[1,0]*(_Fi-Fdminus) + Jninv[1,1]*(_Gi-Gdminus)

                # take the dot-product with the transformed element
                # normal ans store it as the transformed correction
                # flux
                nr, ns = transformed_normals[elem]
                nr = nr[face*2]
                ns = ns[face*2]

                Fc[elem, face*Np1:(face+1)*Np1, :] = nr*tmpF + ns*tmpG

    def flux_divergence(self):
        nelements = self.nelements
        
        xr = self.xr; xs = self.xs
        yr = self.yr; ys = self.ys

        psi_r = self.psi_r_solution
        psi_s = self.psi_s_solution

        psi_solution = self.psi_solution_points
        psi_flux = self.psi_flux_points

        # transformed discontinuous fluxes at the solution points
        Fd = self.Fd_solution
        Gd = self.Gd_solution

        # ransformed flux corrections
        Fc = self.Fc

        Jn = self.Jn
        Vinv = self.Vinv

        # rhs solution vector
        k1 = self.k1

        sigmas = self.sigmas

        for var in range(self.nvar):
            for elem in range(nelements):

                # compute the divergence from the discontinuous flux at
                # the solution points.
                Fdmodal = Vinv.dot(Fd[elem, :, var])
                Gdmodal = Vinv.dot(Gd[elem, :, var])

                Fr = psi_r.dot(Fdmodal)
                Gs = psi_s.dot(Gdmodal)

                _fdiv = -(Fr + Gs)

                # compute the flux corrections
                #sigma = psi_flux[elem].T
                sigma = sigmas[elem].T
                psifj = psi_solution.dot(sigma)

                _fdiv = _fdiv - psifj.dot(Fc[elem, :, var])
            
                # get and store the rhs in physical space
                detJ = numpy.linalg.det(Jn[elem])
                _fdiv = _fdiv/detJ

                k1[elem, :, var] = _fdiv

    def compute_cell_averages(self):
        nelements = self.nelements
        wts = poly.triangle_integration_weights(self.N)
        
        u = self.u
        uavg = self.uavg

        ux_avg = self.ux_avg
        uy_avg = self.uy_avg

        ux_solution = self.ux_solution
        uy_solution = self.uy_solution

        uavg[:] = 0.0
        ux_avg[:] = 0.0
        uy_avg[:] = 0.0
        for i in range(nelements):
            Jn = abs(numpy.linalg.det(self.Jn[i]))
            
            for var in range(self.nvar):
                uavg[i, var] += Jn*wts.dot(u[i,:,var])

                ux_avg[i, var] += Jn*wts.dot(ux_solution[i,:,var])
                uy_avg[i, var] += Jn*wts.dot(uy_solution[i,:,var])

            # volume average
            area = self.get_element_area(i)
            uavg[i,:] /= area
            ux_avg[i,:] /= area
            uy_avg[i,:] /= area

    def interpolate(self, elem, r, s):
        V = self.V
        Vinv = self.Vinv

        # get the modal representation of the solution within the
        # element
        uelem = self.u[elem]
        umodal = Vinv.dot(uelem)

        # get the modal basis evaluated at the requested points
        psi = self._evaluate_modal_basis(r, s)

        # interpolate the modal solution at the flux points
        uinterp = psi.dot(umodal)
        return uinterp

    def _get_flux(self, u, ux, uy):
        gamma = 1.4
        two3rd = 2.0/3.0
        mu = 0.000

        rho = u[:, 0]
        Mx  = u[:, 1]
        My  = u[:, 2]
        E   = u[:, 3]
        
        rho_x = ux[:, 0]
        Mx_x  = ux[:, 1]
        My_x  = ux[:, 2]
        E_x   = ux[:, 3]
        
        rho_y = uy[:, 0]
        Mx_y  = uy[:, 1]
        My_y  = uy[:, 2]
        E_y   = uy[:, 3]
        
        velx = Mx/rho; vely = My/rho; v2 = velx**2 + vely**2
        p = (gamma - 1.0)*(E - 0.5*rho*v2)
        
        velx_x = 1./rho*(Mx_x - velx*rho_x)
        velx_y = 1./rho*(Mx_y - velx*rho_y)
        
        vely_x = 1./rho*(My_x - vely*rho_x)
        vely_y = 1./rho*(My_y - vely*rho_y)
        
        div = velx_x + vely_y
        vid = velx_y + vely_x
    
        # compute the individual fluxes
        f0 = Mx
        f1 = (Mx*velx + p) + mu*( 2*velx_x - two3rd*div )
        f2 = (Mx*vely)     + mu*vid
        f3 = velx*(E + p)  + mu*( velx*(2*velx_x - two3rd*div) + vely*vid )
        
        g0 = My
        g1 = (My*velx)     + mu*vid
        g2 = (My*vely + p) + mu*( 2*vely_y - two3rd*div )
        g3 = vely*(E + p)  + mu*( vely*(2*vely_y - two3rd*div) + velx*vid )

        return f0, f1, f2, f3, g0, g1, g2, g3

    def _inviscid_interaction_flux(self, uminus, uxminus, uyminus, nminus, uplus, uxplus, uyplus, nplus):
        gamma = 1.4
        two3rd = 2.0/3.0
        lam = 1.0/2.0

        rho_minus = uminus[:, 0]; rho_plus = uplus[:, 0]
        Mx_minus  = uminus[:, 1]; Mx_plus  = uplus[:, 1]
        My_minus  = uminus[:, 2]; My_plus  = uplus[:, 2]
        E_minus   = uminus[:, 3]; E_plus   = uplus[:, 3]
        
        rho_x_minus = uxminus[:, 0]; rho_x_plus = uxplus[:, 0]
        Mx_x_minus  = uxminus[:, 1]; Mx_x_plus  = uxplus[:, 1]
        My_x_minus  = uxminus[:, 2]; My_x_plus  = uxplus[:, 2]
        E_x_minus   = uxminus[:, 3]; E_x_plus   = uxplus[:, 3]
        
        rho_y_minus = uyminus[:, 0]; rho_y_plus = uyplus[:, 0]
        Mx_y_minus  = uyminus[:, 1]; Mx_y_plus  = uyplus[:, 1]
        My_y_minus  = uyminus[:, 2]; My_y_plus  = uyplus[:, 2]
        E_y_minus   = uyminus[:, 3]; E_y_plus   = uyplus[:, 3]
        
        velx_minus = Mx_minus/rho_minus; velx_plus = Mx_plus/rho_plus 
        vely_minus = My_minus/rho_minus; vely_plus = My_plus/rho_plus

        p_minus = (gamma - 1.0)*(E_minus - 0.5*rho_minus*(velx_minus**2 + vely_minus**2))
        p_plus  = (gamma - 1.0)*(E_plus  - 0.5*rho_plus* (velx_plus**2 + vely_plus**2))

        cs_minus = numpy.sqrt(gamma*p_minus/rho_minus)
        cs_plus  = numpy.sqrt(gamma*p_plus/rho_plus)

        cs = max( abs(velx_minus - cs_minus).max(), abs(velx_minus + cs_minus).max() )
        cs = max(cs, max( abs(velx_plus - cs_plus).max(), abs(velx_plus + cs_plus).max() ))
        cs = max(cs, max( abs(vely_minus - cs_minus).max(), abs(vely_minus + cs_minus).max() ))
        cs = max(cs, max( abs(vely_plus - cs_plus).max(), abs(vely_plus + cs_plus).max() ))
        
        # compute the individual fluxes
        f0 = 0.5*(Mx_minus + Mx_plus) +\
             lam*cs*(rho_minus*nminus[0] + rho_plus*nplus[0])

        f1 = 0.5*(Mx_minus*velx_minus + p_minus + Mx_plus*velx_plus + p_plus) +\
             lam*cs*(Mx_minus*nminus[0]  + Mx_plus*nplus[0])

        f2 = 0.5*(Mx_minus*vely_minus + Mx_plus*vely_plus) +\
             lam*cs*(My_minus*nminus[0]  + My_plus*nplus[0])

        f3 = 0.5*(velx_minus*(E_minus + p_minus) + velx_plus*(E_plus + p_plus)) +\
             lam*cs*(E_minus*nminus[0]   + E_plus*nplus[0])
        
        g0 = 0.5*(My_minus + My_plus) +\
             lam*cs*(rho_minus*nminus[1] + rho_plus*nplus[1])

        g1 = 0.5*(My_minus*velx_minus + My_plus*velx_plus) +\
             lam*cs*(Mx_minus*nminus[1] + Mx_plus*nplus[1])

        g2 = 0.5*(My_minus*vely_minus + p_minus + My_plus*vely_plus + p_plus) +\
             lam*cs*(My_minus*nminus[1] + My_plus*nplus[1])

        g3 = 0.5*(vely_minus*(E_minus + p_minus) + vely_plus*(E_plus + p_plus)) +\
             lam*cs*(E_minus*nminus[1] + E_plus*nplus[1])

        return f0, f1, f2, f3 ,g0, g1, g2, g3

    def _diffusive_interaction_flux(
            self, uminus, Fdminus, Gdminus, nminus, uplus, Fdplus, Gdplus, nplus):
        beta = 0.5; tau = 0.1

        rho_minus = uminus[:, 0]; rho_plus = uplus[:, 0]
        Mx_minus  = uminus[:, 1]; Mx_plus  = uplus[:, 1]
        My_minus  = uminus[:, 2]; My_plus  = uplus[:, 2]
        E_minus   = uminus[:, 3]; E_plus   = uplus[:, 3]

        Favg = 0.5 * (Fdminus + Fdplus)
        Gavg = 0.5 * (Gdminus + Gdplus)

        tmp = Fdminus*nminus[0] + Gdminus*nminus[1] +\
              Fdplus* nplus[ 0] + Gdplus* nplus[ 1]

        # F-fluxes
        f0 = Favg[:, 0] + tau*(rho_minus*nminus[0] + rho_plus*nplus[0]) + beta*nminus[0]*tmp[:, 0]

        f1 = Favg[:, 1] + tau*(Mx_minus*nminus[0] + Mx_plus*nplus[0]) +   beta*nminus[0]*tmp[:, 1]

        f2 = Favg[:, 2] + tau*(My_minus*nminus[0] + My_plus*nplus[0]) +   beta*nminus[0]*tmp[:, 2]

        f3 = Favg[:, 3] + tau*(E_minus*nminus[0] + E_plus*nplus[0]) +     beta*nminus[0]*tmp[:, 3]

        # G-fluxes
        g0 = Gavg[:, 0] + tau*(rho_minus*nminus[1] + rho_plus*nplus[1]) + beta*nminus[1]*tmp[:, 0]

        g1 = Gavg[:, 1] + tau*(Mx_minus*nminus[1] + Mx_plus*nplus[1]) +   beta*nminus[1]*tmp[:, 1]

        g2 = Gavg[:, 2] + tau*(My_minus*nminus[1] + My_plus*nplus[1]) +   beta*nminus[1]*tmp[:, 2]

        g3 = Gavg[:, 3] + tau*(E_minus*nminus[1] + E_plus*nplus[1]) +     beta*nminus[1]*tmp[:, 3]

        return f0, f1, f2, f3, g0, g1, g2, g3

    def get_boundary_cells(self):
        """Return indices for cells adjoining the boundary"""
        return self.bcell_indices

    def _check_meshpy(self):
        """Check the mesh.

        We want to ensure that the mesh was generated using the
        correct options like `generate_faces` which is critical for
        our construction.

        """
        if not self._mesh.faces.allocated:
            raise RuntimeError("MeshPy mesh generated without faces option")

    def _setup(self):
        if self._mesh.type == MeshType.MeshPy:
            self._check_meshpy()
            self._setup_meshpy()

    def _setup_meshpy(self):
        """Setup our mesh structure."""
        mesh = self._mesh; nvar = self.nvar

        # points created by MeshPy
        self._x = _x = numpy.array( [point[0] for point in mesh.points] )
        self._y = _y = numpy.array( [point[1] for point in mesh.points] )

        # create the ghost cells
        self._create_ghost_cells()

        elements = self.elements
        nelements = self.nelements

        self._faces = faces = mesh.faces
        self.faces = []
        self.xfaces = []
        self.face2elem = []
        nfaces = len(faces)

        face_centers = []
        face_normals = []
        face_tangents = []

        face_types = [ [-1, -1, -1] for i in range(self.nelements) ]

        self.face_markers = numpy.array( mesh.face_markers )
        self.element_attributes = numpy.array(mesh.element_attributes)

        elem2face = [ [-1, -1, -1] for i in range(self.nelements) ]
        
        # get the coordinates for the mesh
        #x = numpy.array( [pnt[0] for pnt in mesh.points] )
        #y = numpy.array( [pnt[1] for pnt in mesh.points] )

        # neighbors for the elements
        #neighbors = mesh.neighbors
        neighbors = self.neighbors

        # First loop over each element in the underlying mesh and
        # create the Element objects
        # for i in range(nelements):
        #     _e = elements[i]

        #     # get the coordinates for the element
        #     xe = x[_e]; ye = y[_e]

        #     # store this element
        #     element = Element(mesh, i, xe, ye, self.q[:, i], self.w[:, i])
        #     self.elements.append(element)

        # Now loop over the elements, consider neighbors and create
        # the faces.
        face_index = 0
        for i in range(nelements):
            
            #e = self.elements[i]
            #xe = e.x; ye = e.y
            points = self.get_element_vertices(i)
            xe = points[:, 0]; ye = points[:, 1]

            # element faces
            efaces = elem2face[i]

            # get the neighbors for this element
            #e.neighbors = nbrs = neighbors[i]
            nbrs = self.neighbors[ i ]

            # consider each neighbor (there are always 3). We only
            # consider a neighbor with an element index greater than
            # the current index.            
            for j in [2, 0, 1]:

                # we assume that this element is on the left. That is,
                # the normal points away from this element.
                #left = e; right = None
                left = i; right = nbrs[j]

                # We only consider neighbors with an element index
                # greater than the current element.
                if nbrs[j] > i:

                    # the neighbors face list
                    nfaces = elem2face[ nbrs[j] ]

                    # Get the coordinates for the face
                    xf = xe[ (j+1)%3 ], xe[ (j+2)%3 ]
                    yf = ye[ (j+1)%3 ], ye[ (j+2)%3 ]

                    # create the face object for this neighbor
                    marker = self.face_markers[ face_index ]
                    #face = Face(mesh, face_index, xf, yf, nvar,
                    #            left, right, marker=marker)

                    # set face marker if any
                    #self._set_marker(face, marker )
                    
                    # set the coordinate of the face and the face to
                    # element mapping
                    self.xfaces.append( (xf, yf) )
                    self.face2elem.append( (left, right) )

                    # set the normal and tangent for the face
                    delx = xf[1] - xf[0]; dely = yf[1] - yf[0]
                    mag = numpy.sqrt(delx**2 + dely**2)

                    delx /= mag; dely /= mag

                    face_tangents.append([delx,  dely] )
                    face_normals.append( [dely, -delx] )
                    face_centers.append( [0.5*(xf[0]+xf[1]), 0.5*(yf[0]+yf[1])] )
                    
                    # store this face index
                    efaces[ (j+1)%3 ] = face_index
                    
                    reversed_j = numpy.where(self.neighbors[ nbrs[j] ] == i)[0]
                    nfaces[ (reversed_j+1)%3 ] = face_index

                    # the kind of face for this and the neighbor triangle (0, 1 or 2)
                    #face_types[i].append((j+1)%3)
                    #face_types[nbrs[j]].append( (reversed_j+1)%3 )

                    face_types[i][ (j+1)%3 ] = (j+1)%3
                    face_types[nbrs[j]][(reversed_j+1)%3] = (reversed_j+1)%3
                    
                    #self.faces.append( face )
                    face_index += 1

        # store the number of faces
        self.nfaces = len(self.xfaces)
        self.xfaces = numpy.asarray(self.xfaces)
        self.face2elem = numpy.asarray(self.face2elem)
        self.elem2face = numpy.asarray(elem2face)
        self.face_normals = numpy.asarray(face_normals)
        self.face_tangents = numpy.asarray(face_tangents)
        self.face_centers = numpy.asarray(face_centers)

        for elem in range(self.nelements):
            if len(face_types[elem]) != 3:
                nfaces = len(face_types[elem])
                for j in range(nfaces, 3):
                    face_types[elem].append(-1)                    

        self.face_types = numpy.asarray(face_types)

    def _compute_geometric_factors(self):
        # geometric factors for the mesh
        nelements = self.nelements
        points = self.points

        self.Jn = Jn = numpy.zeros(shape=(nelements, 2,2))

        self.xr = xr = numpy.zeros(shape=(nelements, self.Np))
        self.xs = xs = numpy.zeros(shape=(nelements, self.Np))

        self.yr = yr = numpy.zeros(shape=(nelements, self.Np))
        self.ys = ys = numpy.zeros(shape=(nelements, self.Np))
        
        for i in range(nelements):
            indices = self.elements[i]

            # vertices of the element in physical space
            xe = points[indices][:,0]; ye = points[indices][:,1]
            
            # location of the solution points in physical space
            x, y = poly.rs2xy(self.r, self.s, xe, ye)

            # get the geometric coefficients for this triangle
            _xr = -0.5*(xe[0] - xe[1]); _yr = -0.5*(ye[0] - ye[1])
            _xs = -0.5*(xe[0] - xe[2]); _ys = -0.5*(ye[0] - ye[2])

            xr[i] = self.dr.dot(x); xs[i] = self.ds.dot(x)
            yr[i] = self.dr.dot(y); ys[i] = self.ds.dot(y)

            # Jacobian
            Jn[i] = numpy.array([ [_xr, _xs], [_yr, _ys] ])

    def _setup_transformed_element_normals(self):
        self.transformed_element_normals = normals = numpy.zeros(shape=(self.nelements, 2, 3*(self.N+1)))

        face_types = self.face_types
        elem2face = self.elem2face

        # transformed normals
        root2i = 1./numpy.sqrt(2)
        _normals = numpy.array([ [0.0, -1.0], [root2i, root2i], [-1.0, 0.0] ])

        sigma = numpy.zeros( shape=(self.nelements, 3*(self.N+1), self.Np) )

        r, wts = poly.get_gauss_lobatto_legendge_nodes(self.N)
        r1d = numpy.array( list(r)*3 )
        s1d = numpy.zeros_like(r1d)
        a1d, b1d  = poly.rs2ab(r1d, s1d)

        for i in range(self.N+1):
            for j in range(self.N+1):
                if (i+j) <= self.N:
                    m = int(j + (self.N+1)*i + 1 - 0.5*i*(i-1))
                    
                    pa = poly.jacobi_polynomial(a1d, i, 0, 0)
                    pb = poly.jacobi_polynomial(b1d, j, 2*i+1, 0)
                    
                    sigma[:, :, m-1] = numpy.sqrt(2)*pa*pb*(1-b1d)**i

        for elem in range(self.nelements):
            face_indices = elem2face[elem]
            for face in range(3):

                face_idnex = face_indices[face]
                face_type = face_types[elem, face]

                if face_type == 1:
                    sigma[elem, face*(self.N+1):(face+1)*(self.N+1)] *= numpy.sqrt(2)
                
                for j in range(self.N+1):
                    normals[elem, :, face*(self.N+1)+j] = _normals[face_type]

        self.sigmas = sigma

    def _setup_flux_points(self):
        nfaces = self.nfaces
        nelements = self.nelements

        # gauss nodes along the faces
        _r, wts = poly.get_gauss_lobatto_legendge_nodes(self.N)

        # now generate the element coordinates for the flux points
        elem_rs = []

        for elem in range(nelements):
            _elem_r = []
            _elem_s = []

            xe = self.get_element_vertices(elem)
            xv = xe[:, 0]; yv = xe[:, 1]

            for face in range(3):
                face_index = self.elem2face[elem][face]

                xf, yf = self.xfaces[face_index]
                x = xf[0] + 0.5*(1 + _r)*(xf[1]-xf[0])
                y = yf[0] + 0.5*(1 + _r)*(yf[1]-yf[0])

                r, s = poly.xy2rs(x, y, xv, yv)

                _elem_r.append( r )
                _elem_s.append( s )
                
            elem_rs.append( (_elem_r, _elem_s) )

        self.elem_rs = numpy.asarray(elem_rs)

    def _set_marker(self, face, marker):
        """Setup attributes for the faces.

        Attributes are used in the mesh definition to mark
        boundaries. For example, a solid wall or a periodic domain
        boundary may be assigned a unique identifier. All faces along
        these entities have as an attribute, this identifier.

        At the time of creation of the faces for the mesh, we check if
        the face has any special identifier and assign this identifier
        to the Face2D and Element2D objects.

        """
        # check for Markers
        if marker == TransmissiveBoundary.id:
            self.transmissive_boundaries.append( face )

        if marker == ReflectiveBoundary.id:
            self.reflective_boundaries.append( face )

    def _create_ghost_cells(self):
        """Create the ghost cells for the mesh"""
        mesh = self._mesh

        # find the number of ghost cells required
        neighbors = numpy.array( mesh.neighbors )
        self.bcell_indices = bcell_indices = find( neighbors == -1 ) /3

        self.nghost = nghost = len( bcell_indices )
        
        _x = self._x; _y = self._y

        neighbors = list( mesh.neighbors )
        
        points = list( mesh.points )
        npoints = len( points )

        elements = list( mesh.elements )
        nelements = len( elements )

        bcell_indices = self.bcell_indices
        for bcell_index in bcell_indices:
            vertices = elements[ bcell_index ]     # element vertices
            xe = _x[vertices]; ye = _y[vertices]   # element coordinates

            # get the neighbor index which is a ghost
            j = neighbor_index = neighbors[bcell_index].index(-1)

            # face indices shared by the ghost cell
            face_indices =  (j + 1)%3, (j + 2)%3
            
            # coordinates of the two vertices for the face. We will
            # reflect the third node about the line formed by this
            # face
            xf = xe[ [face_indices[0], face_indices[1]] ]
            yf = ye[ [face_indices[0], face_indices[1]] ]

            # From the node numbering used by MeshPy, the neighbor_index'th 
            # node must be reflected. Call this point 'p'
            p = Vertex( xe[neighbor_index], ye[neighbor_index] )

            # get the reflected point. Call it pstar.
            va = Vertex( xf[0], yf[0] )
            tangent = Vector(xf[1] - xf[0], yf[1] - yf[0]); tangent.normalize()
            
            pstar = p.reflect( va, tangent )
            points.append( [pstar.x, pstar.y] )
            npoints += 1

            # add the element to the list of elements. The face
            # indices and newly created point is used as the vertex
            # indices for this ghost cell
            elements.append( [vertices[face_indices[0]],
                              vertices[face_indices[1]],
                              npoints-1] )
            nelements += 1

            # finally, we need to update the -1 in the neighbor's
            # array for the real cell and the neighbor for the ghost
            # cell.
            neighbors[bcell_index][neighbor_index] = nelements-1
            neighbors.append( [-1, -1, bcell_index] )

        # store the neighbors, elements and points array
        self.points = numpy.array( points )
        self.elements = numpy.array( elements )
        self.neighbors = numpy.array( neighbors )

        self.npoints = npoints
        self.nelements = nelements        

    def _evaluate_modal_basis_at_flux_points(self):
        N = self.N
        
        # the modal basis matrix. This is of size Np X 3 X N+1, where
        # the first dimension (Np) denotes the number of basis
        # functions, 3 is the number of faces and N+1 are the number
        # of flux points along a face
        self.psi_flux_points = psi_flux_points = numpy.zeros(shape=(self.nelements, 3*(N+1), self.Np))
        
        for elem in range(self.nelements):
            # get the (r,s) and (a, b) coordinates for the flux points for
            # this triangle
            r, s = self.elem_rs[elem]

            r = r.ravel(); s = s.ravel()
            a, b = poly.rs2ab(r, s)

            # now evaluate the modal Dubiner basis at these points and
            # store the arrays
            for i in range(N+1):
                for j in range(N+1):
                    if (i+j) <= N:
                        m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))
                    
                        pa = poly.jacobi_polynomial(a, i, 0, 0)
                        pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)

                        psi_flux_points[elem, :, m-1] = numpy.sqrt(2)*pa*pb*(1-b)**i

    def _evaluate_modal_basis_at_solution_points(self):
        N = self.N
        
        # the modal basis matrix. This is of size Np X 3 X N+1, where
        # the first dimension (Np) denotes the number of basis
        # functions, 3 is the number of faces and N+1 are the number
        # of flux points along a face
        self.psi_solution_points = psi = numpy.zeros(shape=(self.Np, self.Np))

        # get the (r,s) and (a, b) coordinates for the solution points
        a, b = poly.rs2ab(self.r, self.s)

        # now evaluate the modal Dubiner basis at these points and
        # store the arrays
        for i in range(N+1):
            for j in range(N+1):
                if (i+j) <= N:
                    m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))
                    
                    pa = poly.jacobi_polynomial(a, i, 0, 0)
                    pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)
                    
                    psi[:, m-1] = numpy.sqrt(2)*pa*pb*(1-b)**i

    def _evaluate_grad_modal_basis_at_solution_points(self):
        N = self.N
        
        # the modal basis matrix
        self.psi_r_solution = psi_r = numpy.zeros(shape=(self.r.size, self.Np))
        self.psi_s_solution = psi_s = numpy.zeros(shape=(self.r.size, self.Np))
        
        # get the (r,s) and (a, b) coordinates for the points
        r = self.r; s = self.s
        a, b = poly.rs2ab(r, s)
        
        # now evaluate the modal Dubiner basis at these points and
        # store the arrays
        for i in range(N+1):
            for j in range(N+1):
                if (i+j) <= N:
                    m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))

                    pa = poly.jacobi_polynomial(a, i, 0, 0)
                    pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)
                
                    dpa = poly.grad_jacobi_polynomial(a, i, 0, 0)
                    dpb = poly.grad_jacobi_polynomial(b, j, 2*i+1, 0)
                
                    # r-derivative
                    psi_r[:,m-1] = numpy.sqrt(2)*pb*dpa*2/(1-b)*(1-b)**i
                
                    # s-derivative
                    psi_s[:,m-1] = numpy.sqrt(2) * (2*(1+r)/(1-b)**2*pb*dpa*(1-b)**i) + \
                                   numpy.sqrt(2) * (pa*(dpb*(1-b)**i - pb*i*(1-b)**(i-1)))

    def _evaluate_grad_modal_basis_at_flux_points(self):
        N = self.N
        
        # the modal basis matrix
        self.psi_r_flux = psi_r = numpy.zeros(shape=(self.nelements, 3*(self.N+1), self.Np))
        self.psi_s_flux = psi_s = numpy.zeros(shape=(self.nelements, 3*(self.N+1), self.Np))
        
        # get the (r,s) coordinates for the elemets at the flux points
        elem_rs = self.elem_rs
        
        # now evaluate the modal Dubiner basis at the element points
        # and store the arrays
        for elem in range(self.nelements):

            r, s = elem_rs[elem]

            r = r.ravel(); s = s.ravel()
            a, b = poly.rs2ab(r,s)

            for i in range(N+1):
                for j in range(N+1):
                    if (i+j) <= N:
                        m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))

                        pa = poly.jacobi_polynomial(a, i, 0, 0)
                        pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)
                
                        dpa = poly.grad_jacobi_polynomial(a, i, 0, 0)
                        dpb = poly.grad_jacobi_polynomial(b, j, 2*i+1, 0)
                
                        # r-derivative
                        psi_r[elem,:,m-1] = numpy.sqrt(2)*pb*dpa*2/(1-b)*(1-b)**i
                
                        # s-derivative
                        psi_s[elem,:,m-1] = numpy.sqrt(2) * (2*(1+r)/(1-b)**2*pb*dpa*(1-b)**i) + \
                                            numpy.sqrt(2) * (pa*(dpb*(1-b)**i - pb*i*(1-b)**(i-1)))

    def _evaluate_modal_basis(self, r, s):
        N = self.N
        
        # the modal basis matrix
        psi = numpy.zeros(shape=(r.size, self.Np))
        
        # get the (r,s) and (a, b) coordinates for the points
        a, b = poly.rs2ab(r, s)
        
        # now evaluate the modal Dubiner basis at these points and
        # store the arrays
        for i in range(N+1):
            for j in range(N+1):
                if (i+j) <= N:
                    m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))
                    
                    pa = poly.jacobi_polynomial(a, i, 0, 0)
                    pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)

                    psi[:, m-1] = numpy.sqrt(2)*pa*pb*(1-b)**i

        return psi

    def _evaluate_grad_modal_basis(self, r, s):
        N = self.N
        
        # the basis matrices
        psi_r = numpy.zeros(shape=(r.size, self.Np))
        psi_s = numpy.zeros(shape=(r.size, self.Np))
        
        # get the (r,s) and (a, b) coordinates for the points
        a, b = poly.rs2ab(r, s)
        
        # now evaluate the modal Dubiner basis at these points and
        # store the arrays
        for i in range(N+1):
            for j in range(N+1):
                if (i+j) <= N:
                    m = int(j + (N+1)*i + 1 - 0.5*i*(i-1))
                    
                    pa = poly.jacobi_polynomial(a, i, 0, 0)
                    pb = poly.jacobi_polynomial(b, j, 2*i+1, 0)

                    dpa = poly.grad_jacobi_polynomial(a, i, 0, 0)
                    dpb = poly.grad_jacobi_polynomial(b, j, 2*i+1, 0)
                
                    # r-derivative
                    psi_r[:,m-1] = numpy.sqrt(2)*pb*dpa*2/(1-b)*(1-b)**i
                
                    # s-derivative
                    psi_s[:,m-1] = numpy.sqrt(2) * (2*(1+r)/(1-b)**2*pb*dpa*(1-b)**i) + \
                                        numpy.sqrt(2) * (pa*(dpb*(1-b)**i - pb*i*(1-b)**(i-1)))

        return psi_r, psi_s
