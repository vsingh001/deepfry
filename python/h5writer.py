"""HDF5 writer for the DeepFRy mesh"""
import numpy

HAVE_H5PY = True

try:
    import h5py
except ImportError:
    HAVE_H5PY = False
    print("HDF for Python module H5Py not available")
    

def write_mesh(mesh, fname, numPartitions=1):
    f = h5py.File(fname, "w")
    
    # write out the number of elements as an integer array
    dset_nelements = f.create_dataset("nelements", (5,), dtype="i")
    data = numpy.array( [mesh.nnodes, mesh.nfaces, mesh.nelements, mesh.nghost, mesh.ninterior_faces] )
    dset_nelements.write_direct( data )

    dset_nimport = f.create_dataset("nimport", (numPartitions,), dtype="i")
    data = mesh.nimport_per_part[:]
    dset_nimport.write_direct(data)

    dset_nexport = f.create_dataset("nexport", (numPartitions,), dtype="i")
    data = numpy.array(mesh.nexport_per_part[:])
    dset_nexport.write_direct(data)

    if (numPartitions > 1):
        dset_exportIndices = f.create_dataset("exportIndices", (mesh.exportIndices.size,), dtype="i")
        data = mesh.exportIndices
        data += 1
        dset_exportIndices.write_direct(data)

    # points
    x = mesh.points[:, 0].ravel(); y = mesh.points[:, 1].ravel()
    z = numpy.zeros_like(x)
    if mesh.dim == 3:
        z = mesh.points[:, 2].ravel()

    # points = mesh.points.ravel()
    # dset_nodes = f.create_dataset("nodes", points.shape, dtype="d")

    dset_x = f.create_dataset("x", x.shape, dtype="d")
    dset_x.write_direct(x)

    dset_y = f.create_dataset("y", y.shape, dtype="d")
    dset_y.write_direct(y)

    dset_z = f.create_dataset("z", z.shape, dtype="d")
    dset_z.write_direct(z)

    # xfaces
    xfaces = mesh.xfaces.ravel()
    xfaces += 1
    dset_xfaces = f.create_dataset("xfaces", xfaces.shape, dtype="i")
    dset_xfaces.write_direct(xfaces)

    # element to vertices
    element2vertices = mesh.element2vertices.ravel()
    element2vertices += 1
    dset_elem2vertices = f.create_dataset("element2vertices", element2vertices.shape, dtype="i")
    dset_elem2vertices.write_direct(element2vertices)

    # face2elem
    face2elem = mesh.face2elem.ravel()
    face2elem += 1
    dset_face2elem = f.create_dataset("face2elem", face2elem.shape, dtype="i")
    dset_face2elem.write_direct(face2elem)

    # elem2face
    elem2face = mesh.elem2face.ravel()
    elem2face += 1
    dset_elem2face = f.create_dataset("elem2face", elem2face.shape, dtype="i")
    dset_elem2face.write_direct(elem2face)

    # neighbors
    neighbors = mesh.neighbors.ravel()
    neighbors += 1
    dset_neihbors = f.create_dataset("neighbors", neighbors.shape, dtype="i")
    dset_neihbors.write_direct(neighbors)

    # element types
    dset_elemtype = f.create_dataset("elem_type", mesh.elem_type.shape, dtype="i")
    dset_elemtype.write_direct(mesh.elem_type)

    # face markers
    dset_fmarkers = f.create_dataset("face_markers", mesh.face_markers.shape, dtype="i")
    dset_fmarkers.write_direct(mesh.face_markers)

    # element attributes
    dset_eattrs = f.create_dataset("elem_attrs", mesh.elem_attrs.shape, dtype="i")
    dset_eattrs.write_direct(mesh.elem_attrs)    

    # neighbor face indices
    nbr_face_indices = mesh.nbr_face_indices.ravel()
    nbr_face_indices += 1
    dset_nbr_face_indices = f.create_dataset(
        "nbr_face_indices", nbr_face_indices.shape, dtype="i")
    dset_nbr_face_indices.write_direct(nbr_face_indices)

    # faceindex2nbr
    faceindex2nbr = mesh.faceindex2nbr.ravel()
    faceindex2nbr += 1
    dset_faceindex2nbr = f.create_dataset(
        "faceindex2nbr", faceindex2nbr.shape, dtype="i")
    dset_faceindex2nbr.write_direct(faceindex2nbr)

    # faceindex2nbrfaceindex
    faceindex2nbrfaceindex = mesh.faceindex2nbrfaceindex.ravel()
    faceindex2nbrfaceindex += 1
    dset_faceindex2nbrfaceindex = f.create_dataset(
        "faceindex2nbrfaceindex", faceindex2nbrfaceindex.shape, dtype="i")
    dset_faceindex2nbrfaceindex.write_direct(faceindex2nbrfaceindex)

    # bc_map
    bc_map = mesh.bc_map
    bc_map += 1
    dset_bc_map = f.create_dataset("bc_map", bc_map.shape, dtype='i')
    dset_bc_map.write_direct(bc_map)

    # periodic exchange data in parallel
    if (numPartitions > 1 and mesh.is_periodic):
        nexport_per_part_periodic = mesh.nexport_per_part_periodic
        nimport_per_part_periodic = mesh.nimport_per_part_periodic
        
        #maxNexport_periodic = nexport_per_part_periodic.sum(1)
        #maxNimport_periodic = nimport_per_part_periodic.sum(1)

        #dset_max_ne = f.create_dataset("max_numExport_periodic", (numPartitions,), dtype="i")
        #dset_max_ne.write_direct(maxNexport_periodic)

        #dset_max_ni = f.create_dataset("max_numImport_periodic", (numPartitions,), dtype="i")
        #dset_max_ni.write_direct(maxNimport_periodic)

        # number of periodic exchanges for each direction
        dset_nexport_periodic = f.create_dataset("nexport_per_part_periodic", (6*numPartitions,), dtype="i")
        dset_nexport_periodic.write_direct( nexport_per_part_periodic.ravel() )

        dset_nimport_periodic = f.create_dataset("nimport_per_part_periodic", (6*numPartitions,), dtype="i")
        dset_nimport_periodic.write_direct( nimport_per_part_periodic.ravel() )

        # export and import indices
        export_indices_periodic = numpy.array(mesh.exportIndices_periodic)
        export_faces_periodic   = numpy.array(mesh.exportFaces_periodic)

        import_indices_periodic = numpy.array(mesh.importIndices_periodic)
        import_faces_periodic   = numpy.array(mesh.importFaces_periodic)

        dset_periodic_nie = f.create_dataset("periodic_nie", (2,), dtype="i")
        data = numpy.array( [export_indices_periodic.size, import_indices_periodic.size], int )
        dset_periodic_nie.write_direct(data)

        export_indices_periodic += 1
        dset_export_indices_periodic = f.create_dataset(
            "export_indices_periodic", export_indices_periodic.shape, dtype="i")
        dset_export_indices_periodic.write_direct(export_indices_periodic)

        export_faces_periodic += 1
        dset_export_faces_periodic = f.create_dataset(
            "export_faces_periodic", export_faces_periodic.shape, dtype="i")
        dset_export_faces_periodic.write_direct(export_faces_periodic)

        import_indices_periodic += 1
        dset_import_indices_periodic = f.create_dataset(
            "import_indices_periodic", import_indices_periodic.shape, dtype="i")
        dset_import_indices_periodic.write_direct(import_indices_periodic)

        import_faces_periodic += 1
        dset_import_faces_periodic = f.create_dataset(
            "import_faces_periodic", import_faces_periodic.shape, dtype="i")
        dset_import_faces_periodic.write_direct(import_faces_periodic)
    
    # close the file
    f.close()

