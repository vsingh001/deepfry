"""Module to work with Polynomials used in the FR scheme"""

import numpy
from numpy import sqrt
from scipy.special import gamma
from scipy.integrate import simps, quad
import scipy.special.orthogonal as ortho

class Nodes:
    GaussLobatto=0
    GaussLobattoLegendre=1

# def jacobi_polynomial(x, n, alpha=0, beta=0):
#     r"""Return the Nth order Jacobi polynomial evaluated at :math:`x`
    
#     Parameters:

#     x : numpy.ndarray (dim=1)
#         The coordinates at which to evaluate the Jacobi polynomial

#     n : int 
#         Order of the Jacobi polynomial

#     alpha, beta : int (defalut = 0)
#         Index for the Jacobi polynomial. The default value of 0 defines a
#         normalized Legendre polynomial.

#     Returns a polynomial evaluated at :math:`x`

#     The Jacobi polynomial :math:`P_n^{(\alpha,\beta)}`of order
#     :math:`n` is defined as the solution to the Sturm-Liouville
#     eigenvalue problem
    
#     .. math::
    
#         \frac{d}{dx}(1 -
#         x^2)\omega(x)\frac{d}{dx}P_n^{(\alpha,\beta)}(x) + n(n +
#         \alpha + \beta + 1)\omega(x)P_n^{(\alpha,\beta)}(x) = 0\,,

#     where :math:`x \in [-1,1]` and the weight function
#     :math:`\omega(x) = (1-x)^\alpha (1 + x)^\beta`.

#     This definition is taken from book (Appendix A) "Nodal
#     Discontinuous Galerkin Methods" by Jan Hesthaven and Tim
#     Warburton.

#     The Jacobi polynomials are normalized so that

#     .. math::

#         \int_{-1}^1P_i^{(\alpha,\beta)}(x)
#         P_j^{(\alpha,\beta)}(x)\omega(x) dx = \delta_{ij}

#     Another important relation for the Jacobi polynomials is

#     .. math::
    
#         \frac{d}{dx}P_{n}^{(\alpha,\beta)}(x) = \sqrt{n(n + \alpha +
#         \beta + 1)}P_{n-1}^{(\alpha + 1,\beta + 1)}(x)
    
#     """
#     pl = numpy.zeros( shape=(n+1, x.size) )
    
#     # initial values to seed the recurrence
#     gamma0 = 2**(alpha + beta + 1.)/(alpha + beta + 1.)*gamma(alpha + 1.) \
#         * gamma(beta + 1.)/gamma(alpha + beta + 1.)
    
#     pl[0,:] = 1./sqrt(gamma0)

#     if (n == 0):
#         return pl[0, :]
    
#     gamma1 = (alpha + 1.0)*(beta + 1.0)/(alpha + beta + 3.0)*gamma0
#     pl[1,:] = ( (alpha+beta+2.)*x/2.0 + (alpha - beta)/2.0 )/sqrt(gamma1)
    
#     if ( n == 1 ):
#         return pl[1, :]

#     # repeat using the recurrence relation
#     aold = 2./(2.+alpha+beta)*sqrt( (alpha+1.)*(beta+1.)/(alpha+beta+3.) )
    
#     for i in range(1,n):
#         h1 = 2*i+alpha+beta
#         tmp = (i+1)*(i+1+alpha+beta)*(i+1+alpha)*(i+1+beta)/(h1+1.0)/(h1+3.0)
#         anew = 2.0/(h1+2.0)*sqrt( tmp )
#         bnew = -(alpha*alpha - beta*beta)/h1/(h1 + 2.)
#         pl[i+1, :] = 1./anew*( -aold*pl[i-1, :] + (x-bnew)*pl[i, :] )
#         aold = anew
        
#     return pl[n,:]

def jacobi_polynomial(x, n, alpha=0, beta=0):
    # Use SciPy to get the orthonormal Jacobi Polynomial
    p1 = ortho.jacobi(n, alpha, beta)

    def _f(x, p, alpha, beta):
        return p(x) * (1-x)**alpha * (1+x)**beta

    # compute the integral and the normalizing factor
    integral = quad( _f, -1, 1, (p1*p1, alpha, beta) )[0]
    factor = 1./sqrt(integral)
    
    return factor*p1(x)

def grad_jacobi_polynomial(x, n, alpha, beta):
    r"""Evaluate the gradient of the Jacobi Polynomial"""
    grad = numpy.zeros( shape=(x.size) )
    
    if n > 0:
        tmp = sqrt(n*(n+alpha+beta+1.0))
        grad = tmp * jacobi_polynomial(x, n-1, alpha+1, beta+1)

    return grad

def get_gauss_lobatto_nodes(n):
    r"""Get the nth order Gauss Lobatto nodes for interpolation"""
    x = numpy.zeros( shape=(n+1,) )
    x[0] = -1.0; x[-1] = +1.0

    if ( n == 1 ):
        return x
    
    # Legendre Polynomial of order 'n' and the coefficeints for the
    # derivative
    j = ortho.legendre(n)
    jd = j.deriv()

    # the interior GL nodes are the roots of the derivative
    xr = numpy.roots( jd )
    x[1:-1] = xr[:]

    # sort the nodes in the interval -1,1
    x.sort()

    return x

def get_lobatto_quadrature_weights(xr):
    n = xr.size

    # return array for the weights
    w = numpy.zeros_like(xr)

    # number of free abcissas and order of the polynomial
    r = n-2
    order = n-1

    if order == 0:
        w[0] = 1.0
        return w

    # Taken from http://mathworld.wolfram.com/LobattoQuadrature.html
    for i in range(1, n-1):
        val = ortho.jacobi(n-1, 0, 0)(xr[i])
        w[i] = 2./( n*(n-1) * val*val )
        
    # weights at the end points
    w[0] = 2./(n*(n-1))
    w[-1]= 2./(n*(n-1))

    return w

def get_gauss_nodes(n):
    r"""Get the nth order Gauss Lobatto nodes for interpolation"""
    x = numpy.zeros( shape=(n+1,) )
    
    # Legendre Polynomial of order 'n+2' and the coefficeints for the
    # derivative
    j = ortho.legendre(n+2)
    jd = j.deriv()

    # the interior GL nodes are the roots of the derivative
    x[:] = numpy.roots( jd )

    # sort the nodes in the interval -1,1
    x.sort()

    return x

def get_gauss_lobatto_legendge_nodes(n):
    """Get the Nth order Gauss-Lobatto-Legendre nodes and weights"""
    p = ortho.legendre(n+1)
    p1 = ortho.legendre(n+2)

    pd = p.deriv()
    pr = numpy.roots(p)

    wi = 2*(1-pr**2)/(n+2)**2/(p1(pr))**2

    sorted_indices = numpy.argsort(pr)
    return pr[sorted_indices], wi[sorted_indices]

def get_vandermode1d(r, n):
    r"""Return the Nth order 1D Vandermode matrix

    Parameters:
    
    r : numpy.ndarray
        Interpolation nodes for the solution

    n : int
        Order of the basis. The Vandermode matrix will habe (n+1) columns

    The matrix is defined as

    .. math::

        \mathcal{V}_{i,j} = \tilde{P}_{j}(r_i)\,,

    where the Legendre polynomials are normalized.

    """
    v = numpy.zeros( shape=(r.size, n+1) )
    for j in range(n+1):
        v[:, j] = jacobi_polynomial(r, j, alpha=0, beta=0)

    return v

def get_grad_vandermode1d(r, n):
    r"""Gradient of the Vandermode matrix"""
    gradv = numpy.zeros( shape=(r.size, n+1) )
    for i in range(n+1):
        gradv[:, i] = grad_jacobi_polynomial(r, i, alpha=0, beta=0)

    return gradv

def triangle_solution_points(n=1):
    Np = int(0.5 * (n+1)*(n+2))

    r = numpy.zeros(Np)
    s = numpy.zeros(Np)

    d1 = 1./6; d2 = 2./3

    r[0] = -1.0 + 2*d1
    r[1] = -1.0 + 2*d1
    r[2] = -1.0 + 2*d2
    
    s[0] = -1.0 + 2*d2
    s[1] = -1.0 + 2*d1
    s[2] = -1.0 + 2*d1

    return Np, r, s

def _triangle_flux_point(face, n=1):
    d1 = 2*1./6; d2 = 2*2./3

    if face == 0:
        r = [-1 + d1, -1 + d2]
        s = [-1, -1]
    elif face == 1:
        r = [-1 + d2, 1 - d2]
        s = [1 - d2, -1 + d2]
    elif face == 2:
        r = [-1 , -1]
        s = [-1 + d2, -1 + d1]
    else:
        raise ValueError("Invalid face %d for triangle"%face)    

    return numpy.asarray(r), numpy.asarray(s)

def triangle_flux_point(face, xv, yv, n=1):
    r, s = _triangle_flux_point(face, n)
    return rs2xy(r, s, xv, yv)

def triangle_flux_points_local(n=1):
    r = numpy.zeros(shape=(3, n+1))
    s = numpy.zeros(shape=(3, n+1))

    for face in range(3):
        r[face], s[face] = _triangle_flux_point(face, n)

    r = r.ravel(); s = s.ravel()
    return r, s

def triangle_integration_weights(n=1):
    Np = int(0.5 * (n+1)*(n+2))
    return numpy.asarray([1./6*4]*3)

def rs2ab(r,s):
    a = 2*(1+r)/(1-s) -1 
    b = s
    return a, b

def rs2xy(r,s,x,y):
    x = 0.5 *  (-(r+s)*x[0] + (r+1)*x[1] + (s+1)*x[2] )
    y = 0.5  * (-(r+s)*y[0] + (r+1)*y[1] + (s+1)*y[2] )
    return x, y

def xy2rs(x,y,xv,yv):
    r = (2*x - (xv[2] + xv[1]))*(yv[2]-yv[0]) - (2*y - (yv[2] + yv[1]))*(xv[2]-xv[0])
    r /= ( (xv[1]-xv[0])*(yv[2]-yv[0]) - (yv[1]-yv[0])*(xv[2]-xv[0]) )

    s = (2*x - (xv[2] + xv[1]))*(yv[1]-yv[0]) - (2*y - (yv[2]+yv[1]))*(xv[1]-xv[0])
    s /= ( (xv[2]-xv[0])*(yv[1]-yv[0]) - (yv[2]-yv[0])*(xv[1]-xv[0]) )

    return r, s

def get_jacobi_polynomials2d(n=1):
    Np, r, s = triangle_solution_points(n)
    a, b = rs2ab(r,s)

    pa = []
    pb = []

    for i in range(Np):
        for j in range(Np):
            if (i + j) <= n:
                m = int(j + (n+1)*i + 1 - 0.5*i*(i-1))
                pa.extend(jacobi_polynomial(a, i, 0, 0))
                pb.extend(jacobi_polynomial(b, j, 2*i+1, 0))

    pa = numpy.asarray(pa); pb = numpy.asarray(pb)
    pa.shape = Np, Np
    pb.shape = Np, Np
    return pa, pb

def get_grad_jacobi_polynomials2d(n=1):
    Np, r, s = triangle_solution_points(n)
    a, b = rs2ab(r,s)

    dpa = []
    dpb = []

    for i in range(Np):
        for j in range(Np):
            if (i + j) <= n:
                m = int(j + (n+1)*i + 1 - 0.5*i*(i-1))
                dpa.extend(grad_jacobi_polynomial(a, i, 0, 0))
                dpb.extend(grad_jacobi_polynomial(b, j, 2*i+1, 0))

    dpa = numpy.asarray(dpa); dpb = numpy.asarray(dpb)
    dpa.shape = Np, Np
    dpb.shape = Np, Np
    return dpa, dpb

def get_vandermode2d(n=1):
    Np, r, s = triangle_solution_points(n)
    a, b = rs2ab(r,s)

    v = numpy.zeros( shape=(Np, Np) )

    for i in range(Np):
        for j in range(Np):
            if (i + j) <= n:
                m = int(j + (n+1)*i + 1 - 0.5*i*(i-1))
                v[:, m-1] = numpy.sqrt(2) * jacobi_polynomial(a, i, 0, 0) * \
                            jacobi_polynomial(b, j, 2*i+1, 0) * (1-b)**i

    return r, s, v

def get_grad_vandermode2d(n=1):
    Np, r, s = triangle_solution_points(n)
    a, b = rs2ab(r,s)

    vr = numpy.zeros( shape=(r.size, Np) )
    vs = numpy.zeros( shape=(r.size, Np) )

    v = get_vandermode2d(n)[-1]

    for i in range(Np):
        for j in range(Np):
            if (i + j) <= n:
                m = int(j + (n+1)*i + 1 - 0.5*i*(i-1))

                pa = jacobi_polynomial(a, i, 0, 0)
                pb = jacobi_polynomial(b, j, 2*i+1, 0)
                
                dpa = grad_jacobi_polynomial(a, i, 0, 0)
                dpb = grad_jacobi_polynomial(b, j, 2*i+1, 0)
                
                # r-derivative
                vr[:,m-1] = sqrt(2)*pb*dpa*2/(1-s)*(1-b)**i
                
                # s-derivative
                vs[:,m-1] = sqrt(2) * (2*(1+r)/(1-s)**2*pb*dpa*(1-b)**i) + \
                            sqrt(2) * (pa*(dpb*(1-b)**i - pb*i*(1-b)**(i-1)))

    vinv = numpy.linalg.inv(v)

    return vr.dot(vinv), vs.dot(vinv)

def get_lagrange_derivative_matrix(r, n):
    r"""Get the differentiation matrix for the Lagrange polynomials"""
    # vandermode matrix and it's inverse
    v = get_vandermode1d(r, n)
    vi = numpy.linalg.inv(v)

    # gradient of the vandermode matrix
    vg = get_grad_vandermode1d(r, n)
    
    return vg.dot(vi)

def radau_left(n):
    r"""Return the coefficients for the nth order, left Radau polynomial

    .. math::

        g_L = \frac{(-1)^n}{2}\left( L_n - L_{n+1} \right)\,,

    where, :math:`L_n` is the nth order Legendre polynomial
    
    """
    factor = 0.5 * numpy.power(-1, n)
    return factor * (ortho.legendre(n) - ortho.legendre(n+1))

def radau_right(n):
    r"""Return the coefficients for the nth order right Radau polynomial

    .. math::

    g_R = \frac{1}{2}\left(L_k + L_{k+1} \right)

    """
    factor = 0.5
    return factor * (ortho.legendre(n) + ortho.legendre(n+1))

def get_lagrange_interpolant_vectors(xr):
    kp1 = xr.size

    lm1 = numpy.zeros(kp1)
    lp1 = numpy.zeros(kp1)
    
    for i in range(kp1):
        val1 = 1.0
        val2 = 1.0
        
        for j in range(kp1):
            if not (i == j):
                val1 = val1*( -1 - xr[j] )/( xr[i] - xr[j] )
                val2 = val2*( +1 - xr[j] )/( xr[i] - xr[j] )
            
        lm1[i] = val1
        lp1[i] = val2

    return lm1, lp1

def test_legendre_polynomials(n1, n2, alpha, beta):
    r"""Test the orthogonality for the Legendre polynomials

    Parameters:
    
    n1, n2 : int
        Order of the Legendre polynomials

    We use SciPy's Simpson integration routine on sampled data to
    check for orthogonality.

    """
    # grid and sampled data
    x = numpy.linspace(-1, 1, 10001)
    y = jacobi_polynomial(x, n1, alpha, beta) * jacobi_polynomial(x, n2, alpha, beta) * (1-x)**alpha * (1+x)**beta
    dx = x[1] - x[0]
    
    # integrate
    val = simps(y, x, dx=dx)
    if n1 == n2 :
        error = abs(val-1.0)
        assert( error < 1e-8 )
                
    else:
        error = abs(val)
        assert( error < 1e-8 )

    print error, 'OK'
