import numpy
from pylab import find

class FaceType:
    default = 0
    inflow = 11
    outflow = 12

    solid_wall = 20

class DeepFRyMesh(object):
    # number of nodes, elements and faces
    nnodes    = None
    nfaces    = None
    nelements = None
    nghost    = None

    nelements_local  = None
    nremote_per_part = None

    # (x, y) coordinates of the element nodes
    points = None
    
    # (x, y) coordinates of the faces
    xfaces = None
    
    # element to vertex mapping
    element2vertices = None

    # face to element mapping
    face2elem = None

    # element to face mapping
    elem2face = None

    # element neighbors
    neighbors = None

    # element types
    elem_type = None

    # face markers
    face_markers = None

    # regions
    elem_attrs = None

    # neighbor face indices
    nbr_face_indices = None
    
    # faceindex2nbr
    faceindex2nbr = None

    # faceindex2nbrfaceindex
    faceindex2nbrfaceindex = None

    # bc_map
    bc_map = None

    def get_vertices(self, elem_index):
        return self.element2vertices[elem_index]

    def get_coords(self, elem_index):
        vertices = self.get_vertices(elem_index)
        coords = self.points[ vertices ]
        if self.dim == 2:
            return coords[:,0], coords[:,1]
        else:
            return coords[:,0], coords[:,1], coords[:,2]

def meshpy2deepfry(mesh):

    # points created by MeshPy
    _x = numpy.array( [point[0] for point in mesh.points] )
    _y = numpy.array( [point[1] for point in mesh.points] )

    ################# Create ghost cells by reflection ################# 

    # find the number of ghost cells needed
    bcell_indices = find( numpy.asarray(mesh.neighbors) == -1)/3
    nghost = len(bcell_indices)

    # get the MeshPy arrays as lists
    neighbors = list(mesh.neighbors) 
    points    = list(mesh.points);    npoints   = len(points)
    elements  = list(mesh.elements);  nelements = len(elements)

    for bcell_index in bcell_indices:
        # element vertices and coordinates
        vertices = elements[ bcell_index ]   
        xe = _x[vertices]; ye = _y[vertices]

        # get the neighbor index which is a ghost
        j = neighbor_index = neighbors[bcell_index].index(-1)

        # face indices shared by the ghost cell
        face_indices =  (j + 1)%3, (j + 2)%3

        # coordinates of the two vertices for the face. We will
        # reflect the third node about the line formed by this
        # face
        xf = xe[ [face_indices[0], face_indices[1]] ]
        yf = ye[ [face_indices[0], face_indices[1]] ]

        # From the node numbering used by MeshPy, the neighbor_index'th 
        # node must be reflected. Call this point 'p'
        p = Vertex( xe[neighbor_index], ye[neighbor_index] )

        # get the reflected point. Call it pstar. Increment the number of
        # points by 1
        va = Vertex( xf[0], yf[0] )
        tangent = Vector(xf[1] - xf[0], yf[1] - yf[0]); tangent.normalize()

        pstar = p.reflect( va, tangent )
        points.append( [pstar.x, pstar.y] )
        npoints += 1

        # add the element to the list of elements. The face
        # indices and newly created point is used as the vertex
        # indices for this ghost cell
        elements.append( [vertices[face_indices[0]],
                          vertices[face_indices[1]],
                          npoints-1] )
        nelements += 1

        # finally, we need to update the -1 in the neighbor's
        # array for the real cell and the neighbor for the ghost
        # cell.
        neighbors[bcell_index][neighbor_index] = nelements-1
        neighbors.append( [-1, -1, bcell_index] )

    # store the lists as numpy arrays
    points    = numpy.asarray(points)
    elements  = numpy.asarray(elements)
    neighbors = numpy.asarray(neighbors)

    elem_type = numpy.ones( nelements, dtype=numpy.int64 )
    elem_type[-nghost:] = -1

    ################# Setup Element Connectivity ################# 
    faces = mesh.faces

    _faces    = []
    xfaces    = []
    face2elem = []

    face_centers   = []
    face_normals   = []
    face_tangents  = []

    face_types = [ [-1, -1, -1] for i in range(nelements) ]

    face_markers = numpy.asarray( mesh.face_markers )
    elem_attrs   = numpy.asarray( mesh.element_attributes )

    elem2face = [ [-1, -1, -1] for i in range(nelements) ]

    # loop over the elements and generate the faces
    face_index = 0
    for i in range(nelements):
        vertices = elements[i]
        pnts     = points[vertices]

        xe = pnts[:, 0]; ye = pnts[:, 1]

        # element faces and neighbors
        efaces = elem2face[i]
        nbrs   = neighbors[i]

        # consider each neighbor (there are always 3).
        for j in [2, 0, 1]:

            # we assume that this element is on the left. That is,
            # the normal points away from this element.
            #left = e; right = None
            left = i; right = nbrs[j]

            # We only consider neighbors with an element index
            # greater than the current element.
            if nbrs[j] > i:
                # the neighbors face list
                nfaces = elem2face[ nbrs[j] ]

                # Get the coordinates for the face
                xf = xe[ (j+1)%3 ], xe[ (j+2)%3 ]
                yf = ye[ (j+1)%3 ], ye[ (j+2)%3 ]

                # set the coordinate of the face and the face to
                # element mapping
                #xfaces.append( (xf, yf) )
                xfaces.append( (vertices[(j+1)%3], vertices[(j+2)%3]) )
                face2elem.append( (left, right) )

                # set the normal and tangent for the face
                delx = xf[1] - xf[0]; dely = yf[1] - yf[0]
                mag = numpy.sqrt(delx**2 + dely**2)

                delx /= mag; dely /= mag

                face_tangents.append([delx,  dely] )
                face_normals.append( [dely, -delx] )
                face_centers.append( [0.5*(xf[0]+xf[1]), 0.5*(yf[0]+yf[1])] )

                # store this face index
                efaces[ (j+1)%3 ] = face_index

                reversed_j = numpy.where(neighbors[ nbrs[j] ] == i)[0]
                nfaces[ (reversed_j+1)%3 ] = face_index

                face_types[i][ (j+1)%3 ] = (j+1)%3
                face_types[nbrs[j]][(reversed_j+1)%3] = (reversed_j+1)%3

                face_index += 1

    nfaces = len(xfaces)

    xfaces    = numpy.asarray(xfaces)
    face2elem = numpy.asarray(face2elem)
    elem2face = numpy.asarray(elem2face)

    for elem in range(nelements):
        if len(face_types[elem]) != 3:
            nfaces = len(face_types[elem])
            for j in range(nfaces, 3):
                face_types[elem].append(-1)

    face_types = numpy.asarray(face_types)

    # create the DeepFRy compatible mesh object
    dmesh = DeepFRyMesh()

    dmesh.nnodes = npoints
    dmesh.nfaces = nfaces
    dmesh.nelements = nelements
    dmesh.nghost = nghost

    dmesh.points = points
    dmesh.xfaces = xfaces

    dmesh.face2elem = face2elem
    dmesh.elem2face = elem2face

    dmesh.element2vertices = elements
    dmesh.neighbors = neighbors

    dmesh.elem_type = elem_type
    dmesh.face_markers = face_markers

    # MeshPy element attributes based on regions
    dmesh.elem_attrs = numpy.asarray( mesh.element_attributes )

    dmesh.nbr_face_indices = numpy.ones_like(dmesh.neighbors) * -1
    for i in range(dmesh.nelements):
        nbrs = dmesh.neighbors[i]
        for index, j in enumerate(nbrs):
            if j > -1:
                dmesh.nbr_face_indices[i][index] = (index+1)%3
              
    dmesh.faceindex2nbr = numpy.ones_like(dmesh.neighbors) * -1
    for i in range(dmesh.nelements):
        nbrs = dmesh.neighbors[i]
        for index, j in enumerate(nbrs):
            if j > -1:
                nbr_face = dmesh.nbr_face_indices[i, index]
                dmesh.faceindex2nbr[j, nbr_face] = i
    
    return dmesh

class Point(object):
    """A Point represents well, a vertex in 3D.

    Data Attributes:
    ----------------

    x, y, z : float
         coordinate positions

    """

    def __init__(self, x=0, y=0, z=0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

############################################################################
# `Vector` class
############################################################################
class Vector(Point):
    """" A three dimensional vector supporting basic operations"""
    
    def __init__(self, x = 0.0, y = 0.0, z=0.0):
        """ Default constructor """

        Point.__init__(self, x, y, z)
        self.w = 1.0
        
    ########################################################################
    #`Vector` interface
    ########################################################################

    def norm(self):
        """ Return the norm of the point """
        return numpy.sqrt(self.x*self.x + self.y*self.y + self.z*self.z)

    def asvector(self):
        """ Return a numpy vector for the object """
        return  numpy.array([self.x, self.y, self.z, self.w])

    def fromarray(self, vec):
        return Vector(vec[0], vec[1], vec[2], vec[3])
    
    def normalize(self):
        """ Normalize the vector """
        norm = self.norm()
        self.x /= norm
        self.y /= norm
        self.z /= norm

    def set(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def dot(self, vec):
        """ Inner product with another vector """
        return self.x*vec.x + self.y*vec.y + self.z*vec.z

    def cross(self, vec):
        """ Return the cross product with another vector """
        return Vector(self.y*vec.z - self.z*vec.y,
                      self.z*vec.x - self.x*vec.z,
                      self.x*vec.y - self.y*vec.x)

    def transform(self, matrix):
        return self.fromvector( numpy.dot(matrix, self.asvector()) )

    ########################################################################
    #`object` interface
    ########################################################################
    
    def __str__(self):
        return '(%f, %f, %f)'%(self.x, self.y, self.z)

    def __repr__(self):
        return 'Vector(%g, %g, %g)'%(self.x, self.y, self.z)

    def __add__(self, vec):
        return Vector(self.x+vec.x, self.y+vec.y, self.z+vec.z)

    def __sub__(self, vec):
        return Vector(self.x-vec.x, self.y-vec.y, self.z-vec.z)

    def __mul__(self, a):
        return Vector(self.x*a, self.y*a, self.z*a)

    def __div__(self, a):
        return Vector(self.x/a, self.y/a, self.z/a)

    def __eq__(self, other):
        return (self.x==other.x) and (self.y==other.y) and (self.z==other.z)
    
    def __neg__(self):
        return Vector(-self.x, -self.y, -self.z)
    
    def __iadd__(self, vec):
        self.x += vec.x
        self.y += vec.y
        self.z += vec.z
        return self

    def __isub__(self, p):
        self.x -= p.x
        self.y -= p.y
        self.z -= p.z
        return self

    def __imul__(self, m):
        self.x *= m
        self.y *= m
        self.z *= m
        return self

    def __idiv__(self, m):
        self.x /= m
        self.y /= m
        self.z /= m
        return self

#End of Vector Class

class Vertex(Vector):

    def reflect(self, va, tangent):
        """Reflect the vertex about a given vector"""
        va_p = self - va

        # projected distance along the vector
        l = va_p.dot( tangent )

        # projected point pl
        pl = va + (tangent * l)

        # return the reflected point
        return pl + (pl - self)
