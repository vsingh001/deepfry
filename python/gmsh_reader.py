"""Reader for GMSH ASCII meshes"""

import numpy as np
import numpy.ma as ma

import imp
import os
import sys

curdir = os.path.abspath(os.curdir)
thisdir = os.path.realpath(__file__).split('gmsh')[0]

print thisdir

try:
    import utils, genmesh, peano_hilbert
except ImportError:
    utils = imp.load_source("utils", os.path.join(thisdir, 'utils.py'))
    genmesh = imp.load_source('genmesh', os.path.join(thisdir, 'genmesh.py'))
    peano_hilbert = imp.load_source('peano_hilbert', os.path.join(thisdir, 'peano_hilbert.py'))

    # utils = imp.load_source('utils', os.path.join(curdir, '../../../python/utils.py'))
    # genmesh = imp.load_source('genmesh', os.path.join(curdir, '../../../python/genmesh.py'))
    # peano_hilbert = imp.load_source('peano_hilbert', os.path.join(curdir, '../../../python/peano_hilbert.py'))

OrderedSet = utils.OrderedSet
round_trip_connect = utils.round_trip_connect

class GMSHReader(object):
    def __init__(self, fname, etype='tri'):
        self.fname = fname
        self.etype = etype

        self.dim = 2
        if etype == 'tri':
            self.elem_nface = 3
            self.elem_nnodes = 3

        elif etype == 'quad':
            self.elem_nface = 4
            self.elem_nnodes = 4

        elif etype == "tet":
            self.dim = 3
            self.elem_nface = 4
            self.elem_nnodes = 4

        elif etype == "hex":
            self.dim = 3
            self.elem_nface = 6
            self.elem_nnodes = 8

        self._read()

        self._initialize()

        # Generate the DeepFRy mesh data
        self.dmesh = dmesh = [genmesh.DeepFRyMesh() for i in range(self.numPartitions)]

        self.nexport_per_part = self.nremote_per_part.T

        max_numExport = np.zeros((self.numPartitions), int)
        
        for partition in range(self.numPartitions):
            dmesh[partition].dim = self.dim
            dmesh[partition].nelements = self.nelements_per_part[partition]
            
            dmesh[partition].nremote_per_part = self.nremote_per_part[partition]
            dmesh[partition].nelements_local = dmesh[partition].nelements
            for i in range(self.numPartitions):
                if (dmesh[partition].nremote_per_part[i] > -1):
                    dmesh[partition].nelements_local -= dmesh[partition].nremote_per_part[i]

            # number of elements to be imported from different processors
            dmesh[partition].nimport_per_part = self.nremote_per_part[partition]
            dmesh[partition].nexport_per_part = self.nexport_per_part[partition]

            max_numExport = max(0, np.max(dmesh[partition].nexport_per_part))
            dmesh[partition].max_numExport = max_numExport
            dmesh[partition].exportIndices = []

            partition_data = self.elements.data[self.indices_per_part[partition]]
            partition_mask = self.elements.mask[self.indices_per_part[partition]]

            for i in range(self.numPartitions):
                export_indices = np.ones(max_numExport, int) * -1
                index = 0
                for elem_index in range(dmesh[partition].nelements_local):
                    data = partition_data[elem_index][np.where(partition_mask[elem_index] == False)[0]]
                    if -(i+1) in data:
                        export_indices[index] = elem_index
                        index += 1

                dmesh[partition].exportIndices.append(export_indices)

            dmesh[partition].exportIndices = np.ravel(np.asarray(dmesh[partition].exportIndices))

            # get the unique nodes for the partition
            partition_nodes = []
            for i in self.enodes[partition].ravel():
                if i not in partition_nodes:
                    partition_nodes.append(i)
                    
            partition_nodes = np.asarray(partition_nodes)

            dmesh[partition].nnodes = len(partition_nodes)
            dmesh[partition].points = self.points[partition_nodes]

            # get the local element2vertices numbering
            e2v = np.ones(shape=(dmesh[partition].nelements, self.elem_nnodes), dtype=int)
            for i in range(dmesh[partition].nelements):
                for j in range(self.elem_nnodes):
                    global_node_index = self.enodes[partition][i,j]
                    local_node_index = np.where(partition_nodes == global_node_index)[0]
                
                    e2v[i, j] = local_node_index

            dmesh[partition].element2vertices_global = self.enodes[partition]
            dmesh[partition].element2vertices        = e2v

            # number of faces and ghost elements
            dmesh[partition].nfaces  = self.nfaces[partition]
            dmesh[partition].nghost  = self.nghost[partition]

            # face node numbering
            xfaces = []
            for i in range(dmesh[partition].nfaces):
                face = self.faces[partition][i]
                for index, j in enumerate(face):
                    face[index] = np.where(partition_nodes == j)[0]

                xfaces.append(face)
            
            dmesh[partition].xfaces = np.asarray(xfaces)
            dmesh[partition].xfaces_global = np.asarray( 
                [tuple(face) for face in self.faces[partition]] )            

            # mesh connectivity
            dmesh[partition].face2elem        = self.face2elem[partition]
            dmesh[partition].elem2face        = self.elem2face[partition]
            dmesh[partition].neighbors        = self.neighbors[partition]
            dmesh[partition].elem_type        = self.elem_type[partition]

            # count the number of interior faces
            fm = self.face_markers[partition]
            ninterior_faces = len(np.where(fm == 0)[0])
            
            dmesh[partition].face_markers = fm
            dmesh[partition].ninterior_faces = ninterior_faces

            dmesh[partition].elem_attrs = self.elem_attrs[partition]

            # additional connectivity information
            self._get_nbr_faces(partition)

            # FIXME: We have to be more careful for periodicity

            # set up boundary maps for periodic problems
            bc_map = np.asarray(range(self.nelements))
            dmesh[partition].bc_map = np.asarray(range(self.nelements_per_part[partition]))

        for i in range(self.numPartitions):
                self.dmesh[i].is_periodic = False

        if self.numPartitions > 1:
            if any( [tag.startswith('periodic') for tag in self.btags] ):
                self.setup_periodic_exchange_data()
                
                for i in range(self.numPartitions):
                    self.dmesh[i].is_periodic = True

    def _initialize(self):
        print 'Reading physical tags ...'
        self._read_physical_tags()

        print 'Reading Mesh Partitions'
        self._read_mesh_partitions()

        print 'Reading nodes ...'
        self._read_nodes()

        print 'Reading elements ...'
        self._read_elements()

        self.enodes        = [[] for i in range(self.numPartitions)]
        self.face2elem     = [[] for i in range(self.numPartitions)]
        self.elem2face     = [[] for i in range(self.numPartitions)]
        self.neighbors     = [[] for i in range(self.numPartitions)]
        self.face2vertices = [[] for i in range(self.numPartitions)]

        self.faces         = [[] for i in range(self.numPartitions)]
        self.sorted_faces  = [[] for i in range(self.numPartitions)]
        self.nfaces        = [0  for i in range(self.numPartitions)]        

        print 'Generate faces ...'
        for partition in range(self.numPartitions):
            self._get_faces(partition)

        self.belems = [{} for i in range(self.numPartitions)]
        self.bfaces = [{} for i in range(self.numPartitions)]
        self.nghost = [0  for i in range(self.numPartitions)]

        self.face_markers = [[] for i in range(self.numPartitions)]

        print 'Setting boundary elements'
        for partition in range(self.numPartitions):
            self._set_boundary_elements(partition)

        self.fluid_elems = [[] for i in range(self.numPartitions)]
        self.elem_attrs  = [[] for i in range(self.numPartitions)]
        self.elem_type   = [[] for i in range(self.numPartitions)]            

        print 'Setting regions and attributes'
        for partition in range(self.numPartitions):
            self._set_regions(partition)

    def _read(self):
        self.f = open(os.path.abspath(os.path.join(curdir,self.fname)), 'r')

    def _read_nodes(self):
        f = self.f
        f.seek(0)
        while True:
            l = f.readline()
            if l.startswith('$Nodes'):
                break
        
        self.nnodes = nnodes = int(f.readline())
        points = np.ones( shape=(nnodes, self.dim), dtype=float )

        for i in range(nnodes):
            points[i] = [ float(j) for j in f.readline().split()[1:1+self.dim] ]
            
        self.points = np.array(points)[:,:self.dim]
        self.npoints = self.points.shape[0]

        self.x = self.points[:, 0]
        self.y = self.points[:, 1]
        self.z = np.zeros_like(self.x)
        if self.dim == 3:
            self.z = self.points[:, 2]

    def _read_physical_tags(self):
        f = self.f
        f.seek(0)
        
        while True:
            l = f.readline()
            if l.startswith('$PhysicalNames'):
                break
            if l == '':
                print 'Physical tags not found. At EOF, Exiting ...'
                sys.exit()

        n_physical_tags = int( f.readline() )

        tags = {}
        for i in range(n_physical_tags):
            l = f.readline().split(' ')
            tagdim, tagval  = [int (j) for j in l[:2]]
            tags[ l[2][1:-2] ] = (tagdim, tagval)

        self.tags = tags

    def _read_mesh_partitions(self):
        f = self.f
        f.seek(0)
        
        while True:
            l = f.readline()
            if l.startswith('$MeshPartitions'):
                break
            if l == '':
                print 'Mesh Partitions tags not found. At EOF, Exiting ...'
                sys.exit()

        self.numPartitions = int( f.readline() )
        self.partition_numbers = range(1, self.numPartitions+1)

    def _read_elements(self):
        f = self.f
        f.seek(0)
        while True:
            l = f.readline()
            if l.startswith('$Elements'):
                break
        
        self.nelements_total = nelements_total = int(f.readline())

        element_data = np.ones( shape=(nelements_total, 25), dtype=int ) * -1
        mask = np.ones( shape=(nelements_total, 25), dtype=bool )

        self.elements_total = ma.masked_array(data=element_data, mask=mask)
        
        for i in range(nelements_total):
            data = [ int(j) for j in f.readline().split()[1:] ]

            self.elements_total.data[i][:len(data)] = data
            self.elements_total.mask[i][:len(data)] = False

            # convert from GMSH 1 based indexing to Python 0 based indexing
            ntags = data[1]
            self.elements_total.data[i][2+ntags:] -= 1
            self.elements_total.data[i][2+ntags+self.elem_nnodes:] -= 1022

        # fix the partition numbers for the single partition case
        # (GMSH assigns a weird numbering system in this case :/)
        if self.numPartitions == 1:
            self.elements_total.data[:,5] = 1

        # read the different types of elements
        self.lines = lines = self.elements_total[np.where(self.elements_total[:,0] == 1)[0]]
        self.tri   = tri   = self.elements_total[np.where(self.elements_total[:,0] == 2)[0]]
        self.quads = quads = self.elements_total[np.where(self.elements_total[:,0] == 3)[0]]
        self.tets  = tets  = self.elements_total[np.where(self.elements_total[:,0] == 4)[0]]
        self.hexs  = hexs  = self.elements_total[np.where(self.elements_total[:,0] == 5)[0]]

        self.nlines = len(lines)
        self.ntri   = len(tri)
        self.nquads = len(quads)
        self.ntets  = len(tets)
        self.nhexs  = len(hexs)

        # concatenate the elements into one data structure
        if self.dim == 2:
            if quads.size == 0:
                self.elements = self.tri
            elif tri.size == 0:
                self.elements = self.quads
            else:
                self.elements = np.concatenate( (self.tri, self.quads) )
            
        else:
            if hexs.size == 0:
                self.elements = self.tets
            elif tets.size == 0:
                self.elements = self.hexs
            else:
                self.elements = np.concatenate( (self.tets, self.hexs) )

        self.nelements = self.elements.shape[0]

        # sort element data
        xc = np.zeros(shape=(self.nelements))
        yc = np.zeros(shape=(self.nelements))
        zc = np.zeros(shape=(self.nelements))

        keys = np.zeros(shape=(self.nelements), dtype=long)

        enodes = np.ones(shape=(self.nelements, self.elem_nnodes), dtype=int)
        for i in range(self.nelements):
            data = self.elements.data[i][np.where(self.elements.mask[i] == False)[0]]
            enodes[i] = self.elements.data[i][np.where(self.elements.mask[i] == False)[0]][-self.elem_nnodes:]
    
        # get the element centroids that will be used to sort the
        # elements
        for i in range(self.nelements):
            cents = 1./self.elem_nnodes * sum(self.points[enodes[i]], 0)
            xc[i] = cents[0]; yc[i] = cents[1]
            if self.dim == 3:
                zc[i] = cents[2]
    
        # generate the peano-hilbert indexing keys
        domainlen = max( self.x.max()-self.x.min(),
                         self.y.max()-self.y.min(),
                         self.z.max()-self.z.min() )

        for i in range(self.nelements):
            keys[i] = peano_hilbert.peano_hilbert_key(
                domainlen, xc[i], yc[i], zc[i],
                self.x.min(), self.y.min(), self.z.min())

        self.sorted_element_indices = sorted_element_indices = np.argsort(keys)
        elements_tmp = self.elements[sorted_element_indices]
        self.elements = elements_tmp

        # read number of elements per partition
        nelements_part = np.ones(self.numPartitions, int) * -1
        nghosts_part   = np.ones((self.numPartitions, self.numPartitions), int) * -1
        indices_part   = [[] for i in range(self.numPartitions)]
        for i in range(1, self.numPartitions+1):
            indices_i = np.where(self.elements.data[:,5] == i)[0]
            for j in range(1, self.numPartitions+1):
                if (i != j):
                    indices_j = np.where(self.elements.data[:,5] == j)[0]
                    indices_ij = indices_j[np.where(self.elements.data[indices_j] == -i)[0]]
                    if (len(indices_ij) > 0):
                        indices_i = np.concatenate( (indices_i, indices_ij) )
                        nghosts_part[i-1, j-1] = len(indices_ij)

            nelements_part[i-1] = indices_i.size
            indices_part[i-1] = indices_i
            pass
         
        self.nremote_per_part  = nghosts_part
        self.nelements_per_part = nelements_part
        self.indices_per_part   = indices_part

    def _get_faces(self, partition):
        efaces = []

        nelements = self.nelements_per_part[partition]
        partition_data = self.elements.data[self.indices_per_part[partition]]
        partition_mask = self.elements.mask[self.indices_per_part[partition]]

        enodes = np.ones(shape=(nelements, self.elem_nnodes), dtype=int)

        for i in range(nelements):
            #data = self.elements.data[i][np.where(self.elements.mask[i] == False)[0]]
            data = partition_data[i][np.where(partition_mask[i] == False)[0]]

            efaces.append(
                [j for j in round_trip_connect(data[-self.elem_nnodes:], self.dim, self.etype)])

            # enodes[i] = self.elements.data[i][
            #     np.where(self.elements.mask[i] == False)[0]][-self.elem_nnodes:]
            enodes[i] = data[-self.elem_nnodes:]

        #efacesa = np.array(efaces)

        self.enodes[partition] = enodes

        face2elem = []
        face2vertices = []
        faces_done = []
        face_array = []
        nfaces = 0
        neighbors = np.ones(shape=(nelements, self.elem_nface), dtype=int ) * -1
        elem2face = np.ones(shape=(nelements, self.elem_nface), dtype=int ) *-1

        for i, face_list in enumerate(efaces):
            for j, face in enumerate(face_list):
                sorted_face = tuple(np.sort(face))

                if not sorted_face in faces_done:
                    face_array.append(face)

                    faces_done.append(sorted_face)
                    face2vertices.append(face)

                    face2elem.append( [i, -1] )
                    elem2face[i][j] = nfaces

                    tmp = []
                    for node in range(self.elem_nnodes):
                        tmp.append( np.where(enodes == enodes[i][node])[0] )

                    potential_neighbors = np.unique(np.concatenate(tmp))

                    for nbr in potential_neighbors:
                        if nbr != i:
                            for face_index in range(self.elem_nface):
                                if sorted_face == tuple(np.sort(efaces[nbr][face_index])):

                                    if self.dim == 2:
                                        nbr_index = (j+2)%self.elem_nface
                                    else:
                                        nbr_index = j

                                    face2elem[nfaces][1] = nbr
                                    elem2face[nbr][face_index] = nfaces
                                    
                                    neighbors[i][nbr_index] = nbr
                                    
                                    if self.dim == 2:
                                        neighbors[nbr][(face_index+2)%self.elem_nface] = i
                                    else:
                                        neighbors[nbr][face_index] = i

                    nfaces += 1                                

        self.faces[partition]        = np.array(face_array)
        self.sorted_faces[partition] = np.array(faces_done)
        self.nfaces[partition]       = nfaces

        self.face2elem[partition] = face2elem
        self.elem2face[partition] = elem2face

        self.neighbors[partition]     = neighbors
        self.face2vertices[partition] = face2vertices

    def _set_boundary_elements(self, partition):
        btags  = []
        belems = {}
        bfaces = {}

        for tagname, tagval in self.tags.iteritems():
            if tagval[0] != self.dim:

                btags.append(tagname)
                if self.etype == "quad" or self.etype == "tri":
                    indices = np.where(self.lines.data[:,2] == self.tags[tagname][1])[0]
                    #faces = np.ones( shape=(indices.size, 2), dtype=int )
                    faces = []
                    for i, index in enumerate(indices):
                        mask = self.lines.mask[index]
                        data = self.lines.data[index][np.where(mask == False)[0]]
                        #faces[i] = tuple(np.sort(self.lines.data[index][4:6]))
                        if data[5] == partition+1:
                            faces.append(tuple(np.sort(data[-2:])))

                elif self.etype == "hex":
                    indices = np.where(self.quads.data[:,2] == self.tags[tagname][1])[0]
                    #faces = np.ones( shape=(indices.size, 4), dtype=int )
                    faces = []
                    for i, index in enumerate(indices):
                        mask = self.quads.mask[index]
                        data = self.quads.data[index][np.where(mask == False)[0]]
                        #faces[i] = tuple(np.sort(self.quads.data[index][4:8]))
                        if data[5] == partition+1:
                            faces.append( tuple(np.sort(data[-4:])) )

                elif self.etype == "tet":
                    indices = np.where(self.tri.data[:,2] == self.tags[tagname][1])[0]
                    #faces = np.ones( shape=(indices.size, 3), dtype=int )
                    faces = []
                    for i, index in enumerate(indices):
                        mask = self.tri.mask[index]
                        data = self.tri.data[index][np.where(mask == False)[0]]
                        #faces[i] = tuple(np.sort(self.tri.data[index][4:7]))
                        if data[5] == partition+1:
                            faces.append( tuple(np.sort(data[-3:])) )

                faces = np.asarray(faces)

                nfaces = len(faces)

                belems[tagname] = np.zeros(nfaces, int)
                bfaces[tagname] = np.zeros(nfaces, int)

                for i in range(nfaces):
                    for k in range(self.nfaces[partition]):
                        if np.alltrue(self.sorted_faces[partition][k] == faces[i]):
                            glb_face_index = k

                            elem_index      = self.face2elem[partition][glb_face_index][0]
                            elem_face_index = np.where(
                                self.elem2face[partition][elem_index] == glb_face_index)[0]

                            belems[tagname][i] = elem_index
                            bfaces[tagname][i] = elem_face_index

        self.btags  = btags
        self.belems[partition] = belems
        self.bfaces[partition] = bfaces

        self.nghost[partition] = sum( [len(i) for i in belems.values()] )

        # save the face2elem array
        self.face2elem[partition] = np.asarray(self.face2elem[partition])

        # generate the face marker array
        face_markers = np.zeros(self.nfaces[partition], int)
        #print self.belems
        rows, cols = np.where(self.face2elem[partition] == -1)

        boundary_tags = [key for key in self.tags.keys() if self.tags[key][0] == self.dim-1]
        for tagname in boundary_tags:
            tagval = self.tags[tagname][1]
            belems = self.belems[partition][tagname]
            for i, elem in enumerate(belems):
                elem_face_index = self.bfaces[partition][tagname][i]
                face_index = self.elem2face[partition][elem][elem_face_index]
                face_markers[face_index] = tagval

        self.face_markers[partition] = face_markers

    def _set_regions(self, partition):
        fluid_elems = []

        # element attributes to distinguish regions
        nelements = self.nelements_per_part[partition]
        partition_data = self.elements.data[self.indices_per_part[partition]]

        self.elem_attrs[partition] = np.ones(nelements, dtype=int) #* self.tags['fluid'][1]

        for tagname, tagval in self.tags.iteritems():
            if tagval[0] == self.dim:
                indices = np.where(partition_data[:,2] == tagval[1])[0]
                self.elem_attrs[partition][indices] *= tagval[1]
                
        self.elem_type[partition] = elem_type = np.ones(nelements, dtype=int)

        # for i in range(nelements):
        #     for tagname in self.btags:
        #         if tagname.startswith("periodic"):
        #             if i in self.belems[partition][tagname]:
        #                 self.elem_type[partition][i] = -1
                    
        indices = np.where(self.elem_type == 1)[0]
        self.fluid_elems[partition] = indices

    def _get_nbr_faces(self, partition):
        dmesh = self.dmesh[partition]

        nbr_face_indices = np.ones_like(dmesh.neighbors) * -1

        for i in range(dmesh.nelements):
            elem_faces = np.array( [set(j) for j in dmesh.xfaces[dmesh.elem2face[i]]] )
            nbrs = dmesh.neighbors[i]
            
            for nbr_index, nbr_elem in enumerate(nbrs):
                if nbr_elem > -1:
                    nbr_faces = np.array( [set(j) for j in dmesh.xfaces[dmesh.elem2face[nbr_elem]]] )

                    for index, face in enumerate(elem_faces):
                        indices = np.where( nbr_faces == face )[0]
                        if indices.size > 0:
                            nbr_face_indices[i][nbr_index] = indices[0]

        dmesh.nbr_face_indices = nbr_face_indices

        faceindex2nbr = np.ones_like(dmesh.neighbors) * -1
        for i in range(dmesh.nelements):
            nbrs = dmesh.neighbors[i]
            for nbr_index, nbr_elem in enumerate(nbrs):
                if nbr_elem > -1:
                    nbr_face = dmesh.nbr_face_indices[i, nbr_index]
                    
                    faceindex2nbr[nbr_elem, nbr_face] = i

        dmesh.faceindex2nbr = faceindex2nbr

        dmesh.faceindex2nbrfaceindex = np.ones_like(dmesh.neighbors) * -1
        for i in range(dmesh.nelements):
            for index, nbr_elem in enumerate(dmesh.faceindex2nbr[i]):
                if nbr_elem > -1:
                    nbr_face_index = dmesh.nbr_face_indices[
                        i, np.where(dmesh.neighbors[i] == nbr_elem)[0]]

                    dmesh.faceindex2nbrfaceindex[i, index] = nbr_face_index

    def setup_periodic_exchange_data(self):
        numPartitions = self.numPartitions

        if self.dim == 2:
            max_num_export_periodic = self.nlines
        else:
            max_num_export_periodic = max(self.nquads, self.ntri)

        tags = [ ['periodic_lo_x', 'periodic_hi_x'],
                 ['periodic_hi_x', 'periodic_lo_x'],
                 ['periodic_lo_y', 'periodic_hi_y'],
                 ['periodic_hi_y', 'periodic_lo_y'],
                 ['periodic_lo_z', 'periodic_hi_z'],
                 ['periodic_hi_z', 'periodic_lo_z'] ]

        ntags = len(tags)

        nexport_per_part_periodic = np.zeros((self.numPartitions, self.numPartitions, ntags), np.int32)
        nimport_per_part_periodic = np.zeros((self.numPartitions, self.numPartitions, ntags), np.int32)

        #export_indices_periodic = [[] for i in range(self.numPartitions)]
        #import_indices_periodic = [[] for i in range(self.numPartitions)]

        nghosts = []
        for i in range(1, self.numPartitions+1):
            nghosts.append( self.dmesh[i-1].nghost )

        max_nghost = np.max(nghosts)
        
        export_indices_periodic = np.ones((numPartitions,numPartitions,max_nghost,ntags), np.int64) * -1
        export_faces_periodic   = np.ones((numPartitions,numPartitions,max_nghost,ntags), np.int64) * -1
        ecp = np.zeros((numPartitions, numPartitions, ntags), np.int64)

        import_indices_periodic = np.ones((numPartitions,numPartitions,max_nghost,ntags), np.int64) * -1
        import_faces_periodic   = np.ones((numPartitions,numPartitions,max_nghost,ntags), np.int64) * -1
        icp = np.zeros((numPartitions, numPartitions, ntags), np.int64)

        # eip = [ [[]]*ntags for i in range(self.numPartitions)]

        # eip = [ {'periodic_lo_x':[], 'periodic_hi_x':[],
        #          'periodic_lo_y':[], 'periodic_hi_y':[],
        #          'periodic_lo_z':[], 'periodic_hi_z':[]} for i in range(self.numPartitions) ]

        # iip = [ {'periodic_lo_x':[], 'periodic_hi_x':[],
        #          'periodic_lo_y':[], 'periodic_hi_y':[],
        #          'periodic_lo_z':[], 'periodic_hi_z':[]} for i in range(self.numPartitions) ]

        # go over each partition
        for i in range(1, self.numPartitions+1):
            i_dmsh = self.dmesh[i-1]

            for tagnum in range(ntags):
                tag, opp_tag = tags[tagnum]
                if (tag in self.btags):
                    print 'Doing tag', tag
                    i_belems = self.belems[i-1][tag]
                    i_bfaces = self.bfaces[i-1][tag]

                    for index, elem_index in enumerate(i_belems):
                        face_index = i_dmsh.elem2face[elem_index][i_bfaces[index]]
                        pts = i_dmsh.points[i_dmsh.xfaces[face_index]]
                        npts = 1./pts.shape[0]

                        # get the face centroid for the periodic face
                        xc = npts*sum(pts[:, 0]); yc = npts*sum(pts[:, 1]); zc = 0.0
                        if self.dim == 3: 
                            zc = npts*sum(pts[:, 2])

                        # now search for matching faces in other
                        # partitions
                        for j in range(1, self.numPartitions+1):
                            if i != j:
                                j_dmsh = self.dmesh[j-1]

                                j_belems = self.belems[j-1][opp_tag]
                                j_bfaces = self.bfaces[j-1][opp_tag]

                                for j_index, j_elem_index in enumerate(j_belems):
                                    j_face_index = j_dmsh.elem2face[j_elem_index][j_bfaces[j_index]]
                                    pts = j_dmsh.points[j_dmsh.xfaces[j_face_index]]
                                    npts = 1./pts.shape[0]

                                    # get the face centroid
                                    jxc = npts*sum(pts[:, 0]); jyc = npts*sum(pts[:, 1]); jzc = 0.0
                                    if self.dim == 3:
                                        jzc = npts*sum(pts[:, 2])
                        
                                    # check if the centroid is the same                                
                                    if   tag.endswith('y'):
                                        diff = (xc-jxc)**2 + (zc-jzc)**2
                                    elif tag.endswith('x'):
                                        diff = (yc-jyc)**2 + (zc-jzc)**2
                                    elif tag.endswith('z'):
                                        diff = (xc-jxc)**2 + (yc-jyc)**2

                                    if ( diff < 1e-12 ):
                                        nimport_per_part_periodic[i-1, j-1, tagnum] += 1

                                        import_indices_periodic[i-1,j-1,icp[i-1,j-1,tagnum],tagnum] = elem_index
                                        import_faces_periodic[i-1,  j-1,icp[i-1,j-1,tagnum],tagnum] = face_index
                                        icp[i-1,j-1,tagnum] += 1

                                        nexport_per_part_periodic[j-1, i-1, tagnum] += 1

                                        export_indices_periodic[j-1,i-1,ecp[j-1,i-1,tagnum],tagnum] = j_elem_index
                                        export_faces_periodic[j-1,  i-1,ecp[j-1,i-1,tagnum],tagnum] = j_face_index
                                        ecp[j-1,i-1,tagnum] += 1

                                        #iip[i-1][tag].append(elem_index)
                                        #eip[j-1][tag].append(j_elem_index)

        #self.eip = eip
        #self.iip = iip
        self.nexport_per_part_periodic = nexport_per_part_periodic
        self.nimport_per_part_periodic = nimport_per_part_periodic

        self.export_indices_periodic = export_indices_periodic
        self.export_faces_periodic   = export_faces_periodic

        self.nimport_per_part_periodic = self.icp = icp
        self.nexport_per_part_periodic = self.ecp = ecp

        max_numImport_periodic = icp.max()
        max_numExport_periodic = ecp.max()

        for i in range(self.numPartitions):
            idmsh = self.dmesh[i]
            
            exportIndices = []
            exportFaces   = []

            importIndices = []
            importFaces   = []        

            for j in range(self.numPartitions):
                if i != j:
                    jdmsh = self.dmesh[j]
                
                    for tag in range(ntags):
                        eij = export_indices_periodic[i][j][:, tag]
                        fij = export_faces_periodic[  i][j][:, tag]

                        indices_to_export  = np.where(eij != -1)[0]
                        for index in range(len(indices_to_export)):
                            exportIndices.append( eij[index] )
                            exportFaces.append(   fij[index] )

                        eij = import_indices_periodic[i][j][:, tag]
                        fij = import_faces_periodic[  i][j][:, tag]
                        indices_to_import = np.where(eij != -1)[0]

                        for index in range(len(indices_to_import)):
                            importIndices.append( eij[index] )
                            importFaces.append(   fij[index] )

            idmsh.exportIndices_periodic    = exportIndices
            idmsh.exportFaces_periodic      = exportFaces
            idmsh.nexport_per_part_periodic = ecp[i]
            
            idmsh.importIndices_periodic    = importIndices
            idmsh.importFaces_periodic      = importFaces
            idmsh.nimport_per_part_periodic = icp[i]
        
        self.import_indices_periodic = import_indices_periodic
        self.import_faces_periodic   = import_faces_periodic

    # def _setup_local_bcmaps(self, partition):
    #     dmesh = self.dmesh[partition]
    #     bc_map = dmesh.bc_map

    #     tol = 1e-12

    #     tags = [ ['periodic_lo_x', 'periodic_hi_x'],
    #              ['periodic_hi_x', 'periodic_lo_x'],
    #              ['periodic_lo_y', 'periodic_hi_y'],
    #              ['periodic_hi_y', 'periodic_lo_y'],
    #              ['periodic_lo_z', 'periodic_hi_z'],
    #              ['periodic_hi_z', 'periodic_lo_z'] ]

    #     ntags = len(tags)
    #     for tagnum in range(ntags):
    #         tag, opp_tag = tags[tagnum]
    #         if tag in self.btags:
    #             print 'Doing tag', tag

    #             tag_belems = self.belems[partition][tag]
    #             tag_bfaces = self.bfaces[partition][tag]

    #             opp_tag_belems = self.belems[partition][opp_tag]
    #             opp_tag_bfaces = self.bfaces[partition][opp_tag]

    #             for i_index, i_elem_index in enumerate(tag_belems):
    #                 i_face_index = dmesh.elem2face[i_elem_index][tag_bfaces[i_index]]
    #                 pts = dmesh.points[dmesh.xfaces[i_face_index]]
    #                 npts = 1./pts.shape[0]
                    
    #                 # get the face centroid for the periodic face
    #                 xc1 = npts*sum(pts[:, 0]); yc1 = npts*sum(pts[:, 1]); zc1 = 0.0
    #                 if self.dim == 3: 
    #                     zc1 = npts*sum(pts[:, 2])
                        
    #                 found_matching = False

    #                 # now search for oppsite tag faces
    #                 for j_index, j_elem_index in enumerate(opp_tag_belems):
    #                     j_face_index = dmesh.elem2face[j_elem_index][opp_tag_bfaces[j_index]]
    #                     pts = dmesh.points[dmesh.xfaces[j_face_index]]
    #                     npts = 1./pts.shape[0]

    #                     # get th face centroid for the opposite face
    #                     xc2 = npts*sum(pts[:, 0]); yc2 = npts*sum(pts[:, 1]); zc2 = 0.0
    #                     if self.dim == 3: 
    #                         zc2 = npts*sum(pts[:, 2])
                    
    #                     # check if the face centroids match
    #                     if   tag.endswith('y'):
    #                         diff = (xc1-xc2)**2 + (zc1-zc2)**2
    #                     elif tag.endswith('x'):
    #                         diff = (yc1-yc2)**2 + (zc1-zc2)**2
    #                     elif tag.endswith('z'):
    #                         diff = (xc1-xc2)**2 + (yc1-yc1)**2

    #                     if diff < tol:
    #                         found_matching = True
    #                         bc_map[i_elem_index


    #     for tag in self.btags:
    #         if tag == 'periodic_lo_y':
    #             loy = self.belems[partition][tag]
    #             hiy = self.belems[partition]['periodic_hi_y']

    #             xc_hiy = np.zeros(hiy.size); yc_hiy = np.zeros(hiy.size); zc_hiy = np.zeros(hiy.size)

    #             vertices = dmesh.points[dmesh.element2vertices[hiy]]
    #             for i in range(hiy.size):
    #                 xc_hiy[i] = factor * sum(vertices[i,:,0])
    #                 yc_hiy[i] = factor * sum(vertices[i,:,1])
    #                 if self.dim == 3: zc_hiy[i] = factor * sum(vertices[i,:,2])

    #             xc_loy = np.zeros(loy.size); yc_loy = np.zeros(loy.size); zc_loy = np.zeros(loy.size)

    #             vertices = dmesh.points[dmesh.element2vertices[loy]]
    #             for i in range(loy.size):
    #                 xc_loy[i] = factor * sum(vertices[i,:,0])
    #                 yc_loy[i] = factor * sum(vertices[i,:,1])
    #                 if self.dim == 3: zc_loy[i] = factor * sum(vertices[i,:,2])

    #             # map cells
    #             for i in range(loy.size):
    #                 elem_index = loy[i]
    #                 xc1 = xc_loy[i]; zc1 = 0.0
    #                 if self.dim == 3: zc1 = zc_loy[i]

    #                 opposite_element = -1
    #                 for j in range(hiy.size):
    #                     xc2 = xc_hiy[j]; zc2 = 0.0
    #                     if self.dim == 3: zc2 = zc_hiy[j]
                        
    #                     diff = np.sqrt( (xc1-xc2)**2 + (zc1-zc2)**2 )
    #                     if (diff < tol):
    #                         opposite_element = hiy[j]
    #                         break
                            
    #                 if (opposite_element < 0):
    #                     raise ValueError("Opposite element not found! periodic_Y")

    #                 # set the mapped elements
    #                 for nbr in dmesh.neighbors[opposite_element]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[elem_index] = nbr

    #                 for nbr in dmesh.neighbors[elem_index]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[opposite_element] = elem_index

    #         elif tag == 'periodic_lo_x':
    #             lox = self.belems[partition][tag]
    #             hix = self.belems[partition]['periodic_hi_x']

    #             xc_lox = np.zeros(lox.size); yc_lox = np.zeros(lox.size); zc_lox = np.zeros(lox.size)

    #             vertices = dmesh.points[dmesh.element2vertices[lox]]
    #             for i in range(lox.size):
    #                 xc_lox[i] = factor * sum(vertices[i, :, 0])
    #                 yc_lox[i] = factor * sum(vertices[i, :, 1])
    #                 if self.dim == 3: zc_lox[i] = factor * sum(vertices[i, :, 2])

    #             xc_hix = np.zeros(hiy.size); yc_hix = np.zeros(hiy.size); zc_hix = np.zeros(hiy.size)

    #             vertices = dmesh.points[dmesh.element2vertices[hix]]
    #             for i in range(hix.size):
    #                 xc_hix[i] = factor * sum(vertices[i, :, 0])
    #                 yc_hix[i] = factor * sum(vertices[i, :, 1])
    #                 if self.dim == 3: zc_hix[i] = factor * sum(vertices[i, :, 2])

    #             # map cells
    #             for i in range(lox.size):
    #                 elem_index = lox[i]
    #                 yc1 = yc_lox[i]; zc1 = 0.0
    #                 if self.dim == 3: zc1 = zc_lox[i]

    #                 opposite_element = -1
    #                 for j in range(hix.size):
    #                     yc2 = yc_hix[j]; zc2 = 0
    #                     if self.dim == 3: zc2 = zc_hix[j]

    #                     diff = np.sqrt( (yc1 - yc2)**2 + (zc1 - zc2)**2 )
    #                     if (diff < tol):
    #                         opposite_element = hix[j]
    #                         break

    #                 if (opposite_element < 0):
    #                     raise ValueError("Opposite element not found")

    #                 for nbr in dmesh.neighbors[opposite_element]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[elem_index] = nbr

    #                 for nbr in dmesh.neighbors[elem_index]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[opposite_element] = nbr

    #         elif tag == "periocic_lo_z":
    #             loz = self.belems[partition][tag]
    #             hiz = self.belems[partition]['periocic_hi_z']

    #             xc_loz = np.zeros(loz.size); yc_loz = np.zeros(loz.size); zc_loz = np.zeros(loz.size)

    #             vertices = dmesh.points[dmesh.element2vertices[loz]]
    #             for i in range(loz.size):
    #                 xc_loz[i] = factor * sum(vertices[i, :, 0])
    #                 yc_loz[i] = factor * sum(vertices[i, :, 1])
    #                 if self.dim == 3: zc_loz[i] = factor * sum(vertices[i, :, 2])

    #             xc_hiz = np.zeros(hiz.size); yc_hiz = np.zeros(hiz.size); zc_hiz = np.zeros(hiz.size)

    #             vertices = dmesh.points[dmesh.element2vertices[hiz]]
    #             for i in range(hiz.size):
    #                 xc_hiz[i] = factor * sum(vertices[i, :, 0])
    #                 yc_hiz[i] = factor * sum(vertices[i, :, 1])
    #                 zc_hiz[i] = factor * sum(vertices[i, :, 2])

    #             # map cells
    #             for i in range(loz.size):
    #                 elem_index = loz[i]
    #                 xc1 = xc_loz[i]; yc1 = yc_loz[i]

    #                 opposite_element = -1
    #                 for j in range(hiz.size):
    #                     xc2 = xc_hiz[j]; yc2 = yc_hiz[j]

    #                     diff = np.sqrt( (xc1-xc2)**2 + (yc1-yc2)**2 )
    #                     if (diff < tol):
    #                         opposite_element = hiz[j]
    #                         break
                    
    #                 if (opposite_element < 0):
    #                     raise RuntimeError("Opposite element not found! Periodic-Z")
                    
    #                 for nbr in dmesh.neighbors[opposite_element]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[elem_index] = nbr
                    
    #                 for nbr in dmesh.neighbors[elem_index]:
    #                     if nbr in self.fluid_elems[partition]: bc_map[opposite_element] = nbr
