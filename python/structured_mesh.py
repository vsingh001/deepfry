import numpy

npts = 5
ncells = (npts-1)**2
nelements = ncells*2
nfaces = nelements*3 - ncells - 2*(npts-1)*(npts-2)

dx = 1./2
xx = numpy.arange(0-dx,1+dx+1e-10, dx)
x, y = numpy.meshgrid(xx,xx); x = x.ravel(); y = y.ravel()

pnts = numpy.empty(shape=(x.size,2))
pnts[:,0] = x; pnts[:,1] = y

elements = numpy.ones(shape=(nelements,3), dtype=int)*-1
neighbors = numpy.ones(shape=(nelements,3), dtype=int)*-1
faces = numpy.ones(shape=(nfaces,2), dtype=int)*-1
elem2face = numpy.ones(shape=(nelements, 3), dtype=int)*-1
face2elem = numpy.ones(shape=(nfaces, 2), dtype=int)*-1

# generate the element2vertices and face2vertices arrays
n1d = npts-1

_element = 0
_face = 0
for i in range(n1d):
    for j in range(n1d):
        cell_index = i*(npts-1) + j
        
        xa_id = cell_index + j
        xb_id = xa_id + 1
        xc_id = xb_id + npts
        xd_id = xa_id + npts
        
        elements[_element] = xa_id, xb_id, xd_id
        _element +=1

        elements[_element] = xb_id, xc_id, xd_id
        _element += 1

_face = 0
for elem in range(nelements):
    cell_index = elem/2
    
    i = cell_index/n1d
    j = cell_index - i*(n1d)

    xv = elements[elem]
    if (elem %2) == 0:
        neighbors[elem][0] = elem + 1
        neighbors[elem][1] = elem - 1
        neighbors[elem][2] = 2*(cell_index-n1d)+1

        if i == 0:
            if j == 0:
                faces[_face] = xv[0], xv[1]
                face2elem[_face][0] = elem
                face2elem[_face][1] = 2*(cell_index-n1d)+1

                elem2face[elem][0] = _face

                _face += 1

                faces[_face] = xv[1], xv[2]
                face2elem[_face][0] = elem
                face2elem[_face][1] = elem + 1

                elem2face[elem][1] = _face
                elem2face[elem+1][2] = _face

                _face += 1

                faces[_face] = xv[2], xv[0]
                face2elem[_face][0] = elem
                face2elem[_face][1] = elem - 1
                
                elem2face[elem][2] = _face

                _face += 1

            else:
                faces[_face] = xv[0], xv[1]
                face2elem[_face][0] = elem
                face2elem[_face][1] = 2*(cell_index-n1d)+1
                
                elem2face[elem][0] = _face

                _face += 1

                faces[_face] = xv[1], xv[2]
                face2elem[_face][0] = elem
                face2elem[_face][1] = elem + 1

                elem2face[elem][1] = _face
                elem2face[elem+1][2] = _face
                
                _face += 1

        elif j == 0:
            faces[_face] = xv[1], xv[2]
            face2elem[_face][0] = elem
            face2elem[_face][1] = elem + 1

            elem2face[elem][1] = _face
            elem2face[elem+1][2] = _face
            
            _face += 1

            faces[_face] = xv[2], xv[1]
            face2elem[_face][0] = elem
            face2elem[_face][1] = - 1

            elem2face[elem][2] = _face

            _face += 1
        else:
            faces[_face] = xv[1], xv[2]
            face2elem[_face][0] = elem
            face2elem[_face][1] = elem + 1
            
            elem2face[elem][1] = _face
            elem2face[elem+1][2] = _face

            _face += 1

    else:
        neighbors[elem][0] = 2*(cell_index+n1d)
        neighbors[elem][1] = elem-1
        neighbors[elem][2] = elem+1

        if j == n1d-1:
            faces[_face] = xv[0], xv[1]
            face2elem[_face][0] = elem
            face2elem[_face][1] = -1

            elem2face[elem][0] = _face

            _face += 1
        else:
            faces[_face] = xv[0], xv[1]
            face2elem[_face][0] = elem
            face2elem[_face][1] = elem + 1

            elem2face[elem][0] = _face
            elem2face[elem+1][2] = _face

            _face += 1

        faces[_face] = xv[1], xv[2]
        face2elem[_face][0] = elem
        face2elem[_face][1] = 2*(cell_index+n1d)

        elem2face[elem][1] = _face
        if i != n1d-1:
            elem2face[2*(cell_index+n1d)][0] = _face

        _face += 1
        
    # clip the values
    rows, cols = numpy.where(neighbors < 0)
    for i in range(rows.size):
        neighbors[rows[i]][cols[i]] = -1

    rows, cols = numpy.where(neighbors >= nelements)
    for i in range(rows.size):
        neighbors[rows[i]][cols[i]] = -1

    rows, cols = numpy.where(face2elem < 0)
    for i in range(rows.size):
        face2elem[rows[i]][cols[i]] = -1
        
    rows, cols = numpy.where(face2elem >= nelements)
    for i in range(rows.size):
        face2elem[rows[i]][cols[i]] = -1
