HAVE_PYVISFILE=True
try:
    import pyvisfile
except ImportError:
    HAVE_PYVISFILE=False

if not HAVE_PYVISFILE:
    raise RuntimeError("PyVisfile not installed!")

HAVE_H5PY=True
try:
    import h5py
except ImportError:
    raise RuntimeError("H5PY not installed!")

import h5py
from pyvisfile.vtk import (UnstructuredGrid, DataArray, AppendedDataXMLGenerator,
                           VTK_VERTEX, VF_LIST_OF_VECTORS, VF_LIST_OF_COMPONENTS)


import numpy as np
import optparse
import os

################################################################################
# Get all solution files in a given directory
############################################################################### 
def get_files(dirname=None, fname=None, partno=0):

    if dirname is None:
        return []

    if fname is None:
        fname = dirname.split("_output")[0]

    path = os.path.abspath( dirname )
    files = os.listdir( path )

    # get all the output files in the directory
    files = [f for f in files if f.startswith(fname) and f.endswith(".hdf5") ]
    files = [f for f in files if 'part%03d'%partno in f]
    files = [os.path.join(path, f) for f in files]

    # sort the files
    def _sort_func(x, y):
        """Sort the files correctly."""
        def _process(arg):
            a = os.path.splitext(arg)[0]
            return int(a[a.rfind('_')+1:])
        return cmp(_process(x), _process(y))

    files.sort(_sort_func)

    return files

class OutFile(object):
    def __init__(self, meshfile, partno, files, read_only_local=True):
        self.meshfile = meshfile
        self.partno   = partno
        self.read_only_local = read_only_local
        self.files = files

    def _read_meshinfo(self):
        self.meshinfo = f = h5py.File('part_%03d.nodes'%(self.partno), 'r')
        meshinfo = np.array(f['meshinfo'])
        
        self.order     = meshinfo[0]
        self.Np        = meshinfo[1]
        self.elem_type = meshinfo[2]
        self.nelements = meshinfo[3]
        self.nelements_local = meshinfo[4]

    def _write_nodes(self):
        f = self.meshinfo

        nelements = self.nelements
        if self.read_only_local: nelements = self.nelements_local

        x = np.array(f['x'])[:nelements*self.Np]
        y = np.array(f['y'])[:nelements*self.Np]
        z = np.array(f['z'])[:nelements*self.Np]

        points = np.empty((x.size, 3))
        points[:, 0] = x
        points[:, 1] = y
        points[:, 2] = z

        grid = UnstructuredGrid(
            (nelements*self.Np, DataArray("points", points, vector_format=VF_LIST_OF_VECTORS)),
            cells=np.arange(x.size, dtype=np.uint32),
            cell_types=np.asarray([VTK_VERTEX]*x.size, dtype=np.uint8))

        file_name = "test.vtu"
        compressor = None
        outf = open(file_name, "w")
        AppendedDataXMLGenerator(compressor)(grid).write(outf)
        outf.close()        

if __name__ == '__main__':
    parser = optparse.OptionParser()

    parser.add_option("-m", "--meshfile", type="str", action="store",
                      dest="meshfile", default=None)

    parser.add_option("-d", "--dir", type="str", action="store",
                      dest="dir", default="./")

    parser.add_option("-o", "--output-file", type="str", action="store", 
                      dest="output_file", default=None)

    parser.add_option("-p", "--num-partition", type="int", action="store",
                      dest="num_partition", default=0)

    parser.add_option("--local", type="bool", action="store",
                      dest="local", default=True)

    options, args = parser.parse_args()
