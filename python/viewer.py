from tvtk.api import tvtk
from mayavi.scripts import mayavi2

from tvtk.array_handler import array2vtk

import numpy
from meshpy_examples import airfoil2d, test2D, isentropic_vortex2D
import grid

def get_gmsh_mesh():
    import gmsh_read as gmsh
    g = gmsh.Gmsh2D('gmsh_examples/wedge.msh')
    
    points = g.points
    elements = g.elements
    quad_type = tvtk.Quad().cell_type

    ug = tvtk.UnstructuredGrid(points=points)
    ug.set_cells(quad_type, elements)

    return ug, g

def get_mesh():
    #grid = test2D()
    grid = isentropic_vortex2D()

    _mesh = grid._mesh

    points = numpy.zeros( shape=(grid.npoints, 3) )
    points[:, 0] = grid.points[:, 0]
    points[:, 1] = grid.points[:, 1]

    triangles = grid.elements
    triangle_type = tvtk.Triangle().cell_type

    ug = tvtk.UnstructuredGrid(points=points)
    ug.set_cells(triangle_type, triangles)
    
    return ug, grid

ug, g = get_mesh()

npoints = len(ug.points)
velocity = numpy.random.randn(npoints, 3)
temp = numpy.random.random(npoints)

ncells = g.nelements
nghost = g.nghost

cellid = numpy.zeros( ncells )
cellid[-nghost:] = 1

cellid = tvtk.to_tvtk( array2vtk(cellid) ); cellid.name = "id"


rho = g.uavg[:,0]
Mx  = g.uavg[:,1]
My  = g.uavg[:,2]
E   = g.uavg[:,3]

rho_x = g.ux_avg[:, 0]
Mx_x = g.ux_avg[:, 1]
My_x = g.ux_avg[:, 2]
E_x  = g.ux_avg[:, 3]

rho_y = g.uy_avg[:, 0]
Mx_y  = g.uy_avg[:, 1]
My_y  = g.uy_avg[:, 2]
E_y   = g.uy_avg[:, 3]

velx = Mx/rho; vely = My/rho
p = (1.4-1.0)*(E - 0.5*rho*(velx**2 + vely**2))

velx_x = 1./rho*(Mx_x - velx*rho_x)
velx_y = 1./rho*(Mx_y - velx*rho_y)

vely_x = 1./rho*(My_x - vely*rho_x)
vely_y = 1./rho*(My_y - vely*rho_y)

vort = vely_x - velx_y

rho = tvtk.to_tvtk( array2vtk(rho) ); rho.name = "density"
velx = tvtk.to_tvtk( array2vtk(velx) ); velx.name = "velx"
vely = tvtk.to_tvtk( array2vtk(vely) ); vely.name = "vely"
p = tvtk.to_tvtk( array2vtk(p) ); p.name = "p"

vort = tvtk.to_tvtk( array2vtk(vort) ); vort.name = "vort"
rho_x = tvtk.to_tvtk( array2vtk(rho_x) ); rho_x.name = "rho_x"
rho_y = tvtk.to_tvtk( array2vtk(rho_y) ); rho_y.name = "rho_y"

ug.cell_data.add_array(cellid)
ug.cell_data.add_array(rho)
ug.cell_data.add_array(velx)
ug.cell_data.add_array(vely)
ug.cell_data.add_array(p)

ug.cell_data.add_array(vort)
ug.cell_data.add_array(rho_x)
ug.cell_data.add_array(rho_y)

ug.cell_data.set_active_scalars("density")

#ug.point_data.vectors = velocity
#ug.point_data.vectors.name = "velocity"

@mayavi2.standalone
def view():
    from mayavi.sources.vtk_data_source import VTKDataSource
    from mayavi.modules.outline import Outline
    from mayavi.modules.surface import Surface
    from mayavi.modules.vectors import Vectors

    mayavi.new_scene()
    src = VTKDataSource(data = ug)
    mayavi.add_source(src)

    mayavi.add_module(Outline())

    surface = Surface()

    mayavi.add_module(surface)
    surface.actor.property.representation = "surface"
    surface.actor.property.shading = "true"
    surface.actor.property.edge_visibility = True
    surface.actor.property.line_width = 0.5

    engine = mayavi.engine
    scene = engine.scenes[0]
    vtk_src = scene.children[0]
    mm = vtk_src.children[0]
    modules = mm.children
    surface = modules[1]

    # surface.actor.property.linewidth = 2.0
    # surface.actor.property.representation = "wireframe"
    # vtk_src.point_scalars_name = ''

if __name__ == "__main__":
    view()
