"""Example scripts for some 2D meshes"""

import meshpy.triangle as triangle

import polynomials as poly

# BOUNDARY CONDITION CONSTANTS TO IDENTIFY THE MESH
from grid import TransmissiveBoundary
from grid import FaceType
from grid import Grid2D

# MeshType
from grid import MeshType

# NumPy
import numpy

def round_trip_connect(seq):
      result = []
      for i in range(len(seq)):
        result.append((seq[i], seq[(i+1)%len(seq)]))
      return result

def test2D(xlo=-0.5, xhi=0.5, ylo=-0.5, yhi = 0.5):
      points = [ (xlo, ylo), (xhi, ylo), (xhi, yhi), (xlo, yhi) ]
      info = triangle.MeshInfo()
      info.set_points(points)

      info.set_facets( facets=round_trip_connect([0,1,2,3]), 
                       facet_markers=[0,0,0,0] )

      info.regions.resize(1)
      info.regions[0] = ( 0.0, 0.0, 0, 0.00025 )

      # Generate the mesh
      mesh = triangle.build(info,
                            generate_faces=True,
                            verbose=False,
                            attributes=True,
                            volume_constraints=True,
                            quality_meshing=True)
      
      mesh.type = MeshType.MeshPy

      g = Grid2D(mesh, 3)
      for i in range(g.nelements):
            xs, ys = g.get_element_solution_points(i)
            g.u[i,:,0] = numpy.exp(-20*(xs**2+ys**2))

            g.u[i,:,1] =  xs - ys
            g.u[i,:,2] =  2392.22

      g.get_gradients_at_solution_points()
      g.get_gradients_at_flux_points()
      g.compute_cell_averages()

      # we will test the interpolation of the linear function (x-y) (var=1) at
      # the flux points.
      g.interpolate_at_flux_points()
      g.get_common_solution_values()
      
      Np1 = g.N + 1
      for elem in range(g.nelements):
            xe = g.get_element_vertices(elem)
            xv = xe[:, 0]; yv = xe[:, 1]

            # get the face coordinates in physical space which will be
            # used for testing
            for face in range(3):
                  xf, yf = g.get_flux_points(elem, face)
                  
                  # test against the expected value
                  expected = xf - yf
                  uinterp = g.uflux[elem,face*Np1:(face+1)*Np1,1]

                  assert( numpy.allclose(uinterp, expected, atol=1e-12, rtol=1e-12) )
      return g

def shocktube2D(xl=-0.6, xr=0.6):
    """Generate a 2D mesh for the shocktube problem"""

    boundaryid = TransmissiveBoundary.id

    xmid = 0.5 * (xl + xr)
    points = [ (xl, 0.0), (xmid, 0), (xr, 0), (xr, 1), (xmid, 1), (xl, 1) ]
    #             0          1           2       3          4         5

    # generate the basic mesh and set the points
    info = triangle.MeshInfo()
    info.set_points(points)

    # Set the facets and markers.
    info.set_facets( facets=round_trip_connect( [0, 1, 4, 5] ) + \
                     round_trip_connect( [1, 2, 3, 4] ),

                     facet_markers=[ boundaryid, 0, boundaryid,
                                     boundaryid, boundaryid,
                                     boundaryid, boundaryid, 0]
                     )

    # set the two regions for the pre and post states
    info.regions.resize(2)
    info.regions[0] = ( 0.5 * (xl + xmid), 0.5, 0, 0.001 )
    info.regions[1] = ( 0.5 * (xmid + xr), 0.5, 1, 0.001 )

    # Generate the mesh
    mesh = triangle.build(info,
                          generate_faces=True,
                          verbose=False,
                          attributes=True,
                          volume_constraints=True,
                          quality_meshing=True)

    mesh.type = MeshType.MeshPy

    return Grid2D(mesh, 4)


def airfoil2d(xlo=-4, xhi=8, ylo=-4, yhi=4):
    
    # define the domain boundary
    points = [ (xlo,ylo), (xhi,ylo), (xhi,yhi), (xlo,yhi) ]
    #              0          1          2          3

    # get the coordinates of the airfoil
    surf_mesh = numpy.loadtxt('naca0012.txt')
    x = surf_mesh[:,0]; y = surf_mesh[:, 1]

    # dt = 2*numpy.pi/100
    # t  = numpy.arange(0, 2*numpy.pi,dt)
    # x = numpy.cos(t); y = numpy.sin(t)
    npnts = x.size    

    points += [(x[i], y[i]) for i in range(npnts)]

    # generate the basic mesh for the domain and set the points
    info = triangle.MeshInfo()
    info.set_points(points)    

    # Set the facets and markers.
    inflow_id  = FaceType.Inflow
    outflow_id = FaceType.Outflow
    wall_id    = FaceType.SolidWall

    facets = round_trip_connect([0,1,2,3]) + round_trip_connect(range(4,npnts+4))
    facet_markers = [outflow_id, outflow_id, outflow_id, inflow_id] + \
                    list(numpy.ones(npnts, dtype=int)*wall_id)

    info.set_facets( facets=facets, facet_markers=facet_markers )

    info.regions.resize(1)
    info.regions[0] = ( xlo+1, ylo+1, 0, 0.1 )

    info.holes.resize(1)
    info.holes[0] = (0,0)

    # Generate the mesh
    mesh = triangle.build(info,
                          generate_faces=True,
                          verbose=False,
                          attributes=True,
                          volume_constraints=True,
                          quality_meshing=True)

    mesh.type = MeshType.MeshPy

    return Grid2D(mesh, 4)

def isentropic_vortex2D(xlo=-5.0, xhi=5.0, ylo=-5.0, yhi=5.0):
      gamma = 1.4; gamma1 = gamma - 1; gamma1i = 1./gamma1
      eps = 5.0
      def _isentropic_vortex(x, y):
            pi = numpy.pi; exp = numpy.exp
            r2 = x**2 + y**2
            
            delu = -y*eps/(2*pi)*exp( (1-r2)/2. )
            delv = +x*eps/(2*pi)*exp( (1-r2)/2. )
            delT = -gamma1*eps**2/(8*gamma*pi**2)*exp(1-r2)
      
            u = 1. + delu
            v = 1. + delv
            T = 1. + delT

            rho = T**gamma1i
            p   = T*rho

            return rho, u, v, p

      points = [ (xlo, ylo), (xhi, ylo), (xhi, yhi), (xlo, yhi) ]
      info = triangle.MeshInfo()
      info.set_points(points)

      info.set_facets( facets=round_trip_connect([0,1,2,3]), 
                       facet_markers=[0,0,0,0] )

      info.regions.resize(1)
      info.regions[0] = ( 0.0, 0.0, 0, 0.02 )

      # Generate the mesh
      mesh = triangle.build(info,
                            generate_faces=True,
                            verbose=False,
                            attributes=True,
                            volume_constraints=True,
                            quality_meshing=True)
      
      mesh.type = MeshType.MeshPy

      g = Grid2D(mesh, 4)
      print g.nelements
      for i in range(g.nelements):
            xs, ys = g.get_element_solution_points(i)

            _rho, _u, _v, _p = _isentropic_vortex(xs, ys)
            g.u[i, :, 0] = _rho
            g.u[i, :, 1] = _rho*_u
            g.u[i, :, 2] = _rho*_v
            g.u[i, :, 3] = _rho*(0.5*(_u**2 + _v**2) + _p/( (1.4-1)*_rho ))

      # compute gradients and cell averages
      g.get_gradients_at_solution_points()
      g.get_gradients_at_flux_points()
      g.compute_cell_averages()

      # interpolate the solution at the flux points and get the common
      # solution values
      g.interpolate_at_flux_points()
      g.get_common_solution_values()

      return g
