import sys
import imp
import os

import vtk
import optparse

from subprocess import call

curdir = os.path.abspath(os.curdir)
thisdir = os.path.realpath(__file__).split('vtk')[0]

def mkdir(newdir):
    """works the way a good mkdir should :)
        - already exists, silently complete
        - regular file in the way, raise an exception
        - parent directory(ies) does not exist, make them as well
    """
    if os.path.isdir(newdir):
        pass

    elif os.path.isfile(newdir):
        raise OSError("a file with the same name as the desired " \
                      "dir, '%s', already exists." % newdir)

    else:
        head, tail = os.path.split(newdir)

        if head and not os.path.isdir(head):
            mkdir(head)

        if tail:
            try:
                os.mkdir(newdir)
            # To prevent race in mpi runs
            except OSError as e:
                import errno
                if e.errno == errno.EEXIST and os.path.isdir(newdir):
                    pass
                else:
                    raise

def get_files_per_partition(fname=None, partno=0):
    """Get all solution files in a given directory, `dirname`.

    Parameters
    ----------
    """

    if fname is None:
        return []

    path = os.path.abspath( curdir )
    files = os.listdir( path )

    # get all the output files in the directory
    files = [f for f in files if f.startswith(fname) and 'part%03d_'%partno in f]
    files = [os.path.join(path, f) for f in files]

    # sort the files
    def _key_func(arg):
        a = os.path.splitext(arg)[0]
        return int(a[a.rfind('_')+1:])

    files.sort(key=_key_func)

    return files

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("-s", "--solnfile", type="str", action="store",
                      dest="solnfile", default=None)

    parser.add_option("-d", "--dirname", type="str", action="store",
                      dest="dirname", default=os.path.join(curdir, "test_output"))

    parser.add_option("-p", "--nprocs", type="int", action="store",
                      dest="nprocs", default=1)

    parser.add_option("--delete-old", type="int", action="store",
                      dest="delete", default=1)

    options, args = parser.parse_args()

    mkdir(options.dirname)

    # create the multiblock writer
    Nprocs = options.nprocs
    Niters = len(get_files_per_partition(options.solnfile, 0))
    
    for iteration in range(Niters):
        mb = vtk.vtkMultiBlockDataSet()

        mb.SetNumberOfBlocks(Nprocs)
        
        for proc in range(Nprocs):
            f = get_files_per_partition(options.solnfile, proc)[iteration]

            ug = vtk.vtkXMLUnstructuredGridReader()
            ug.SetFileName(f)
            ug.Update()
            
            mb.SetBlock(proc,ug.GetOutput())
        
        iteration_number = int(f.split('_')[-1].split('.')[0])

        full_path = os.path.join( curdir, options.dirname )
        full_path = os.path.join( full_path, options.solnfile + '_%08d'%iteration_number + '.vtm' )
            
        writer = vtk.vtkXMLMultiBlockDataWriter()
        writer.SetFileName(   full_path )
        writer.SetDataModeToBinary()

        if vtk.VTK_MAJOR_VERSION <= 5:
            writer.SetInput(mb)
        else:
            writer.SetInputData(mb)
        
        writer.Write()

    # delete old files
    if options.delete:
        for proc in range(Nprocs):
            files = get_files_per_partition(options.solnfile, proc)
            for f in files:
                call(["rm", f])
