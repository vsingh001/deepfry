quadrants = [
    #/* rotx=0, roty=0-3 */
    [[[0, 7], [1, 6]], [[3, 4], [2, 5]]],
    [[[7, 4], [6, 5]], [[0, 3], [1, 2]]],
    [[[4, 3], [5, 2]], [[7, 0], [6, 1]]],
    [[[3, 0], [2, 1]], [[4, 7], [5, 6]]],
    #/* rotx=1, roty=0-3 */
    [[[1, 0], [6, 7]], [[2, 3], [5, 4]]],
    [[[0, 3], [7, 4]], [[1, 2], [6, 5]]],
    [[[3, 2], [4, 5]], [[0, 1], [7, 6]]],
    [[[2, 1], [5, 6]], [[3, 0], [4, 7]]],
    #/* rotx=2, roty=0-3 */
    [[[6, 1], [7, 0]], [[5, 2], [4, 3]]],
    [[[1, 2], [0, 3]], [[6, 5], [7, 4]]],
    [[[2, 5], [3, 4]], [[1, 6], [0, 7]]],
    [[[5, 6], [4, 7]], [[2, 1], [3, 0]]],
    #/* rotx=3, roty=0-3 */
    [[[7, 6], [0, 1]], [[4, 5], [3, 2]]],
    [[[6, 5], [1, 2]], [[7, 4], [0, 3]]],
    [[[5, 4], [2, 3]], [[6, 7], [1, 0]]],
    [[[4, 7], [3, 0]], [[5, 6], [2, 1]]],
    #/* rotx=4, roty=0-3 */
    [[[6, 7], [5, 4]], [[1, 0], [2, 3]]],
    [[[7, 0], [4, 3]], [[6, 1], [5, 2]]],
    [[[0, 1], [3, 2]], [[7, 6], [4, 5]]],
    [[[1, 6], [2, 5]], [[0, 7], [3, 4]]],
    #/* rotx=5, roty=0-3 */
    [[[2, 3], [1, 0]], [[5, 4], [6, 7]]],
    [[[3, 4], [0, 7]], [[2, 5], [1, 6]]],
    [[[4, 5], [7, 6]], [[3, 2], [0, 1]]],
    [[[5, 2], [6, 1]], [[4, 3], [7, 0]]]
]


rotxmap_table = [ 
    4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2,
    3, 17, 18, 19, 16, 23, 20, 21, 22 
]

rotymap_table = [ 
    1, 2, 3, 0, 16, 17, 18, 19, 11, 8, 9, 10, 22, 23, 20,
    21, 14, 15, 12, 13, 4, 5, 6, 7 
]

rotx_table = [ 3, 0, 0, 2, 2, 0, 0, 1 ]
roty_table = [ 0, 1, 1, 2, 2, 3, 3, 0 ]

sense_table = [ -1, -1, -1, +1, +1, -1, -1, -1 ]


def peano_hilbert_key(domainlen, xc, yc, zc=0, xmin=0, ymin=0, zmin=0):

    bits = 18
    fac  = 1.0/(1.001*domainlen) * (1l << bits)

    x = int((xc - xmin)*fac)
    y = int((yc - ymin)*fac)
    z = int((zc - zmin)*fac)

    rotation = 0
    sense    = 1
    mask     = 1<<(bits)
    key      = 0

    # print domainlen, fac
    # print "Working with %d, %d, %d, %d"%(x, y, z, bits)

    for i in range(bits):
        mask >>= 1

        bitx = 0
        if ( x & mask ): bitx = 1

        bity = 0
        if ( y & mask ): bity = 1
        
        bitz = 0
        if ( z & mask ): bitz = 1

        quad = quadrants[rotation][bitx][bity][bitz]

        key <<= 3
        if (sense == 1):
            key += quad
        else:
            key += (7-quad)

        rotx = rotx_table[quad]
        roty = roty_table[quad]
        sense *= sense_table[quad]

        # print "Iteration", i
        # print "rotation = %d, quad = %d"%(rotation, quad)
        # print "Mask = %d, bits = (%d, %d, %d)"%(mask, bitx, bity, bitz)
        # print "Sense = %d, rotx, roty = (%d, %d)"%(sense, rotx, roty)
        # print
        
        while(rotx >0):
            rotation = rotxmap_table[rotation]
            rotx -= 1

        while(roty >0):
            rotation = rotymap_table[rotation]
            roty -= 1
    
    return key
