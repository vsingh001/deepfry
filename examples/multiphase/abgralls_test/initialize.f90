module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    type(FieldData_t), intent(inout) :: data
    type(mesh2d),      intent(inout) :: mesh
    
    ! locals
    integer(c_int) :: elem_index
    real(c_double), allocatable :: xs(:), ys(:), zs(:)

    real(c_double) :: ag, al, rhog, rhol, ug, ul, vg, vl, pressure
    real(c_double) :: eg, el, cg, cl, Etg, Etl
    real(c_double) :: xc, yc, zc
    
    allocate(xs(mesh%Np), ys(mesh%Np), zs(mesh%Np))
    zs = ZERO
    
    ! iterate over all elements and set the initial solution
    do elem_index = 1, mesh%Nelements
       call get_element_solution_points(mesh, elem_index, xs, ys, zs)

       xc = mesh%xc(elem_index)
       yc = mesh%yc(elem_index)
       zc = mesh%zc(elem_index)
       
       if ( xc .le. ZERO ) then
          ag = TWO/TEN
          al = ONE - ag

          rhog = 1.3_c_double
          rhol = 1000.0_c_double

          ug = 10.0_c_double
          ul = 10.0_c_double

          vg = ZERO
          vl = ZERO

          pressure = 100000.0_c_double

       else
          ag = EIGHT/TEN
          al = ONE - ag
             
          rhog = 1.3_c_double
          rhol = 1000.0_c_double

          ug = 10.0_c_double
          ul = 10.0_c_double

          vg = ZERO
          vl = ZERO

          pressure = 100000.0_c_double
       
       end if

       ! Stiffened-gas EOS
       eg = (pressure + gamma_gas*pinf_gas)/((gamma_gas-ONE)*rhog) + q_gas
       el = (pressure + gamma_liq*pinf_liq)/((gamma_liq-ONE)*rhol) + q_liq

       cg = sqrt( gamma_gas*(pressure + pinf_gas)/rhog )
       cl = sqrt( gamma_liq*(pressure + pinf_liq)/rhol )

       Etg = eg + HALF * (ug**2 + vg**2)
       Etl = el + HALF * (ul**2 + vl**2)

       ! conserved variables
       data%u(:, 1, elem_index) = ag*rhog
       data%u(:, 2, elem_index) = ag*rhog*ug
       data%u(:, 3, elem_index) = ag*rhog*vg
       data%u(:, 4, elem_index) = ag*rhog*Etg
       
       data%u(:, 5, elem_index) = al*rhol
       data%u(:, 6, elem_index) = al*rhol*ul
       data%u(:, 7, elem_index) = al*rhol*vl
       data%u(:, 8, elem_index) = al*rhol*Etl
       
    end do
    
    deallocate(xs, ys, zs)

  end subroutine set_initial_values

end module initialize_module
