module userbc_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use flux_module

  implicit none

contains

subroutine get_userbc_common_solution_values(mesh, time, beta)
  use input_module, only: gamma, p_inlet, rho_inlet
  type(mesh2d),             intent(inout) :: mesh
  real(c_double),           intent(in   ) :: time
  real(c_double), optional, intent(in   ) :: beta
  
end subroutine get_userbc_common_solution_values
   
subroutine get_userbc_interaction_flux(mesh, time, mu, Cp, Cv, Pr, Rs)
  use input_module, only: p_inlet, rho_inlet, velx_inlet, vely_inlet, gamma, tau, vdiff, beta

  real(c_double), intent(in ) :: time
  real(c_double), intent(in ) :: mu, Cp, Cv, Pr, Rs
  type(mesh2d), intent(inout) :: mesh

  ! locals
  integer(c_int) :: j, var, elem_num_faces, left
  integer(c_int) :: tmp_index, glb_face_index, left_face_index
  integer(c_int) :: face2elem(2)
  integer(c_int) :: elem2face_left(mesh%elem_num_faces), elem2face_rght(mesh%elem_num_faces)
  integer(c_int) :: left_indices(2)
  
  real(c_double) :: Pfluid, Vxfluid, Vyfluid, rhofluid, Efluid
  real(c_double) :: rhoghost, Vxghost, Vyghost, Pghost, Eghost
  
  real(c_double) :: rho_x, rho_y, velx_x, velx_y, vely_x, vely_y, T_x, T_y, T_x_ghost, T_y_ghost
  real(c_double) :: E_x, E_y, div, vid
  real(c_double) :: c_fluid
  
  real(c_double) :: Fd_left(mesh%P1, mesh%Nvar), Fd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Fv_left(mesh%P1, mesh%Nvar), Fv_rght(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Gd_left(mesh%P1, mesh%Nvar), Gd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv_left(mesh%P1, mesh%Nvar), Gv_rght(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fi(mesh%P1, mesh%Nvar)
  real(c_double) :: Gi(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fv(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv(mesh%P1, mesh%Nvar)
  
  real(c_double) :: uflux_left(mesh%P1, mesh%Nvar), uflux_rght(mesh%P1, mesh%Nvar)
    
  real(c_double) :: ilambda, ibeta_viscous, itau
  
  real(c_double) :: nx, ny

  real(c_double) :: xs, tmp
  logical :: postshock
  
  tmp = ONE/sqrt(THREE)
  xs  = ONE/SIX + tmp*(ONE + TWO*TEN*time)

  !$OMP PARALLEL DO PRIVATE(tmp_index, glb_face_index, j, var, nx, ny, face2elem, left, elem2face_left,  &
  !$OMP                     left_face_index, left_indices, &
  !$OMP                     rhofluid, Vxfluid, Vyfluid, Pfluid, Efluid, c_fluid, &
  !$OMP                     rhoghost, Vxghost, Vyghost, Pghost, Eghost, &
  !$OMP                     rho_x, rho_y, velx_x, velx_y, vely_x, vely_y, E_x, E_y, T_x, T_y, &
  !$OMP                     T_x_ghost, T_y_ghost, div, vid, &
  !$OMP                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
  !$OMP                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
  !$OMP                     uflux_left, uflux_rght, &
  !$OMP                     Fi, Gi, Fv, Gv, &
  !$OMP                     ilambda, ibeta_viscous, itau, &
  !$OMP                     xs, postshock) &
  !$OMP SCHEDULE(dynamic,2)
  do tmp_index = 1, mesh%Nfaces - mesh%Ninterior_faces
     glb_face_index    = mesh%bfaces(tmp_index)
     face2elem         = mesh%face2elem(glb_face_index, :)
     left              = face2elem(1)
     elem2face_left(:) = mesh%elem2face(left, :)
     
     do j = 1, mesh%elem_num_faces
        if(elem2face_left(j) .eq. glb_face_index) left_face_index = j
     end do
     
     left_indices(1) = mesh%P1*(left_face_index-1)+1
     left_indices(2) = mesh%P1*left_face_index
     
     nx = mesh%normals(glb_face_index,1)
     ny = mesh%normals(glb_face_index,2)

     ! get the left solution values
     do var = 1, mesh%Nvar
        uflux_left(:, var) = mesh%uflux(left_indices(1):left_indices(2), var, left)
     end do
     
     if (mesh%face_markers(glb_face_index) .eq. 15 .or. &
         mesh%face_markers(glb_face_index) .eq. 20) then
        ilambda = HALF; ibeta_viscous = ZERO; itau = ZERO

        ! determine if the shock has crossed the face
        postshock = .true.

        if (mesh%face_markers(glb_face_index) .eq. 15) then
           if (mesh%xfc(glb_face_index) .gt. xs) then
              postshock = .false.
           end if
        end if
        
        do j = 1, mesh%P1

           ! Directly imposed values.
           rhoghost = 1.4_c_double
           Vxghost  = ZERO
           Vyghost  = ZERO
           Pghost   = ONE

           if (postshock) then
              rhoghost = rho_inlet
              Vxghost  = velx_inlet
              Vyghost  = vely_inlet
              Pghost   = p_inlet
           end if

           Eghost = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2)
           
           ! set the right states for the flux
           uflux_rght(j, 1) = rhoghost
           uflux_rght(j, 2) = rhoghost*Vxghost
           uflux_rght(j, 3) = rhoghost*Vyghost
           uflux_rght(j, 4) = Eghost
           
           ! set the inviscid fluxes
           Fd_left(j, 1) = mesh%Fd_flux(left_indices(1) + (j-1), 1, left)
           Fd_left(j, 2) = mesh%Fd_flux(left_indices(1) + (j-1), 2, left)
           Fd_left(j, 3) = mesh%Fd_flux(left_indices(1) + (j-1), 3, left)
           Fd_left(j, 4) = mesh%Fd_flux(left_indices(1) + (j-1), 4, left)
           
           Fd_rght(j, 1) = rhoghost*Vxghost
           Fd_rght(j, 2) = rhoghost*Vxghost**2 + Pghost
           Fd_rght(j, 3) = rhoghost*Vxghost*Vyghost
           Fd_rght(j, 4) = Vxghost*(Eghost + Pghost)
            
           Gd_left(j, 1) = mesh%Gd_flux(left_indices(1) + (j-1), 1, left)
           Gd_left(j, 2) = mesh%Gd_flux(left_indices(1) + (j-1), 2, left)
           Gd_left(j, 3) = mesh%Gd_flux(left_indices(1) + (j-1), 3, left)
           Gd_left(j, 4) = mesh%Gd_flux(left_indices(1) + (j-1), 4, left)
           
           Gd_rght(j, 1) = rhoghost*Vyghost
           Gd_rght(j, 2) = rhoghost*Vxghost*Vyghost
           Gd_rght(j, 3) = rhoghost*Vyghost**2 + Pghost
           Gd_rght(j, 4) = Vyghost*(Eghost + Pghost)
        end do

        ! ZERO viscous fluxes
        Fv_left = ZERO; Fv_rght = ZERO
        Gv_left = ZERO; Gv_rght = ZERO

        ! Evaluate the interaction flux for this face
        call evaluate_interaction_flux(mesh%P1, mesh%Nvar, &
             mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
             uflux_left, uflux_rght, &
             Fd_left, Fd_rght, Fv_left, Fv_rght, &
             Gd_left, Gd_rght, Gv_left, Gv_rght, &
             Fi, Gi, Fv, Gv, &
             gamma, mesh%face_markers(glb_face_index), &
             ilambda, ibeta_viscous, itau)
        
        ! store the PHYSICAL interaction flux for the element
        ! abutting this face
        do j = 1, mesh%P1
           mesh%Fi(left_indices(1) + (j-1), :, left) = Fi(j, :) + Fv(j, :)
           mesh%Gi(left_indices(1) + (j-1), :, left) = Gi(j, :) + Gv(j, :)
        end do
        
     end if
  end do
  !$OMP END PARALLEL DO
  
end subroutine get_userbc_interaction_flux

end module userbc_module
 
