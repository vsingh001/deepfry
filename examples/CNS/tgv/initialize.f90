module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index, i
    real(c_double), allocatable :: xs(:), ys(:), v2(:), velx(:), vely(:), velz(:), P(:)
    real(c_double), allocatable :: zs(:)
    logical :: test

    real(c_double) :: p0

    allocate(xs(mesh%Np), ys(mesh%Np), v2(mesh%Np), velx(mesh%Np), vely(mesh%Np), velz(mesh%Np))
    allocate(zs(mesh%Np))
    allocate(P(mesh%Np))

    p0 = TEN*TEN

    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       zs = ZERO
       call get_element_solution_points(mesh, index, xs, ys, zs)

       data%u(:, 1, index) = ONE

       do i = 1, mesh%Np
          velx(i) =  sin(xs(i))*cos(ys(i))*cos(zs(i))
          vely(i) = -cos(xs(i))*sin(ys(i))*cos(zs(i))
          velz(i) = ZERO

          P(i) = p0 + ONE/(FOUR*FOUR)*(cos(TWO*xs(i)) + cos(TWO*ys(i)))*(cos(TWO*zs(i)) + TWO)

       end do

       v2 = velx**2 + vely**2 + velz**2

       data%u(:, 1, index) = gamma*TENTH**2 * P
       data%u(:, 2, index) = data%u(:, 1, index)*velx
       data%u(:, 3, index) = data%u(:, 1, index)*vely
       data%u(:, 4, index) = P/(gamma-ONE) + HALF*data%u(:, 1, index)*v2

#ifdef DIM3
       data%u(:, 4, index) = data%u(:, 1, index)*velz
       data%u(:, 5, index) = P/(gamma-ONE) + HALF*data%u(:, 1, index)*v2
#endif

    end do

    deallocate(xs, ys, velx, vely, velz, v2)
    deallocate(zs)
    deallocate(P)

  end subroutine set_initial_values

end module initialize_module
