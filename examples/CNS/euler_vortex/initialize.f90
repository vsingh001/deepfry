module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    use input_module, only: gamma, eps
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int)              :: index
    real(c_double), allocatable :: xs(:), ys(:)
    real(c_double)              :: W(mesh%Np, 4)
    
    allocate(xs(mesh%Np), ys(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys)
       call isentropic_vortex(mesh%NP, xs, ys, W, eps, gamma)
       
       mesh%u(:, 1, index) = W(:, 1)
       mesh%u(:, 2, index) = W(:, 1)*W(:, 2)
       mesh%u(:, 3, index) = W(:, 1)*W(:, 3)
       mesh%u(:, 4, index) = W(:,4)/(gamma-ONE) + &
                             HALF*mesh%u(:, 1, index)*(W(:,2)**2 + W(:,3)**2)

    end do

    call get_gradients(mesh)
    call compute_averages(mesh)
    
    deallocate(xs, ys)

  end subroutine set_initial_values

  subroutine isentropic_vortex(Np, x, y, W, eps, gamma)
    integer(c_int), intent(in ) :: Np
    real(c_double), intent(in ) :: x(Np), y(Np)
    real(c_double), intent(out) :: W(Np, 4)

    real(c_double), optional, intent(in) :: eps, gamma

    ! locals
    integer(c_int) :: i
    real(c_double) :: ieps, igamma, gamma1
    real(c_double) :: r2(Np)
    real(c_double) :: delu(Np), delv(Np), delT(Np)
    real(c_double) :: rho(Np), u(Np), v(Np), T(Np), p(Np)

    igamma = 1.4_c_double; if (present(gamma)) igamma = gamma
    ieps = FIVE; if (present(eps)) ieps = eps

    gamma1 = igamma - ONE

    do i = 1, Np
       r2(i) = x(i)**2 + y(i)**2
       
       delu(i) = -y(i)*ieps/(TWO*M_PI)*exp( (1.0_c_double-r2(i))/TWO )
       delv(i) = +x(i)*ieps/(TWO*M_PI)*exp( (1.0_c_double-r2(i))/TWO )
       delT(i) = -gamma1*ieps**2/(EIGHT*igamma*M_PI**2)*exp( 1.0_c_double - r2(i) )

    end do

    u = ONE + delu
    v = ZERO + delv
    T = ONE + delT
    
    rho = T**(ONE/gamma1)
    p   = rho**igamma

    W(:, 1) = rho
    W(:, 2) = u
    W(:, 3) = v
    W(:, 4) = p
    
  end subroutine isentropic_vortex

  subroutine test_initial_values(mesh)
    type(mesh2d), intent(in) :: mesh
  end subroutine test_initial_values

end module initialize_module
