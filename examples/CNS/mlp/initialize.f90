module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: index, i
    real(c_double) :: kappa, delta, beta, r1, r2, r3
    real(c_double) :: xs(mesh%Np), ys(mesh%Np)
    real(c_double) :: xi, yi

    ! set some constants
    kappa = TEN
    delta = 0.0005_c_double
    beta = log(TWO)/(360.0_c_double * delta**2)

    mesh%u = ZERO
    
    do index = 1, mesh%Nelements
       call get_element_solution_points(mesh, index, xs, ys)
       do i = 1, mesh%Np
          xi = xs(i); yi = ys(i)

          r1 = sqrt( (xi-FIFTH)**2 + (yi-FIFTH)**2 )
          r2 = sqrt( (xi+FIFTH)**2 + (yi-FIFTH)**2 )
          r3 = sqrt( (xi+FIFTH)**2 + (yi+FIFTH)**2 )
          
          if ( (xi .gt. 0.1_c_double .and. xi .lt. 0.3_c_double) .and. &
               (yi .gt. 0.1_c_double .and. yi .lt. 0.3_c_double) ) then
             mesh%u(i, 1, index) = G(r1, beta, -delta) + G(r1, beta, ZERO) + G(r1, beta, delta)
             
          else if ( (xi .gt. -0.3_c_double .and. xi .lt. -0.1_c_double) .and. &
                    (yi .gt. 0.1_c_double .and. yi .lt. 0.3_c_double) ) then
             mesh%u(i, 1, index) = max(ONE - TEN*r2, ZERO)

          else if ( (xi .gt. -0.3_c_double .and. xi .lt. -0.1_c_double) .and. &
                    (yi .gt. -0.3_c_double .and. yi .lt. -0.1_c_double) ) then
             mesh%u(i, 1, index) = F(r3, kappa, -delta) + F(r3, kappa, ZERO) + F(r3, kappa, delta)

          else if ( (xi .gt. 0.1_c_double .and. xi .lt. 0.3_c_double) .and. &
                    (yi .gt. -0.3_c_double .and. yi .lt. -0.1_c_double) ) then
             mesh%u(i, 1, index) = ONE
          end if
          
       end do
    end do

    call get_gradients(mesh)
    call compute_averages(mesh)

  end subroutine set_initial_values
  
  subroutine test_initial_values(mesh)
    type(mesh2d), intent(in) :: mesh

  end subroutine test_initial_values

  pure function G(x, beta, xc) result(rho)
    real(c_double), intent(in) :: x, beta, xc
    real(c_double) :: rho
    rho = exp(-beta*(x - xc)**2)
  end function G

  pure function F(x, kappa, xc) result(rho)
    real(c_double), intent(in) :: x, kappa, xc
    real(c_double) :: rho
    rho = max(ZERO, ONE-kappa**2*(x-xc)**2)
    rho = sqrt(rho)
  end function F

end module initialize_module
