Point(1) = {-0.5, -0.05, 0, 1.0};
Point(2) = {0.5, -0.05, 0, 1.0};
Point(3) = {0.5, 0.05, 0, 1.0};
Point(4) = {-0.5, 0.05, 0, 1.0};
Point(5) = {0, -0.05, 0, 1.0};
Point(6) = {0, 0.05, 0, 1.0};	 


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};
Physical Line(7) = {4};
Physical Line(8) = {2};
Physical Line(9) = {1};
Physical Line(10) = {3};
Physical Surface(11) = {6};
Transfinite Line {4, 2} = 11 Using Progression 1;
Transfinite Line {1, 3} = 101 Using Progression 1;
Transfinite Surface {6} = {1, 2, 3, 4};
