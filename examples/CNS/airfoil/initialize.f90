module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    use input_module, only: gamma, eps, p_inlet, velx_inlet, vely_inlet, rho_inlet
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int) :: index 
    real(c_double) :: velx(mesh%Np), vely(mesh%Np), v2(mesh%Np)

    velx = velx_inlet
    vely = vely_inlet
    v2 = velx**2 + vely**2

    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       mesh%u(:, 1, index) = rho_inlet
       
       mesh%u(:, 2, index) = mesh%u(:, 1, index)*velx
       mesh%u(:, 3, index) = mesh%u(:, 1, index)*vely
       mesh%u(:, 4, index) = p_inlet/(gamma-ONE) + HALF*mesh%u(:, 1, index)*v2

    end do

    call get_gradients(mesh)
    call compute_averages(mesh)

  end subroutine set_initial_values
  
  subroutine test_initial_values(mesh)
    type(mesh2d), intent(in) :: mesh

  end subroutine test_initial_values

end module initialize_module
