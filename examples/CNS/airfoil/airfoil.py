import imp
import os

import meshpy.triangle as triangle
import numpy

# load the genmesh module
curdir = os.path.abspath(os.curdir)
genmesh = imp.load_source(
      'genmesh', os.path.join(curdir, '../../../python/genmesh.py'))
writer  = imp.load_source(
      'h5writer', os.path.join(curdir, '../../../python/h5writer.py'))
utils = imp.load_source(
      'utils', os.path.join(curdir, '../../../python/utils.py'))

xlo = -4.0
xhi = +8.0

ylo = -4.0
yhi = +4.0

def airfoil2d():
    
    # define the domain boundary
    points = [ (xlo,ylo), (xhi,ylo), (xhi,yhi), (xlo,yhi) ]
    #              0          1          2          3

    # get the coordinates of the airfoil
    surf_mesh = numpy.loadtxt('naca0012.txt')
    x = surf_mesh[:,0]; y = surf_mesh[:, 1]

    # number of loaded points
    npnts = x.size    

    points += [(x[i], y[i]) for i in range(npnts)]

    # generate a region around the airfoil which we will use to set
    # the mesh size
    num_points1 = len(points)
    points += [ (-1.0, -1.0), (2.0, -1.0), (2.0, 1.0), (-1.0, 1.0) ]
    num_points2 = len(points)

    # generate the basic mesh for the domain and set the points
    info = triangle.MeshInfo()
    info.set_points(points)

    facets = utils.round_trip_connect([0,1,2,3]) + utils.round_trip_connect(range(4,npnts+4)) + \
             utils.round_trip_connect( range(num_points1, num_points2) )

    facet_markers = [genmesh.FaceType.outflow, genmesh.FaceType.outflow, 
                     genmesh.FaceType.outflow, genmesh.FaceType.inflow] + \
                     list(numpy.ones(npnts, dtype=int)*genmesh.FaceType.solid_wall) + \
                     [genmesh.FaceType.default]*4

    # set the facets and markers
    info.set_facets( facets=facets, facet_markers=facet_markers )

    # define any specific regions
    info.regions.resize(2)
    info.regions[0] = ( 0.0, 0.5, 0, 0.0025 )
    #info.regions[1] = ( 0.4, 0.1, 1, 0.0001 )
    info.regions[1] = (-2.0, -2.0, 2, 0.01)

    # define holes
    info.holes.resize(1)
    info.holes[0] = (0,0)

    # Generate the mesh
    mesh = triangle.build(info,
                          generate_faces=True,
                          verbose=False,
                          attributes=True,
                          volume_constraints=True,
                          quality_meshing=True)

    return mesh

if __name__ == '__main__':
      # write out the mesh in HDF5 format
      mesh = airfoil2d()
      dmesh = genmesh.meshpy2deepfry(mesh)
      writer.write_mesh(dmesh, "test.hdf5")
