module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    use input_module, only: gamma, eps
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int)              :: index
    real(c_double), allocatable :: xs(:), ys(:), zs(:)
    real(c_double)              :: W(mesh%Np, 4)
    
    allocate(xs(mesh%Np), ys(mesh%Np), zs(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys, zs)
       call forward_facing_step(mesh%NP, xs, ys, W, eps, gamma)
       
       mesh%u(:, 1, index) = W(:, 1)
       mesh%u(:, 2, index) = W(:, 1)*W(:, 2)
       mesh%u(:, 3, index) = W(:, 1)*W(:, 3)
       mesh%u(:, 4, index) = W(:,4)/(gamma-ONE) + &
                             HALF*mesh%u(:, 1, index)*(W(:,2)**2 + W(:,3)**2)

    end do

    deallocate(xs, ys, zs)

  end subroutine set_initial_values

  subroutine forward_facing_step(Np, x, y, W, eps, gamma)
    use input_module, only: rho_inlet, p_inlet
    integer(c_int), intent(in ) :: Np
    real(c_double), intent(in ) :: x(Np), y(Np)
    real(c_double), intent(out) :: W(Np, 4)

    real(c_double), optional, intent(in) :: eps, gamma

    ! locals
    integer(c_int) :: i
    real(c_double) :: ieps, igamma, gamma1
    real(c_double) :: rho(Np), u(Np), v(Np), p(Np)

    igamma = 1.4_c_double; if (present(gamma)) igamma = gamma
    ieps = FIVE; if (present(eps)) ieps = eps

    gamma1 = igamma - ONE

    rho = rho_inlet
    u = velx_inlet
    v = ZERO
    p = p_inlet

    W(:, 1) = rho
    W(:, 2) = u
    W(:, 3) = v
    W(:, 4) = p
    
  end subroutine forward_facing_step

end module initialize_module
