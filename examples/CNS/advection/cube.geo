Point(1) = {0, 0, 0, 1.0};
Point(2) = {1, 0, 0, 1.0};
Point(3) = {1, 1, 0, 1.0};
Point(4) = {0, 1, 0, 1.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};
Transfinite Surface {6} = {1, 2, 3, 4};
Extrude {0, 0, 1} {
  Surface{6}; Layers{2}; Recombine;
}
//Physical Surface(20) = {19,27,23,15,6,28};
//Physical Surface(21) = {27};
//Physical Surface(22) = {23};
//Physical Surface(23) = {15};
//Physical Surface(24) = {6};	
//Physical Surface(25) = {28};

Physical Surface(20) = {19};
Physical Surface(21) = {27};
Physical Surface(22) = {23};
Physical Surface(23) = {15};
Physical Surface(24) = {6};	
Physical Surface(25) = {28};
Physical Volume(30) = {1};
