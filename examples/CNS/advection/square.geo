Point(1) = {0, 0, 0, 1.0};
Point(2) = {1, 0, 0, 1.0};
Point(3) = {1, 1, 0, 1.0};
Point(4) = {0, 1, 0, 1.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};

Physical Line(20) = {4};
Physical Line(21) = {1};
Physical Line(22) = {2};
Physical Line(23) = {3};
Physical Surface(30) = {6};
Transfinite Surface {6} = {1, 2, 3, 4};
Transfinite Line {1, 2, 3, 4} = 3 Using Progression 1;
