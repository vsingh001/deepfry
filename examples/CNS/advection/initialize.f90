module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index, i
    real(c_double), allocatable :: xs(:), ys(:), r2(:), v2(:), velx(:), vely(:), velz(:)
    real(c_double), allocatable :: zs(:)

    allocate(xs(mesh%Np), ys(mesh%Np), r2(mesh%Np), v2(mesh%Np), velx(mesh%Np), vely(mesh%Np), velz(mesh%Np))
    allocate(zs(mesh%Np))

    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys, zs)

#ifdef DIM3
       r2 = (xs-HALF)**2 + (ys-HALF)**2 + (zs-HALF)**2
#else
       r2 = (xs-HALF)**2 + (ys-HALF)**2
#endif
       
       data%u(:, 1, index) = ONE

       do i = 1, mesh%Np
          if ( sqrt(r2(i)) .le. 0.15 ) then
             data%u(i, 1, index) = ONE + TWO*exp(-100*r2(i))
          end if
       end do

       velx = ONE
       vely = ONE
       velz = ONE
       
#ifdef DIM3
       v2 = velx**2 + vely**2 + velz**2
#else
       v2 = velx**2 + vely**2
#endif
       
       data%u(:, 2,index) = data%u(:, 1, index)*velx
       data%u(:, 3,index) = data%u(:, 1, index)*vely

#ifdef DIM3
       data%u(:, 4,index) = data%u(:, 1, index)*velz
       data%u(:, 5,index) = ONE/(gamma-ONE) + HALF*data%u(:,1, index)*v2
#else
       data%u(:, 4,index) = ONE/(gamma-ONE) + HALF*data%u(:,1, index)*v2
#endif

    end do

    deallocate(xs, ys, velx, vely, velz, v2, r2)
    deallocate(zs)

  end subroutine set_initial_values

end module initialize_module
