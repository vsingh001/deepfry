module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%Nelements
       data%u(:, 1, index) = ONE
       data%u(:, 2, index) = ZERO
       data%u(:, 3, index) = ZERO
       data%u(:, 4, index) = 17.857142857142858/(gamma-ONE)

#ifdef DIM3
       data%u(:, 4, index) = ZERO
       data%u(:, 5, index) = 17.857142857142858/(gamma-ONE)
#endif
    end do
  end subroutine set_initial_values

end module initialize_module
