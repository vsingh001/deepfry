module userbc_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use flux_module
  use fieldvar_module

  implicit none

contains

subroutine get_userbc_common_solution_values(mesh, data, beta)
  use input_module, only: gamma
  type(mesh2d),             intent(inout) :: mesh
  type(FieldData_t),        intent(inout) :: data
  real(c_double), optional, intent(in   ) :: beta
  
  ! locals
  integer(c_int) :: j, tmp_index, glb_face_index, left_face_index, left
  integer(c_int) :: left_indices(2)
  real(c_double) :: rhofluid, Vxfluid, Vyfluid, Vzfluid, Efluid, Pfluid
  real(c_double) :: rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost
  real(c_double) :: nx, ny, nz

  left_face_index = -1

  !$omp parallel do private(j, tmp_index, glb_face_index, left, &
  !$omp                     left_face_index, left_indices, &
  !$omp                     rhofluid, Vxfluid, Vyfluid, Vzfluid, Efluid, Pfluid, &
  !$omp                     rhoghost, Vxghost, Vyghost, Vzghost, Eghost, Pghost, &
  !$omp                     nx, ny, nz) &
  !$omp schedule(dynamic,2)
  do tmp_index = 1, mesh%Nbfaces !mesh%Nfaces - mesh%Ninterior_faces
     glb_face_index    = mesh%bfaces(tmp_index)

     left            = mesh%face2elem(glb_face_index, 1)
     left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
     left_indices(1) = mesh%P1*(left_face_index-1)+1
     left_indices(2) = mesh%P1*left_face_index
     
     nx = mesh%normals(glb_face_index,1)
     ny = mesh%normals(glb_face_index,2)
     nz = ZERO
     
#ifdef DIM3
     nz = mesh%normals(glb_face_index, 3)
#endif
     
     if (mesh%face_markers(glb_face_index) .eq. 20) then ! LDC top surface
        
        ! get the fluid states to the left
        do j = 1, mesh%P1
           rhofluid = data%uflux(left_indices(1) + (j-1), 1, left)
           Vxfluid  = data%uflux(left_indices(1) + (j-1), 2, left)/rhofluid
           Vyfluid  = data%uflux(left_indices(1) + (j-1), 3, left)/rhofluid
           Efluid   = data%uflux(left_indices(1) + (j-1), 4, left)
           Vzfluid  = ZERO

#ifdef DIM3
           Vzfluid = data%uflux(left_indices(1) + (j-1), 4, left)/rhofluid
           Efluid  = data%uflux(left_indices(1) + (j-1), 5, left)
#endif
        
           Pfluid  = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2))
           Pghost  = Pfluid

           ! Directly imposed ghost state values
           rhoghost = rhofluid
           Vxghost  = ONE
           Vyghost  = ZERO
           Vzghost  = ZERO
           Eghost   = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2 + Vzghost**2)

           data%ucommon(left_indices(1) + (j-1), 1, left) = rhoghost
           data%ucommon(left_indices(1) + (j-1), 2, left) = HALF * rhoghost*(Vxghost + Vxfluid)
           data%ucommon(left_indices(1) + (j-1), 3, left) = HALF * rhoghost*(Vyghost + Vyfluid)
           data%ucommon(left_indices(1) + (j-1), 4, left) = HALF * (Eghost + Efluid)
           
#ifdef DIM3
           data%ucommon(left_indices(1) + (j-1), 4, left) = HALF * rhoghost*(Vzghost + Vzfluid)
           data%ucommon(left_indices(1) + (j-1), 5, left) = HALF * (Eghost + Efluid)
#endif

        end do
     end if
  end do
  !$omp end parallel do
     
end subroutine get_userbc_common_solution_values
   
subroutine get_userbc_interaction_flux(mesh, data, time, mu, Cp, Cv, Pr, Rs)
  use input_module, only: gamma, ldg_tau, ldg_beta, avisc, vdiff
  
  real(c_double), intent(in ) :: mu, Cp, Cv, Pr, Rs, time
  type(mesh2d), intent(inout) :: mesh
  type(FieldData_t), intent(inout) :: data

  ! locals
  integer(c_int) :: j, var, left
  integer(c_int) :: tmp_index, glb_face_index, left_face_index
  integer(c_int) :: left_indices(2)
  
  real(c_double) :: rhofluid, Vxfluid, Vyfluid, Vzfluid, Pfluid, Efluid
  real(c_double) :: rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost
  
  real(c_double) :: rho_x, rho_y, rho_z
  real(c_double) :: velx_x, velx_y, velx_z
  real(c_double) :: vely_x, vely_y, vely_z
  real(c_double) :: velz_x, velz_y, velz_z
  real(c_double) :: T_x, T_y, T_z, T_x_ghost, T_y_ghost, T_z_ghost
  real(c_double) :: E_x, E_y, E_z, div
  
  real(c_double) :: Fd_left(mesh%P1, mesh%Nvar), Fd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Fv_left(mesh%P1, mesh%Nvar), Fv_rght(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Gd_left(mesh%P1, mesh%Nvar), Gd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv_left(mesh%P1, mesh%Nvar), Gv_rght(mesh%P1, mesh%Nvar)

  real(c_double) :: Hd_left(mesh%P1, mesh%Nvar), Hd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Hv_left(mesh%P1, mesh%Nvar), Hv_rght(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fi(mesh%P1, mesh%Nvar)
  real(c_double) :: Gi(mesh%P1, mesh%Nvar)
  real(c_double) :: Hi(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fv(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv(mesh%P1, mesh%Nvar)
  real(c_double) :: Hv(mesh%P1, mesh%Nvar)
  
  real(c_double) :: uflux_left(mesh%P1, mesh%Nvar), uflux_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: ux_left(mesh%P1, mesh%Nvar)
  real(c_double) :: uy_left(mesh%P1, mesh%Nvar)
  real(c_double) :: uz_left(mesh%P1, mesh%Nvar)
    
  real(c_double) :: tauxx, tauxy, tauxz
  real(c_double) :: tauyy, tauyz
  real(c_double) :: tauzz

  real(c_double) :: Fv_dot_n(mesh%P1, mesh%Nvar)
  real(c_double) :: vleft_prefactor, vrght_prefactor

  integer(c_int) :: sfi_left(mesh%P1)
  integer(c_int) :: sfi_rght(mesh%P1)

  real(c_double) :: nx, ny, nz

  real(c_double) :: ilambda, ibeta_viscous, itau
  ilambda = HALF; ibeta_viscous = ZERO; itau = ldg_tau

  vleft_prefactor = ZERO
  vrght_prefactor = TWO
  
  !$omp parallel do private(tmp_index, glb_face_index, j, var, nx, ny, nz, &
  !$omp                     left, left_face_index, left_indices, &
  !$omp                     rhofluid, Vxfluid, Vyfluid, Vzfluid, Pfluid, Efluid, &
  !$omp                     rhoghost, Vxghost, Vyghost, Vzghost, Pghost, Eghost, &
  !$omp                     rho_x, rho_y, rho_z, &
  !$omp                     velx_x, velx_y, velx_z, &
  !$omp                     vely_x, vely_y, vely_z, &
  !$omp                     velz_x, velz_y, velz_z, &
  !$omp                     E_x, E_y, E_z, &
  !$omp                     T_x, T_y, T_z, &
  !$omp                     T_x_ghost, T_y_ghost, T_z_ghost, div, &
  !$omp                     tauxx, tauxy, tauxz, tauyy, tauyz, tauzz, &
  !$omp                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
  !$omp                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
  !$omp                     Hd_left, Hd_rght, Hv_left, Hv_rght, &
  !$omp                     uflux_left, uflux_rght, &
  !$omp                     ux_left, uy_left, uz_left, &
  !$omp                     Fi, Gi, Hi, &
  !$omp                     Fv, Gv, Hv, Fv_dot_n) &
  !$omp schedule(static,2)
  do tmp_index = 1, mesh%Nbfaces
     glb_face_index  = mesh%bfaces(tmp_index)

     left            = mesh%face2elem(glb_face_index, 1)
     left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
     left_indices(1) = mesh%P1*(left_face_index-1)+1
     left_indices(2) = mesh%P1*left_face_index

     nx = mesh%normals(glb_face_index, 1)
     ny = mesh%normals(glb_face_index, 2)
     nz = ZERO

#ifdef DIM3
     nz = mesh%normals(glb_face_index, 3)
#endif

     ! initialize sfi
     do j = 1, mesh%P1 
        sfi_rght(j) = j
        sfi_left(j) = j
     end do
         
     ! get the left discontinuous flux and solution values
     do var = 1, mesh%Nvar
        uflux_left(:, var) = data%uflux(left_indices(1):left_indices(2), var, left)
        
        ux_left(:, var) = data%ux_flux(left_indices(1):left_indices(2), var, left)
        uy_left(:, var) = data%uy_flux(left_indices(1):left_indices(2), var, left)
        uz_left(:, var) = data%uz_flux(left_indices(1):left_indices(2), var, left)
     end do

     ! interior (left state inviscid flux)
     call euler_inviscid_flux(mesh%P1, mesh%Nvar, gamma, uflux_left, left_indices(1), sfi_left, &
                                Fd_left, Gd_left, Hd_left)

     if (mesh%face_markers(glb_face_index) .eq. 20) then       

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Inviscid boundary flux
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        do j = 1, mesh%P1

           ! Get the fluid and ghost states for the inviscid wall flux
           rhofluid = uflux_left(j, 1)
           Vxfluid  = uflux_left(j, 2)/rhofluid
           Vyfluid  = uflux_left(j, 3)/rhofluid
           Efluid   = uflux_left(j, 4)         
           Vzfluid  = ZERO
           
#ifdef DIM3
           Vzfluid = uflux_left(j, 4)/rhofluid
           Efluid  = uflux_left(j, 5)
#endif       
           Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid*Vxfluid + &
                                                           Vyfluid*Vyfluid + &
                                                           Vzfluid*Vzfluid))
             
           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           ! Ghost values
           rhoghost = rhofluid
           Vxghost =  ONE    
           Vyghost =  ZERO
           Vzghost =  ZERO
             
           Pghost = Pfluid
           Eghost = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost*Vxghost + &
                                                        Vyghost*Vyghost + &
                                                        Vzghost*Vzghost)
                    

           uflux_rght(j, 1) = rhoghost
           uflux_rght(j, 2) = rhoghost*Vxghost
           uflux_rght(j, 3) = rhoghost*Vyghost           

#ifdef DIM3
           uflux_rght(j, 4) = rhoghost*Vzghost
           uflux_rght(j, 5) = Eghost
#else
           uflux_rght(j, 4) = Eghost
#endif

           ! inviscid fluxes
           Fd_left(j, 1) = ZERO;   Fd_rght(j, 1) = ZERO
           Fd_left(j, 2) = Pfluid; Fd_rght(j, 2) = Pghost
           Fd_left(j, 3) = ZERO;   Fd_rght(j, 3) = ZERO
           Fd_left(j, 4) = ZERO;   Fd_rght(j, 4) = ZERO
           
           Gd_left(j, 1) = ZERO;   Gd_rght(j, 1) = ZERO
           Gd_left(j, 2) = ZERO  ; Gd_rght(j, 2) = ZERO
           Gd_left(j, 3) = Pfluid; Gd_rght(j, 3) = Pghost
           Gd_left(j, 4) = ZERO;   Gd_rght(j, 4) = ZERO

#ifdef DIM3
           Fd_left(j, 4) = ZERO; Fd_rght(j, 4) = ZERO
           Fd_left(j, 5) = ZERO; Fd_rght(j, 5) = ZERO
           
           Gd_left(j, 4) = ZERO; Gd_rght(j, 4) = ZERO
           Gd_left(j, 5) = ZERO; Gd_rght(j, 5) = ZERO
           
           Hd_left(j, 1) = ZERO;   Hd_rght(j, 1) = ZERO
           Hd_left(j, 2) = ZERO;   Hd_rght(j, 2) = ZERO
           Hd_left(j, 3) = ZERO;   Hd_rght(j, 3) = ZERO
           Hd_left(j, 4) = Pfluid; Hd_rght(j, 4) = Pghost
           Hd_left(j, 5) = ZERO;   Hd_rght(j, 5) = ZERO
#endif
        end do
        
        call evaluate_inviscid_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
             mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
             uflux_left, uflux_rght, &
             Fd_left, Fd_rght, &
             Gd_left, Gd_rght, &
             Hd_left, Hd_rght, &
             Fi, Gi, Hi, &
             gamma, ilambda)

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Viscous boundary flux
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        do j = 1, mesh%P1

           ! Get the fluid  states
           rhofluid = uflux_left(j, 1)
           Vxfluid  = uflux_left(j, 2)/rhofluid
           Vyfluid  = uflux_left(j, 3)/rhofluid
           Efluid   = uflux_left(j, 4)         
           Vzfluid  = ZERO
           
#ifdef DIM3
           Vzfluid = uflux_left(j, 4)/rhofluid
           Efluid  = uflux_left(j, 5)
#endif       
           Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid*Vxfluid + &
                                                           Vyfluid*Vyfluid + &
                                                           Vzfluid*Vzfluid))

           ! fluid gradients
           rho_x = data%ux_flux(left_indices(1) + (j-1), 1, left)
           rho_y = data%uy_flux(left_indices(1) + (j-1), 1, left)
           rho_z = ZERO
           
           E_x = data%ux_flux(left_indices(1) + (j-1), 4, left)
           E_y = data%uy_flux(left_indices(1) + (j-1), 4, left)
           E_z   = ZERO
             
           velx_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_x)               
           velx_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_y)
           
           vely_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_x)               
           vely_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_y)
           
           velx_z = ZERO; vely_z = ZERO
           velz_x = ZERO; velz_y = ZERO; velz_z = ZERO
           
#ifdef DIM3
           rho_z = data%uz_flux(left_indices(1) + (j-1), 1, left)

           E_x   = data%ux_flux(left_indices(1) + (j-1), 5, left)
           E_y   = data%uy_flux(left_indices(1) + (j-1), 5, left)
           E_z   = data%uz_flux(left_indices(1) + (j-1), 5, left)
           
           velx_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_z)
           vely_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_z)
           
           velz_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_x)
           velz_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_y)
           velz_z = ONE/rhofluid*(data%uz_flux(left_indices(1) + (j-1), 4, left) - Vzfluid*rho_z)
#endif             
           
           ! fluid-side temperature gradients
           T_x = ONE/(rhofluid*Cv)*(E_x - Efluid/rhofluid*rho_x - &
                                    rhofluid*(Vxfluid*velx_x + Vyfluid*vely_x + Vzfluid*velz_x))
           
           T_y = ONE/(rhofluid*Cv)*(E_y - Efluid/rhofluid*rho_y - &
                                    rhofluid*(Vxfluid*velx_y + Vyfluid*vely_y + Vzfluid*velz_y))

           T_z = ONE/(rhofluid*Cv)*(E_z - Efluid/rhofluid*rho_z - &
                                    rhofluid*(Vxfluid*velx_z + Vyfluid*vely_z + Vzfluid*velz_z))

           ! Viscous wall states and fluxes
           rhoghost = rhofluid
           Vxghost  = ONE
           Vyghost  = ZERO
           Vzghost  = ZERO
           Pghost   = Pfluid
           Eghost   = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost*Vxghost + &
                                                          Vyghost*Vyghost + &
                                                          Vzghost*Vzghost)

           !Eghost   = Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2 + Vzfluid**2)

           ! correct the wall gradients for T
           T_x_ghost = T_x - nx*(nx*T_x + ny*T_y + nz*T_z)
           T_y_ghost = T_y - ny*(nx*T_x + ny*T_y + nz*T_z)
           T_z_ghost = T_z - nz*(nx*T_x + ny*T_y + nz*T_z)
             
           ! divergence & gradient tensor
           div = velx_x + vely_y + velz_z
           
           tauxx = mu*(TWO*velx_x - TWO3RD*div)
           tauyy = mu*(TWO*vely_y - TWO3RD*div)
           tauzz = mu*(TWO*velz_z - TWO3RD*div)

           tauxy = mu*(velx_y + vely_x)
           tauxz = mu*(velx_z + velz_x)
           tauyz = mu*(vely_z + velz_y)
           
           ! set the right states
           uflux_rght(j, 1) = rhoghost
           uflux_rght(j, 2) = rhoghost*Vxghost
           uflux_rght(j, 3) = rhoghost*Vyghost
           
#ifdef DIM3
           uflux_rght(j, 4) = rhoghost*Vzghost
           uflux_rght(j, 5) = Eghost
#else
           uflux_rght(j, 4) = Eghost
#endif
             
           ! viscous fluxes
           Fv_left(j, 1) =  ZERO;  Fv_rght(j, 1) =  ZERO
           Fv_left(j, 2) = -tauxx; Fv_rght(j, 2) = -tauxx
           Fv_left(j, 3) = -tauxy; Fv_rght(j, 3) = -tauxy
           
           Fv_left(j, 4) = -(Vxfluid*tauxx + Vyfluid*tauxy) - mu*Cp/Pr*T_x
           Fv_rght(j, 4) = -(Vxghost*tauxx + Vyghost*tauxy) - mu*Cp/Pr*T_x_ghost
           
           Gv_left(j, 1) =  ZERO;  Gv_rght(j, 1) =  ZERO
           Gv_left(j, 2) = -tauxy; Gv_rght(j, 2) = -tauxy
           Gv_left(j, 3) = -tauyy; Gv_rght(j, 3) = -tauyy
           
           Gv_left(j, 4) = -mu*(Vxfluid*tauxy + Vyfluid*tauyy) - mu*Cp/Pr*T_y
           Gv_rght(j, 4) = -mu*(Vxghost*tauxy + Vyghost*tauyy) - mu*Cp/Pr*T_y_ghost
             
#ifdef DIM3
           Fv_left(j, 4) = -tauxz
           Fv_rght(j, 4) = -tauxz
           
           Fv_left(j, 5) = -(Vxfluid*tauxx + Vyfluid*tauxy + Vzfluid*tauxz) - mu*Cp/Pr*T_x
           Fv_rght(j, 5) = -(Vxghost*tauxx + Vyghost*tauxy + Vzghost*tauxz) - mu*Cp/Pr*T_x_ghost
           
           Gv_left(j, 4) = -tauyz
           Gv_rght(j, 4) = -tauyz
           
           Gv_left(j, 5) = -(Vxfluid*tauxy + Vyfluid*tauyy + Vzfluid*tauyz) - mu*Cp/Pr*T_y
           Gv_rght(j, 5) = -(Vxghost*tauxy + Vyghost*tauyy + Vzghost*tauyz) - mu*Cp/Pr*T_y_ghost
           
           Hv_left(j, 1) =   ZERO; Hv_rght(j, 1) =  ZERO
           Hv_left(j, 2) = -tauxz; Hv_rght(j, 2) = -tauxz
           Hv_left(j, 3) = -tauyz; Hv_rght(j, 3) = -tauyz
           Hv_left(j, 4) = -tauzz; Hv_rght(j, 4) = -tauzz
           
           Hv_left(j, 5) = -(Vxfluid*tauxz + Vyfluid*tauyz + Vzfluid*tauzz) - mu*Cp/Pr*T_z
           Hv_rght(j, 5) = -(Vxghost*tauxz + Vyghost*tauyz + Vzghost*tauzz) - mu*Cp/Pr*T_z_ghost
#else
           Hv_left(j, :) = ZERO
           Hv_rght(j, :) = ZERO
#endif
        end do
          
        Fv_left = ZERO; Fv_rght = ZERO
        Gv_left = ZERO; Gv_rght = ZERO
        Hv_left = ZERO; Hv_rght = ZERO

        call evaluate_viscous_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
             mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
             uflux_left, uflux_rght, &
             Fv_left, Fv_rght, &
             Gv_left, Gv_rght, &
             Hv_left, Hv_rght, &
             Fv, Gv, Hv, &
             gamma, max(avisc, vdiff), &
             ibeta_viscous, itau,&
             mesh%face_markers(glb_face_index), glb_face_index, &
             vleft_prefactor, vrght_prefactor)
        
        ! store the PHYSICAL interaction flux for the element
        ! abutting this face
        do j = 1, mesh%P1
           data%Fi(left_indices(1) + (j-1), :, left) = Fi(j, :) + Fv(j, :) 
           data%Gi(left_indices(1) + (j-1), :, left) = Gi(j, :) + Gv(j, :)
           data%Hi(left_indices(1) + (j-1), :, left) = Hi(j, :) + Hv(j, :)
          end do
          
       end if
    end do
    !$omp end parallel do
  
  end subroutine get_userbc_interaction_flux

end module userbc_module
 
