module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    use input_module, only: gamma, eps
    type(mesh2d), intent(inout) :: mesh

    ! locals
    integer(c_int)              :: index
    real(c_double), allocatable :: xs(:), ys(:)
    real(c_double)              :: W(mesh%Np, 4)
    
    allocate(xs(mesh%Np), ys(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys)

       ! background state
       W(:, 1) = ONE
       W(:, 2) = ZERO
       W(:, 3) = ZERO
       W(:, 4) = ONE

       if ( sqrt((mesh%xc(index)-HALF)**2 + mesh%yc(index)**2) .lt. 0.15_c_double) then ! bubble
          W(:, 1) = 0.15_c_double
       else if ( mesh%xc(index) .lt. 0.3_c_double ) then                      ! post-shock
          W(:, 1) = rho_inlet
          W(:, 2) = velx_inlet
          W(:, 3) = ZERO
          W(:, 4) = p_inlet
       end if
       
       mesh%u(:, 1, index) = W(:, 1)
       mesh%u(:, 2, index) = W(:, 1)*W(:, 2)
       mesh%u(:, 3, index) = W(:, 1)*W(:, 3)
       mesh%u(:, 4, index) = W(:,4)/(gamma-ONE) + &
                             HALF*mesh%u(:, 1, index)*(W(:,2)**2 + W(:,3)**2)

    end do

    call get_gradients(mesh)
    call compute_averages(mesh)
    
    deallocate(xs, ys)

  end subroutine set_initial_values

  subroutine test_initial_values(mesh)
    type(mesh2d), intent(in) :: mesh
  end subroutine test_initial_values

end module initialize_module
