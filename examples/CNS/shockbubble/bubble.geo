Point(1) = {-0.5, -0.5, 0.0, 1.0};
Point(2) = {1.5, -0.5, 0.0, 1.0};
Point(3) = {1.5, 0.5, 0.0, 1.0};
Point(4) = {-0.5, 0.5, 0.0, 1.0};
Point(5) = {0.5, 0, 0.0, 1.0};
Point(6) = {0.65, 0, 0.0, 1.0};
Point(7) = {0.35, 0, 0.0, 1.0};
Point(8) = {0.3, -0.5, 0, 1.0};
Point(9) = {0.3, 0.5, 0, 1.0};

Circle(1) = {6, 5, 7};
Circle(2) = {7, 5, 6};

Line(3) = {4, 1};
Line(4) = {1, 8};
Line(5) = {8, 9};
Line(6) = {9, 4};
Line(7) = {8, 2};
Line(8) = {2, 3};
Line(9) = {3, 9};
Line Loop(10) = {6, 3, 4, 5};
Plane Surface(11) = {10};
Line Loop(12) = {9, -5, 7, 8};
Line Loop(13) = {1, 2};
Plane Surface(14) = {12, 13};
Plane Surface(15) = {13};
Physical Line(16) = {3};

Physical Line(17) = {4, 7, 8, 9, 6};
Physical Surface(18) = {11, 14, 15};
Transfinite Line {1, 2} = 40 Using Progression 1;
Transfinite Line {3, 8} = 30 Using Progression 1;
Transfinite Line {4, 6} = 15 Using Progression 1;
Transfinite Line {7, 9} = 45 Using Progression 1;
Transfinite Line {5} = 30 Using Progression 1;
