Point(1) = {-0.5, -0.05, 0, 1.0};
Point(2) = {0.5, -0.05, 0, 1.0};
Point(3) = {0.5, 0.05, 0, 1.0};
Point(4) = {-0.5, 0.05, 0, 1.0};
Point(5) = {0, -0.05, 0, 1.0};
Point(6) = {0, 0.05, 0, 1.0};	 

Line(1) = {1, 5};
Line(2) = {5, 2};
Line(3) = {2, 3};
Line(4) = {3, 6};
Line(5) = {6, 4};
Line(6) = {4, 1};
Line(7) = {5, 6};

Line Loop(8) = {5, 6, 1, 7};
Plane Surface(9) = {8};
Line Loop(10) = {4, -7, 2, 3};
Plane Surface(11) = {10};
Physical Line(12) = {6};
Physical Line(13) = {3};

Physical Surface(15) = {9, 11};
Transfinite Surface {9} = {4, 1, 5, 6};
Transfinite Surface {11} = {6, 5, 2, 3};
Transfinite Line {1, 2, 4, 5} = 50 Using Progression 1;
Transfinite Line {6, 3} = 10 Using Progression 1;
Transfinite Line {7} = 10 Using Progression 1;
Physical Line(16) = {1, 2};
Physical Line(17) = {5, 4};
