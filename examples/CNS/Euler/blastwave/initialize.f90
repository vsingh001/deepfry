module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma, eps
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index
    real(c_double), allocatable :: xs(:), ys(:), zs(:)
    real(c_double)              :: W(mesh%Np, 4)
    
    allocate(xs(mesh%Np), ys(mesh%Np), zs(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys, zs)
       call blastwave(mesh%NP, xs, ys, W, eps, gamma)
       
       data%u(:, 1, index) = W(:, 1)
       data%u(:, 2, index) = W(:, 1)*W(:, 2)
       data%u(:, 3, index) = W(:, 1)*W(:, 3)
       data%u(:, 4, index) = W(:,4)/(gamma-ONE) + &
                             HALF*data%u(:, 1, index)*(W(:,2)**2 + W(:,3)**2)

    end do

    deallocate(xs, ys)

  end subroutine set_initial_values

  subroutine blastwave(Np, x, y, W, eps, gamma)
    use input_module, only: rho_inlet, p_inlet
    integer(c_int), intent(in ) :: Np
    real(c_double), intent(in ) :: x(Np), y(Np)
    real(c_double), intent(out) :: W(Np, 4)

    real(c_double), optional, intent(in) :: eps, gamma

    ! locals
    integer(c_int) :: i
    real(c_double) :: ieps, igamma, gamma1
    real(c_double) :: rho(Np), u(Np), v(Np), p(Np)

    igamma = 1.4_c_double; if (present(gamma)) igamma = gamma
    ieps = FIVE; if (present(eps)) ieps = eps

    gamma1 = igamma - ONE

    rho = rho_inlet
    u = ZERO
    v = ZERO
    p = p_inlet

    do i = 1, Np
       if ( x(i) .ge. ZERO ) then
          rho = rho_inlet!/EIGHT
          p   = p_inlet/(100000.0_c_double)
       end if
    end do

    W(:, 1) = rho
    W(:, 2) = u
    W(:, 3) = v
    W(:, 4) = p
    
  end subroutine blastwave

end module initialize_module
