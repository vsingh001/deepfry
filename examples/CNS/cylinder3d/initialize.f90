module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains
  
  function get_blend(r, rm, ra) result(S)
    real(c_double), intent(in) :: r, rm, ra
    real(c_double) :: S

    ! locals
    real(c_double) :: numerator, denominator
    
    numerator   = TWO*ra * (r-rm)
    denominator = ra**2 - (r-rm)**2
    
    S = HALF * (ONE + tanh(numerator/denominator))
    
  end function get_blend

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma
    type(mesh2d),      intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index, i
    real(c_double), allocatable :: xs(:), ys(:), v2(:), velx(:), vely(:), velz(:), P(:)
    real(c_double), allocatable :: zs(:), r(:)
    logical :: test

    real(c_double) :: p0, perturb
    real(c_double) :: theta, blendS

    allocate(xs(mesh%Np), ys(mesh%Np), v2(mesh%Np), velx(mesh%Np), vely(mesh%Np), velz(mesh%Np))
    allocate(zs(mesh%Np))
    allocate(P(mesh%Np))
    allocate(r(mesh%Np))

    p0 = TEN*TEN
    perturb = ONE/(TEN*TEN*TEN)

    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       zs = ZERO
       call get_element_solution_points(mesh, index, xs, ys, zs)

       ! free stream values
       velx = velx_inlet
       vely = vely_inlet
       velz = velz_inlet
       P    = p_inlet

       ! blending for no-slip at the wall
       r = sqrt( xs**2 + ys**2 )
       do i = 1, mesh%Np
          if (r(i) .lt. THREE/TWO) then
             blendS = get_blend(r(i), ONE, HALF)
             
             velx(i) = velx_inlet*blendS

             if (r(i) .gt. HALF .and. r(i) .le. ONE) then

                blendS = get_blend(r(i), THREE/FOUR, FOURTH)
                velz(i) = TENTH*velx_inlet*blendS * sin(zs(i)) * sin(zs(i))

             else
                blendS = get_blend(r(i), 1.25_c_double, FOURTH)
                velz(i) = TENTH*velx_inlet*(ONE - blendS) * sin(zs(i)) * sin(zs(i))
             end if
            
             theta = atan2( ys(i), xs(i) )
             if (theta .gt. ZERO .and. theta .lt. M_PI) then

                if ( r(i) .gt. HALF .and. r(i) .le. ONE ) then
                   blendS = get_blend(r(i), THREE/FOUR, FOURTH)
                   vely(i) = TENTH*velx_inlet*blendS * get_blend(theta, HALF*M_PI, HALF*M_PI)
                else
                   blendS = get_blend(r(i), 1.25_c_double, FOURTH)
                   vely(i) = TENTH*velx_inlet * (ONE - blendS) * get_blend(theta, HALF*M_PI, HALF*M_PI)
                end if

             else
                
                if ( r(i) .gt. HALF .and. r(i) .le. ONE ) then
                   blendS = get_blend(r(i), THREE/FOUR, FOURTH)
                   vely(i) = TENTH*velx_inlet * blendS * (ONE - get_blend(theta, THREE*M_PI/TWO, M_PI/TWO))
                else
                   blendS = get_blend(r(i), 1.25_c_double, FOURTH)
                   vely(i) = TENTH*velx_inlet * (ONE - blendS) * (ONE - get_blend(theta, THREE*M_PI/TWO, M_PI/TWO))
                end if

             end if
          end if
       end do

       data%u(:, 1, index) = rho_inlet
       data%u(:, 2, index) = rho_inlet*velx
       data%u(:, 3, index) = rho_inlet*vely
       data%u(:, 4, index) = rho_inlet*velz
       data%u(:, 5, index) = P/(gamma-ONE) + HALF*rho_inlet*(velx**2 + vely**2 + velz**2)
       
    end do

    deallocate(xs, ys, velx, vely, velz, v2)
    deallocate(zs)
    deallocate(P)
    deallocate(r)

  end subroutine set_initial_values

end module initialize_module
