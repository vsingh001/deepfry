module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module
    type(mesh2d), intent(inout) :: mesh
    type(FieldData_t), intent(inout) :: data

    ! locals
    integer(c_int)              :: index, i
    real(c_double), allocatable :: xs(:), ys(:), r2(:), v2(:), velx(:), vely(:), velz(:)
    real(c_double), allocatable :: zs(:)
    logical :: test
    
    allocate(xs(mesh%Np), ys(mesh%Np), r2(mesh%Np), v2(mesh%Np), velx(mesh%Np), vely(mesh%Np), velz(mesh%Np))
    allocate(zs(mesh%Np))

    do index = 1, mesh%Nelements
       data%u(:, 1, index) = rho_inlet

       velx = velx_inlet
       vely = vely_inlet
       velz = velz_inlet

       data%u(:, 2, index) = data%u(:, 1, index)*velx
       data%u(:, 3, index) = data%u(:, 1, index)*vely      
       
       v2 = velx**2 + vely**2
       data%u(:, 4,index) = p_inlet/(gamma-ONE) + HALF*data%u(:,1, index)*v2
       
#ifdef DIM3
       v2 = velx**2 + vely**2 + velz**2
       
       data%u(:, 4,index) = data%u(:, 1, index)*velz
       data%u(:, 5,index) = ONE/(gamma-ONE) + HALF*data%u(:,1, index)*v2
#endif
    end do

    deallocate(xs, ys, velx, vely, velz, v2, r2)
    deallocate(zs)

  end subroutine set_initial_values

#ifdef BOXLIB
  subroutine init_data(U, dx, the_bc_level, mla, prob_lo)
    use multifab_module
    use ml_layout_module
    use define_bc_module
    use ml_restrict_fill_module

    type(multifab),  intent(inout) :: U(:)
    real(c_double),  intent(in   ) :: dx(:, :)
    type(bc_level),  intent(in   ) :: the_bc_level(:)
    type(ml_layout), intent(inout) :: mla
    real(c_double),  intent(in   ) :: prob_lo(mla%dim)

    ! locals
    real(c_double), pointer :: Up(:,:,:,:)
    integer :: lo(mla%dim), hi(mla%dim)
    integer :: i, n, ng, dm, nlevs

    dm = mla%dim
    nlevs = mla%nlevel
    ng = nghost(U(1))

    do n = 1, nlevs
       do i = 1, nfabs(U(n))
          Up => dataptr(U(n), i)
          lo =  lwb(get_box(U(n), i))
          hi =  upb(get_box(U(n), i))

          select case(dm)
             case (2)
                call init_data2d(Up(:,:,1,:), lo, hi, ng, dx(n,:), prob_lo)
          end select

       end do
    end do

    ! restrict data and fill all ghost cells
    call ml_restrict_and_fill(nlevs, U, mla%mba%rr, the_bc_level, &
         icomp=1, bcomp=1, &
         nc=multifab_ncomp(U(1)), &
         ng=multifab_nghost(U(1)))

  end subroutine init_data

  subroutine init_data_on_level(n, U, dx, the_bc_level, prob_lo)
    use multifab_module
    use define_bc_module
    use ml_layout_module

    integer(c_int),     intent(in   ) :: n
    type(multifab),     intent(inout) :: U
    real(c_double),     intent(in   ) :: dx(:)
    type(bc_level),     intent(in   ) :: the_bc_level
    real(c_double),     intent(in   ) :: prob_lo(get_dim(U))

    ! locals
    real(c_double), pointer :: Up(:, :, :, :)
    integer(c_int) :: lo(get_dim(U)), hi(get_dim(U))
    integer :: i, dm, ng

    dm = get_dim(U)
    ng = nghost(U)

    do i = 1, nfabs(U)
       Up => dataptr(U, i)
       lo =  lwb(get_box(U, i))
       hi =  upb(get_box(U, i))

       select case (dm)
          case (2)
             call init_data2d(Up(:, :, 1, :), lo, hi, ng, dx, prob_lo)
       end select
    end do

  end subroutine init_data_on_level

  subroutine init_data2d(U, lo, hi, ng, dx, prob_lo)
    integer,        intent(in ) :: lo(:), hi(:), ng
    real(c_double), intent(out) :: U(lo(1)-ng:, lo(2)-ng:, :)
    real(c_double), intent(in ) :: dx(:)
    real(c_double), intent(in ) :: prob_lo(2)

    ! locals
    integer(c_int) :: i, j
    real(c_double) :: xc, yc, r2
    real(c_double) :: velx, vely, v2

    velx = ONE
    vely = ZERO
    v2   = velx**2 + vely**2

    ! iterate over the grid and set the initial values
    do j = lo(2)-ng, hi(2)+ng
       yc = prob_lo(2) + (dble(j) + HALF)*dx(2)
       do i = lo(1)-ng, hi(1)+ng
          xc = prob_lo(1) + (dble(i) + HALF)*dx(1)

          r2 = (xc-0.3)**2 + (yc-0.15)**2

          u(i, j, 1) = ONE
          if (r2 .lt. 0.125) then 
             u(i, j, 1) = ONE + TWO*exp(-100*r2)
          end if
          u(i, j, 2) = ONE
          u(i, j, 3) = ZERO
          u(i, j, 4) = ONE/(gamma-ONE) + HALF*u(i, j, 1)*v2

       end do
    end do

  end subroutine init_data2d

#endif

end module initialize_module
