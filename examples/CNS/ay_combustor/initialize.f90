module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use fieldvar_module

  implicit none

contains

  subroutine set_initial_values(mesh, data)
    use input_module, only: gamma, eps
    type(FieldData_t), intent(inout) ::data
    type(mesh2d),      intent(inout) :: mesh

    ! locals
    integer(c_int)              :: index
    real(c_double), allocatable :: xs(:), ys(:), zs(:)
    real(c_double)              :: W(mesh%Np, 4)
    
    allocate(xs(mesh%Np), ys(mesh%Np), zs(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys, zs)
       call sodshock(mesh%NP, xs, ys, W, eps, gamma)
       
       data%u(:, 1, index) = W(:, 1)
       data%u(:, 2, index) = W(:, 1)*W(:, 2)
       data%u(:, 3, index) = W(:, 1)*W(:, 3)
       data%u(:, 4, index) = W(:,4)/(gamma-ONE) + &
                             HALF*data%u(:, 1, index)*(W(:,2)**2 + W(:,3)**2)

    end do

    deallocate(xs, ys, zs)

  end subroutine set_initial_values

  subroutine sodshock(Np, x, y, W, eps, gamma)
    use input_module, only: Cp

    integer(c_int), intent(in ) :: Np
    real(c_double), intent(in ) :: x(Np), y(Np)
    real(c_double), intent(out) :: W(Np, 4)

    real(c_double), optional, intent(in) :: eps, gamma

    ! locals
    integer(c_int) :: i
    real(c_double) :: ieps, igamma, gamma1
    real(c_double) :: rho(Np), u(Np), v(Np), p(Np)
    real(c_double) :: XbL, YbH

    real(c_double) :: tmp, V_w, V_i, wall_mw
    real(c_double) :: R_c, T_c, M_c, C_c, V_c, P_c
    real(c_double) :: Rth, Tth, Mth, Cth, Vth, Pth
    real(c_double) :: dx, dy, angle

    real(c_double) :: rho_head, p_head, t_head
    real(c_double) :: Cv, Rs
    real(c_double) :: rho0, V0, T0, P0

    igamma = 1.4_c_double; if (present(gamma)) igamma = gamma
    ieps = FIVE; if (present(eps)) ieps = eps

    gamma1 = igamma - ONE

    Cv = Cp/igamma
    Rs = Cp-Cv

    rho0 = ONE
    V0   = 3.1_c_double;
    T0   = 260_c_double;
    P0   = rho0*V0**2;

    rho_head = ONE!4.264336107921928_c_double
    p_head   = 101325.0*3.142
    t_head   = 260_c_double

    do i = 1, Np
       if (x(i) .lt. 0.240_c_double) then
          XbL = (x(i) + 0.240_c_double)/0.480_c_double
          YbH = y(i)*TEN*TEN

          tmp = sqrt(ONE - XbL**2)
          M_c = (ONE - tmp)/(ONE + igamma*tmp)
          M_c = sqrt(M_c)

          P_c = p_head*(ONE/(ONE + igamma) * (ONE + igamma*tmp))
                   
          T_c = (ONE + igamma)**(ONE/igamma - ONE)
          T_c = t_head*(T_c * (ONE + igamma*tmp)**(ONE - ONE/igamma))

          !R_c = P_c/T_c
          R_c  = P_c/(Rs*T_c)

          C_c = sqrt(igamma*P_c/R_c)
          V_c = M_c*C_c  

          rho(i) = R_c
          u(i)   = V_c*cos(HALF*M_PI*abs(YbH))
          
          V_w = 13.0_c_double/rho(i)

          v(i)   = -V_w*sin(HALF*M_PI*YbH)
          p(i)   = P_c

       else
          dx = x(i) - 0.240_c_double
          angle = 15.0_c_double*M_PI/180.0_c_double
          dy = dx*tan(angle)

          YbH = y(i)/(ONE/(TEN*TEN) + dy)

          Mth = ONE
          Pth = p_head*( ONE/(ONE + igamma) )
          Tth = t_head*( (ONE + igamma)**((ONE - igamma)/igamma) )
          
          Rth = Pth/(Rs*Tth) !Rth = (igamma*Pth)/Tth
          Cth = sqrt(igamma*Pth/Rth)
          Vth = Mth*Cth

          rho(i) = Rth
          u(i)   = Vth*cos(HALF*M_PI*abs(YbH))
          v(i)   = ZERO
          p(i)   = Pth

       end if
    end do

    W(:, 1) = rho
    W(:, 2) = u
    W(:, 3) = v
    W(:, 4) = p
    
  end subroutine sodshock

end module initialize_module
