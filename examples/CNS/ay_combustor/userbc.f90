module userbc_module
  use iso_c_binding, only: c_int, c_double
  use deepfry_constants_module
  use mesh_module
  use flux_module
  use fieldvar_module

  implicit none

  real(c_double) :: WALLMW   = 13.0_c_double

contains

subroutine get_userbc_common_solution_values(mesh, data, time, beta)
  use input_module, only: gamma, Cp
  real(c_double),           intent(in   ) :: time
  type(mesh2d),             intent(inout) :: mesh
  type(FieldData_t),        intent(inout) :: data
  real(c_double), optional, intent(in   ) :: beta
  
  ! locals
  integer(c_int) :: j, tmp_index, glb_face_index, left_face_index, left
  integer(c_int) :: left_indices(2)
  real(c_double) :: rhofluid, Vxfluid, Vyfluid, Efluid, Pfluid, c_fluid
  real(c_double) :: rhoghost, Vxghost, Vyghost, Pghost, Eghost

  real(c_double) :: v_inlet
  real(c_double) :: T_inject, P_inject
  real(c_double) :: Rs

  v_inlet = ONE
  T_inject = 260.0_c_double
  P_inject = 3.142_c_double*101325.0_c_double

  Rs = Cp - (Cp/gamma)

  !$omp parallel do private(j, tmp_index, glb_face_index, left, &
  !$omp                     left_face_index, left_indices, &
  !$omp                     rhofluid, Vxfluid, Vyfluid, Efluid, Pfluid, c_fluid, &
  !$omp                     rhoghost, Vxghost, Vyghost, Eghost, Pghost, v_inlet) &
  !$omp schedule(dynamic,2)
  do tmp_index = 1, mesh%Nbfaces
     glb_face_index    = mesh%bfaces(tmp_index)
     left              = mesh%face2elem(glb_face_index,1)
     
     left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
     left_indices(1) = mesh%P1*(left_face_index-1)+1    
     
     if (mesh%face_markers(glb_face_index) .eq. 2 .or. &
         mesh%face_markers(glb_face_index) .eq. 3) then    ! combustor inlet surfaces
        
        ! get the fluid states to the left
        do j = 1, mesh%P1
           rhofluid = data%uflux(left_indices(1) + (j-1), 1, left)
           Vxfluid  = data%uflux(left_indices(1) + (j-1), 2, left)/rhofluid
           Vyfluid  = data%uflux(left_indices(1) + (j-1), 3, left)/rhofluid
           Efluid   = data%uflux(left_indices(1) + (j-1), 4, left)
        
           Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2))

           ! Directly imposed values.
           rhoghost = rhofluid
           Pghost   = rhoghost*Rs*T_inject

           !Pghost   = P_inject
           !rhoghost = Pghost/(Rs*T_inject)

           ! get the inlet velocity by fixing the mean mass flow rate
           v_inlet = WALLMW/rhoghost
           if (mesh%face_markers(glb_face_index) .eq. 3) then
              v_inlet = -v_inlet
           end if

           ! ghost state velocities and energy
           Vxghost  = ZERO
           Vyghost  = v_inlet
           Eghost   = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2)

           data%ucommon(left_indices(1) + (j-1), 1, left) = HALF*(rhoghost+rhofluid)
           data%ucommon(left_indices(1) + (j-1), 2, left) = HALF*(rhoghost*Vxghost + rhofluid*Vxfluid)
           data%ucommon(left_indices(1) + (j-1), 3, left) = HALF*(rhoghost*Vyghost + rhofluid*Vyfluid)
           data%ucommon(left_indices(1) + (j-1), 4, left) = HALF*(Eghost+Efluid)
        end do
     end if
  end do
  !$omp end parallel do
     
end subroutine get_userbc_common_solution_values
   
subroutine get_userbc_interaction_flux(mesh, data, time, mu, Cp, Cv, Pr, Rs)
  use input_module, only: gamma, ldg_tau, vdiff, avisc

  real(c_double),     intent(in   ) :: time
  real(c_double),     intent(in   ) :: mu, Cp, Cv, Pr, Rs
  type(mesh2d),       intent(inout) :: mesh
  type(FieldData_t),  intent(inout) :: data

  ! locals
  integer(c_int) :: j, var, left
  integer(c_int) :: tmp_index, glb_face_index, left_face_index
  integer(c_int) :: left_indices(2)
  
  real(c_double) :: Pfluid, Vxfluid, Vyfluid, rhofluid, Efluid
  real(c_double) :: rhoghost, Vxghost, Vyghost, Pghost, Eghost
  
  real(c_double) :: rho_x, rho_y, velx_x, velx_y, vely_x, vely_y, T_x, T_y
  real(c_double) :: E_x, E_y, div
  real(c_double) :: tauxx, tauxy, tauxz, tauyy, tauyz, tauzz

  real(c_double) :: Fd_left(mesh%P1, mesh%Nvar), Fd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Gd_left(mesh%P1, mesh%Nvar), Gd_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Hd_left(mesh%P1, mesh%Nvar), Hd_rght(mesh%P1, mesh%Nvar)

  real(c_double) :: Fv_left(mesh%P1, mesh%Nvar), Fv_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv_left(mesh%P1, mesh%Nvar), Gv_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: Hv_left(mesh%P1, mesh%Nvar), Hv_rght(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fi(mesh%P1, mesh%Nvar)
  real(c_double) :: Gi(mesh%P1, mesh%Nvar)
  real(c_double) :: Hi(mesh%P1, mesh%Nvar)
  
  real(c_double) :: Fv(mesh%P1, mesh%Nvar)
  real(c_double) :: Gv(mesh%P1, mesh%Nvar)
  real(c_double) :: Hv(mesh%P1, mesh%Nvar)

  integer(c_int) :: sfi_left(mesh%P1)
  
  real(c_double) :: uflux_left(mesh%P1, mesh%Nvar), uflux_rght(mesh%P1, mesh%Nvar)
  real(c_double) :: ux_left(mesh%P1, mesh%Nvar)
  real(c_double) :: uy_left(mesh%P1, mesh%Nvar)
  real(c_double) :: uz_left(mesh%P1, mesh%Nvar)
    
  real(c_double) :: ilambda, ibeta_viscous, itau
  
  real(c_double) :: v_inlet, T_inject, P_inject
  real(c_double) :: vleft_prefactor, vrght_prefactor, viscous_prefactor
  
  v_inlet = ONE

  T_inject = 260.0_c_double
  P_inject = 3.142_c_double*101325.0_c_double

  viscous_prefactor = max(avisc, vdiff)
  vleft_prefactor = TWO
  vrght_prefactor = ZERO
  
  !$omp parallel do private(tmp_index, glb_face_index, j, var, left, &
  !$omp                     left_face_index, left_indices, &
  !$omp                     rhofluid, Vxfluid, Vyfluid, Pfluid, Efluid, &
  !$omp                     rhoghost, Vxghost, Vyghost, Pghost, Eghost, &
  !$omp                     rho_x, rho_y, velx_x, velx_y, vely_x, vely_y, E_x, E_y, &
  !$omp                     T_x, T_y, div, &
  !$omp                     tauxx, tauxy, tauxz, tauyy, tauyz, tauzz, &
  !$omp                     Fd_left, Fd_rght, Fv_left, Fv_rght, &
  !$omp                     Gd_left, Gd_rght, Gv_left, Gv_rght, &
  !$omp                     Hd_left, Hd_rght, Hv_left, Hv_rght, &
  !$omp                     uflux_left, uflux_rght, &
  !$omp                     ux_left, uy_left, uz_left, &
  !$omp                     sfi_left, &
  !$omp                     Fi, Gi, Hi, Fv, Gv, Hv, &
  !$omp                     ilambda, ibeta_viscous, itau, &
  !$omp                     v_inlet) &
  !$omp schedule(dynamic,2)
  do tmp_index = 1, mesh%Nbfaces
     glb_face_index    = mesh%bfaces(tmp_index)
     left              = mesh%face2elem(glb_face_index, 1)

     ! left face index
     left_face_index = mesh%faceindex2elemfaceindex(glb_face_index, 1)
     
     left_indices(1) = mesh%P1*(left_face_index-1)+1
     left_indices(2) = mesh%P1*left_face_index
     
     ! initialize sfi
     do j = 1, mesh%P1
        sfi_left(j) = j
     end do
     
     ! get the left solution values
     do var = 1, mesh%Nvar
        uflux_left(:, var) = data%uflux(left_indices(1):left_indices(2), var, left)

        ux_left(:, var) = data%ux_flux(left_indices(1):left_indices(2), var, left)
        uy_left(:, var) = data%uy_flux(left_indices(1):left_indices(2), var, left)
        uz_left(:, var) = data%uz_flux(left_indices(1):left_indices(2), var, left)
     end do

     ! interior (left state inviscid flux)
     call euler_inviscid_flux(mesh%P1, mesh%Nvar, gamma, uflux_left, left_indices(1), sfi_left, &
          Fd_left, Gd_left, Hd_left)

     Hi = ZERO
     Hv = ZERO

     Hd_rght = ZERO
     Hv_rght = ZERO
     
     if (mesh%face_markers(glb_face_index) .eq. 2 .or. &
         mesh%face_markers(glb_face_index) .eq. 3) then

        ilambda = HALF; ibeta_viscous = ZERO; itau = ldg_tau
        
        do j = 1, mesh%P1
           rhofluid = data%uflux(left_indices(1) + (j-1), 1, left)
           Vxfluid  = data%uflux(left_indices(1) + (j-1), 2, left)/rhofluid
           Vyfluid  = data%uflux(left_indices(1) + (j-1), 3, left)/rhofluid
           Efluid   = data%uflux(left_indices(1) + (j-1), 4, left)
           
           Pfluid = (gamma - ONE)*(Efluid - HALF*rhofluid*(Vxfluid**2 + Vyfluid**2))

           ! Directly imposed values.
           rhoghost = rhofluid
           Pghost   = rhoghost*Rs*T_inject

           !Pghost   = P_inject
           !rhoghost = Pghost/(Rs*T_inject)

           ! get the inlet velocity by fixing the mean mass flow rate
           v_inlet = WALLMW/rhoghost
           if (mesh%face_markers(glb_face_index) .eq. 3) then
              v_inlet = -v_inlet
           end if

           ! ghost state velocities and energy
           Vxghost  = ZERO
           Vyghost  = v_inlet
           Eghost   = Pghost/(gamma-ONE) + HALF*rhoghost*(Vxghost**2 + Vyghost**2)
           
           ! set the right states for the flux
           uflux_rght(j, 1) = rhoghost
           uflux_rght(j, 2) = rhoghost*Vxghost
           uflux_rght(j, 3) = rhoghost*Vyghost
           uflux_rght(j, 4) = Eghost
           
           Fd_rght(j, 1) = rhoghost*Vxghost
           Fd_rght(j, 2) = rhoghost*Vxghost*Vxghost + Pghost
           Fd_rght(j, 3) = rhoghost*Vxghost*Vyghost
           Fd_rght(j, 4) = Vxghost*(Eghost + Pghost)
            
           Gd_rght(j, 1) = rhoghost*Vyghost
           Gd_rght(j, 2) = rhoghost*Vyghost*Vxghost
           Gd_rght(j, 3) = rhoghost*Vyghost*Vyghost + Pghost
           Gd_rght(j, 4) = Vyghost*(Eghost + Pghost)

           !print*, glb_face_index, mesh%face_markers(glb_face_index), Gd_left(j, :), Gd_rght(j, :)

           ! Fd_left(j, 1) = rhoghost*Vxghost
           ! Fd_left(j, 2) = rhoghost*Vxghost*Vxghost + Pghost
           ! Fd_left(j, 3) = rhoghost*Vxghost*Vyghost
           ! Fd_left(j, 4) = Vxghost*(Eghost + Pghost)
            
           ! Gd_left(j, 1) = rhoghost*Vyghost
           ! Gd_left(j, 2) = rhoghost*Vyghost*Vxghost
           ! Gd_left(j, 3) = rhoghost*Vyghost*Vyghost + Pghost
           ! Gd_left(j, 4) = Vyghost*(Eghost + Pghost)
           
           ! viscous fluid states
           rho_x = data%ux_flux(left_indices(1) + (j-1), 1, left)
           rho_y = data%uy_flux(left_indices(1) + (j-1), 1, left)
           
           E_x = data%ux_flux(left_indices(1) + (j-1), 4, left)
           E_y = data%uy_flux(left_indices(1) + (j-1), 4, left)
           
           velx_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_x)               
           velx_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 2, left) - Vxfluid*rho_y)
           
           vely_x = ONE/rhofluid*(data%ux_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_x)               
           vely_y = ONE/rhofluid*(data%uy_flux(left_indices(1) + (j-1), 3, left) - Vyfluid*rho_y)
           
           T_x = ONE/(rhofluid*Cv)*(E_x - Efluid/rhofluid*rho_x - rhofluid*(Vxfluid*velx_x + Vyfluid*vely_x))
           T_y = ONE/(rhofluid*Cv)*(E_y - Efluid/rhofluid*rho_y - rhofluid*(Vxfluid*velx_y + Vyfluid*vely_y))
           
           div = velx_x + vely_y
           
           tauxx = mu*(TWO*velx_x - TWO3RD*div)
           tauyy = mu*(TWO*vely_y - TWO3RD*div)
           
           tauxy = mu*(velx_y + vely_x)

           ! F-viscous fluxes
           Fv_left(j, 1) =  ZERO ; Fv_rght(j, 1) = ZERO
           Fv_left(j, 2) = -tauxx; Fv_rght(j, 2) = ZERO
           Fv_left(j, 3) = -tauxy; Fv_rght(j, 3) = ZERO

           Fv_left(j, 4) = -(Vxfluid*tauxx + Vyfluid*tauxy) - mu*Cp/Pr*T_x
           Fv_rght(j, 4) = ZERO
           
           ! G-viscous fluxes
           Gv_left(j, 1) =  ZERO;  Gv_rght(j, 1) = ZERO
           Gv_left(j, 2) = -tauxy; Gv_rght(j, 2) = ZERO
           Gv_left(j, 3) = -tauyy; Gv_rght(j, 3) = ZERO

           Gv_left(j, 4) = -(Vxfluid*tauxy + Vyfluid*tauyy) - mu*Cp/Pr*T_y
           Gv_rght(j, 4) = ZERO
        end do

        ! Evaluate the interaction flux for this face
        call evaluate_interaction_flux(mesh%DIM, mesh%P1, mesh%Nvar, &
             mesh%normals(glb_face_index,:), mesh%pnormals(glb_face_index,:), &
             uflux_left, uflux_rght, &
             Fd_left, Fd_rght, Fv_left, Fv_rght, &
             Gd_left, Gd_rght, Gv_left, Gv_rght, &
             Hd_left, Hd_rght, Hv_left, Hv_rght, &
             Fi, Gi, Hi, Fv, Gv, Hv, &
             gamma, viscous_prefactor, &
             mesh%face_markers(glb_face_index), &
             ilambda, ibeta_viscous, itau, &
             glb_face_index, vleft_prefactor, vrght_prefactor)
        
        ! store the PHYSICAL interaction flux for the element
        ! abutting this face
        do var = 1, mesh%Nvar
           do j = 1, mesh%P1
              data%Fi(left_indices(1) + (j-1), var, left) = Fi(j, var)! + Fv(j, var)
              data%Gi(left_indices(1) + (j-1), var, left) = Gi(j, var)! + Gv(j, var)
              data%Hi(left_indices(1) + (j-1), var, left) = Hi(j, var)! + Hv(j, var) 
           end do
        end do

     end if
  end do
  !$omp end parallel do
  
end subroutine get_userbc_interaction_flux

end module userbc_module
 
