module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module
  use polynomials_module
  use visit_writer_module

  implicit none

contains

  subroutine set_initial_values(mesh)
    type(triangle_mesh), intent(inout) :: mesh
    
    ! locals
    integer(c_int) :: index
    real(c_double), allocatable :: xs(:), ys(:), r2(:)
    
    allocate(xs(mesh%Np), ys(mesh%Np), r2(mesh%Np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys)
       r2 = xs**2 + ys**2

       mesh%u(index, :, 1) = ONE + exp(-TEN*r2)
       mesh%u(index, :, 2) = xs + ys
       mesh%u(index, :, 3) = xs - ys
       mesh%u(index, :, 4) = FOUR3RD
    end do
    
    deallocate(xs, ys, r2)

    call get_gradients(   mesh)
    call compute_averages(mesh)
    
  end subroutine set_initial_values

  subroutine test_initial_values(mesh)
    type(triangle_mesh), intent(inout) :: mesh
    
    ! locals
    integer(c_int)              :: index, i
    real(c_double)              :: Lxnorm(3), Lynorm(3), tmpx, tmpy
    real(c_double), allocatable :: xs(:), ys(:), r2(:)
    real(c_double), allocatable :: ux(:), uy(:)

    real(c_double) :: xv( mesh%elem_num_faces), yv(mesh%elem_num_faces)
    real(c_double) :: xf( mesh%elem_num_faces*mesh%P1)
    real(c_double) :: yf( mesh%elem_num_faces*mesh%P1)
    real(c_double) :: r2f(mesh%elem_num_faces*mesh%P1)
    real(c_double) :: uxf(mesh%elem_num_faces*mesh%P1)
    real(c_double) :: uyf(mesh%elem_num_faces*mesh%P1)

    character(len=256) :: dummy_filename

    dummy_filename = "test.silo"
    call write_visit_file(trim(dummy_filename), mesh)

    allocate(xs(mesh%Np), ys(mesh%Np), r2(mesh%Np))
    allocate(ux(mesh%Np))
    allocate(uy(mesh%Np))

    Lxnorm = ZERO
    Lynorm = ZERO

    do index = 1, mesh%Nelements
    if (mesh%elem_type(index) .ne. -1) then

       !!!!!!!!!!!!!!!!!!!! Test gradients !!!!!!!!!!!!!!!!!!!!
       call get_element_solution_points(mesh, index, xs, ys)
       r2 = xs**2 + ys**2
       
       ux = -TWO*TEN*xs*exp(-TEN*r2)
       uy = -TWO*TEN*ys*exp(-TEN*r2)

       ! X - derivatives
       if ( .not. assert(mesh%uxavg(index, 2) - ONE, SMALL) ) then
          write(*, *) 'Error in X-derivative mesh%uxavg(index, 2)', index, mesh%uxavg(index, 2)
          stop
       end if

       if ( .not. assert(mesh%uxavg(index, 3) - ONE, SMALL) ) then
          write(*, *) 'Error in X-derivative', index, mesh%uxavg(index, 3)
          stop
       end if

       if ( .not. assert(mesh%uxavg(index, 4) - ZERO, SMALL)) then
          write(*, *) 'Error in X-derivative', index, mesh%uxavg(index, 4)
          stop
       end if

       ! Y - derivatives
       if ( .not. assert(mesh%uyavg(index, 2) - ONE, SMALL) ) then
          write(*, *) 'Error in X-derivative', index, mesh%uxavg(index, 2)
          stop
       end if

       if ( .not. assert(mesh%uyavg(index, 3) + ONE, SMALL) ) then
          write(*, *) 'Error in X-derivative', index, mesh%uxavg(index, 3)
          stop
       end if

       if ( .not. assert(mesh%uyavg(index, 4) - ZERO, SMALL)) then
          write(*, *) 'Error in X-derivative', index, mesh%uxavg(index, 4)
          stop
       end if

       do i = 1, mesh%Np
          Lxnorm(1) = max(Lxnorm(1), maxval(abs(ux-mesh%ux(index,:,1))))
          Lynorm(1) = max(Lynorm(1), maxval(abs(uy-mesh%uy(index,:,1))))

          Lxnorm(2) = Lxnorm(2) + sum( abs(ux-mesh%ux(index,:,1)) )
          Lynorm(2) = Lynorm(2) + sum( abs(uy-mesh%uy(index,:,1)) )

          Lxnorm(3) = Lxnorm(3) + sum( (abs(ux-mesh%ux(index,:,1)))**2 )
          Lynorm(3) = Lynorm(3) + sum( (abs(uy-mesh%uy(index,:,1)))**2 )
       end do
    end if
    end do

    Lxnorm(3) = sqrt(Lxnorm(3))/mesh%nelements
    Lynorm(3) = sqrt(Lynorm(3))/mesh%nelements

    Lxnorm(2:) = Lxnorm(2:)/mesh%nelements
    Lynorm(2:) = Lynorm(2:)/mesh%nelements

    write(*, *) 'Norm errors for Gradients'
    write(*, *) Lxnorm
    write(*, *) Lynorm

    deallocate(xs, ys, r2)
    deallocate(ux, uy)

    !!!!!!!!!!!!!!!!!!!! Test values at the flux points !!!!!!!!!!!!!!!!!!!!
    do index = 1, mesh%nelements
    if (mesh%elem_type(index) .ne. -1) then

       call get_element_vertices(mesh, index, xv, yv)
       call rs2xy(mesh%rf, mesh%sf, xv, yv, xf, yf  )

       r2f = xf**2 + yf**2
       uxf = -TWO*TEN*xf*exp(-TEN*r2f)
       uyf = -TWO*TEN*yf*exp(-TEN*r2f)

       do i = 1, mesh%elem_num_faces*mesh%P1
          
          tmpx = mesh%ux_flux(index,i,1)
          if (.not. assert( abs(tmpx - uxf(i)), 1D-3)) then
             write(*, *) 'Error in flux point gradient ux_flux(1)', index, i, tmpx, uxf(i), abs(tmpx-uxf(i))
             !stop
          end if

          tmpy = mesh%uy_flux(index,i,1)
          if (.not. assert( abs(tmpy - uyf(i)), 1D-3 )) then
             write(*, *) 'Error in flux point gradient uy_flux(1)', index, i, tmpy, uyf(i), abs(tmpy-uyf(i))
             !stop
          end if

          if (.not. assert( abs((xf(i)+yf(i)) - mesh%uflux(index, i, 2)) - ZERO, SMALL )) then
             write(*, *) 'Error. Flux point interpolation', index, mesh%uflux(index, i, 2)
             stop
          end if
          
          if (.not. assert( abs((xf(i)-yf(i)) - mesh%uflux(index, i, 3)) - ZERO, SMALL )) then
             write(*, *) 'Error. Flux point interpolation', index, mesh%uflux(index, i, 3)
             stop
          end if
          
          if (.not. assert( mesh%uflux(index, i, 4) - FOUR3RD, SMALL )) then
             write(*, *) 'Error. Flux point interpolation', index, mesh%uflux(index, i, 4)
             stop
          end if
       end do
       
    end if
    end do

    ! Test anything else you want
    call test_mesh_jacobians(mesh)
    
    ! Test common solution values
    call test_common_solution_values(mesh)

    write(*, *) 'OK'
    stop
    
  end subroutine test_initial_values

  subroutine test_common_solution_values(mesh)
    type(triangle_mesh), intent(in) :: mesh

    ! locals
    integer(c_int) :: elem_index, nbr_index, nbr_face_index, face, var, j
    real(c_double) :: uflux_external(    mesh%P1, mesh%Nvar)
    real(c_double) :: uflux_external_tmp(mesh%P1, mesh%Nvar)

    real(c_double) :: diff(mesh%P1, mesh%Nvar)
    integer(c_int) :: index_local(2), index_external(2)

    elements: do elem_index = 1, mesh%Nelements
       if (mesh%elem_type(elem_index) .ne. -1) then
          faces: do face = 1, mesh%elem_num_faces
             
             index_local(1) = mesh%P1*(face-1) + 1
             index_local(2) = mesh%P1*(face)

             nbr_index      = mesh%faceindex2nbr(         elem_index, face)
             nbr_face_index = mesh%faceindex2nbrfaceindex(elem_index, face)

             index_external(1) = mesh%P1*(nbr_face_index-1) + 1
             index_external(2) = mesh%P1*(nbr_face_index)
             uflux_external_tmp = mesh%uflux(nbr_index, index_external(1):index_external(2), :)

             do j = 1, mesh%P1
                uflux_external(j,:) = uflux_external_tmp(mesh%P1 -(j-1),:)
             end do

             diff = abs(mesh%ucommon(elem_index, index_local(1):index_local(2), :) - &
                    uflux_external(:, :))

             ! test for the common solution values
             do j = 1, mesh%P1
                do var = 1, mesh%Nvar
                   if (.not. assert(diff(j, var), EPSILON)) then
                      write(*, *) 'Error in common solution values', elem_index, face
                      stop
                   end if
                end do
             end do

          end do faces
       end if
    end do elements

  end subroutine test_common_solution_values

  subroutine test_mesh_jacobians(mesh)
    type(triangle_mesh), intent(in) :: mesh

    ! locals
    integer(c_int) :: elem_index, i
    real(c_double) :: det_ref, ref_area, detJn(mesh%Np)
    
    ref_area = TWO
    if (MESH_ELEM_TYPE .eq. QUADS) then
       ref_area = FOUR
    end if

    do elem_index = 1, mesh%nelements
       det_ref = get_element_area(mesh, elem_index)/ref_area
       detJn = mesh%detJn(elem_index, :)

       do i = 1, mesh%Np
          if (.not. assert( abs(detJn(i) - det_ref), 1D-4 )) then
             write(*, *) 'Errors in Mesh Jacobian', elem_index, detJn-det_ref
             !stop
          end if
       end do

    end do
  end subroutine test_mesh_jacobians
  
  pure function assert(x, tol) result(is_true)
    real(c_double), intent(in) :: x, tol
    logical                    :: is_true
    
    is_true = ( abs(x) .le. tol )
    
  end function assert

end module initialize_module
