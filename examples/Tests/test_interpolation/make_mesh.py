import sys
import imp
import os

import optparse

# load the genmesh module
curdir = os.path.abspath(os.curdir)
gmsh_reader = imp.load_source(
      'gmsh_reader', os.path.join(curdir, '../../../python/gmsh_reader.py'))
writer  = imp.load_source(
      'h5writer', os.path.join(curdir, '../../../python/h5writer.py'))

if __name__ == '__main__':

    parser = optparse.OptionParser()
    parser.add_option("-m", "--mshfile", type="str", action="store",
                      dest="mshfile", default=None)

    parser.add_option("-o", "--outfile", type="str", action="store",
                      dest="outfile", default="test.hdf")

    parser.add_option("--etype", type="str", action="store",
                      dest="etype", default="quad")
    
    options, args = parser.parse_args()

    if options.mshfile is None:
        print "No Input meshfile provided!"
        print "Exiting..."
        sys.exit()

    print "Reading %s with element types %s"%(options.mshfile, options.etype)

    msh = gmsh_reader.GMSHReader(options.mshfile, options.etype)
    writer.write_mesh(msh.dmesh, options.outfile)
