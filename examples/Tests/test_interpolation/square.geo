Point(1) = {-1, -1, 0, 1.0};
Point(2) = {1, -1, 0, 1.0};
Point(3) = {1, 1, 0, 1.0};
Point(4) = {-1, 1, 0, 1.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Physical Line(5) = {4, 1, 2, 3};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(7) = {6};
Physical Surface(8) = {7};

