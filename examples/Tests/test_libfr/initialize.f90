module initialize_module
  use deepfry_constants_module
  use mesh_module
  use system_module

  implicit none

contains
  subroutine set_initial_values(mesh)
    type(triangle_mesh), intent(inout) :: mesh
    
    ! locals
    integer(c_int) :: index
    real(c_double), allocatable :: xs(:), ys(:)
    
    allocate(xs(mesh%np), ys(mesh%np))
    
    ! iterate over all elements and set the initial solution
    do index = 1, mesh%nelements
       call get_element_solution_points(mesh, index, xs, ys)
       mesh%u(index, :, 1) = sin(M_PI*xs)*sin(M_PI*ys)
       mesh%u(index, :, 2) = xs + ys
       mesh%u(index, :, 3) = xs - ys
       mesh%u(index, :, 4) = FOUR3RD
    end do
    
    deallocate(xs, ys)

    call get_gradients(mesh)
    call compute_averages(mesh)
    
  end subroutine set_initial_values

end module initialize_module
