!gfortran deepfry_constants.f90 polynomials.f90 mesh.f90 test.f90 visit_writer.f90 libfr.f90 main.f90 -L /usr/lib/x86_64-linux-gnu/ -lhdf5 -lsiloh5 -I /home/kpuri/usr/local/silo/include/ -lz -lstdc++ -lblas -llapack -o main.exe

program main
  use iso_c_binding, only: c_double, c_int
  use mesh_module
  use fr_module
  use system_module
  use visit_writer_module
  use test_module
  use runtime_init_module
  use initialize_module
  implicit none

  ! the mesh data structure
  type(triangle_mesh), target :: triangles
  type(NavierStokesSystem) :: system

  ! READ THE INPUT DATA FFROM THE COMMAND LINE
  call runtime_init()

  ! set the mesh for the system
  system%mesh => triangles

  ! BUILD THE MESH AND ASSOCIATED DATA
  call initialize_deepfry(triangles)

  ! INITIALIZE THE SYSTEM
  call initialize_system(system)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialization
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! SET INITIAL NODAL VALUES ON THE MESH
  call set_initial_values(triangles)

  ! INTERPOLATE VALUES AT THE FLUX POINTS AND COMPUTE THE COMMON
  ! SOLUTION VALUES
  call set_modal_coefficients(system%mesh)
  call interpolate_at_flux_points(system%mesh)
  call get_common_solution_values(system%mesh)

  ! COMPTUE THE GRADIENTS
  call get_gradients_at_solution_points(system%mesh)
  call get_gradients_at_flux_points(system%mesh)

  ! Write out the VisIt file
  call write_visit_file(plot_filename, triangles)

  ! do some testing for the values
  write(*, *) 'TESTING...'
  call test_normals(triangles)
  call test_interpolation(triangles)
  call test_cell_averages(triangles)
  call test_interpolation_at_flux_points(triangles)
  call testxyrs(triangles)
  call test_interpolation_across_elements(triangles)
  call test_get_common_solution_value(triangles)
  write(*, *) 'OK'

  call shutdown_deepfry(triangles)

end program main
