program test_poly
  use iso_c_binding, only: c_double
  use polynomials_module
  implicit none

  
  real(c_double), dimension(7) :: x
  real(c_double), allocatable  :: cx(:,:)
  real(c_double)               :: alpha, beta, integral
  integer                      :: m, n, i,j

  m = 7
  n = 2
  x = (/-0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75 /)

  alpha = 1.0
  beta  = 1.0

  allocate(cx(m, n+1))
  cx = -222.0

  call j_polynomial(m, n, alpha, beta, x, cx)
  integral = j_double_product_integral(0,0,alpha,beta)

  do j = 1, n+1
     do i = 1, m
        write(*, *) i,j, cx(i,j)
     end do
  end do
  
  write(*,*) 'INTEGRAL', 1./sqrt(integral)
  deallocate(cx)

end program test_poly
